// Filename: TClas12DetectorECAL.cc
// Description: CLAS12 ECAL base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorECAL.h"


ClassImp(TClas12DetectorECAL)
//-------------------------------------------------------------------------------------
TClas12DetectorECAL::TClas12DetectorECAL(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorECAL::~TClas12DetectorECAL()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorECAL::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorECAL::SetBranches()
{
//================== ECAL::hits:reconstructed hits from ECAL ===================
	fECAL_hits_id = fHipoReader->getBranch<uint16_t>("ECAL::hits", "id"); 	 //id of the hit
	fECAL_hits_status = fHipoReader->getBranch<uint16_t>("ECAL::hits", "status"); 	 //status of the hit
	fECAL_hits_sector = fHipoReader->getBranch<uint8_t>("ECAL::hits", "sector"); 	 //sector of ECAL
	fECAL_hits_layer = fHipoReader->getBranch<uint8_t>("ECAL::hits", "layer"); 	 //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	fECAL_hits_strip = fHipoReader->getBranch<uint8_t>("ECAL::hits", "strip"); 	 //Strip number
	fECAL_hits_peakid = fHipoReader->getBranch<uint8_t>("ECAL::hits", "peakid"); 	 //Peak id
	fECAL_hits_energy = fHipoReader->getBranch<float>("ECAL::hits", "energy"); 	 //Energy of the hit
	fECAL_hits_time = fHipoReader->getBranch<float>("ECAL::hits", "time"); 	 //Time of the hit
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::peaks:reconstructed peaks from ECAL ===================
	fECAL_peaks_id = fHipoReader->getBranch<uint16_t>("ECAL::peaks", "id"); 	 //id of the hit
	fECAL_peaks_status = fHipoReader->getBranch<uint16_t>("ECAL::peaks", "status"); 	 //status of the hit
	fECAL_peaks_sector = fHipoReader->getBranch<uint8_t>("ECAL::peaks", "sector"); 	 //sector of ECAL
	fECAL_peaks_layer = fHipoReader->getBranch<uint8_t>("ECAL::peaks", "layer"); 	 //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	fECAL_peaks_energy = fHipoReader->getBranch<float>("ECAL::peaks", "energy"); 	 //Energy of the hit
	fECAL_peaks_time = fHipoReader->getBranch<float>("ECAL::peaks", "time"); 	 //Time of the hit
	fECAL_peaks_xo = fHipoReader->getBranch<float>("ECAL::peaks", "xo"); 	 //strip origin X coordinate
	fECAL_peaks_yo = fHipoReader->getBranch<float>("ECAL::peaks", "yo"); 	 //strip origin Y coordinate
	fECAL_peaks_zo = fHipoReader->getBranch<float>("ECAL::peaks", "zo"); 	 //strip origin Z coordinate
	fECAL_peaks_xe = fHipoReader->getBranch<float>("ECAL::peaks", "xe"); 	 //strip end    X coordinate
	fECAL_peaks_ye = fHipoReader->getBranch<float>("ECAL::peaks", "ye"); 	 //strip end    Y coordinate
	fECAL_peaks_ze = fHipoReader->getBranch<float>("ECAL::peaks", "ze"); 	 //strip end    Z coordinate
	fECAL_peaks_width = fHipoReader->getBranch<float>("ECAL::peaks", "width"); 	 //width of the peak
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::clusters:reconstructed clusters in ECAL ===================
	fECAL_clusters_id = fHipoReader->getBranch<uint16_t>("ECAL::clusters", "id"); 	 //id of the hit
	fECAL_clusters_status = fHipoReader->getBranch<uint16_t>("ECAL::clusters", "status"); 	 //status of the hit
	fECAL_clusters_sector = fHipoReader->getBranch<uint8_t>("ECAL::clusters", "sector"); 	 //sector of ECAL
	fECAL_clusters_layer = fHipoReader->getBranch<uint8_t>("ECAL::clusters", "layer"); 	 //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	fECAL_clusters_x = fHipoReader->getBranch<float>("ECAL::clusters", "x"); 	 //X coordinate of the hit
	fECAL_clusters_y = fHipoReader->getBranch<float>("ECAL::clusters", "y"); 	 //Y coordinate of the hit
	fECAL_clusters_z = fHipoReader->getBranch<float>("ECAL::clusters", "z"); 	 //Z coordinate of the hit
	fECAL_clusters_energy = fHipoReader->getBranch<float>("ECAL::clusters", "energy"); 	 //Energy of the hit
	fECAL_clusters_time = fHipoReader->getBranch<float>("ECAL::clusters", "time"); 	 //Energy of the hit
	fECAL_clusters_widthU = fHipoReader->getBranch<float>("ECAL::clusters", "widthU"); 	 //width of U peak
	fECAL_clusters_widthV = fHipoReader->getBranch<float>("ECAL::clusters", "widthV"); 	 //width of V peak
	fECAL_clusters_widthW = fHipoReader->getBranch<float>("ECAL::clusters", "widthW"); 	 //width of W peak
	fECAL_clusters_idU = fHipoReader->getBranch<uint8_t>("ECAL::clusters", "idU"); 	 //id of U peak
	fECAL_clusters_idV = fHipoReader->getBranch<uint8_t>("ECAL::clusters", "idV"); 	 //id of V peak
	fECAL_clusters_idW = fHipoReader->getBranch<uint8_t>("ECAL::clusters", "idW"); 	 //id of W peak
	fECAL_clusters_coordU = fHipoReader->getBranch<uint32_t>("ECAL::clusters", "coordU"); 	 //U coordinate 
	fECAL_clusters_coordV = fHipoReader->getBranch<uint32_t>("ECAL::clusters", "coordV"); 	 //V coordinate
	fECAL_clusters_coordW = fHipoReader->getBranch<uint32_t>("ECAL::clusters", "coordW"); 	 //W coordinate
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::calib:Raw and recon peak energy from ECAL ===================
	fECAL_calib_sector = fHipoReader->getBranch<uint8_t>("ECAL::calib", "sector"); 	 //sector of ECAL
	fECAL_calib_layer = fHipoReader->getBranch<uint8_t>("ECAL::calib", "layer"); 	 //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	fECAL_calib_energy = fHipoReader->getBranch<float>("ECAL::calib", "energy"); 	 //Energy of the hit
	fECAL_calib_rawEU = fHipoReader->getBranch<float>("ECAL::calib", "rawEU"); 	 //raw U peak energy
	fECAL_calib_rawEV = fHipoReader->getBranch<float>("ECAL::calib", "rawEV"); 	 //raw V peak energy
	fECAL_calib_rawEW = fHipoReader->getBranch<float>("ECAL::calib", "rawEW"); 	 //raw W peak energy
	fECAL_calib_recEU = fHipoReader->getBranch<float>("ECAL::calib", "recEU"); 	 //recon U peak energy
	fECAL_calib_recEV = fHipoReader->getBranch<float>("ECAL::calib", "recEV"); 	 //recon V peak energy
	fECAL_calib_recEW = fHipoReader->getBranch<float>("ECAL::calib", "recEW"); 	 //recon W peak energy
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::moments:ECCAL bank for clusters containing distances an moments ===================
	fECAL_moments_distU = fHipoReader->getBranch<float>("ECAL::moments", "distU"); 	 //distance fomr u edge
	fECAL_moments_distV = fHipoReader->getBranch<float>("ECAL::moments", "distV"); 	 //distance from v edge
	fECAL_moments_distW = fHipoReader->getBranch<float>("ECAL::moments", "distW"); 	 //distance from w edge
	fECAL_moments_m1u = fHipoReader->getBranch<float>("ECAL::moments", "m1u"); 	 //second moment
	fECAL_moments_m1v = fHipoReader->getBranch<float>("ECAL::moments", "m1v"); 	 //second moment
	fECAL_moments_m1w = fHipoReader->getBranch<float>("ECAL::moments", "m1w"); 	 //second moment
	fECAL_moments_m2u = fHipoReader->getBranch<float>("ECAL::moments", "m2u"); 	 //second moment
	fECAL_moments_m2v = fHipoReader->getBranch<float>("ECAL::moments", "m2v"); 	 //second moment
	fECAL_moments_m2w = fHipoReader->getBranch<float>("ECAL::moments", "m2w"); 	 //second moment
	fECAL_moments_m3u = fHipoReader->getBranch<float>("ECAL::moments", "m3u"); 	 //third moment
	fECAL_moments_m3v = fHipoReader->getBranch<float>("ECAL::moments", "m3v"); 	 //third moment
	fECAL_moments_m3w = fHipoReader->getBranch<float>("ECAL::moments", "m3w"); 	 //third moment
//--------------------------------------------------------------------------------------------------------------------------------------

}
