// Filename: TClas12DetectorHTCC.cc
// Description: CLAS12 HTCC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorHTCC.h"


ClassImp(TClas12DetectorHTCC)
//-------------------------------------------------------------------------------------
TClas12DetectorHTCC::TClas12DetectorHTCC(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorHTCC::~TClas12DetectorHTCC()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorHTCC::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorHTCC::SetBranches()
{
//================== HTCC::rec:reconstructed clusters in HTCC ===================
	fHTCC_rec_id = fHipoReader->getBranch<uint16_t>("HTCC::rec", "id"); 	 //id of the hit
	fHTCC_rec_nhits = fHipoReader->getBranch<uint16_t>("HTCC::rec", "nhits"); 	 //number of hits in cluster
	fHTCC_rec_nphe = fHipoReader->getBranch<float>("HTCC::rec", "nphe"); 	 //number of photoelectrons in cluster
	fHTCC_rec_ntheta = fHipoReader->getBranch<uint16_t>("HTCC::rec", "ntheta"); 	 //ntheta
	fHTCC_rec_nphi = fHipoReader->getBranch<uint16_t>("HTCC::rec", "nphi"); 	 //nphi
	fHTCC_rec_mintheta = fHipoReader->getBranch<uint16_t>("HTCC::rec", "mintheta"); 	 //Minimum theta angle
	fHTCC_rec_maxtheta = fHipoReader->getBranch<uint16_t>("HTCC::rec", "maxtheta"); 	 //Maximum theta angle
	fHTCC_rec_minphi = fHipoReader->getBranch<uint16_t>("HTCC::rec", "minphi"); 	 //Minimum phi angle
	fHTCC_rec_maxphi = fHipoReader->getBranch<uint16_t>("HTCC::rec", "maxphi"); 	 //Maximum phi angle
	fHTCC_rec_time = fHipoReader->getBranch<float>("HTCC::rec", "time"); 	 //Time at the vertex
	fHTCC_rec_theta = fHipoReader->getBranch<float>("HTCC::rec", "theta"); 	 //Theta of the cluster
	fHTCC_rec_dtheta = fHipoReader->getBranch<float>("HTCC::rec", "dtheta"); 	 //Error of the theta
	fHTCC_rec_phi = fHipoReader->getBranch<float>("HTCC::rec", "phi"); 	 //Phi of the cluster
	fHTCC_rec_dphi = fHipoReader->getBranch<float>("HTCC::rec", "dphi"); 	 //Error of the phi
	fHTCC_rec_x = fHipoReader->getBranch<float>("HTCC::rec", "x"); 	 //X coordinate of the hit
	fHTCC_rec_y = fHipoReader->getBranch<float>("HTCC::rec", "y"); 	 //Y coordinate of the hit
	fHTCC_rec_z = fHipoReader->getBranch<float>("HTCC::rec", "z"); 	 //Z coordinate of the hit
//--------------------------------------------------------------------------------------------------------------------------------------

}
