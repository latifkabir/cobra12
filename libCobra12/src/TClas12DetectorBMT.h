// Filename: TClas12DetectorBMT.h
// Description: CLAS12 BMT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORBMT_H
#define TCLAS12DETECTORBMT_H

#include "TClas12Detector.h"

class TClas12DetectorBMT: public TClas12Detector
{
public:
	TClas12DetectorBMT(TClas12Run *run);
	~TClas12DetectorBMT();
	void Init();
	void SetBranches();

protected:
//================== BMTRec::Hits:reconstructed BMT hits on track ===================
	hipo::node<uint16_t> *fBMTRec_Hits_ID;   //hit ID
	hipo::node<uint8_t> *fBMTRec_Hits_sector;   //hit sector
	hipo::node<uint8_t> *fBMTRec_Hits_layer;   //hit layer
	hipo::node<uint32_t> *fBMTRec_Hits_strip;   //hit strip
	hipo::node<float> *fBMTRec_Hits_fitResidual;   //fitted hit residual
	hipo::node<uint32_t> *fBMTRec_Hits_trkingStat;   //tracking status
	hipo::node<uint16_t> *fBMTRec_Hits_clusterID;   //associated cluster ID
	hipo::node<uint16_t> *fBMTRec_Hits_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::Clusters:reconstructed BMT clusters ===================
	hipo::node<uint16_t> *fBMTRec_Clusters_ID;   //ID
	hipo::node<uint8_t> *fBMTRec_Clusters_sector;   //sector
	hipo::node<uint8_t> *fBMTRec_Clusters_layer;   //layer
	hipo::node<uint16_t> *fBMTRec_Clusters_size;   //cluster size
	hipo::node<float> *fBMTRec_Clusters_ETot;   //cluster total energy
	hipo::node<float> *fBMTRec_Clusters_seedE;   //energy of the seed 
	hipo::node<uint32_t> *fBMTRec_Clusters_seedStrip;   //seed strip
	hipo::node<float> *fBMTRec_Clusters_centroid;   //centroid strip number
	hipo::node<float> *fBMTRec_Clusters_centroidResidual;   //centroid residual
	hipo::node<float> *fBMTRec_Clusters_seedResidual;   //seed residual
	hipo::node<uint16_t> *fBMTRec_Clusters_Hit1_ID;   //Index of hit 1 in cluster
	hipo::node<uint16_t> *fBMTRec_Clusters_Hit2_ID;   //Index of hit 2 in cluster
	hipo::node<uint16_t> *fBMTRec_Clusters_Hit3_ID;   //Index of hit 3 in cluster
	hipo::node<uint16_t> *fBMTRec_Clusters_Hit4_ID;   //Index of hit 4 in cluster
	hipo::node<uint16_t> *fBMTRec_Clusters_Hit5_ID;   //Index of hit 5 in cluster
	hipo::node<uint16_t> *fBMTRec_Clusters_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::Crosses:reconstructed BMT crosses ===================
	hipo::node<uint16_t> *fBMTRec_Crosses_ID;   //ID
	hipo::node<uint8_t> *fBMTRec_Crosses_sector;   //sector
	hipo::node<uint8_t> *fBMTRec_Crosses_region;   //region
	hipo::node<float> *fBMTRec_Crosses_x;   //BMT cross x-coordinate
	hipo::node<float> *fBMTRec_Crosses_y;   //BMT cross y-coordinate
	hipo::node<float> *fBMTRec_Crosses_z;   //BMT cross z-coordinate
	hipo::node<float> *fBMTRec_Crosses_err_x;   //BMT cross x-coordinate error
	hipo::node<float> *fBMTRec_Crosses_err_y;   //BMT cross y-coordinate error
	hipo::node<float> *fBMTRec_Crosses_err_z;   //BMT cross z-coordinate error
	hipo::node<float> *fBMTRec_Crosses_ux;   //BMT cross x-direction (track unit tangent vector at the cross)
	hipo::node<float> *fBMTRec_Crosses_uy;   //BMT cross y-direction (track unit tangent vector at the cross)
	hipo::node<float> *fBMTRec_Crosses_uz;   //BMT cross z-direction (track unit tangent vector at the cross)
	hipo::node<uint16_t> *fBMTRec_Crosses_Cluster1_ID;   //ID of the  cluster in the Cross
	hipo::node<uint16_t> *fBMTRec_Crosses_Cluster2_ID;   //ID of the top layer  cluster in the Cross
	hipo::node<uint16_t> *fBMTRec_Crosses_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::LayerEffs:layer efficiencies ===================
	hipo::node<uint8_t> *fBMTRec_LayerEffs_sector;   //sector
	hipo::node<uint8_t> *fBMTRec_LayerEffs_layer;   //layer
	hipo::node<float> *fBMTRec_LayerEffs_residual;   //excluded layer residual of the matched cluster centroid
	hipo::node<uint8_t> *fBMTRec_LayerEffs_status;   //status (-1: not used, i.e. no track; 0: inefficient; 1: efficient
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== BMTRec::Hits:reconstructed BMT hits on track ===================
	unsigned int GetBMTRec_Hits_ID(int i){  return (unsigned int)fBMTRec_Hits_ID->getValue(i);}
	unsigned int GetBMTRec_Hits_sector(int i){  return (unsigned int)fBMTRec_Hits_sector->getValue(i);}
	unsigned int GetBMTRec_Hits_layer(int i){  return (unsigned int)fBMTRec_Hits_layer->getValue(i);}
	unsigned int GetBMTRec_Hits_strip(int i){  return (unsigned int)fBMTRec_Hits_strip->getValue(i);}
	float GetBMTRec_Hits_fitResidual(int i){  return (float)fBMTRec_Hits_fitResidual->getValue(i);}
	unsigned int GetBMTRec_Hits_trkingStat(int i){  return (unsigned int)fBMTRec_Hits_trkingStat->getValue(i);}
	unsigned int GetBMTRec_Hits_clusterID(int i){  return (unsigned int)fBMTRec_Hits_clusterID->getValue(i);}
	unsigned int GetBMTRec_Hits_trkID(int i){  return (unsigned int)fBMTRec_Hits_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::Clusters:reconstructed BMT clusters ===================
	unsigned int GetBMTRec_Clusters_ID(int i){  return (unsigned int)fBMTRec_Clusters_ID->getValue(i);}
	unsigned int GetBMTRec_Clusters_sector(int i){  return (unsigned int)fBMTRec_Clusters_sector->getValue(i);}
	unsigned int GetBMTRec_Clusters_layer(int i){  return (unsigned int)fBMTRec_Clusters_layer->getValue(i);}
	unsigned int GetBMTRec_Clusters_size(int i){  return (unsigned int)fBMTRec_Clusters_size->getValue(i);}
	float GetBMTRec_Clusters_ETot(int i){  return (float)fBMTRec_Clusters_ETot->getValue(i);}
	float GetBMTRec_Clusters_seedE(int i){  return (float)fBMTRec_Clusters_seedE->getValue(i);}
	unsigned int GetBMTRec_Clusters_seedStrip(int i){  return (unsigned int)fBMTRec_Clusters_seedStrip->getValue(i);}
	float GetBMTRec_Clusters_centroid(int i){  return (float)fBMTRec_Clusters_centroid->getValue(i);}
	float GetBMTRec_Clusters_centroidResidual(int i){  return (float)fBMTRec_Clusters_centroidResidual->getValue(i);}
	float GetBMTRec_Clusters_seedResidual(int i){  return (float)fBMTRec_Clusters_seedResidual->getValue(i);}
	unsigned int GetBMTRec_Clusters_Hit1_ID(int i){  return (unsigned int)fBMTRec_Clusters_Hit1_ID->getValue(i);}
	unsigned int GetBMTRec_Clusters_Hit2_ID(int i){  return (unsigned int)fBMTRec_Clusters_Hit2_ID->getValue(i);}
	unsigned int GetBMTRec_Clusters_Hit3_ID(int i){  return (unsigned int)fBMTRec_Clusters_Hit3_ID->getValue(i);}
	unsigned int GetBMTRec_Clusters_Hit4_ID(int i){  return (unsigned int)fBMTRec_Clusters_Hit4_ID->getValue(i);}
	unsigned int GetBMTRec_Clusters_Hit5_ID(int i){  return (unsigned int)fBMTRec_Clusters_Hit5_ID->getValue(i);}
	unsigned int GetBMTRec_Clusters_trkID(int i){  return (unsigned int)fBMTRec_Clusters_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::Crosses:reconstructed BMT crosses ===================
	unsigned int GetBMTRec_Crosses_ID(int i){  return (unsigned int)fBMTRec_Crosses_ID->getValue(i);}
	unsigned int GetBMTRec_Crosses_sector(int i){  return (unsigned int)fBMTRec_Crosses_sector->getValue(i);}
	unsigned int GetBMTRec_Crosses_region(int i){  return (unsigned int)fBMTRec_Crosses_region->getValue(i);}
	float GetBMTRec_Crosses_x(int i){  return (float)fBMTRec_Crosses_x->getValue(i);}
	float GetBMTRec_Crosses_y(int i){  return (float)fBMTRec_Crosses_y->getValue(i);}
	float GetBMTRec_Crosses_z(int i){  return (float)fBMTRec_Crosses_z->getValue(i);}
	float GetBMTRec_Crosses_err_x(int i){  return (float)fBMTRec_Crosses_err_x->getValue(i);}
	float GetBMTRec_Crosses_err_y(int i){  return (float)fBMTRec_Crosses_err_y->getValue(i);}
	float GetBMTRec_Crosses_err_z(int i){  return (float)fBMTRec_Crosses_err_z->getValue(i);}
	float GetBMTRec_Crosses_ux(int i){  return (float)fBMTRec_Crosses_ux->getValue(i);}
	float GetBMTRec_Crosses_uy(int i){  return (float)fBMTRec_Crosses_uy->getValue(i);}
	float GetBMTRec_Crosses_uz(int i){  return (float)fBMTRec_Crosses_uz->getValue(i);}
	unsigned int GetBMTRec_Crosses_Cluster1_ID(int i){  return (unsigned int)fBMTRec_Crosses_Cluster1_ID->getValue(i);}
	unsigned int GetBMTRec_Crosses_Cluster2_ID(int i){  return (unsigned int)fBMTRec_Crosses_Cluster2_ID->getValue(i);}
	unsigned int GetBMTRec_Crosses_trkID(int i){  return (unsigned int)fBMTRec_Crosses_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::LayerEffs:layer efficiencies ===================
	unsigned int GetBMTRec_LayerEffs_sector(int i){  return (unsigned int)fBMTRec_LayerEffs_sector->getValue(i);}
	unsigned int GetBMTRec_LayerEffs_layer(int i){  return (unsigned int)fBMTRec_LayerEffs_layer->getValue(i);}
	float GetBMTRec_LayerEffs_residual(int i){  return (float)fBMTRec_LayerEffs_residual->getValue(i);}
	unsigned int GetBMTRec_LayerEffs_status(int i){  return (unsigned int)fBMTRec_LayerEffs_status->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorBMT,0)
};
#endif




