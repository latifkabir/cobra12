// Filename: TClas12DetectorFMT.cc
// Description: CLAS12 FMT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorFMT.h"


ClassImp(TClas12DetectorFMT)
//-------------------------------------------------------------------------------------
TClas12DetectorFMT::TClas12DetectorFMT(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorFMT::~TClas12DetectorFMT()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorFMT::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorFMT::SetBranches()
{
//================== FMTRec::Hits:reconstructed FMT hits ===================
	fFMTRec_Hits_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Hits", "ID"); 	 //hit ID
	fFMTRec_Hits_sector = fHipoReader->getBranch<uint8_t>("FMTRec::Hits", "sector"); 	 //hit sector
	fFMTRec_Hits_layer = fHipoReader->getBranch<uint8_t>("FMTRec::Hits", "layer"); 	 //hit layer
	fFMTRec_Hits_strip = fHipoReader->getBranch<uint32_t>("FMTRec::Hits", "strip"); 	 //hit strip
	fFMTRec_Hits_fitResidual = fHipoReader->getBranch<float>("FMTRec::Hits", "fitResidual"); 	 //fitted hit residual
	fFMTRec_Hits_trkingStat = fHipoReader->getBranch<uint32_t>("FMTRec::Hits", "trkingStat"); 	 //tracking status
	fFMTRec_Hits_clusterID = fHipoReader->getBranch<uint16_t>("FMTRec::Hits", "clusterID"); 	 //associated cluster ID
	fFMTRec_Hits_trkID = fHipoReader->getBranch<uint16_t>("FMTRec::Hits", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FMTRec::Clusters:reconstructed FMT clusters ===================
	fFMTRec_Clusters_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "ID"); 	 //ID
	fFMTRec_Clusters_sector = fHipoReader->getBranch<uint8_t>("FMTRec::Clusters", "sector"); 	 //sector
	fFMTRec_Clusters_layer = fHipoReader->getBranch<uint8_t>("FMTRec::Clusters", "layer"); 	 //layer
	fFMTRec_Clusters_size = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "size"); 	 //cluster size
	fFMTRec_Clusters_ETot = fHipoReader->getBranch<float>("FMTRec::Clusters", "ETot"); 	 //cluster total energy
	fFMTRec_Clusters_seedE = fHipoReader->getBranch<float>("FMTRec::Clusters", "seedE"); 	 //energy of the seed 
	fFMTRec_Clusters_seedStrip = fHipoReader->getBranch<uint32_t>("FMTRec::Clusters", "seedStrip"); 	 //seed strip
	fFMTRec_Clusters_centroid = fHipoReader->getBranch<float>("FMTRec::Clusters", "centroid"); 	 //centroid strip number
	fFMTRec_Clusters_centroidResidual = fHipoReader->getBranch<float>("FMTRec::Clusters", "centroidResidual"); 	 //centroid residual
	fFMTRec_Clusters_seedResidual = fHipoReader->getBranch<float>("FMTRec::Clusters", "seedResidual"); 	 //seed residual
	fFMTRec_Clusters_Hit1_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "Hit1_ID"); 	 //Index of hit 1 in cluster
	fFMTRec_Clusters_Hit2_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "Hit2_ID"); 	 //Index of hit 2 in cluster
	fFMTRec_Clusters_Hit3_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "Hit3_ID"); 	 //Index of hit 3 in cluster
	fFMTRec_Clusters_Hit4_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "Hit4_ID"); 	 //Index of hit 4 in cluster
	fFMTRec_Clusters_Hit5_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "Hit5_ID"); 	 //Index of hit 5 in cluster
	fFMTRec_Clusters_trkID = fHipoReader->getBranch<uint16_t>("FMTRec::Clusters", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FMTRec::Crosses:reconstructed FMT crosses ===================
	fFMTRec_Crosses_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Crosses", "ID"); 	 //ID
	fFMTRec_Crosses_sector = fHipoReader->getBranch<uint8_t>("FMTRec::Crosses", "sector"); 	 //sector
	fFMTRec_Crosses_region = fHipoReader->getBranch<uint8_t>("FMTRec::Crosses", "region"); 	 //region
	fFMTRec_Crosses_x = fHipoReader->getBranch<float>("FMTRec::Crosses", "x"); 	 //BMT cross x-coordinate
	fFMTRec_Crosses_y = fHipoReader->getBranch<float>("FMTRec::Crosses", "y"); 	 //BMT cross y-coordinate
	fFMTRec_Crosses_z = fHipoReader->getBranch<float>("FMTRec::Crosses", "z"); 	 //BMT cross z-coordinate
	fFMTRec_Crosses_err_x = fHipoReader->getBranch<float>("FMTRec::Crosses", "err_x"); 	 //BMT cross x-coordinate error
	fFMTRec_Crosses_err_y = fHipoReader->getBranch<float>("FMTRec::Crosses", "err_y"); 	 //BMT cross y-coordinate error
	fFMTRec_Crosses_err_z = fHipoReader->getBranch<float>("FMTRec::Crosses", "err_z"); 	 //BMT cross z-coordinate error
	fFMTRec_Crosses_ux = fHipoReader->getBranch<float>("FMTRec::Crosses", "ux"); 	 //BMT cross x-direction (track unit tangent vector at the cross)
	fFMTRec_Crosses_uy = fHipoReader->getBranch<float>("FMTRec::Crosses", "uy"); 	 //BMT cross y-direction (track unit tangent vector at the cross)
	fFMTRec_Crosses_uz = fHipoReader->getBranch<float>("FMTRec::Crosses", "uz"); 	 //BMT cross z-direction (track unit tangent vector at the cross)
	fFMTRec_Crosses_Cluster1_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Crosses", "Cluster1_ID"); 	 //ID of the  cluster in the Cross
	fFMTRec_Crosses_Cluster2_ID = fHipoReader->getBranch<uint16_t>("FMTRec::Crosses", "Cluster2_ID"); 	 //ID of the top layer  cluster in the Cross
	fFMTRec_Crosses_trkID = fHipoReader->getBranch<uint16_t>("FMTRec::Crosses", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

}
