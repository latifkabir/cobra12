// Filename: TClas12DetectorHEADER.cc
// Description: CLAS12 HEADER base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorHEADER.h"


ClassImp(TClas12DetectorHEADER)
//-------------------------------------------------------------------------------------
TClas12DetectorHEADER::TClas12DetectorHEADER(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorHEADER::~TClas12DetectorHEADER()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorHEADER::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorHEADER::SetBranches()
{
//================== RUN::config:Run Configuration ===================
	fRUN_config_run = fHipoReader->getBranch<uint32_t>("RUN::config", "run"); 	 //RUN number from CODA or GEMC
	fRUN_config_event = fHipoReader->getBranch<uint32_t>("RUN::config", "event"); 	 //Event number
	fRUN_config_unixtime = fHipoReader->getBranch<uint32_t>("RUN::config", "unixtime"); 	 //Unix time
	fRUN_config_trigger = fHipoReader->getBranch<uint64_t>("RUN::config", "trigger"); 	 //trigger bits
	fRUN_config_timestamp = fHipoReader->getBranch<uint64_t>("RUN::config", "timestamp"); 	 //time stamp from Trigger Interface (TI) board in 4 ns cycles)
	fRUN_config_type = fHipoReader->getBranch<uint8_t>("RUN::config", "type"); 	 //type of the run
	fRUN_config_mode = fHipoReader->getBranch<uint8_t>("RUN::config", "mode"); 	 //run mode
	fRUN_config_torus = fHipoReader->getBranch<float>("RUN::config", "torus"); 	 //torus setting relative value(-1.0 to 1.0)
	fRUN_config_solenoid = fHipoReader->getBranch<float>("RUN::config", "solenoid"); 	 //solenoid field setting (-1.0 to 1.0)
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RUN::rf:RF information ===================
	fRUN_rf_id = fHipoReader->getBranch<uint16_t>("RUN::rf", "id"); 	 //id of the RF signal
	fRUN_rf_time = fHipoReader->getBranch<float>("RUN::rf", "time"); 	 //time of RF signal
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RUN::trigger:RUN trigger information ===================
	fRUN_trigger_id = fHipoReader->getBranch<uint32_t>("RUN::trigger", "id"); 	 //id
	fRUN_trigger_trigger = fHipoReader->getBranch<uint32_t>("RUN::trigger", "trigger"); 	 //trigger word
//--------------------------------------------------------------------------------------------------------------------------------------

}
