// Filename: TClas12.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Tue Oct 17 18:10:57 2017 (-0400)
// URL: latifkabir.github.io

#ifndef TCLAS12_H
#define TCLAS12_H

#include <iostream>
#include "TObject.h"
#include "TClas12Config.h"

class TClas12: public TObject
{
public:
    TClas12();
    ~TClas12();
    
    static Bool_t fTurnOnEventMap;
    static Bool_t fTurnOnDCMap;
    static Bool_t fTurnOnECMap;
   
    static TClas12Config *Config;
    static Int_t GetCounter();
    static void SetCounter(Int_t cout);
    static void PrintCounter();
    static void Exit();
    static void ExitIfInvalid(TString filePath);
    static Bool_t IsValid(TString filePath);
    static void VaultViewer(Int_t fileNumber);
    static void VaultPrint(Int_t lowerRange, Int_t upperRange);
    static void VaultListing(Int_t lowerRange, Int_t upperRange);
    static void TurnOffMap(TString bankName);
    static void TurnOnMap(TString bankName);
    
    static Double_t GetMass(TString particle_name);
    ClassDef(TClas12,0)
};

#endif
