// Filename: TClas12DetectorCND.h
// Description: CLAS12 CND base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORCND_H
#define TCLAS12DETECTORCND_H

#include "TClas12Detector.h"

class TClas12DetectorCND: public TClas12Detector
{
public:
	TClas12DetectorCND(TClas12Run *run);
	~TClas12DetectorCND();
	void Init();
	void SetBranches();

protected:
//================== CND::hits:reconstructed hit info from CND ===================
	hipo::node<uint16_t> *fCND_hits_id;   //id of the hit
	hipo::node<uint16_t> *fCND_hits_status;   //status of the hit
	hipo::node<uint16_t> *fCND_hits_trkID;   //match CVT track index
	hipo::node<uint8_t> *fCND_hits_sector;   //sector of CND
	hipo::node<uint8_t> *fCND_hits_layer;   //panel id of CND
	hipo::node<uint16_t> *fCND_hits_component;   //paddle id of CND
	hipo::node<float> *fCND_hits_energy;   //E dep (MeV) of hit
	hipo::node<float> *fCND_hits_time;   //Hit time (ns)
	hipo::node<float> *fCND_hits_energy_unc;   //E dep unc (MeV) of hit
	hipo::node<float> *fCND_hits_time_unc;   //Hit time unc (ns)
	hipo::node<float> *fCND_hits_x;   //Global X coor (cm) of hit
	hipo::node<float> *fCND_hits_y;   //Global Y coor (cm) of hit
	hipo::node<float> *fCND_hits_z;   //Global Z coor (cm) of hit
	hipo::node<float> *fCND_hits_x_unc;   //Global X coor unc (cm) of hit
	hipo::node<float> *fCND_hits_y_unc;   //Global Y coor unc (cm) of hit
	hipo::node<float> *fCND_hits_z_unc;   //Global Z coor unc (cm) of hit
	hipo::node<float> *fCND_hits_tx;   //Global X coor (cm) of hit from CVT info -trkID index
	hipo::node<float> *fCND_hits_ty;   //Global Y coor (cm) of hit from CVT info - trkID index
	hipo::node<float> *fCND_hits_tz;   //Global Z coor (cm) of hit from CVT info - trkID index
	hipo::node<float> *fCND_hits_tlength;   //pathlength of the track from the entrance point to the exit point through the hit bar
	hipo::node<float> *fCND_hits_pathlength;   //pathlength of the track from the vertex (doca point to the beamline to the midpoint between the entrance and exit of the hit bar
	hipo::node<uint16_t> *fCND_hits_indexLadc;   //index of the left adc
	hipo::node<uint16_t> *fCND_hits_indexRadc;   //index of the right adc
	hipo::node<uint16_t> *fCND_hits_indexLtdc;   //index of the left tdc
	hipo::node<uint16_t> *fCND_hits_indexRtdc;   //index of the right tdc
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CND::clusters:reconstructed cluster info from CND and CTOF ===================
	hipo::node<uint16_t> *fCND_clusters_id;   //id of the cluster
	hipo::node<uint8_t> *fCND_clusters_sector;   //sector of the cluster's seed hit
	hipo::node<uint8_t> *fCND_clusters_layer;   //layer of the cluster's seed hit
	hipo::node<uint16_t> *fCND_clusters_component;   //component of the cluster's seed hit
	hipo::node<uint16_t> *fCND_clusters_nhits;   //number of hits of the cluster
	hipo::node<float> *fCND_clusters_energy;   //Sum of E dep (MeV) of the hits
	hipo::node<float> *fCND_clusters_x;   //center of X coor (cm) of the hits
	hipo::node<float> *fCND_clusters_y;   //center of Y coor (cm) of the hits
	hipo::node<float> *fCND_clusters_z;   //center of Z coor (cm) of the hits
	hipo::node<float> *fCND_clusters_time;   //center of time (ns) of the hits
	hipo::node<uint16_t> *fCND_clusters_status;   //cluster status
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== CND::hits:reconstructed hit info from CND ===================
	unsigned int GetCND_hits_id(int i){  return (unsigned int)fCND_hits_id->getValue(i);}
	unsigned int GetCND_hits_status(int i){  return (unsigned int)fCND_hits_status->getValue(i);}
	unsigned int GetCND_hits_trkID(int i){  return (unsigned int)fCND_hits_trkID->getValue(i);}
	unsigned int GetCND_hits_sector(int i){  return (unsigned int)fCND_hits_sector->getValue(i);}
	unsigned int GetCND_hits_layer(int i){  return (unsigned int)fCND_hits_layer->getValue(i);}
	unsigned int GetCND_hits_component(int i){  return (unsigned int)fCND_hits_component->getValue(i);}
	float GetCND_hits_energy(int i){  return (float)fCND_hits_energy->getValue(i);}
	float GetCND_hits_time(int i){  return (float)fCND_hits_time->getValue(i);}
	float GetCND_hits_energy_unc(int i){  return (float)fCND_hits_energy_unc->getValue(i);}
	float GetCND_hits_time_unc(int i){  return (float)fCND_hits_time_unc->getValue(i);}
	float GetCND_hits_x(int i){  return (float)fCND_hits_x->getValue(i);}
	float GetCND_hits_y(int i){  return (float)fCND_hits_y->getValue(i);}
	float GetCND_hits_z(int i){  return (float)fCND_hits_z->getValue(i);}
	float GetCND_hits_x_unc(int i){  return (float)fCND_hits_x_unc->getValue(i);}
	float GetCND_hits_y_unc(int i){  return (float)fCND_hits_y_unc->getValue(i);}
	float GetCND_hits_z_unc(int i){  return (float)fCND_hits_z_unc->getValue(i);}
	float GetCND_hits_tx(int i){  return (float)fCND_hits_tx->getValue(i);}
	float GetCND_hits_ty(int i){  return (float)fCND_hits_ty->getValue(i);}
	float GetCND_hits_tz(int i){  return (float)fCND_hits_tz->getValue(i);}
	float GetCND_hits_tlength(int i){  return (float)fCND_hits_tlength->getValue(i);}
	float GetCND_hits_pathlength(int i){  return (float)fCND_hits_pathlength->getValue(i);}
	unsigned int GetCND_hits_indexLadc(int i){  return (unsigned int)fCND_hits_indexLadc->getValue(i);}
	unsigned int GetCND_hits_indexRadc(int i){  return (unsigned int)fCND_hits_indexRadc->getValue(i);}
	unsigned int GetCND_hits_indexLtdc(int i){  return (unsigned int)fCND_hits_indexLtdc->getValue(i);}
	unsigned int GetCND_hits_indexRtdc(int i){  return (unsigned int)fCND_hits_indexRtdc->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CND::clusters:reconstructed cluster info from CND and CTOF ===================
	unsigned int GetCND_clusters_id(int i){  return (unsigned int)fCND_clusters_id->getValue(i);}
	unsigned int GetCND_clusters_sector(int i){  return (unsigned int)fCND_clusters_sector->getValue(i);}
	unsigned int GetCND_clusters_layer(int i){  return (unsigned int)fCND_clusters_layer->getValue(i);}
	unsigned int GetCND_clusters_component(int i){  return (unsigned int)fCND_clusters_component->getValue(i);}
	unsigned int GetCND_clusters_nhits(int i){  return (unsigned int)fCND_clusters_nhits->getValue(i);}
	float GetCND_clusters_energy(int i){  return (float)fCND_clusters_energy->getValue(i);}
	float GetCND_clusters_x(int i){  return (float)fCND_clusters_x->getValue(i);}
	float GetCND_clusters_y(int i){  return (float)fCND_clusters_y->getValue(i);}
	float GetCND_clusters_z(int i){  return (float)fCND_clusters_z->getValue(i);}
	float GetCND_clusters_time(int i){  return (float)fCND_clusters_time->getValue(i);}
	unsigned int GetCND_clusters_status(int i){  return (unsigned int)fCND_clusters_status->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorCND,0)
};
#endif




