// Filename: TClas12.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Mon Jul  9 15:22:01 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include <fstream>
#include "TSystem.h"
#include "TApplication.h"
#include "TFile.h"
#include "TObject.h"
#include "TKey.h"
#include "TIterator.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TClass.h"
#include "TDictionary.h"
#include "TClas12.h"

using namespace std;

ClassImp(TClas12)

TClas12::TClas12()
{
    
}

TClas12::~TClas12()
{
    delete Config;    
}

TClas12Config* TClas12::Config = new TClas12Config();

void TClas12::Exit()
{
    gApplication->Terminate();
}

void TClas12::ExitIfInvalid(TString filePath)
{
    if(gSystem->AccessPathName(filePath))
    {
	cout << "Invalid path: "<<filePath<<endl;
	Exit();
    }
}

Bool_t TClas12::IsValid(TString filePath)
{
    if(gSystem->AccessPathName(filePath))
    {
	cout << "Invalid path: "<<filePath<<endl;
	return kFALSE;
    }
    return kTRUE;
}


Int_t TClas12::GetCounter()
{
    TString filePath = Config->GetVaultPath();
    filePath += "VaultFileCounter.txt";    
    fstream counterFile(filePath,ios::in | ios::out);
    Int_t fileNo = -1;
    if(!counterFile)
    {
	cout << "WARNING: The vault file counter NOT found" <<endl;
	return -1;
    }

    counterFile>>fileNo;	
    ++fileNo;
    counterFile.seekg(0,ios::beg);
    counterFile<<fileNo;
    counterFile.close();

    TString rootFilePath = Config->GetVaultPath();
    rootFilePath += fileNo;
    rootFilePath += ".root";

    if(!gSystem->AccessPathName(rootFilePath))
    {
	cout << "WARNING: The vault-file already exists. Overwriting NOT permitted." <<endl;
	return -1;
    }
    return fileNo;
}

void TClas12::SetCounter(Int_t count)
{
    TString filePath = Config->GetVaultPath();
    filePath += "VaultFileCounter.txt";    
    fstream counterFile(filePath, ios::out);
    Int_t fileNo = count;
    if(!counterFile)
    {
	cout << "WARNING: The vault file counter NOT found" <<endl;
	return;
    }
    counterFile<<fileNo;
    cout << "Counter set to: "<<fileNo <<endl;
    counterFile.close();
}

void TClas12::PrintCounter()
{
    TString filePath = Config->GetVaultPath();
    filePath += "VaultFileCounter.txt";    
    fstream counterFile(filePath, ios::in);
    Int_t fileNo = -1;
    if(!counterFile)
    {
	cout << "WARNING: The vault file-counter NOT found" <<endl;
	return;
    }

    counterFile>>fileNo;	
    counterFile.close();
    cout << "File counter in the vault: "<< fileNo <<endl;
}


void TClas12::VaultViewer(Int_t fileNumber)
{
    //Open the file for reading
    TString fileName = Config->GetVaultPath();
    fileName += fileNumber;
    fileName += ".root";
    if(!IsValid(fileName))
	return;
    TFile *file = new TFile(fileName);
    Int_t hist_counter = 0;

    //Create an iterator
    TIter nextkey (file->GetListOfKeys());
    TKey *key;

    while ( (key = (TKey*) nextkey() ))
    {
	//Read object from first source file
	TObject *obj = key->ReadObj();

	//Check that the object the key points to is a histogram
	if (obj->IsA()->InheritsFrom("TH1") || obj->IsA()->InheritsFrom("TGraph"))
	{
	    //Cast the TObject pointer to a histogram one
	    // TH1 * hist = (TH1*) (obj);
	    
	    TCanvas *c = new TCanvas();
	    obj->Draw();
	}
	else if(obj->IsA()->InheritsFrom("TCanvas"))
	    obj->Draw();
    }    
}


void TClas12::VaultPrint(Int_t lowerRange, Int_t upperRange)
{
    for(int i = lowerRange; i <= upperRange; ++i)
    {
	//Open the file for reading
	TString fileName = Config->GetVaultPath();
	fileName += i;
	fileName += ".root";
	if(gSystem->AccessPathName(fileName))
	    continue;
	cout <<"\n============================================================================="<<endl;
	cout << "\t\t\t File Number: "<<i<<endl;

	cout <<"============================================================================="<<endl;
	TFile *file = new TFile(fileName);
	file->ls();
	file->Close();
    }
}

void TClas12::VaultListing(Int_t lowerRange, Int_t upperRange)
{
    for(int i = lowerRange; i <= upperRange; ++i)
    {
	//Open the file for reading
	TString fileName = Config->GetVaultPath();
	fileName += i;
	fileName += ".root";
	if(gSystem->AccessPathName(fileName))
	    continue;
	TFile *file = new TFile(fileName);
	file->Print();
	file->Close();
    }
}

Double_t TClas12::GetMass(TString particle_name)
{
    Double_t elecMass = 0.510998/1000.0;       //!< Electron mass in GeV/c^2 
    Double_t protonMass = 938.27208/1000.0;    //!< Proton mass in GeV/c^2 
    Double_t piPlusMass = 139.57018/1000.0;    //!< Pion plus mass in GeV/c^2 
    Double_t piMinusMass  = 139.57018/1000.0;  //!< Pion minus mass in GeV/c^2 

    if(particle_name=="electron")
	return elecMass;
    else if(particle_name=="proton")
	return protonMass;
    else if(particle_name=="pion+")
	return piPlusMass;
    else if(particle_name=="pion-")
	return piMinusMass;
    else
    {
	cout << "Particle Mass NOT defined. Existing ...." <<endl;
	Exit();
    }
}

Bool_t TClas12::fTurnOnEventMap = kTRUE;
Bool_t TClas12::fTurnOnDCMap = kTRUE;
Bool_t TClas12::fTurnOnECMap = kTRUE;

void TClas12::TurnOffMap(TString bankName)
{
    if(bankName=="EVENT")
	fTurnOnEventMap = kFALSE;
    else if(bankName=="DC")
	fTurnOnDCMap = kFALSE;
    else if(bankName=="EC")
	fTurnOnECMap = kFALSE;
    else
    {
	cout << "Invalid bank name. Existing ...." <<endl;
	Exit();
    }    
}

void TClas12::TurnOnMap(TString bankName)
{
    if(bankName=="EVENT")
	fTurnOnEventMap = kTRUE;
    else if(bankName=="DC")
	fTurnOnDCMap = kTRUE;
    else if(bankName=="EC")
	fTurnOnECMap = kTRUE;
    else
    {
	cout << "Invalid bank name. Existing ...." <<endl;
	Exit();
    }    
}
