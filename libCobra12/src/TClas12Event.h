// Filename: TClas12Event.cc
// Description:  Base class for Clas12Run
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 11:56:52 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12EVENT_H
#define TCLAS12EVENT_H

#include <iostream>
#include <unordered_map>
#include "TObject.h"
#include "TString.h"
#include "../../external/hipo-io/libcpp/reader.h"

using namespace std;

class TClas12Event: public TObject
{
    void Init();
protected:    
    Int_t fRunNumber;
    TString fFileName;
    Int_t fFileNo;
    
    void SetBranches();
    
public:
    TClas12Event();
    TClas12Event(Int_t runNumber, Int_t fileNo);
    TClas12Event(TString fileName);
    //TClas12Event(Int_t firstRun, Int_t firstFile, Int_t lastFile);
    virtual ~TClas12Event();

    virtual Bool_t HasEvent() = 0;
    void PrintDictionary();
    void Print();
    Bool_t FileExist();
    
    hipo::reader *fHipoReader;
    Bool_t fRunExist;
    Bool_t fFileExist;
    Int_t fEventNo;

    //protected:   
    //=====================================Event Buffer =========================================

    //=================  EVENT BANK=================================================================
    //================== RECHB::Event:Event Header Bank ===================
    hipo::node<uint32_t> *fRECHB_Event_NRUN;   //Run Number
    hipo::node<uint32_t> *fRECHB_Event_NEVENT;   //Event Number
    hipo::node<float> *fRECHB_Event_EVNTime;   //Event Time
    hipo::node<uint8_t> *fRECHB_Event_TYPE;   //Event Type (Data or MC)
    hipo::node<uint16_t> *fRECHB_Event_EvCAT;   //Event Category, if >0:  e-, e-p, e-pi+.....
    hipo::node<uint16_t> *fRECHB_Event_NPGP;   //Number of Final (Timed-based) Reconstructed Particles*100 + Number of Geometrically Reconstructed Particles
    hipo::node<uint64_t> *fRECHB_Event_TRG;   //Trigger Type (CLAS12_e-, FT_CLAS12_h, CLAS12_H,...) + Prescale Factor for that Trigger
    hipo::node<float> *fRECHB_Event_BCG;   //Beam charge, gated (Coulomb)
    hipo::node<double> *fRECHB_Event_LT;   //Lifetime
    hipo::node<float> *fRECHB_Event_STTime;   //Event Start Time (ns)
    hipo::node<float> *fRECHB_Event_RFTime;   //RF Time (ns)
    hipo::node<uint8_t> *fRECHB_Event_Helic;   //Helicity of Event
    hipo::node<float> *fRECHB_Event_PTIME;   //Event Processing Time (UNIX Time = seconds)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Particle:Reconstructed Particle Information ===================
    hipo::node<uint32_t> *fRECHB_Particle_pid;   //particle id in LUND conventions
    hipo::node<float> *fRECHB_Particle_px;   //x component of the momentum
    hipo::node<float> *fRECHB_Particle_py;   //y component of the momentum
    hipo::node<float> *fRECHB_Particle_pz;   //z component of the momentum
    hipo::node<float> *fRECHB_Particle_vx;   //x component of the vertex
    hipo::node<float> *fRECHB_Particle_vy;   //y component of the vertex
    hipo::node<float> *fRECHB_Particle_vz;   //z component of the vertex
    hipo::node<uint8_t> *fRECHB_Particle_charge;   //particle charge
    hipo::node<float> *fRECHB_Particle_beta;   //particle beta measured by TOF
    hipo::node<float> *fRECHB_Particle_chi2pid;   //Chi2 of assigned PID
    hipo::node<uint16_t> *fRECHB_Particle_status;   //particle status (represents detector collection it passed)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Calorimeter:Calorimeter Responses for Particles bank ===================
    hipo::node<uint16_t> *fRECHB_Calorimeter_index;   //index of the hit in the specific detector bank
    hipo::node<uint16_t> *fRECHB_Calorimeter_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fRECHB_Calorimeter_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fRECHB_Calorimeter_sector;   //Sector of the Detector hit
    hipo::node<uint8_t> *fRECHB_Calorimeter_layer;   //Layer of the Detector hit
    hipo::node<float> *fRECHB_Calorimeter_energy;   //Energy associated with the hit
    hipo::node<float> *fRECHB_Calorimeter_time;   //Time associated with the hit
    hipo::node<float> *fRECHB_Calorimeter_path;   //Path from vertex to the hit position
    hipo::node<float> *fRECHB_Calorimeter_chi2;   //Chi2 (or quality) of hit-track matching
    hipo::node<float> *fRECHB_Calorimeter_x;   //X coordinate of the hit
    hipo::node<float> *fRECHB_Calorimeter_y;   //Y coordinate of the hit
    hipo::node<float> *fRECHB_Calorimeter_z;   //Z coordinate of the hit
    hipo::node<float> *fRECHB_Calorimeter_hx;   //X coordinate of the matched hit
    hipo::node<float> *fRECHB_Calorimeter_hy;   //Y coordinate of the mathced hit
    hipo::node<float> *fRECHB_Calorimeter_hz;   //Z coordinate of the matched hit
    hipo::node<float> *fRECHB_Calorimeter_lu;   //distance on U-side
    hipo::node<float> *fRECHB_Calorimeter_lv;   //distance on V-side
    hipo::node<float> *fRECHB_Calorimeter_lw;   //distance on W-side
    hipo::node<float> *fRECHB_Calorimeter_du;   //shower width on U-side
    hipo::node<float> *fRECHB_Calorimeter_dv;   //shower width on V-side
    hipo::node<float> *fRECHB_Calorimeter_dw;   //shower width on w-side
    hipo::node<float> *fRECHB_Calorimeter_m2u;   //2nd moment of the shower on U-side
    hipo::node<float> *fRECHB_Calorimeter_m2v;   //2nd moment of the shower on V-side
    hipo::node<float> *fRECHB_Calorimeter_m2w;   //2nd moment of the shower on W-side
    hipo::node<float> *fRECHB_Calorimeter_m3u;   //3rd moment of the shower on U-side
    hipo::node<float> *fRECHB_Calorimeter_m3v;   //3rd moment of the shower on V-side
    hipo::node<float> *fRECHB_Calorimeter_m3w;   //3rd moment of the shower on W-side
    hipo::node<uint16_t> *fRECHB_Calorimeter_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Cherenkov:Cherenkov Responses for Particles bank ===================
    hipo::node<uint16_t> *fRECHB_Cherenkov_index;   //index of the hit in the specific detector bank
    hipo::node<uint16_t> *fRECHB_Cherenkov_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fRECHB_Cherenkov_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fRECHB_Cherenkov_sector;   //Sector of the Detector hit
    hipo::node<float> *fRECHB_Cherenkov_nphe;   //Number of photoelectrons from Cherenkov radiation
    hipo::node<float> *fRECHB_Cherenkov_time;   //Time associated with the hit
    hipo::node<float> *fRECHB_Cherenkov_path;   //Path from vertex to the hit position
    hipo::node<float> *fRECHB_Cherenkov_chi2;   //Chi2 (or quality) of hit-track matching
    hipo::node<float> *fRECHB_Cherenkov_x;   //X coordinate of the hit
    hipo::node<float> *fRECHB_Cherenkov_y;   //Y coordinate of the hit
    hipo::node<float> *fRECHB_Cherenkov_z;   //Z coordinate of the hit
    hipo::node<float> *fRECHB_Cherenkov_theta;   //Theta of the matched hit
    hipo::node<float> *fRECHB_Cherenkov_phi;   //Phi of the mathced hit
    hipo::node<float> *fRECHB_Cherenkov_dtheta;   //Expected Theta Resolution
    hipo::node<float> *fRECHB_Cherenkov_dphi;   //Expected Phi Resolution
    hipo::node<uint16_t> *fRECHB_Cherenkov_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::ForwardTagger:Forward Tagger information for Particles bank ===================
    hipo::node<uint16_t> *fRECHB_ForwardTagger_index;   //index of the cluster in the specific detector bank
    hipo::node<uint16_t> *fRECHB_ForwardTagger_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fRECHB_ForwardTagger_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<float> *fRECHB_ForwardTagger_energy;   //Energy associated with the cluster
    hipo::node<float> *fRECHB_ForwardTagger_time;   //Time associated with the cluster
    hipo::node<float> *fRECHB_ForwardTagger_path;   //Path from vertex to the cluster position
    hipo::node<float> *fRECHB_ForwardTagger_chi2;   //Chi2 (or quality) of cluster-particle matching
    hipo::node<float> *fRECHB_ForwardTagger_x;   //X coordinate of the cluster
    hipo::node<float> *fRECHB_ForwardTagger_y;   //Y coordinate of the cluster
    hipo::node<float> *fRECHB_ForwardTagger_z;   //Z coordinate of the cluster
    hipo::node<float> *fRECHB_ForwardTagger_dx;   //Cluster width in x
    hipo::node<float> *fRECHB_ForwardTagger_dy;   //Cluster width in y
    hipo::node<float> *fRECHB_ForwardTagger_radius;   //Cluster radius
    hipo::node<uint16_t> *fRECHB_ForwardTagger_size;   //size of the cluster
    hipo::node<uint16_t> *fRECHB_ForwardTagger_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Scintillator:Scintillator Responses for Particles bank ===================
    hipo::node<uint16_t> *fRECHB_Scintillator_index;   //index of the hit in the specific detector bank
    hipo::node<uint16_t> *fRECHB_Scintillator_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fRECHB_Scintillator_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fRECHB_Scintillator_sector;   //Sector of the Detector hit
    hipo::node<uint8_t> *fRECHB_Scintillator_layer;   //Layer of the Detector hit
    hipo::node<uint16_t> *fRECHB_Scintillator_component;   //Component of the Detector hit
    hipo::node<float> *fRECHB_Scintillator_energy;   //Energy associated with the hit
    hipo::node<float> *fRECHB_Scintillator_time;   //Time associated with the hit
    hipo::node<float> *fRECHB_Scintillator_path;   //Path from vertex to the hit position
    hipo::node<float> *fRECHB_Scintillator_chi2;   //Chi2 (or quality) of hit-track matching
    hipo::node<float> *fRECHB_Scintillator_x;   //X coordinate of the hit
    hipo::node<float> *fRECHB_Scintillator_y;   //Y coordinate of the hit
    hipo::node<float> *fRECHB_Scintillator_z;   //Z coordinate of the hit
    hipo::node<float> *fRECHB_Scintillator_hx;   //X coordinate of the matched hit
    hipo::node<float> *fRECHB_Scintillator_hy;   //Y coordinate of the mathced hit
    hipo::node<float> *fRECHB_Scintillator_hz;   //Z coordinate of the matched hit
    hipo::node<uint16_t> *fRECHB_Scintillator_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Track:Track information for Particles bank ===================
    hipo::node<uint16_t> *fRECHB_Track_index;   //index of the track in the specific detector bank
    hipo::node<uint16_t> *fRECHB_Track_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fRECHB_Track_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint16_t> *fRECHB_Track_status;   //status of the track
    hipo::node<uint8_t> *fRECHB_Track_q;   //charge of the track
    hipo::node<float> *fRECHB_Track_chi2;   //Chi2 (or quality) track fitting
    hipo::node<uint16_t> *fRECHB_Track_NDF;   //number of degrees of freedom in track fitting
    hipo::node<float> *fRECHB_Track_px_nomm;   //x component of the momentum with no MM
    hipo::node<float> *fRECHB_Track_py_nomm;   //y component of the momentum with no MM
    hipo::node<float> *fRECHB_Track_pz_nomm;   //z component of the momentum with no MM
    hipo::node<float> *fRECHB_Track_vx_nomm;   //x component of the vertex
    hipo::node<float> *fRECHB_Track_vy_nomm;   //y component of the vertex
    hipo::node<float> *fRECHB_Track_vz_nomm;   //z component of the vertex
    hipo::node<float> *fRECHB_Track_chi2_nomm;   //Chi2 (or quality) track fitting
    hipo::node<uint16_t> *fRECHB_Track_NDF_nomm;   //number of degrees of freedom in track fitting with no MM
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::TrackCross:Track Cross information for Particles bank ===================
    hipo::node<uint16_t> *fRECHB_TrackCross_index;   //index of the cross in the specific detector bank
    hipo::node<uint16_t> *fRECHB_TrackCross_pindex;   //row number in the track bank hit is associated with
    hipo::node<uint8_t> *fRECHB_TrackCross_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fRECHB_TrackCross_sector;   //sector of the track
    hipo::node<uint8_t> *fRECHB_TrackCross_layer;   //layer of the track
    hipo::node<float> *fRECHB_TrackCross_c_x;   //Cross x-position in the lab (in cm)
    hipo::node<float> *fRECHB_TrackCross_c_y;   //Cross y-position in the lab (in cm)
    hipo::node<float> *fRECHB_TrackCross_c_z;   //Cross z-position in the lab (in cm)
    hipo::node<float> *fRECHB_TrackCross_c_ux;   //Cross unit x-direction vector in the lab
    hipo::node<float> *fRECHB_TrackCross_c_uy;   //Cross unit y-direction vector in the lab
    hipo::node<float> *fRECHB_TrackCross_c_uz;   //Cross unit z-direction vector in the lab
    hipo::node<uint16_t> *fRECHB_TrackCross_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Event:Event Header Bank ===================
    hipo::node<uint32_t> *fREC_Event_NRUN;   //Run Number
    hipo::node<uint32_t> *fREC_Event_NEVENT;   //Event Number
    hipo::node<float> *fREC_Event_EVNTime;   //Event Time
    hipo::node<uint8_t> *fREC_Event_TYPE;   //Event Type (Data or MC)
    hipo::node<uint16_t> *fREC_Event_EvCAT;   //Event Category, if >0:  e-, e-p, e-pi+.....
    hipo::node<uint16_t> *fREC_Event_NPGP;   //Number of Final (Timed-based) Reconstructed Particles*100 + Number of Geometrically Reconstructed Particles
    hipo::node<uint64_t> *fREC_Event_TRG;   //Trigger Type (CLAS12_e-, FT_CLAS12_h, CLAS12_H,...) + Prescale Factor for that Trigger
    hipo::node<float> *fREC_Event_BCG;   //Faraday Cup Gated (Coulomb)
    hipo::node<double> *fREC_Event_LT;   //Clock
    hipo::node<float> *fREC_Event_STTime;   //Event Start Time (ns)
    hipo::node<float> *fREC_Event_RFTime;   //RF Time (ns)
    hipo::node<uint8_t> *fREC_Event_Helic;   //Helicity of Event
    hipo::node<float> *fREC_Event_PTIME;   //Event Processing Time (UNIX Time = seconds)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Particle:Reconstructed Particle Information ===================
    hipo::node<uint32_t> *fREC_Particle_pid;   //particle id in LUND conventions
    hipo::node<float> *fREC_Particle_px;   //x component of the momentum
    hipo::node<float> *fREC_Particle_py;   //y component of the momentum
    hipo::node<float> *fREC_Particle_pz;   //z component of the momentum
    hipo::node<float> *fREC_Particle_vx;   //x component of the vertex
    hipo::node<float> *fREC_Particle_vy;   //y component of the vertex
    hipo::node<float> *fREC_Particle_vz;   //z component of the vertex
    hipo::node<uint8_t> *fREC_Particle_charge;   //particle charge
    hipo::node<float> *fREC_Particle_beta;   //particle beta measured by TOF
    hipo::node<float> *fREC_Particle_chi2pid;   //Chi2 of assigned PID
    hipo::node<uint16_t> *fREC_Particle_status;   //particle status (represents detector collection it passed)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Calorimeter:Calorimeter Responses for Particles bank ===================
    hipo::node<uint16_t> *fREC_Calorimeter_index;   //index of the hit in the specific detector bank
    hipo::node<uint16_t> *fREC_Calorimeter_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fREC_Calorimeter_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fREC_Calorimeter_sector;   //Sector of the Detector hit
    hipo::node<uint8_t> *fREC_Calorimeter_layer;   //Layer of the Detector hit
    hipo::node<float> *fREC_Calorimeter_energy;   //Energy associated with the hit
    hipo::node<float> *fREC_Calorimeter_time;   //Time associated with the hit
    hipo::node<float> *fREC_Calorimeter_path;   //Path from vertex to the hit position
    hipo::node<float> *fREC_Calorimeter_chi2;   //Chi2 (or quality) of hit-track matching
    hipo::node<float> *fREC_Calorimeter_x;   //X coordinate of the hit
    hipo::node<float> *fREC_Calorimeter_y;   //Y coordinate of the hit
    hipo::node<float> *fREC_Calorimeter_z;   //Z coordinate of the hit
    hipo::node<float> *fREC_Calorimeter_hx;   //X coordinate of the matched hit
    hipo::node<float> *fREC_Calorimeter_hy;   //Y coordinate of the mathced hit
    hipo::node<float> *fREC_Calorimeter_hz;   //Z coordinate of the matched hit
    hipo::node<float> *fREC_Calorimeter_lu;   //distance on U-side
    hipo::node<float> *fREC_Calorimeter_lv;   //distance on V-side
    hipo::node<float> *fREC_Calorimeter_lw;   //distance on W-side
    hipo::node<float> *fREC_Calorimeter_du;   //shower width on U-side
    hipo::node<float> *fREC_Calorimeter_dv;   //shower width on V-side
    hipo::node<float> *fREC_Calorimeter_dw;   //shower width on w-side
    hipo::node<float> *fREC_Calorimeter_m2u;   //2nd moment of the shower on U-side
    hipo::node<float> *fREC_Calorimeter_m2v;   //2nd moment of the shower on V-side
    hipo::node<float> *fREC_Calorimeter_m2w;   //2nd moment of the shower on W-side
    hipo::node<float> *fREC_Calorimeter_m3u;   //3rd moment of the shower on U-side
    hipo::node<float> *fREC_Calorimeter_m3v;   //3rd moment of the shower on V-side
    hipo::node<float> *fREC_Calorimeter_m3w;   //3rd moment of the shower on W-side
    hipo::node<uint16_t> *fREC_Calorimeter_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Cherenkov:Cherenkov Responses for Particles bank ===================
    hipo::node<uint16_t> *fREC_Cherenkov_index;   //index of the hit in the specific detector bank
    hipo::node<uint16_t> *fREC_Cherenkov_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fREC_Cherenkov_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fREC_Cherenkov_sector;   //Sector of the Detector hit
    hipo::node<float> *fREC_Cherenkov_nphe;   //Number of photoelectrons from Cherenkov radiation
    hipo::node<float> *fREC_Cherenkov_time;   //Time associated with the hit
    hipo::node<float> *fREC_Cherenkov_path;   //Path from vertex to the hit position
    hipo::node<float> *fREC_Cherenkov_chi2;   //Chi2 (or quality) of hit-track matching
    hipo::node<float> *fREC_Cherenkov_x;   //X coordinate of the hit
    hipo::node<float> *fREC_Cherenkov_y;   //Y coordinate of the hit
    hipo::node<float> *fREC_Cherenkov_z;   //Z coordinate of the hit
    hipo::node<float> *fREC_Cherenkov_theta;   //Theta of the matched hit
    hipo::node<float> *fREC_Cherenkov_phi;   //Phi of the mathced hit
    hipo::node<float> *fREC_Cherenkov_dtheta;   //Expected Theta Resolution
    hipo::node<float> *fREC_Cherenkov_dphi;   //Expected Phi Resolution
    hipo::node<uint16_t> *fREC_Cherenkov_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::ForwardTagger:Forward Tagger information for Particles bank ===================
    hipo::node<uint16_t> *fREC_ForwardTagger_index;   //index of the cluster in the specific detector bank
    hipo::node<uint16_t> *fREC_ForwardTagger_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fREC_ForwardTagger_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<float> *fREC_ForwardTagger_energy;   //Energy associated with the cluster
    hipo::node<float> *fREC_ForwardTagger_time;   //Time associated with the cluster
    hipo::node<float> *fREC_ForwardTagger_path;   //Path from vertex to the cluster position
    hipo::node<float> *fREC_ForwardTagger_chi2;   //Chi2 (or quality) of cluster-particle matching
    hipo::node<float> *fREC_ForwardTagger_x;   //X coordinate of the cluster
    hipo::node<float> *fREC_ForwardTagger_y;   //Y coordinate of the cluster
    hipo::node<float> *fREC_ForwardTagger_z;   //Z coordinate of the cluster
    hipo::node<float> *fREC_ForwardTagger_dx;   //Cluster width in x
    hipo::node<float> *fREC_ForwardTagger_dy;   //Cluster width in y
    hipo::node<float> *fREC_ForwardTagger_radius;   //Cluster radius
    hipo::node<uint16_t> *fREC_ForwardTagger_size;   //size of the cluster
    hipo::node<uint16_t> *fREC_ForwardTagger_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Scintillator:Scintillator Responses for Particles bank ===================
    hipo::node<uint16_t> *fREC_Scintillator_index;   //index of the hit in the specific detector bank
    hipo::node<uint16_t> *fREC_Scintillator_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fREC_Scintillator_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fREC_Scintillator_sector;   //Sector of the Detector hit
    hipo::node<uint8_t> *fREC_Scintillator_layer;   //Layer of the Detector hit
    hipo::node<uint16_t> *fREC_Scintillator_component;   //Component of the Detector hit
    hipo::node<float> *fREC_Scintillator_energy;   //Energy associated with the hit
    hipo::node<float> *fREC_Scintillator_time;   //Time associated with the hit
    hipo::node<float> *fREC_Scintillator_path;   //Path from vertex to the hit position
    hipo::node<float> *fREC_Scintillator_chi2;   //Chi2 (or quality) of hit-track matching
    hipo::node<float> *fREC_Scintillator_x;   //X coordinate of the hit
    hipo::node<float> *fREC_Scintillator_y;   //Y coordinate of the hit
    hipo::node<float> *fREC_Scintillator_z;   //Z coordinate of the hit
    hipo::node<float> *fREC_Scintillator_hx;   //X coordinate of the matched hit
    hipo::node<float> *fREC_Scintillator_hy;   //Y coordinate of the mathced hit
    hipo::node<float> *fREC_Scintillator_hz;   //Z coordinate of the matched hit
    hipo::node<uint16_t> *fREC_Scintillator_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Track:Track information for Particles bank ===================
    hipo::node<uint16_t> *fREC_Track_index;   //index of the track in the specific detector bank
    hipo::node<uint16_t> *fREC_Track_pindex;   //row number in the particle bank hit is associated with
    hipo::node<uint8_t> *fREC_Track_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint16_t> *fREC_Track_status;   //status of the track
    hipo::node<uint8_t> *fREC_Track_q;   //charge of the track
    hipo::node<float> *fREC_Track_chi2;   //Chi2 (or quality) track fitting
    hipo::node<uint16_t> *fREC_Track_NDF;   //number of degrees of freedom in track fitting
    hipo::node<float> *fREC_Track_px_nomm;   //x component of the momentum with no MM
    hipo::node<float> *fREC_Track_py_nomm;   //y component of the momentum with no MM
    hipo::node<float> *fREC_Track_pz_nomm;   //z component of the momentum with no MM
    hipo::node<float> *fREC_Track_vx_nomm;   //x component of the vertex
    hipo::node<float> *fREC_Track_vy_nomm;   //y component of the vertex
    hipo::node<float> *fREC_Track_vz_nomm;   //z component of the vertex
    hipo::node<float> *fREC_Track_chi2_nomm;   //Chi2 (or quality) track fitting
    hipo::node<uint16_t> *fREC_Track_NDF_nomm;   //number of degrees of freedom in track fitting with no MM
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::TrackCross:Track Cross information for Particles bank ===================
    hipo::node<uint16_t> *fREC_TrackCross_index;   //index of the cross in the specific detector bank
    hipo::node<uint16_t> *fREC_TrackCross_pindex;   //row number in the track bank hit is associated with
    hipo::node<uint8_t> *fREC_TrackCross_detector;   //Detector ID, defined in COATJAVA DetectorType
    hipo::node<uint8_t> *fREC_TrackCross_sector;   //sector of the track
    hipo::node<uint8_t> *fREC_TrackCross_layer;   //layer of the track
    hipo::node<float> *fREC_TrackCross_c_x;   //Cross x-position in the lab (in cm)
    hipo::node<float> *fREC_TrackCross_c_y;   //Cross y-position in the lab (in cm)
    hipo::node<float> *fREC_TrackCross_c_z;   //Cross z-position in the lab (in cm)
    hipo::node<float> *fREC_TrackCross_c_ux;   //Cross unit x-direction vector in the lab
    hipo::node<float> *fREC_TrackCross_c_uy;   //Cross unit y-direction vector in the lab
    hipo::node<float> *fREC_TrackCross_c_uz;   //Cross unit z-direction vector in the lab
    hipo::node<uint16_t> *fREC_TrackCross_status;   //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::CovMat:reconstructed track covariance matrix ===================
    hipo::node<uint16_t> *fREC_CovMat_index;   //index of the track in Tracks bank
    hipo::node<uint16_t> *fREC_CovMat_pindex;   //row number in the particle bank hit is associated with
    hipo::node<float> *fREC_CovMat_C11;   //C11 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C12;   //C12 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C13;   //C13 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C14;   //C14 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C15;   //C15 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C22;   //C22 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C23;   //C23 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C24;   //C24 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C25;   //C25 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C33;   //C33 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C34;   //C34 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C35;   //C35 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C44;   //C44 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C45;   //C45 covariance matrix element at last superlayer used in the fit
    hipo::node<float> *fREC_CovMat_C55;   //C55 covariance matrix element at last superlayer used in the fit
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::VertDoca:Track Cross information for Particles bank ===================
    hipo::node<uint16_t> *fREC_VertDoca_index1;   //index of the first track in Tracks bank
    hipo::node<uint16_t> *fREC_VertDoca_index2;   //index of the second track in Tracks bank
    hipo::node<float> *fREC_VertDoca_x;   //x-position of the common vertex
    hipo::node<float> *fREC_VertDoca_y;   //y-position of the common vertex
    hipo::node<float> *fREC_VertDoca_z;   //z-position of the common vertex
    hipo::node<float> *fREC_VertDoca_x1;   //x-position of the first track at the DOCA point
    hipo::node<float> *fREC_VertDoca_y1;   //y-position of the first track at the DOCA point
    hipo::node<float> *fREC_VertDoca_z1;   //z-position of the first track at the DOCA point
    hipo::node<float> *fREC_VertDoca_cx1;   //x-direction vector of the first track at the DOCA point
    hipo::node<float> *fREC_VertDoca_cy1;   //y-direction vector of the first track at the DOCA point
    hipo::node<float> *fREC_VertDoca_cz1;   //z-direction vector of the first track at the DOCA point
    hipo::node<float> *fREC_VertDoca_x2;   //x-position of the second track at the DOCA point
    hipo::node<float> *fREC_VertDoca_y2;   //y-position of the second track at the DOCA point
    hipo::node<float> *fREC_VertDoca_z2;   //z-position of the second track at the DOCA point
    hipo::node<float> *fREC_VertDoca_cx2;   //x-direction vector of the second track at the DOCA point
    hipo::node<float> *fREC_VertDoca_cy2;   //y-direction vector of the second track at the DOCA point
    hipo::node<float> *fREC_VertDoca_cz2;   //z-direction vector of the second track at the DOCA point
    hipo::node<float> *fREC_VertDoca_r;   //The distance between two tracks
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Traj:Trajectory information for Particles bank ===================
    hipo::node<uint16_t> *fREC_Traj_pindex;   //index of the particle in REC::Particle
    hipo::node<uint16_t> *fREC_Traj_index;   //index of the track in its tracking bank
    hipo::node<uint16_t> *fREC_Traj_detId;   //representative index of the detector
    hipo::node<uint8_t> *fREC_Traj_q;   //charge of the track
    hipo::node<float> *fREC_Traj_x;   //x-position of the track at the detector surface
    hipo::node<float> *fREC_Traj_y;   //y-position of the track at the detector surface
    hipo::node<float> *fREC_Traj_z;   //z-position of the track at the detector surface
    hipo::node<float> *fREC_Traj_cx;   //direction cosline of the track at the detector surface
    hipo::node<float> *fREC_Traj_cy;   //direction cosline of the track at the detector surface
    hipo::node<float> *fREC_Traj_cz;   //direction cosline of the track at the detector surface
    hipo::node<float> *fREC_Traj_pathlength;   //pathlength of the track to the detector surface from the DOCA point
    //--------------------------------------------------------------------------------------------------------------------------------------




public:
    //================== RECHB::Event:Event Header Bank ===================
    unsigned int GetRECHB_Event_NRUN(int i){  return (unsigned int)fRECHB_Event_NRUN->getValue(i);}
    unsigned int GetRECHB_Event_NEVENT(int i){  return (unsigned int)fRECHB_Event_NEVENT->getValue(i);}
    float GetRECHB_Event_EVNTime(int i){  return (float)fRECHB_Event_EVNTime->getValue(i);}
    unsigned int GetRECHB_Event_TYPE(int i){  return (unsigned int)fRECHB_Event_TYPE->getValue(i);}
    unsigned int GetRECHB_Event_EvCAT(int i){  return (unsigned int)fRECHB_Event_EvCAT->getValue(i);}
    unsigned int GetRECHB_Event_NPGP(int i){  return (unsigned int)fRECHB_Event_NPGP->getValue(i);}
    unsigned long GetRECHB_Event_TRG(int i){  return (unsigned long)fRECHB_Event_TRG->getValue(i);}
    float GetRECHB_Event_BCG(int i){  return (float)fRECHB_Event_BCG->getValue(i);}
    double GetRECHB_Event_LT(int i){  return (double)fRECHB_Event_LT->getValue(i);}
    float GetRECHB_Event_STTime(int i){  return (float)fRECHB_Event_STTime->getValue(i);}
    float GetRECHB_Event_RFTime(int i){  return (float)fRECHB_Event_RFTime->getValue(i);}
    unsigned int GetRECHB_Event_Helic(int i){  return (unsigned int)fRECHB_Event_Helic->getValue(i);}
    float GetRECHB_Event_PTIME(int i){  return (float)fRECHB_Event_PTIME->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Particle:Reconstructed Particle Information ===================
    unsigned int GetRECHB_Particle_pid(int i){  return (unsigned int)fRECHB_Particle_pid->getValue(i);}
    float GetRECHB_Particle_px(int i){  return (float)fRECHB_Particle_px->getValue(i);}
    float GetRECHB_Particle_py(int i){  return (float)fRECHB_Particle_py->getValue(i);}
    float GetRECHB_Particle_pz(int i){  return (float)fRECHB_Particle_pz->getValue(i);}
    float GetRECHB_Particle_vx(int i){  return (float)fRECHB_Particle_vx->getValue(i);}
    float GetRECHB_Particle_vy(int i){  return (float)fRECHB_Particle_vy->getValue(i);}
    float GetRECHB_Particle_vz(int i){  return (float)fRECHB_Particle_vz->getValue(i);}
    unsigned int GetRECHB_Particle_charge(int i){  return (unsigned int)fRECHB_Particle_charge->getValue(i);}
    float GetRECHB_Particle_beta(int i){  return (float)fRECHB_Particle_beta->getValue(i);}
    float GetRECHB_Particle_chi2pid(int i){  return (float)fRECHB_Particle_chi2pid->getValue(i);}
    unsigned int GetRECHB_Particle_status(int i){  return (unsigned int)fRECHB_Particle_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Calorimeter:Calorimeter Responses for Particles bank ===================
    unsigned int GetRECHB_Calorimeter_index(int i){  return (unsigned int)fRECHB_Calorimeter_index->getValue(i);}
    unsigned int GetRECHB_Calorimeter_pindex(int i){  return (unsigned int)fRECHB_Calorimeter_pindex->getValue(i);}
    unsigned int GetRECHB_Calorimeter_detector(int i){  return (unsigned int)fRECHB_Calorimeter_detector->getValue(i);}
    unsigned int GetRECHB_Calorimeter_sector(int i){  return (unsigned int)fRECHB_Calorimeter_sector->getValue(i);}
    unsigned int GetRECHB_Calorimeter_layer(int i){  return (unsigned int)fRECHB_Calorimeter_layer->getValue(i);}
    float GetRECHB_Calorimeter_energy(int i){  return (float)fRECHB_Calorimeter_energy->getValue(i);}
    float GetRECHB_Calorimeter_time(int i){  return (float)fRECHB_Calorimeter_time->getValue(i);}
    float GetRECHB_Calorimeter_path(int i){  return (float)fRECHB_Calorimeter_path->getValue(i);}
    float GetRECHB_Calorimeter_chi2(int i){  return (float)fRECHB_Calorimeter_chi2->getValue(i);}
    float GetRECHB_Calorimeter_x(int i){  return (float)fRECHB_Calorimeter_x->getValue(i);}
    float GetRECHB_Calorimeter_y(int i){  return (float)fRECHB_Calorimeter_y->getValue(i);}
    float GetRECHB_Calorimeter_z(int i){  return (float)fRECHB_Calorimeter_z->getValue(i);}
    float GetRECHB_Calorimeter_hx(int i){  return (float)fRECHB_Calorimeter_hx->getValue(i);}
    float GetRECHB_Calorimeter_hy(int i){  return (float)fRECHB_Calorimeter_hy->getValue(i);}
    float GetRECHB_Calorimeter_hz(int i){  return (float)fRECHB_Calorimeter_hz->getValue(i);}
    float GetRECHB_Calorimeter_lu(int i){  return (float)fRECHB_Calorimeter_lu->getValue(i);}
    float GetRECHB_Calorimeter_lv(int i){  return (float)fRECHB_Calorimeter_lv->getValue(i);}
    float GetRECHB_Calorimeter_lw(int i){  return (float)fRECHB_Calorimeter_lw->getValue(i);}
    float GetRECHB_Calorimeter_du(int i){  return (float)fRECHB_Calorimeter_du->getValue(i);}
    float GetRECHB_Calorimeter_dv(int i){  return (float)fRECHB_Calorimeter_dv->getValue(i);}
    float GetRECHB_Calorimeter_dw(int i){  return (float)fRECHB_Calorimeter_dw->getValue(i);}
    float GetRECHB_Calorimeter_m2u(int i){  return (float)fRECHB_Calorimeter_m2u->getValue(i);}
    float GetRECHB_Calorimeter_m2v(int i){  return (float)fRECHB_Calorimeter_m2v->getValue(i);}
    float GetRECHB_Calorimeter_m2w(int i){  return (float)fRECHB_Calorimeter_m2w->getValue(i);}
    float GetRECHB_Calorimeter_m3u(int i){  return (float)fRECHB_Calorimeter_m3u->getValue(i);}
    float GetRECHB_Calorimeter_m3v(int i){  return (float)fRECHB_Calorimeter_m3v->getValue(i);}
    float GetRECHB_Calorimeter_m3w(int i){  return (float)fRECHB_Calorimeter_m3w->getValue(i);}
    unsigned int GetRECHB_Calorimeter_status(int i){  return (unsigned int)fRECHB_Calorimeter_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Cherenkov:Cherenkov Responses for Particles bank ===================
    unsigned int GetRECHB_Cherenkov_index(int i){  return (unsigned int)fRECHB_Cherenkov_index->getValue(i);}
    unsigned int GetRECHB_Cherenkov_pindex(int i){  return (unsigned int)fRECHB_Cherenkov_pindex->getValue(i);}
    unsigned int GetRECHB_Cherenkov_detector(int i){  return (unsigned int)fRECHB_Cherenkov_detector->getValue(i);}
    unsigned int GetRECHB_Cherenkov_sector(int i){  return (unsigned int)fRECHB_Cherenkov_sector->getValue(i);}
    float GetRECHB_Cherenkov_nphe(int i){  return (float)fRECHB_Cherenkov_nphe->getValue(i);}
    float GetRECHB_Cherenkov_time(int i){  return (float)fRECHB_Cherenkov_time->getValue(i);}
    float GetRECHB_Cherenkov_path(int i){  return (float)fRECHB_Cherenkov_path->getValue(i);}
    float GetRECHB_Cherenkov_chi2(int i){  return (float)fRECHB_Cherenkov_chi2->getValue(i);}
    float GetRECHB_Cherenkov_x(int i){  return (float)fRECHB_Cherenkov_x->getValue(i);}
    float GetRECHB_Cherenkov_y(int i){  return (float)fRECHB_Cherenkov_y->getValue(i);}
    float GetRECHB_Cherenkov_z(int i){  return (float)fRECHB_Cherenkov_z->getValue(i);}
    float GetRECHB_Cherenkov_theta(int i){  return (float)fRECHB_Cherenkov_theta->getValue(i);}
    float GetRECHB_Cherenkov_phi(int i){  return (float)fRECHB_Cherenkov_phi->getValue(i);}
    float GetRECHB_Cherenkov_dtheta(int i){  return (float)fRECHB_Cherenkov_dtheta->getValue(i);}
    float GetRECHB_Cherenkov_dphi(int i){  return (float)fRECHB_Cherenkov_dphi->getValue(i);}
    unsigned int GetRECHB_Cherenkov_status(int i){  return (unsigned int)fRECHB_Cherenkov_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::ForwardTagger:Forward Tagger information for Particles bank ===================
    unsigned int GetRECHB_ForwardTagger_index(int i){  return (unsigned int)fRECHB_ForwardTagger_index->getValue(i);}
    unsigned int GetRECHB_ForwardTagger_pindex(int i){  return (unsigned int)fRECHB_ForwardTagger_pindex->getValue(i);}
    unsigned int GetRECHB_ForwardTagger_detector(int i){  return (unsigned int)fRECHB_ForwardTagger_detector->getValue(i);}
    float GetRECHB_ForwardTagger_energy(int i){  return (float)fRECHB_ForwardTagger_energy->getValue(i);}
    float GetRECHB_ForwardTagger_time(int i){  return (float)fRECHB_ForwardTagger_time->getValue(i);}
    float GetRECHB_ForwardTagger_path(int i){  return (float)fRECHB_ForwardTagger_path->getValue(i);}
    float GetRECHB_ForwardTagger_chi2(int i){  return (float)fRECHB_ForwardTagger_chi2->getValue(i);}
    float GetRECHB_ForwardTagger_x(int i){  return (float)fRECHB_ForwardTagger_x->getValue(i);}
    float GetRECHB_ForwardTagger_y(int i){  return (float)fRECHB_ForwardTagger_y->getValue(i);}
    float GetRECHB_ForwardTagger_z(int i){  return (float)fRECHB_ForwardTagger_z->getValue(i);}
    float GetRECHB_ForwardTagger_dx(int i){  return (float)fRECHB_ForwardTagger_dx->getValue(i);}
    float GetRECHB_ForwardTagger_dy(int i){  return (float)fRECHB_ForwardTagger_dy->getValue(i);}
    float GetRECHB_ForwardTagger_radius(int i){  return (float)fRECHB_ForwardTagger_radius->getValue(i);}
    unsigned int GetRECHB_ForwardTagger_size(int i){  return (unsigned int)fRECHB_ForwardTagger_size->getValue(i);}
    unsigned int GetRECHB_ForwardTagger_status(int i){  return (unsigned int)fRECHB_ForwardTagger_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Scintillator:Scintillator Responses for Particles bank ===================
    unsigned int GetRECHB_Scintillator_index(int i){  return (unsigned int)fRECHB_Scintillator_index->getValue(i);}
    unsigned int GetRECHB_Scintillator_pindex(int i){  return (unsigned int)fRECHB_Scintillator_pindex->getValue(i);}
    unsigned int GetRECHB_Scintillator_detector(int i){  return (unsigned int)fRECHB_Scintillator_detector->getValue(i);}
    unsigned int GetRECHB_Scintillator_sector(int i){  return (unsigned int)fRECHB_Scintillator_sector->getValue(i);}
    unsigned int GetRECHB_Scintillator_layer(int i){  return (unsigned int)fRECHB_Scintillator_layer->getValue(i);}
    unsigned int GetRECHB_Scintillator_component(int i){  return (unsigned int)fRECHB_Scintillator_component->getValue(i);}
    float GetRECHB_Scintillator_energy(int i){  return (float)fRECHB_Scintillator_energy->getValue(i);}
    float GetRECHB_Scintillator_time(int i){  return (float)fRECHB_Scintillator_time->getValue(i);}
    float GetRECHB_Scintillator_path(int i){  return (float)fRECHB_Scintillator_path->getValue(i);}
    float GetRECHB_Scintillator_chi2(int i){  return (float)fRECHB_Scintillator_chi2->getValue(i);}
    float GetRECHB_Scintillator_x(int i){  return (float)fRECHB_Scintillator_x->getValue(i);}
    float GetRECHB_Scintillator_y(int i){  return (float)fRECHB_Scintillator_y->getValue(i);}
    float GetRECHB_Scintillator_z(int i){  return (float)fRECHB_Scintillator_z->getValue(i);}
    float GetRECHB_Scintillator_hx(int i){  return (float)fRECHB_Scintillator_hx->getValue(i);}
    float GetRECHB_Scintillator_hy(int i){  return (float)fRECHB_Scintillator_hy->getValue(i);}
    float GetRECHB_Scintillator_hz(int i){  return (float)fRECHB_Scintillator_hz->getValue(i);}
    unsigned int GetRECHB_Scintillator_status(int i){  return (unsigned int)fRECHB_Scintillator_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Track:Track information for Particles bank ===================
    unsigned int GetRECHB_Track_index(int i){  return (unsigned int)fRECHB_Track_index->getValue(i);}
    unsigned int GetRECHB_Track_pindex(int i){  return (unsigned int)fRECHB_Track_pindex->getValue(i);}
    unsigned int GetRECHB_Track_detector(int i){  return (unsigned int)fRECHB_Track_detector->getValue(i);}
    unsigned int GetRECHB_Track_status(int i){  return (unsigned int)fRECHB_Track_status->getValue(i);}
    unsigned int GetRECHB_Track_q(int i){  return (unsigned int)fRECHB_Track_q->getValue(i);}
    float GetRECHB_Track_chi2(int i){  return (float)fRECHB_Track_chi2->getValue(i);}
    unsigned int GetRECHB_Track_NDF(int i){  return (unsigned int)fRECHB_Track_NDF->getValue(i);}
    float GetRECHB_Track_px_nomm(int i){  return (float)fRECHB_Track_px_nomm->getValue(i);}
    float GetRECHB_Track_py_nomm(int i){  return (float)fRECHB_Track_py_nomm->getValue(i);}
    float GetRECHB_Track_pz_nomm(int i){  return (float)fRECHB_Track_pz_nomm->getValue(i);}
    float GetRECHB_Track_vx_nomm(int i){  return (float)fRECHB_Track_vx_nomm->getValue(i);}
    float GetRECHB_Track_vy_nomm(int i){  return (float)fRECHB_Track_vy_nomm->getValue(i);}
    float GetRECHB_Track_vz_nomm(int i){  return (float)fRECHB_Track_vz_nomm->getValue(i);}
    float GetRECHB_Track_chi2_nomm(int i){  return (float)fRECHB_Track_chi2_nomm->getValue(i);}
    unsigned int GetRECHB_Track_NDF_nomm(int i){  return (unsigned int)fRECHB_Track_NDF_nomm->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::TrackCross:Track Cross information for Particles bank ===================
    unsigned int GetRECHB_TrackCross_index(int i){  return (unsigned int)fRECHB_TrackCross_index->getValue(i);}
    unsigned int GetRECHB_TrackCross_pindex(int i){  return (unsigned int)fRECHB_TrackCross_pindex->getValue(i);}
    unsigned int GetRECHB_TrackCross_detector(int i){  return (unsigned int)fRECHB_TrackCross_detector->getValue(i);}
    unsigned int GetRECHB_TrackCross_sector(int i){  return (unsigned int)fRECHB_TrackCross_sector->getValue(i);}
    unsigned int GetRECHB_TrackCross_layer(int i){  return (unsigned int)fRECHB_TrackCross_layer->getValue(i);}
    float GetRECHB_TrackCross_c_x(int i){  return (float)fRECHB_TrackCross_c_x->getValue(i);}
    float GetRECHB_TrackCross_c_y(int i){  return (float)fRECHB_TrackCross_c_y->getValue(i);}
    float GetRECHB_TrackCross_c_z(int i){  return (float)fRECHB_TrackCross_c_z->getValue(i);}
    float GetRECHB_TrackCross_c_ux(int i){  return (float)fRECHB_TrackCross_c_ux->getValue(i);}
    float GetRECHB_TrackCross_c_uy(int i){  return (float)fRECHB_TrackCross_c_uy->getValue(i);}
    float GetRECHB_TrackCross_c_uz(int i){  return (float)fRECHB_TrackCross_c_uz->getValue(i);}
    unsigned int GetRECHB_TrackCross_status(int i){  return (unsigned int)fRECHB_TrackCross_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Event:Event Header Bank ===================
    unsigned int GetREC_Event_NRUN(int i){  return (unsigned int)fREC_Event_NRUN->getValue(i);}
    unsigned int GetREC_Event_NEVENT(int i){  return (unsigned int)fREC_Event_NEVENT->getValue(i);}
    float GetREC_Event_EVNTime(int i){  return (float)fREC_Event_EVNTime->getValue(i);}
    unsigned int GetREC_Event_TYPE(int i){  return (unsigned int)fREC_Event_TYPE->getValue(i);}
    unsigned int GetREC_Event_EvCAT(int i){  return (unsigned int)fREC_Event_EvCAT->getValue(i);}
    unsigned int GetREC_Event_NPGP(int i){  return (unsigned int)fREC_Event_NPGP->getValue(i);}
    unsigned long GetREC_Event_TRG(int i){  return (unsigned long)fREC_Event_TRG->getValue(i);}
    float GetREC_Event_BCG(int i){  return (float)fREC_Event_BCG->getValue(i);}
    double GetREC_Event_LT(int i){  return (double)fREC_Event_LT->getValue(i);}
    float GetREC_Event_STTime(int i){  return (float)fREC_Event_STTime->getValue(i);}
    float GetREC_Event_RFTime(int i){  return (float)fREC_Event_RFTime->getValue(i);}
    unsigned int GetREC_Event_Helic(int i){  return (unsigned int)fREC_Event_Helic->getValue(i);}
    float GetREC_Event_PTIME(int i){  return (float)fREC_Event_PTIME->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Particle:Reconstructed Particle Information ===================
    unsigned int GetREC_Particle_pid(int i){  return (unsigned int)fREC_Particle_pid->getValue(i);}
    float GetREC_Particle_px(int i){  return (float)fREC_Particle_px->getValue(i);}
    float GetREC_Particle_py(int i){  return (float)fREC_Particle_py->getValue(i);}
    float GetREC_Particle_pz(int i){  return (float)fREC_Particle_pz->getValue(i);}
    float GetREC_Particle_vx(int i){  return (float)fREC_Particle_vx->getValue(i);}
    float GetREC_Particle_vy(int i){  return (float)fREC_Particle_vy->getValue(i);}
    float GetREC_Particle_vz(int i){  return (float)fREC_Particle_vz->getValue(i);}
    unsigned int GetREC_Particle_charge(int i){  return (unsigned int)fREC_Particle_charge->getValue(i);}
    float GetREC_Particle_beta(int i){  return (float)fREC_Particle_beta->getValue(i);}
    float GetREC_Particle_chi2pid(int i){  return (float)fREC_Particle_chi2pid->getValue(i);}
    unsigned int GetREC_Particle_status(int i){  return (unsigned int)fREC_Particle_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Calorimeter:Calorimeter Responses for Particles bank ===================
    unsigned int GetREC_Calorimeter_index(int i){  return (unsigned int)fREC_Calorimeter_index->getValue(i);}
    unsigned int GetREC_Calorimeter_pindex(int i){  return (unsigned int)fREC_Calorimeter_pindex->getValue(i);}
    unsigned int GetREC_Calorimeter_detector(int i){  return (unsigned int)fREC_Calorimeter_detector->getValue(i);}
    unsigned int GetREC_Calorimeter_sector(int i){  return (unsigned int)fREC_Calorimeter_sector->getValue(i);}
    unsigned int GetREC_Calorimeter_layer(int i){  return (unsigned int)fREC_Calorimeter_layer->getValue(i);}
    float GetREC_Calorimeter_energy(int i){  return (float)fREC_Calorimeter_energy->getValue(i);}
    float GetREC_Calorimeter_time(int i){  return (float)fREC_Calorimeter_time->getValue(i);}
    float GetREC_Calorimeter_path(int i){  return (float)fREC_Calorimeter_path->getValue(i);}
    float GetREC_Calorimeter_chi2(int i){  return (float)fREC_Calorimeter_chi2->getValue(i);}
    float GetREC_Calorimeter_x(int i){  return (float)fREC_Calorimeter_x->getValue(i);}
    float GetREC_Calorimeter_y(int i){  return (float)fREC_Calorimeter_y->getValue(i);}
    float GetREC_Calorimeter_z(int i){  return (float)fREC_Calorimeter_z->getValue(i);}
    float GetREC_Calorimeter_hx(int i){  return (float)fREC_Calorimeter_hx->getValue(i);}
    float GetREC_Calorimeter_hy(int i){  return (float)fREC_Calorimeter_hy->getValue(i);}
    float GetREC_Calorimeter_hz(int i){  return (float)fREC_Calorimeter_hz->getValue(i);}
    float GetREC_Calorimeter_lu(int i){  return (float)fREC_Calorimeter_lu->getValue(i);}
    float GetREC_Calorimeter_lv(int i){  return (float)fREC_Calorimeter_lv->getValue(i);}
    float GetREC_Calorimeter_lw(int i){  return (float)fREC_Calorimeter_lw->getValue(i);}
    float GetREC_Calorimeter_du(int i){  return (float)fREC_Calorimeter_du->getValue(i);}
    float GetREC_Calorimeter_dv(int i){  return (float)fREC_Calorimeter_dv->getValue(i);}
    float GetREC_Calorimeter_dw(int i){  return (float)fREC_Calorimeter_dw->getValue(i);}
    float GetREC_Calorimeter_m2u(int i){  return (float)fREC_Calorimeter_m2u->getValue(i);}
    float GetREC_Calorimeter_m2v(int i){  return (float)fREC_Calorimeter_m2v->getValue(i);}
    float GetREC_Calorimeter_m2w(int i){  return (float)fREC_Calorimeter_m2w->getValue(i);}
    float GetREC_Calorimeter_m3u(int i){  return (float)fREC_Calorimeter_m3u->getValue(i);}
    float GetREC_Calorimeter_m3v(int i){  return (float)fREC_Calorimeter_m3v->getValue(i);}
    float GetREC_Calorimeter_m3w(int i){  return (float)fREC_Calorimeter_m3w->getValue(i);}
    unsigned int GetREC_Calorimeter_status(int i){  return (unsigned int)fREC_Calorimeter_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Cherenkov:Cherenkov Responses for Particles bank ===================
    unsigned int GetREC_Cherenkov_index(int i){  return (unsigned int)fREC_Cherenkov_index->getValue(i);}
    unsigned int GetREC_Cherenkov_pindex(int i){  return (unsigned int)fREC_Cherenkov_pindex->getValue(i);}
    unsigned int GetREC_Cherenkov_detector(int i){  return (unsigned int)fREC_Cherenkov_detector->getValue(i);}
    unsigned int GetREC_Cherenkov_sector(int i){  return (unsigned int)fREC_Cherenkov_sector->getValue(i);}
    float GetREC_Cherenkov_nphe(int i){  return (float)fREC_Cherenkov_nphe->getValue(i);}
    float GetREC_Cherenkov_time(int i){  return (float)fREC_Cherenkov_time->getValue(i);}
    float GetREC_Cherenkov_path(int i){  return (float)fREC_Cherenkov_path->getValue(i);}
    float GetREC_Cherenkov_chi2(int i){  return (float)fREC_Cherenkov_chi2->getValue(i);}
    float GetREC_Cherenkov_x(int i){  return (float)fREC_Cherenkov_x->getValue(i);}
    float GetREC_Cherenkov_y(int i){  return (float)fREC_Cherenkov_y->getValue(i);}
    float GetREC_Cherenkov_z(int i){  return (float)fREC_Cherenkov_z->getValue(i);}
    float GetREC_Cherenkov_theta(int i){  return (float)fREC_Cherenkov_theta->getValue(i);}
    float GetREC_Cherenkov_phi(int i){  return (float)fREC_Cherenkov_phi->getValue(i);}
    float GetREC_Cherenkov_dtheta(int i){  return (float)fREC_Cherenkov_dtheta->getValue(i);}
    float GetREC_Cherenkov_dphi(int i){  return (float)fREC_Cherenkov_dphi->getValue(i);}
    unsigned int GetREC_Cherenkov_status(int i){  return (unsigned int)fREC_Cherenkov_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::ForwardTagger:Forward Tagger information for Particles bank ===================
    unsigned int GetREC_ForwardTagger_index(int i){  return (unsigned int)fREC_ForwardTagger_index->getValue(i);}
    unsigned int GetREC_ForwardTagger_pindex(int i){  return (unsigned int)fREC_ForwardTagger_pindex->getValue(i);}
    unsigned int GetREC_ForwardTagger_detector(int i){  return (unsigned int)fREC_ForwardTagger_detector->getValue(i);}
    float GetREC_ForwardTagger_energy(int i){  return (float)fREC_ForwardTagger_energy->getValue(i);}
    float GetREC_ForwardTagger_time(int i){  return (float)fREC_ForwardTagger_time->getValue(i);}
    float GetREC_ForwardTagger_path(int i){  return (float)fREC_ForwardTagger_path->getValue(i);}
    float GetREC_ForwardTagger_chi2(int i){  return (float)fREC_ForwardTagger_chi2->getValue(i);}
    float GetREC_ForwardTagger_x(int i){  return (float)fREC_ForwardTagger_x->getValue(i);}
    float GetREC_ForwardTagger_y(int i){  return (float)fREC_ForwardTagger_y->getValue(i);}
    float GetREC_ForwardTagger_z(int i){  return (float)fREC_ForwardTagger_z->getValue(i);}
    float GetREC_ForwardTagger_dx(int i){  return (float)fREC_ForwardTagger_dx->getValue(i);}
    float GetREC_ForwardTagger_dy(int i){  return (float)fREC_ForwardTagger_dy->getValue(i);}
    float GetREC_ForwardTagger_radius(int i){  return (float)fREC_ForwardTagger_radius->getValue(i);}
    unsigned int GetREC_ForwardTagger_size(int i){  return (unsigned int)fREC_ForwardTagger_size->getValue(i);}
    unsigned int GetREC_ForwardTagger_status(int i){  return (unsigned int)fREC_ForwardTagger_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Scintillator:Scintillator Responses for Particles bank ===================
    unsigned int GetREC_Scintillator_index(int i){  return (unsigned int)fREC_Scintillator_index->getValue(i);}
    unsigned int GetREC_Scintillator_pindex(int i){  return (unsigned int)fREC_Scintillator_pindex->getValue(i);}
    unsigned int GetREC_Scintillator_detector(int i){  return (unsigned int)fREC_Scintillator_detector->getValue(i);}
    unsigned int GetREC_Scintillator_sector(int i){  return (unsigned int)fREC_Scintillator_sector->getValue(i);}
    unsigned int GetREC_Scintillator_layer(int i){  return (unsigned int)fREC_Scintillator_layer->getValue(i);}
    unsigned int GetREC_Scintillator_component(int i){  return (unsigned int)fREC_Scintillator_component->getValue(i);}
    float GetREC_Scintillator_energy(int i){  return (float)fREC_Scintillator_energy->getValue(i);}
    float GetREC_Scintillator_time(int i){  return (float)fREC_Scintillator_time->getValue(i);}
    float GetREC_Scintillator_path(int i){  return (float)fREC_Scintillator_path->getValue(i);}
    float GetREC_Scintillator_chi2(int i){  return (float)fREC_Scintillator_chi2->getValue(i);}
    float GetREC_Scintillator_x(int i){  return (float)fREC_Scintillator_x->getValue(i);}
    float GetREC_Scintillator_y(int i){  return (float)fREC_Scintillator_y->getValue(i);}
    float GetREC_Scintillator_z(int i){  return (float)fREC_Scintillator_z->getValue(i);}
    float GetREC_Scintillator_hx(int i){  return (float)fREC_Scintillator_hx->getValue(i);}
    float GetREC_Scintillator_hy(int i){  return (float)fREC_Scintillator_hy->getValue(i);}
    float GetREC_Scintillator_hz(int i){  return (float)fREC_Scintillator_hz->getValue(i);}
    unsigned int GetREC_Scintillator_status(int i){  return (unsigned int)fREC_Scintillator_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Track:Track information for Particles bank ===================
    unsigned int GetREC_Track_index(int i){  return (unsigned int)fREC_Track_index->getValue(i);}
    unsigned int GetREC_Track_pindex(int i){  return (unsigned int)fREC_Track_pindex->getValue(i);}
    unsigned int GetREC_Track_detector(int i){  return (unsigned int)fREC_Track_detector->getValue(i);}
    unsigned int GetREC_Track_status(int i){  return (unsigned int)fREC_Track_status->getValue(i);}
    unsigned int GetREC_Track_q(int i){  return (unsigned int)fREC_Track_q->getValue(i);}
    float GetREC_Track_chi2(int i){  return (float)fREC_Track_chi2->getValue(i);}
    unsigned int GetREC_Track_NDF(int i){  return (unsigned int)fREC_Track_NDF->getValue(i);}
    float GetREC_Track_px_nomm(int i){  return (float)fREC_Track_px_nomm->getValue(i);}
    float GetREC_Track_py_nomm(int i){  return (float)fREC_Track_py_nomm->getValue(i);}
    float GetREC_Track_pz_nomm(int i){  return (float)fREC_Track_pz_nomm->getValue(i);}
    float GetREC_Track_vx_nomm(int i){  return (float)fREC_Track_vx_nomm->getValue(i);}
    float GetREC_Track_vy_nomm(int i){  return (float)fREC_Track_vy_nomm->getValue(i);}
    float GetREC_Track_vz_nomm(int i){  return (float)fREC_Track_vz_nomm->getValue(i);}
    float GetREC_Track_chi2_nomm(int i){  return (float)fREC_Track_chi2_nomm->getValue(i);}
    unsigned int GetREC_Track_NDF_nomm(int i){  return (unsigned int)fREC_Track_NDF_nomm->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::TrackCross:Track Cross information for Particles bank ===================
    unsigned int GetREC_TrackCross_index(int i){  return (unsigned int)fREC_TrackCross_index->getValue(i);}
    unsigned int GetREC_TrackCross_pindex(int i){  return (unsigned int)fREC_TrackCross_pindex->getValue(i);}
    unsigned int GetREC_TrackCross_detector(int i){  return (unsigned int)fREC_TrackCross_detector->getValue(i);}
    unsigned int GetREC_TrackCross_sector(int i){  return (unsigned int)fREC_TrackCross_sector->getValue(i);}
    unsigned int GetREC_TrackCross_layer(int i){  return (unsigned int)fREC_TrackCross_layer->getValue(i);}
    float GetREC_TrackCross_c_x(int i){  return (float)fREC_TrackCross_c_x->getValue(i);}
    float GetREC_TrackCross_c_y(int i){  return (float)fREC_TrackCross_c_y->getValue(i);}
    float GetREC_TrackCross_c_z(int i){  return (float)fREC_TrackCross_c_z->getValue(i);}
    float GetREC_TrackCross_c_ux(int i){  return (float)fREC_TrackCross_c_ux->getValue(i);}
    float GetREC_TrackCross_c_uy(int i){  return (float)fREC_TrackCross_c_uy->getValue(i);}
    float GetREC_TrackCross_c_uz(int i){  return (float)fREC_TrackCross_c_uz->getValue(i);}
    unsigned int GetREC_TrackCross_status(int i){  return (unsigned int)fREC_TrackCross_status->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::CovMat:reconstructed track covariance matrix ===================
    unsigned int GetREC_CovMat_index(int i){  return (unsigned int)fREC_CovMat_index->getValue(i);}
    unsigned int GetREC_CovMat_pindex(int i){  return (unsigned int)fREC_CovMat_pindex->getValue(i);}
    float GetREC_CovMat_C11(int i){  return (float)fREC_CovMat_C11->getValue(i);}
    float GetREC_CovMat_C12(int i){  return (float)fREC_CovMat_C12->getValue(i);}
    float GetREC_CovMat_C13(int i){  return (float)fREC_CovMat_C13->getValue(i);}
    float GetREC_CovMat_C14(int i){  return (float)fREC_CovMat_C14->getValue(i);}
    float GetREC_CovMat_C15(int i){  return (float)fREC_CovMat_C15->getValue(i);}
    float GetREC_CovMat_C22(int i){  return (float)fREC_CovMat_C22->getValue(i);}
    float GetREC_CovMat_C23(int i){  return (float)fREC_CovMat_C23->getValue(i);}
    float GetREC_CovMat_C24(int i){  return (float)fREC_CovMat_C24->getValue(i);}
    float GetREC_CovMat_C25(int i){  return (float)fREC_CovMat_C25->getValue(i);}
    float GetREC_CovMat_C33(int i){  return (float)fREC_CovMat_C33->getValue(i);}
    float GetREC_CovMat_C34(int i){  return (float)fREC_CovMat_C34->getValue(i);}
    float GetREC_CovMat_C35(int i){  return (float)fREC_CovMat_C35->getValue(i);}
    float GetREC_CovMat_C44(int i){  return (float)fREC_CovMat_C44->getValue(i);}
    float GetREC_CovMat_C45(int i){  return (float)fREC_CovMat_C45->getValue(i);}
    float GetREC_CovMat_C55(int i){  return (float)fREC_CovMat_C55->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::VertDoca:Track Cross information for Particles bank ===================
    unsigned int GetREC_VertDoca_index1(int i){  return (unsigned int)fREC_VertDoca_index1->getValue(i);}
    unsigned int GetREC_VertDoca_index2(int i){  return (unsigned int)fREC_VertDoca_index2->getValue(i);}
    float GetREC_VertDoca_x(int i){  return (float)fREC_VertDoca_x->getValue(i);}
    float GetREC_VertDoca_y(int i){  return (float)fREC_VertDoca_y->getValue(i);}
    float GetREC_VertDoca_z(int i){  return (float)fREC_VertDoca_z->getValue(i);}
    float GetREC_VertDoca_x1(int i){  return (float)fREC_VertDoca_x1->getValue(i);}
    float GetREC_VertDoca_y1(int i){  return (float)fREC_VertDoca_y1->getValue(i);}
    float GetREC_VertDoca_z1(int i){  return (float)fREC_VertDoca_z1->getValue(i);}
    float GetREC_VertDoca_cx1(int i){  return (float)fREC_VertDoca_cx1->getValue(i);}
    float GetREC_VertDoca_cy1(int i){  return (float)fREC_VertDoca_cy1->getValue(i);}
    float GetREC_VertDoca_cz1(int i){  return (float)fREC_VertDoca_cz1->getValue(i);}
    float GetREC_VertDoca_x2(int i){  return (float)fREC_VertDoca_x2->getValue(i);}
    float GetREC_VertDoca_y2(int i){  return (float)fREC_VertDoca_y2->getValue(i);}
    float GetREC_VertDoca_z2(int i){  return (float)fREC_VertDoca_z2->getValue(i);}
    float GetREC_VertDoca_cx2(int i){  return (float)fREC_VertDoca_cx2->getValue(i);}
    float GetREC_VertDoca_cy2(int i){  return (float)fREC_VertDoca_cy2->getValue(i);}
    float GetREC_VertDoca_cz2(int i){  return (float)fREC_VertDoca_cz2->getValue(i);}
    float GetREC_VertDoca_r(int i){  return (float)fREC_VertDoca_r->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Traj:Trajectory information for Particles bank ===================
    unsigned int GetREC_Traj_pindex(int i){  return (unsigned int)fREC_Traj_pindex->getValue(i);}
    unsigned int GetREC_Traj_index(int i){  return (unsigned int)fREC_Traj_index->getValue(i);}
    unsigned int GetREC_Traj_detId(int i){  return (unsigned int)fREC_Traj_detId->getValue(i);}
    unsigned int GetREC_Traj_q(int i){  return (unsigned int)fREC_Traj_q->getValue(i);}
    float GetREC_Traj_x(int i){  return (float)fREC_Traj_x->getValue(i);}
    float GetREC_Traj_y(int i){  return (float)fREC_Traj_y->getValue(i);}
    float GetREC_Traj_z(int i){  return (float)fREC_Traj_z->getValue(i);}
    float GetREC_Traj_cx(int i){  return (float)fREC_Traj_cx->getValue(i);}
    float GetREC_Traj_cy(int i){  return (float)fREC_Traj_cy->getValue(i);}
    float GetREC_Traj_cz(int i){  return (float)fREC_Traj_cz->getValue(i);}
    float GetREC_Traj_pathlength(int i){  return (float)fREC_Traj_pathlength->getValue(i);}
    //--------------------------------------------------------------------------------------------------------------------------------------

    










    
    
    
    ClassDef(TClas12Event,0)
};

#endif
