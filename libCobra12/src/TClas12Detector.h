// Filename: TClas12Detector.h
// Description: Abstract Base class for all CLAS12 detectors
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTOR_H
#define TCLAS12DETECTOR_H

#include "TObject.h"
#include "../../external/hipo-io/libcpp/reader.h"
#include "TString.h"

class TClas12Run;

class TClas12Detector: public TObject
{
protected:   
    hipo::reader *fHipoReader;
    TClas12Run *fRun;
    
public:
    TClas12Detector(TClas12Run *run);
    virtual ~TClas12Detector();
    virtual void SetBranches()=0;
    static Int_t GetDetectorID(TString detectorName);
    
    ClassDef(TClas12Detector,0)    
};
#endif    
