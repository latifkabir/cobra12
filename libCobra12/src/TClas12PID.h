// Filename: TClas12PID.cpp
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat May 12 18:00:06 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12PID_H
#define TCLAS12PID_H

#include "TClas12Run.h"

class TClas12PID: public TObject
{

    TClas12Run *fClasRun;

public:
    TClas12PID();
    TClas12PID(TClas12Run *clas_run);

    Bool_t FindElctronPositron(Int_t pindex);


    ClassDef(TClas12PID,0)    
};


#endif    
