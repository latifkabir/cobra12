// Filename: json.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Mon Sep  3 01:57:27 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include <fstream>

#include "json.h"

using json = nlohmann::json;

int jsonEx()
{
    // read a JSON file
    std::ifstream i("resources/BankDef/DC.json");
    json j;
    i >> j;

    //Dump the content
    //std::cout << j.dump(4) << std::endl;

    //Access desired element
    std::cout << "Printing desired contents: " << std::endl;    
    //std::cout << j[0]<< std::endl;
    std::cout << j[0]["bank"]<< std::endl;
    std::cout << j[0]["info"]<< std::endl;
    std::cout << j[0]["items"][0]<< std::endl;
    std::cout << j[0]["items"][0]["name"]<< std::endl;
    
    return 0;
}


int PrintRunList()
{
    // read a JSON file
    std::ifstream i("resources/RunList/RunList.json");
    json j;
    i >> j;

    //Dump the content
    std::cout << j.dump(4) << std::endl;

    //Access desired element
    std::cout << "Printing desired contents: " << std::endl;    
    //std::cout << j[0]<< std::endl;

    for(int i = 0; i < j["data"].size(); ++i)
    {
	std::cout <<"run number: "<<j["data"][i]["run"]<< std::endl;
	std::cout <<"beam energy: "<<j["data"][i]["beam_energy"]<< std::endl;
	std::cout <<"beam current: "<<j["data"][i]["beam_current_request"]<< std::endl;
	std::cout <<"---------------------------------------------------------------------\n"<< std::endl;
    }
    return 0;
}
