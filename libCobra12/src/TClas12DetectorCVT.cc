// Filename: TClas12DetectorCVT.cc
// Description: CLAS12 CVT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorCVT.h"


ClassImp(TClas12DetectorCVT)
//-------------------------------------------------------------------------------------
TClas12DetectorCVT::TClas12DetectorCVT(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorCVT::~TClas12DetectorCVT()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorCVT::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorCVT::SetBranches()
{
//================== CVTRec::Tracks:reconstructed SVT tracks ===================
	fCVTRec_Tracks_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "ID"); 	 //ID
	fCVTRec_Tracks_fittingMethod = fHipoReader->getBranch<uint8_t>("CVTRec::Tracks", "fittingMethod"); 	 //fitting method (1 for global fit, 2 for Kalman Filter)
	fCVTRec_Tracks_c_x = fHipoReader->getBranch<float>("CVTRec::Tracks", "c_x"); 	 //x-coordinate of a helical trk point extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	fCVTRec_Tracks_c_y = fHipoReader->getBranch<float>("CVTRec::Tracks", "c_y"); 	 //y-coordinate of a helical trk point extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	fCVTRec_Tracks_c_z = fHipoReader->getBranch<float>("CVTRec::Tracks", "c_z"); 	 //z-coordinate of a helical trk point extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)b
	fCVTRec_Tracks_c_ux = fHipoReader->getBranch<float>("CVTRec::Tracks", "c_ux"); 	 //x-coordinate of a helical trk direction extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	fCVTRec_Tracks_c_uy = fHipoReader->getBranch<float>("CVTRec::Tracks", "c_uy"); 	 //y-coordinate of a helical trk direction extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	fCVTRec_Tracks_c_uz = fHipoReader->getBranch<float>("CVTRec::Tracks", "c_uz"); 	 //z-coordinate of a helical trk direction extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	fCVTRec_Tracks_pathlength = fHipoReader->getBranch<float>("CVTRec::Tracks", "pathlength"); 	 //total pathlength from the origin to the reference point (in cm)
	fCVTRec_Tracks_q = fHipoReader->getBranch<uint8_t>("CVTRec::Tracks", "q"); 	 //charge
	fCVTRec_Tracks_p = fHipoReader->getBranch<float>("CVTRec::Tracks", "p"); 	 //total momentum
	fCVTRec_Tracks_pt = fHipoReader->getBranch<float>("CVTRec::Tracks", "pt"); 	 //transverse momentum
	fCVTRec_Tracks_phi0 = fHipoReader->getBranch<float>("CVTRec::Tracks", "phi0"); 	 //helical track fit parameter: phi at DOCA
	fCVTRec_Tracks_tandip = fHipoReader->getBranch<float>("CVTRec::Tracks", "tandip"); 	 //helical track fit parameter: dip angle
	fCVTRec_Tracks_z0 = fHipoReader->getBranch<float>("CVTRec::Tracks", "z0"); 	 //helical track fit parameter: value of z at the DOCA
	fCVTRec_Tracks_d0 = fHipoReader->getBranch<float>("CVTRec::Tracks", "d0"); 	 //helical track fit parameter: Distance of Closest Approach
	fCVTRec_Tracks_cov_d02 = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_d02"); 	 //helical track fit covariance matrix element : delta_d0^2
	fCVTRec_Tracks_cov_d0phi0 = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_d0phi0"); 	 //helical track fit covariance matrix element : delta_d0.delta_phi0
	fCVTRec_Tracks_cov_d0rho = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_d0rho"); 	 //helical track fit covariance matrix element : delta_d0.delta_rho
	fCVTRec_Tracks_cov_phi02 = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_phi02"); 	 //helical track fit covariance matrix element : delta_phi0^2
	fCVTRec_Tracks_cov_phi0rho = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_phi0rho"); 	 //helical track fit covariance matrix element : delta_phi0.delta_rho
	fCVTRec_Tracks_cov_rho2 = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_rho2"); 	 //helical track fit covariance matrix element : delta_rho.delta_rho
	fCVTRec_Tracks_cov_z02 = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_z02"); 	 //helical track fit covariance matrix element : delta_z0^2
	fCVTRec_Tracks_cov_tandip2 = fHipoReader->getBranch<float>("CVTRec::Tracks", "cov_tandip2"); 	 //helical track fit covariance matrix element : delta_tandip^2
	fCVTRec_Tracks_circlefit_chi2_per_ndf = fHipoReader->getBranch<float>("CVTRec::Tracks", "circlefit_chi2_per_ndf"); 	 //Circle Fit chi^2/ndf 
	fCVTRec_Tracks_linefit_chi2_per_ndf = fHipoReader->getBranch<float>("CVTRec::Tracks", "linefit_chi2_per_ndf"); 	 // Line Fit chi^2/ndf 
	fCVTRec_Tracks_chi2 = fHipoReader->getBranch<float>("CVTRec::Tracks", "chi2"); 	 //Fit chi^2 
	fCVTRec_Tracks_ndf = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "ndf"); 	 //Fit ndf 
	fCVTRec_Tracks_Cross1_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross1_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross2_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross2_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross3_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross3_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross4_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross4_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross5_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross5_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross6_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross6_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross7_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross7_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross8_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross8_ID"); 	 //ID of cross in the track
	fCVTRec_Tracks_Cross9_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Tracks", "Cross9_ID"); 	 //ID of cross in the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CVTRec::Cosmics:reconstructed SVT straight tracks ===================
	fCVTRec_Cosmics_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "ID"); 	 //ID
	fCVTRec_Cosmics_trkline_yx_slope = fHipoReader->getBranch<float>("CVTRec::Cosmics", "trkline_yx_slope"); 	 //trkline_yx_slope
	fCVTRec_Cosmics_trkline_yx_interc = fHipoReader->getBranch<float>("CVTRec::Cosmics", "trkline_yx_interc"); 	 //trkline_yx_interc
	fCVTRec_Cosmics_trkline_yz_slope = fHipoReader->getBranch<float>("CVTRec::Cosmics", "trkline_yz_slope"); 	 //trkline_yz_slope
	fCVTRec_Cosmics_trkline_yz_interc = fHipoReader->getBranch<float>("CVTRec::Cosmics", "trkline_yz_interc"); 	 //trkline_yz_interc
	fCVTRec_Cosmics_theta = fHipoReader->getBranch<float>("CVTRec::Cosmics", "theta"); 	 //theta (deg)
	fCVTRec_Cosmics_phi = fHipoReader->getBranch<float>("CVTRec::Cosmics", "phi"); 	 //phi (deg)
	fCVTRec_Cosmics_chi2 = fHipoReader->getBranch<float>("CVTRec::Cosmics", "chi2"); 	 //Fit chi^2 
	fCVTRec_Cosmics_ndf = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "ndf"); 	 // number of degrees of freedom used in the fit
	fCVTRec_Cosmics_Cross1_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross1_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross2_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross2_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross3_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross3_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross4_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross4_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross5_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross5_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross6_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross6_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross7_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross7_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross8_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross8_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross9_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross9_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross10_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross10_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross11_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross11_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross12_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross12_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross13_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross13_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross14_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross14_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross15_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross15_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross16_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross16_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross17_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross17_ID"); 	 //ID of cross in the track
	fCVTRec_Cosmics_Cross18_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Cosmics", "Cross18_ID"); 	 //ID of cross in the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CVTRec::Trajectory:reconstructed SVT straight tracks trajectory ===================
	fCVTRec_Trajectory_ID = fHipoReader->getBranch<uint16_t>("CVTRec::Trajectory", "ID"); 	 //ID
	fCVTRec_Trajectory_LayerTrackIntersPlane = fHipoReader->getBranch<uint8_t>("CVTRec::Trajectory", "LayerTrackIntersPlane"); 	 //Layer of intersection of Track 
	fCVTRec_Trajectory_SectorTrackIntersPlane = fHipoReader->getBranch<uint8_t>("CVTRec::Trajectory", "SectorTrackIntersPlane"); 	 //Sector of intersection of Track 
	fCVTRec_Trajectory_XtrackIntersPlane = fHipoReader->getBranch<float>("CVTRec::Trajectory", "XtrackIntersPlane"); 	 //x of intersection of Track 
	fCVTRec_Trajectory_YtrackIntersPlane = fHipoReader->getBranch<float>("CVTRec::Trajectory", "YtrackIntersPlane"); 	 //y of intersection of Track 
	fCVTRec_Trajectory_ZtrackIntersPlane = fHipoReader->getBranch<float>("CVTRec::Trajectory", "ZtrackIntersPlane"); 	 //z of intersection of Track 
	fCVTRec_Trajectory_PhiTrackIntersPlane = fHipoReader->getBranch<float>("CVTRec::Trajectory", "PhiTrackIntersPlane"); 	 //Local phi of intersection of Track
	fCVTRec_Trajectory_ThetaTrackIntersPlane = fHipoReader->getBranch<float>("CVTRec::Trajectory", "ThetaTrackIntersPlane"); 	 //Local theta of intersection of Track
	fCVTRec_Trajectory_trkToMPlnAngl = fHipoReader->getBranch<float>("CVTRec::Trajectory", "trkToMPlnAngl"); 	 //Local angle of intersection of Track with the module surface
	fCVTRec_Trajectory_CalcCentroidStrip = fHipoReader->getBranch<float>("CVTRec::Trajectory", "CalcCentroidStrip"); 	 //Estimated Centroid
//--------------------------------------------------------------------------------------------------------------------------------------

}
