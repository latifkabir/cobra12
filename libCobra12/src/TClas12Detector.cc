// Filename: TClas12Detector.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12Detector.h"
#include "TApplication.h"
#include "TClas12Run.h"

ClassImp(TClas12Detector)
//-------------------------------------------------------------------------------------
TClas12Detector::TClas12Detector(TClas12Run *run)
{
    this->fRun = run;
    this->fHipoReader = fRun->fHipoReader;       
}
//-------------------------------------------------------------------------------------
TClas12Detector::~TClas12Detector()
{
    
}
//-------------------------------------------------------------------------------------
Int_t TClas12Detector::GetDetectorID(TString detectorName)
{
    // ----------- Following assignments are based on CoatJava convention. see detector class coatjava source coad for details. -------------------
       	
    if(detectorName == "BMT")
	return 1;    
    else if(detectorName == "BST")
	return 2;
    else if(detectorName ==  "CND")
	return 3;
    else if(detectorName ==  "CTOF")
	return 4;
    else if(detectorName == "CVT")
	return 5;
    else if(detectorName ==  "DC")
	return 6;
    else if(detectorName ==  "ECAL")
	return 7;
    else if(detectorName ==  "FMT")
	return 8;
    else if(detectorName ==  "FT")
	return 9;
    else if(detectorName ==  "FTCAL")
	return 10;
    else if(detectorName ==  "FTHODO")
	return 11;
    else if(detectorName ==  "FTOF")
	return 12;
    else if(detectorName ==  "FTTRK")
	return 13;
    else if(detectorName ==  "HTCC")
	return 15;
    else if(detectorName ==  "LTCC")
	return 16;
    else if(detectorName ==  "RF")
	return 17;
    else if(detectorName ==  "RICH")
	return 18;
    else if(detectorName ==  "RTPC")
	return 19;
    else if(detectorName ==  "HEL")
	return 20;
    else if(detectorName ==  "ECIN")               // Obsolere ??
	return 110;
    else if(detectorName ==  "ECOUT")              // Obsolete or duplication ?? 
	return 111;
    else if(detectorName ==  "ECTOT")
	return 112;
    else if(detectorName ==  "LAC")
	return 113;
    else if(detectorName ==  "SC")
	return 114;
    else if(detectorName ==  "CC")
	return 115;
    else if(detectorName ==  "SVT")
	return 220;

	// FIXME add layer conventions here, e.g.:
	// (or maybe they are defined somewhere else?)
    else if(detectorName ==  "FTOF1A")
	return 1;
    else if(detectorName ==  "FTOF1B")
	return 2;
    else if(detectorName ==  "FTOF2")
	return 3;
    else if(detectorName ==  "PCAL")
	return 1;
    else if(detectorName ==  "ECINNER")
	return 4;
    else if(detectorName ==  "ECOUTER")
	return 7;
    else
    {
	cout << "Invalid Detector Name" <<endl;
	gApplication->Terminate();
	return -1000;
    }
}
