// Filename: TClas12DC.h
// Description: CLAS12 DC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DC_H
#define TCLAS12DC_H

#include <unordered_map>
#include "TClas12DetectorDC.h"

using namespace std;

class TClas12DC: public TClas12DetectorDC
{
    Int_t fHBHitsSize;
    Int_t fHBClustersSize;
    Int_t fHBSegmentsSize;
    Int_t fHBSegmentTrajectorySize;
    Int_t fHBCrossesSize;
    Int_t fHBTracksSize;

    Int_t fTBHitsSize;
    Int_t fTBClustersSize;
    Int_t fTBSegmentsSize;
    Int_t fTBSegmentTrajectorySize;
    Int_t fTBCrossesSize;
    Int_t fTBTracksSize;
    Int_t fTBCovMatSize;
    Int_t fTBTrajectorySize;

    //------- Mapping field ------------
    unordered_map <Int_t, Int_t> *fHBHitsMap;         // Id to row number map
    unordered_map <Int_t, Int_t> *fHBClustersMap;     // Id to row number map
    unordered_map <Int_t, Int_t> *fHBSegmentsMap;     // Id to row number map 
    unordered_map <Int_t, Int_t> *fHBSegmentTrajectoryMap;     // Id to row number map 
    unordered_map <Int_t, Int_t> *fHBCrossesMap;      // Id to row number map
    unordered_map <Int_t, Int_t> *fHBTracksMap;       // Id to row number map
  
    unordered_map <Int_t, Int_t> *fTBHitsMap;         // Id to row number map
    unordered_map <Int_t, Int_t> *fTBClustersMap;     // Id to row number map
    unordered_map <Int_t, Int_t> *fTBSegmentsMap;     // Segment Id to row number map 
    unordered_map <Int_t, Int_t> *fTBSegmentTrajectoryMap;     // Segment Id to row number map 
    unordered_map <Int_t, Int_t> *fTBCrossesMap;      // Id to row number map
    unordered_map <Int_t, Int_t> *fTBTracksMap;       // Id to row number map
    unordered_map <Int_t, Int_t> *fTBCovMatMap;       // Id to row number map
    unordered_map <Int_t, Int_t> *fTBTrajectoryMap;    // Time Id to row number map
    
public:
    TClas12DC(TClas12Run *run);
    ~TClas12DC();
    void Init();
    void Update();
    void GenerateMap();
    
    //--------------- Getter Functions -----------------------------
    Bool_t HasHBHits(){ return (fHBHitsSize > 0);}
    Bool_t HasHBClusters(){ return (fHBClustersSize > 0);}
    Bool_t HasHBSegments(){ return (fHBSegmentsSize > 0);}
    Bool_t HasHBSegmentTrajectory(){ return (fHBSegmentTrajectorySize > 0);}
    Bool_t HasHBCrosses(){ return (fHBCrossesSize > 0);}
    Bool_t HasHBTracks(){ return (fHBTracksSize > 0);}
    Bool_t HasTBHits(){ return (fTBHitsSize > 0);}
    Bool_t HasTBClusters(){ return (fTBClustersSize > 0);}
    Bool_t HasTBSegments(){ return (fTBSegmentsSize > 0);}
    Bool_t HasTBSegmentTrajectory(){ return (fTBSegmentTrajectorySize > 0);}
    Bool_t HasTBCrosses(){ return (fTBCrossesSize > 0);}
    Bool_t HasTBTracks(){ return (fTBTracksSize > 0);}
    Bool_t HasTBCovMat(){ return (fTBCovMatSize > 0);}
    Bool_t HasTBTrajectory(){ return (fTBTrajectorySize > 0);}
    
    //--------------- Getter Functions -----------------------------
    Int_t GetSizeHBHits(){ return fHBHitsSize;}
    Int_t GetSizeHBClusters(){ return fHBClustersSize;}
    Int_t GetSizeHBSegments(){ return fHBSegmentsSize;}
    Int_t GetSizeHBSegmentTrajectory(){ return fHBSegmentTrajectorySize;}
    Int_t GetSizeHBCrosses(){ return fHBCrossesSize;}
    Int_t GetSizeHBTracks(){ return fHBTracksSize;}
    Int_t GetSizeTBHits(){ return fTBHitsSize;}
    Int_t GetSizeTBClusters(){ return fTBClustersSize;}
    Int_t GetSizeTBSegments(){ return fTBSegmentsSize;}
    Int_t GetSizeTBSegmentTrajectory(){ return fTBSegmentTrajectorySize;}
    Int_t GetSizeTBCrosses(){ return fTBCrossesSize;}
    Int_t GetSizeTBTracks(){ return fTBTracksSize;}
    Int_t GetSizeTBCovMat(){ return fTBCovMatSize;}
    Int_t GetSizeTBTrajectory(){ return fTBTrajectorySize;}

    //---------------- Get row/index number associated with desired id ---------    
    Int_t GetIndexHBHits(Int_t id);
    Int_t GetIndexHBClusters(Int_t id);
    Int_t GetIndexHBSegments(Int_t id);
    Int_t GetIndexHBSegmentTrajectory(Int_t id);
    Int_t GetIndexHBCrosses(Int_t id);
    Int_t GetIndexHBTracks(Int_t id);
    Int_t GetIndexTBHits(Int_t id);
    Int_t GetIndexTBClusters(Int_t id);
    Int_t GetIndexTBSegments(Int_t id);
    Int_t GetIndexTBSegmentTrajectory(Int_t id);
    Int_t GetIndexTBCrosses(Int_t id);
    Int_t GetIndexTBTracks(Int_t id);
    Int_t GetIndexTBCovMat(Int_t id);
    Int_t GetIndexTBTrajectory(Int_t id);

        
    ClassDef(TClas12DC,0)    
};
#endif    
