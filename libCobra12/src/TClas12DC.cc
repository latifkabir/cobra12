// Filename: TClas12DC.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12DC.h"
#include "TClas12.h"

ClassImp(TClas12DC)
//-------------------------------------------------------------------------------------
TClas12DC::TClas12DC(TClas12Run *run):TClas12DetectorDC(run)
{
    Init();
}
//-------------------------------------------------------------------------------------
TClas12DC::~TClas12DC()
{
    delete fHBHitsMap;
    delete fHBClustersMap;
    delete fHBSegmentsMap;
    delete fHBSegmentTrajectoryMap;
    delete fHBCrossesMap;
    delete fHBTracksMap;
    delete fTBHitsMap;
    delete fTBClustersMap;
    delete fTBSegmentsMap;
    delete fTBSegmentTrajectoryMap;
    delete fTBCrossesMap;
    delete fTBTracksMap;
    delete fTBCovMatMap;
    delete fTBTrajectoryMap;
}
//-------------------------------------------------------------------------------------
void TClas12DC::Init()
{
    fHBHitsSize = 0;
    fHBClustersSize = 0;
    fHBSegmentsSize = 0;
    fHBSegmentTrajectorySize = 0;
    fHBCrossesSize = 0;
    fHBTracksSize = 0;

    fTBHitsSize = 0;
    fTBClustersSize = 0;
    fTBSegmentsSize = 0;
    fTBSegmentTrajectorySize = 0;
    fTBCrossesSize = 0;
    fTBTracksSize = 0;
    fTBCovMatSize = 0;
    fTBTrajectorySize = 0;

    fHBHitsMap = new unordered_map <Int_t, Int_t>();
    fHBClustersMap = new unordered_map <Int_t, Int_t>();
    fHBSegmentsMap = new unordered_map <Int_t, Int_t>(); 
    fHBSegmentTrajectoryMap = new unordered_map <Int_t, Int_t>(); 
    fHBCrossesMap = new unordered_map <Int_t, Int_t>();
    fHBTracksMap = new unordered_map <Int_t, Int_t>();
  
    fTBHitsMap = new unordered_map <Int_t, Int_t>();
    fTBClustersMap = new unordered_map <Int_t, Int_t>();
    fTBSegmentsMap =  new unordered_map <Int_t, Int_t>(); 
    fTBSegmentTrajectoryMap = new unordered_map <Int_t, Int_t>(); 
    fTBCrossesMap = new unordered_map <Int_t, Int_t>();
    fTBTracksMap = new unordered_map <Int_t, Int_t>();
    fTBCovMatMap = new unordered_map <Int_t, Int_t>();
    fTBTrajectoryMap = new unordered_map <Int_t, Int_t>();     
}
//-------------------------------------------------------------------------------------
void TClas12DC::Update()
{
    fHBHitsSize = fHitBasedTrkg_HBHits_id->getLength();
    fHBClustersSize = fHitBasedTrkg_HBClusters_id->getLength();
    fHBSegmentsSize = fHitBasedTrkg_HBSegments_id->getLength();
    fHBSegmentTrajectorySize = fHitBasedTrkg_HBSegmentTrajectory_sector->getLength();
    fHBCrossesSize = fHitBasedTrkg_HBCrosses_id->getLength();
    fHBTracksSize = fHitBasedTrkg_HBTracks_id->getLength();
    
    fTBHitsSize = fTimeBasedTrkg_TBHits_id->getLength();
    fTBClustersSize = fTimeBasedTrkg_TBClusters_id->getLength();
    fTBSegmentsSize = fTimeBasedTrkg_TBSegments_id->getLength();
    fTBSegmentTrajectorySize = fTimeBasedTrkg_TBSegmentTrajectory_sector->getLength();
    fTBCrossesSize = fTimeBasedTrkg_TBCrosses_id->getLength();
    fTBTracksSize = fTimeBasedTrkg_TBTracks_id->getLength();
    fTBCovMatSize = fTimeBasedTrkg_TBCovMat_id->getLength();
    fTBTrajectorySize = fTimeBasedTrkg_Trajectory_tid->getLength();

    if(TClas12::fTurnOnDCMap)
	GenerateMap();	
}


//-------------------------------------------------------------------------------------
void TClas12DC::GenerateMap()
{
    //HBT
    fHBHitsMap->clear();
    fHBClustersMap->clear();
    fHBSegmentsMap->clear();
    fHBSegmentTrajectoryMap->clear();
    fHBCrossesMap->clear();
    fHBTracksMap->clear();
    //TBT
    fTBHitsMap->clear();
    fTBClustersMap->clear();
    fTBSegmentsMap->clear();
    fTBSegmentTrajectoryMap->clear();
    fTBCrossesMap->clear();
    fTBTracksMap->clear();
    fTBCovMatMap->clear();
    fTBTrajectoryMap->clear();
    //HBT
    for(Int_t i = 0; i < fHBHitsSize; ++i)   
	fHBHitsMap->insert({GetHitBasedTrkg_HBHits_id(i), i});
    for(Int_t i = 0; i < fHBClustersSize; ++i)   
    	fHBClustersMap->insert({GetHitBasedTrkg_HBClusters_id(i), i});
    for(Int_t i = 0; i < fHBSegmentsSize; ++i)   
    	fHBSegmentsMap->insert({GetHitBasedTrkg_HBSegments_id(i), i});
    for(Int_t i = 0; i < fHBSegmentTrajectorySize; ++i)   
    	fHBSegmentTrajectoryMap->insert({GetHitBasedTrkg_HBSegmentTrajectory_segmentID(i) , i});
    for(Int_t i = 0; i < fHBCrossesSize; ++i)   
    	fHBCrossesMap->insert({GetHitBasedTrkg_HBCrosses_id(i), i});
    for(Int_t i = 0; i < fHBTracksSize; ++i)   
    	fHBTracksMap->insert({GetHitBasedTrkg_HBTracks_id(i), i});
    //TBT	
    for(Int_t i = 0; i < fTBHitsSize; ++i)   
    	fTBHitsMap->insert({GetTimeBasedTrkg_TBHits_id(i), i});
    for(Int_t i = 0; i < fTBClustersSize; ++i)   
    	fTBClustersMap->insert({GetTimeBasedTrkg_TBClusters_id(i), i});
    for(Int_t i = 0; i < fTBSegmentsSize; ++i)   
    	fTBSegmentsMap->insert({GetTimeBasedTrkg_TBSegments_id(i), i});
    for(Int_t i = 0; i < fTBSegmentTrajectorySize; ++i)   
    	fTBSegmentTrajectoryMap->insert({GetTimeBasedTrkg_TBSegmentTrajectory_segmentID(i) , i});
    for(Int_t i = 0; i < fTBCrossesSize; ++i)   
    	fTBCrossesMap->insert({GetTimeBasedTrkg_TBCrosses_id(i), i});
    for(Int_t i = 0; i < fTBTracksSize; ++i)   
    	fTBTracksMap->insert({GetTimeBasedTrkg_TBTracks_id(i), i});
    for(Int_t i = 0; i < fTBCovMatSize; ++i)   
    	fTBCovMatMap->insert({GetTimeBasedTrkg_TBCovMat_id(i), i});
    for(Int_t i = 0; i < fTBTrajectorySize; ++i)   
    	fTBTrajectoryMap->insert({GetTimeBasedTrkg_Trajectory_tid(i), i});    
}

//---------------- Get row/index number associated with desired id ---------    
Int_t TClas12DC::GetIndexHBHits(Int_t id)
{
    auto itr = fHBHitsMap->find(id);
    if(itr != fHBHitsMap->end())
	return itr->second;
    else
	return -1;
}
Int_t TClas12DC::GetIndexHBClusters(Int_t id)
{
    auto itr = fHBClustersMap->find(id);
    if(itr != fHBClustersMap->end())
     	return itr->second;
    else
	return -1;
}
Int_t TClas12DC::GetIndexHBSegments(Int_t id)
{
    auto itr = fHBSegmentsMap->find(id);
    if(itr != fHBSegmentsMap->end())
	return itr->second;
    else
	return -1;
}
Int_t TClas12DC::GetIndexHBSegmentTrajectory(Int_t id)
{
    auto itr = fHBSegmentTrajectoryMap->find(id);
    if(itr != fHBSegmentTrajectoryMap->end())
     	return itr->second;
    else
	return -1;
}
Int_t TClas12DC::GetIndexHBCrosses(Int_t id)
{
    auto itr = fHBCrossesMap->find(id);
    if(itr != fHBCrossesMap->end())
     	return itr->second;
    else
	return -1;
}
Int_t TClas12DC::GetIndexHBTracks(Int_t id)
{
    auto itr = fHBTracksMap->find(id);
    if(itr != fHBTracksMap->end())
     	return itr->second;
    else
	return -1;    
}

Int_t TClas12DC::GetIndexTBHits(Int_t id)
{
    auto itr = fTBHitsMap->find(id);
    if(itr != fTBHitsMap->end())
     	return itr->second;
    else
	return -1;    
}
Int_t TClas12DC::GetIndexTBClusters(Int_t id)
{
    auto itr = fTBClustersMap->find(id);
    if(itr != fTBClustersMap->end())
     	return itr->second;
    else
	return -1;    
}
Int_t TClas12DC::GetIndexTBSegments(Int_t id)
{
    auto itr = fTBSegmentsMap->find(id);
    if(itr != fTBSegmentsMap->end())
     	return itr->second;
    else
	return -1;    
}
Int_t TClas12DC::GetIndexTBSegmentTrajectory(Int_t id)
{
    auto itr = fTBSegmentTrajectoryMap->find(id);
    if(itr != fTBSegmentTrajectoryMap->end())
     	return itr->second;
    else
	return -1;    
}
Int_t TClas12DC::GetIndexTBCrosses(Int_t id)
{
    auto itr = fTBCrossesMap->find(id);
    if(itr != fTBCrossesMap->end())
     	return itr->second;
    else
	return -1;    
}
Int_t TClas12DC::GetIndexTBTracks(Int_t id)
{
    auto itr = fTBTracksMap->find(id);
    if(itr != fTBTracksMap->end())
     	return itr->second;
    else
	return -1;    
}
Int_t TClas12DC::GetIndexTBCovMat(Int_t id)
{
    auto itr = fTBCovMatMap->find(id);
    if(itr != fTBCovMatMap->end())
     	return itr->second;
    else
	return -1;    
}
Int_t TClas12DC::GetIndexTBTrajectory(Int_t id)
{
    auto itr = fTBTrajectoryMap->find(id);
    if(itr != fTBTrajectoryMap->end())
     	return itr->second;
    else
	return -1;    
}
