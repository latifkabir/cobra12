// Filename: TClas12Event.cc
// Description: Base class for Clas12Run
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 11:56:52 2018 (-0400)
// URL: jlab.org/~latif

#include <sstream>
#include "TSystem.h"
#include "TClas12Event.h"
#include "TClas12Detector.h"
#include "TClas12DC.h"
#include "TClas12.h"

ClassImp(TClas12Event)
//----------------------------------------------------------------
TClas12Event::TClas12Event()
{
    
}

TClas12Event::~TClas12Event()
{
    delete fHipoReader;    
}
//----------------------------------------------------------------
TClas12Event::TClas12Event(Int_t runNumber, Int_t fileNo)
{
    fRunNumber = runNumber;
    fFileNo = fileNo;
    Init();
}
//----------------------------------------------------------------
TClas12Event::TClas12Event(TString fileName)
{
    fRunNumber = -1;
    fFileNo = -1;
    fFileName = fileName;
    Init();
}
//---------------------------------------------------------------------
void TClas12Event::Init()
{   
    fHipoReader = new hipo::reader();

    if(fRunNumber != -1 && fFileNo != -1)
	fFileName = TClas12::Config->GetDataPath() + TString::Format(TClas12::Config->GetFilePattern().c_str(), fRunNumber, fFileNo);
    	    
    if(!gSystem->AccessPathName(fFileName))
	fFileExist = kTRUE;
    else
    {
	fFileExist = kFALSE;
	cout << "File does not exist" <<endl;
	cout << "Full path:"<<fFileName <<endl;
    }
	
    if(fFileExist)
	fHipoReader->open(fFileName);

    SetBranches();    
}
//----------------------------------------------------------------------------
void TClas12Event::Print()
{
    if(!fFileExist)
    {
	cout << "File does not exist" <<endl;
	cout << "Full path:"<<fFileName <<endl;
	return;
    }
    fHipoReader->showInfo();
    cout<<"-----> file contains "<< fHipoReader->getRecordCount()<<" records\n"<<endl;    
}
//--------------------------------------------------------------------------------
void TClas12Event::PrintDictionary()
{
    if(!fFileExist)
    {
	cout << "File does not exist" <<endl;
	cout << "Full path:"<<fFileName <<endl;
	return;
    }

    fHipoReader->showInfo();
    std::vector<string> dict = fHipoReader->getDictionary();
    for(int i = 0; i < dict.size(); i++)
	printf(" Schema # %4d : %s\n",i, dict[i].c_str());    
}
//--------------------------------------------------------------------------------
Bool_t TClas12Event::FileExist()
{
    return fFileExist;
}
//---------------------------------------------------------------------------------
void TClas12Event::SetBranches()
{

    //================== RECHB::Event:Event Header Bank ===================
    fRECHB_Event_NRUN = fHipoReader->getBranch<uint32_t>("RECHB::Event", "NRUN"); 	 //Run Number
    fRECHB_Event_NEVENT = fHipoReader->getBranch<uint32_t>("RECHB::Event", "NEVENT"); 	 //Event Number
    fRECHB_Event_EVNTime = fHipoReader->getBranch<float>("RECHB::Event", "EVNTime"); 	 //Event Time
    fRECHB_Event_TYPE = fHipoReader->getBranch<uint8_t>("RECHB::Event", "TYPE"); 	 //Event Type (Data or MC)
    fRECHB_Event_EvCAT = fHipoReader->getBranch<uint16_t>("RECHB::Event", "EvCAT"); 	 //Event Category, if >0:  e-, e-p, e-pi+.....
    fRECHB_Event_NPGP = fHipoReader->getBranch<uint16_t>("RECHB::Event", "NPGP"); 	 //Number of Final (Timed-based) Reconstructed Particles*100 + Number of Geometrically Reconstructed Particles
    fRECHB_Event_TRG = fHipoReader->getBranch<uint64_t>("RECHB::Event", "TRG"); 	 //Trigger Type (CLAS12_e-, FT_CLAS12_h, CLAS12_H,...) + Prescale Factor for that Trigger
    fRECHB_Event_BCG = fHipoReader->getBranch<float>("RECHB::Event", "BCG"); 	 //Beam charge, gated (Coulomb)
    fRECHB_Event_LT = fHipoReader->getBranch<double>("RECHB::Event", "LT"); 	 //Lifetime
    fRECHB_Event_STTime = fHipoReader->getBranch<float>("RECHB::Event", "STTime"); 	 //Event Start Time (ns)
    fRECHB_Event_RFTime = fHipoReader->getBranch<float>("RECHB::Event", "RFTime"); 	 //RF Time (ns)
    fRECHB_Event_Helic = fHipoReader->getBranch<uint8_t>("RECHB::Event", "Helic"); 	 //Helicity of Event
    fRECHB_Event_PTIME = fHipoReader->getBranch<float>("RECHB::Event", "PTIME"); 	 //Event Processing Time (UNIX Time = seconds)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Particle:Reconstructed Particle Information ===================
    fRECHB_Particle_pid = fHipoReader->getBranch<uint32_t>("RECHB::Particle", "pid"); 	 //particle id in LUND conventions
    fRECHB_Particle_px = fHipoReader->getBranch<float>("RECHB::Particle", "px"); 	 //x component of the momentum
    fRECHB_Particle_py = fHipoReader->getBranch<float>("RECHB::Particle", "py"); 	 //y component of the momentum
    fRECHB_Particle_pz = fHipoReader->getBranch<float>("RECHB::Particle", "pz"); 	 //z component of the momentum
    fRECHB_Particle_vx = fHipoReader->getBranch<float>("RECHB::Particle", "vx"); 	 //x component of the vertex
    fRECHB_Particle_vy = fHipoReader->getBranch<float>("RECHB::Particle", "vy"); 	 //y component of the vertex
    fRECHB_Particle_vz = fHipoReader->getBranch<float>("RECHB::Particle", "vz"); 	 //z component of the vertex
    fRECHB_Particle_charge = fHipoReader->getBranch<uint8_t>("RECHB::Particle", "charge"); 	 //particle charge
    fRECHB_Particle_beta = fHipoReader->getBranch<float>("RECHB::Particle", "beta"); 	 //particle beta measured by TOF
    fRECHB_Particle_chi2pid = fHipoReader->getBranch<float>("RECHB::Particle", "chi2pid"); 	 //Chi2 of assigned PID
    fRECHB_Particle_status = fHipoReader->getBranch<uint16_t>("RECHB::Particle", "status"); 	 //particle status (represents detector collection it passed)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Calorimeter:Calorimeter Responses for Particles bank ===================
    fRECHB_Calorimeter_index = fHipoReader->getBranch<uint16_t>("RECHB::Calorimeter", "index"); 	 //index of the hit in the specific detector bank
    fRECHB_Calorimeter_pindex = fHipoReader->getBranch<uint16_t>("RECHB::Calorimeter", "pindex"); 	 //row number in the particle bank hit is associated with
    fRECHB_Calorimeter_detector = fHipoReader->getBranch<uint8_t>("RECHB::Calorimeter", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fRECHB_Calorimeter_sector = fHipoReader->getBranch<uint8_t>("RECHB::Calorimeter", "sector"); 	 //Sector of the Detector hit
    fRECHB_Calorimeter_layer = fHipoReader->getBranch<uint8_t>("RECHB::Calorimeter", "layer"); 	 //Layer of the Detector hit
    fRECHB_Calorimeter_energy = fHipoReader->getBranch<float>("RECHB::Calorimeter", "energy"); 	 //Energy associated with the hit
    fRECHB_Calorimeter_time = fHipoReader->getBranch<float>("RECHB::Calorimeter", "time"); 	 //Time associated with the hit
    fRECHB_Calorimeter_path = fHipoReader->getBranch<float>("RECHB::Calorimeter", "path"); 	 //Path from vertex to the hit position
    fRECHB_Calorimeter_chi2 = fHipoReader->getBranch<float>("RECHB::Calorimeter", "chi2"); 	 //Chi2 (or quality) of hit-track matching
    fRECHB_Calorimeter_x = fHipoReader->getBranch<float>("RECHB::Calorimeter", "x"); 	 //X coordinate of the hit
    fRECHB_Calorimeter_y = fHipoReader->getBranch<float>("RECHB::Calorimeter", "y"); 	 //Y coordinate of the hit
    fRECHB_Calorimeter_z = fHipoReader->getBranch<float>("RECHB::Calorimeter", "z"); 	 //Z coordinate of the hit
    fRECHB_Calorimeter_hx = fHipoReader->getBranch<float>("RECHB::Calorimeter", "hx"); 	 //X coordinate of the matched hit
    fRECHB_Calorimeter_hy = fHipoReader->getBranch<float>("RECHB::Calorimeter", "hy"); 	 //Y coordinate of the mathced hit
    fRECHB_Calorimeter_hz = fHipoReader->getBranch<float>("RECHB::Calorimeter", "hz"); 	 //Z coordinate of the matched hit
    fRECHB_Calorimeter_lu = fHipoReader->getBranch<float>("RECHB::Calorimeter", "lu"); 	 //distance on U-side
    fRECHB_Calorimeter_lv = fHipoReader->getBranch<float>("RECHB::Calorimeter", "lv"); 	 //distance on V-side
    fRECHB_Calorimeter_lw = fHipoReader->getBranch<float>("RECHB::Calorimeter", "lw"); 	 //distance on W-side
    fRECHB_Calorimeter_du = fHipoReader->getBranch<float>("RECHB::Calorimeter", "du"); 	 //shower width on U-side
    fRECHB_Calorimeter_dv = fHipoReader->getBranch<float>("RECHB::Calorimeter", "dv"); 	 //shower width on V-side
    fRECHB_Calorimeter_dw = fHipoReader->getBranch<float>("RECHB::Calorimeter", "dw"); 	 //shower width on w-side
    fRECHB_Calorimeter_m2u = fHipoReader->getBranch<float>("RECHB::Calorimeter", "m2u"); 	 //2nd moment of the shower on U-side
    fRECHB_Calorimeter_m2v = fHipoReader->getBranch<float>("RECHB::Calorimeter", "m2v"); 	 //2nd moment of the shower on V-side
    fRECHB_Calorimeter_m2w = fHipoReader->getBranch<float>("RECHB::Calorimeter", "m2w"); 	 //2nd moment of the shower on W-side
    fRECHB_Calorimeter_m3u = fHipoReader->getBranch<float>("RECHB::Calorimeter", "m3u"); 	 //3rd moment of the shower on U-side
    fRECHB_Calorimeter_m3v = fHipoReader->getBranch<float>("RECHB::Calorimeter", "m3v"); 	 //3rd moment of the shower on V-side
    fRECHB_Calorimeter_m3w = fHipoReader->getBranch<float>("RECHB::Calorimeter", "m3w"); 	 //3rd moment of the shower on W-side
    fRECHB_Calorimeter_status = fHipoReader->getBranch<uint16_t>("RECHB::Calorimeter", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Cherenkov:Cherenkov Responses for Particles bank ===================
    fRECHB_Cherenkov_index = fHipoReader->getBranch<uint16_t>("RECHB::Cherenkov", "index"); 	 //index of the hit in the specific detector bank
    fRECHB_Cherenkov_pindex = fHipoReader->getBranch<uint16_t>("RECHB::Cherenkov", "pindex"); 	 //row number in the particle bank hit is associated with
    fRECHB_Cherenkov_detector = fHipoReader->getBranch<uint8_t>("RECHB::Cherenkov", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fRECHB_Cherenkov_sector = fHipoReader->getBranch<uint8_t>("RECHB::Cherenkov", "sector"); 	 //Sector of the Detector hit
    fRECHB_Cherenkov_nphe = fHipoReader->getBranch<float>("RECHB::Cherenkov", "nphe"); 	 //Number of photoelectrons from Cherenkov radiation
    fRECHB_Cherenkov_time = fHipoReader->getBranch<float>("RECHB::Cherenkov", "time"); 	 //Time associated with the hit
    fRECHB_Cherenkov_path = fHipoReader->getBranch<float>("RECHB::Cherenkov", "path"); 	 //Path from vertex to the hit position
    fRECHB_Cherenkov_chi2 = fHipoReader->getBranch<float>("RECHB::Cherenkov", "chi2"); 	 //Chi2 (or quality) of hit-track matching
    fRECHB_Cherenkov_x = fHipoReader->getBranch<float>("RECHB::Cherenkov", "x"); 	 //X coordinate of the hit
    fRECHB_Cherenkov_y = fHipoReader->getBranch<float>("RECHB::Cherenkov", "y"); 	 //Y coordinate of the hit
    fRECHB_Cherenkov_z = fHipoReader->getBranch<float>("RECHB::Cherenkov", "z"); 	 //Z coordinate of the hit
    fRECHB_Cherenkov_theta = fHipoReader->getBranch<float>("RECHB::Cherenkov", "theta"); 	 //Theta of the matched hit
    fRECHB_Cherenkov_phi = fHipoReader->getBranch<float>("RECHB::Cherenkov", "phi"); 	 //Phi of the mathced hit
    fRECHB_Cherenkov_dtheta = fHipoReader->getBranch<float>("RECHB::Cherenkov", "dtheta"); 	 //Expected Theta Resolution
    fRECHB_Cherenkov_dphi = fHipoReader->getBranch<float>("RECHB::Cherenkov", "dphi"); 	 //Expected Phi Resolution
    fRECHB_Cherenkov_status = fHipoReader->getBranch<uint16_t>("RECHB::Cherenkov", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::ForwardTagger:Forward Tagger information for Particles bank ===================
    fRECHB_ForwardTagger_index = fHipoReader->getBranch<uint16_t>("RECHB::ForwardTagger", "index"); 	 //index of the cluster in the specific detector bank
    fRECHB_ForwardTagger_pindex = fHipoReader->getBranch<uint16_t>("RECHB::ForwardTagger", "pindex"); 	 //row number in the particle bank hit is associated with
    fRECHB_ForwardTagger_detector = fHipoReader->getBranch<uint8_t>("RECHB::ForwardTagger", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fRECHB_ForwardTagger_energy = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "energy"); 	 //Energy associated with the cluster
    fRECHB_ForwardTagger_time = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "time"); 	 //Time associated with the cluster
    fRECHB_ForwardTagger_path = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "path"); 	 //Path from vertex to the cluster position
    fRECHB_ForwardTagger_chi2 = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "chi2"); 	 //Chi2 (or quality) of cluster-particle matching
    fRECHB_ForwardTagger_x = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "x"); 	 //X coordinate of the cluster
    fRECHB_ForwardTagger_y = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "y"); 	 //Y coordinate of the cluster
    fRECHB_ForwardTagger_z = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "z"); 	 //Z coordinate of the cluster
    fRECHB_ForwardTagger_dx = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "dx"); 	 //Cluster width in x
    fRECHB_ForwardTagger_dy = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "dy"); 	 //Cluster width in y
    fRECHB_ForwardTagger_radius = fHipoReader->getBranch<float>("RECHB::ForwardTagger", "radius"); 	 //Cluster radius
    fRECHB_ForwardTagger_size = fHipoReader->getBranch<uint16_t>("RECHB::ForwardTagger", "size"); 	 //size of the cluster
    fRECHB_ForwardTagger_status = fHipoReader->getBranch<uint16_t>("RECHB::ForwardTagger", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Scintillator:Scintillator Responses for Particles bank ===================
    fRECHB_Scintillator_index = fHipoReader->getBranch<uint16_t>("RECHB::Scintillator", "index"); 	 //index of the hit in the specific detector bank
    fRECHB_Scintillator_pindex = fHipoReader->getBranch<uint16_t>("RECHB::Scintillator", "pindex"); 	 //row number in the particle bank hit is associated with
    fRECHB_Scintillator_detector = fHipoReader->getBranch<uint8_t>("RECHB::Scintillator", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fRECHB_Scintillator_sector = fHipoReader->getBranch<uint8_t>("RECHB::Scintillator", "sector"); 	 //Sector of the Detector hit
    fRECHB_Scintillator_layer = fHipoReader->getBranch<uint8_t>("RECHB::Scintillator", "layer"); 	 //Layer of the Detector hit
    fRECHB_Scintillator_component = fHipoReader->getBranch<uint16_t>("RECHB::Scintillator", "component"); 	 //Component of the Detector hit
    fRECHB_Scintillator_energy = fHipoReader->getBranch<float>("RECHB::Scintillator", "energy"); 	 //Energy associated with the hit
    fRECHB_Scintillator_time = fHipoReader->getBranch<float>("RECHB::Scintillator", "time"); 	 //Time associated with the hit
    fRECHB_Scintillator_path = fHipoReader->getBranch<float>("RECHB::Scintillator", "path"); 	 //Path from vertex to the hit position
    fRECHB_Scintillator_chi2 = fHipoReader->getBranch<float>("RECHB::Scintillator", "chi2"); 	 //Chi2 (or quality) of hit-track matching
    fRECHB_Scintillator_x = fHipoReader->getBranch<float>("RECHB::Scintillator", "x"); 	 //X coordinate of the hit
    fRECHB_Scintillator_y = fHipoReader->getBranch<float>("RECHB::Scintillator", "y"); 	 //Y coordinate of the hit
    fRECHB_Scintillator_z = fHipoReader->getBranch<float>("RECHB::Scintillator", "z"); 	 //Z coordinate of the hit
    fRECHB_Scintillator_hx = fHipoReader->getBranch<float>("RECHB::Scintillator", "hx"); 	 //X coordinate of the matched hit
    fRECHB_Scintillator_hy = fHipoReader->getBranch<float>("RECHB::Scintillator", "hy"); 	 //Y coordinate of the mathced hit
    fRECHB_Scintillator_hz = fHipoReader->getBranch<float>("RECHB::Scintillator", "hz"); 	 //Z coordinate of the matched hit
    fRECHB_Scintillator_status = fHipoReader->getBranch<uint16_t>("RECHB::Scintillator", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::Track:Track information for Particles bank ===================
    fRECHB_Track_index = fHipoReader->getBranch<uint16_t>("RECHB::Track", "index"); 	 //index of the track in the specific detector bank
    fRECHB_Track_pindex = fHipoReader->getBranch<uint16_t>("RECHB::Track", "pindex"); 	 //row number in the particle bank hit is associated with
    fRECHB_Track_detector = fHipoReader->getBranch<uint8_t>("RECHB::Track", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fRECHB_Track_status = fHipoReader->getBranch<uint16_t>("RECHB::Track", "status"); 	 //status of the track
    fRECHB_Track_q = fHipoReader->getBranch<uint8_t>("RECHB::Track", "q"); 	 //charge of the track
    fRECHB_Track_chi2 = fHipoReader->getBranch<float>("RECHB::Track", "chi2"); 	 //Chi2 (or quality) track fitting
    fRECHB_Track_NDF = fHipoReader->getBranch<uint16_t>("RECHB::Track", "NDF"); 	 //number of degrees of freedom in track fitting
    fRECHB_Track_px_nomm = fHipoReader->getBranch<float>("RECHB::Track", "px_nomm"); 	 //x component of the momentum with no MM
    fRECHB_Track_py_nomm = fHipoReader->getBranch<float>("RECHB::Track", "py_nomm"); 	 //y component of the momentum with no MM
    fRECHB_Track_pz_nomm = fHipoReader->getBranch<float>("RECHB::Track", "pz_nomm"); 	 //z component of the momentum with no MM
    fRECHB_Track_vx_nomm = fHipoReader->getBranch<float>("RECHB::Track", "vx_nomm"); 	 //x component of the vertex
    fRECHB_Track_vy_nomm = fHipoReader->getBranch<float>("RECHB::Track", "vy_nomm"); 	 //y component of the vertex
    fRECHB_Track_vz_nomm = fHipoReader->getBranch<float>("RECHB::Track", "vz_nomm"); 	 //z component of the vertex
    fRECHB_Track_chi2_nomm = fHipoReader->getBranch<float>("RECHB::Track", "chi2_nomm"); 	 //Chi2 (or quality) track fitting
    fRECHB_Track_NDF_nomm = fHipoReader->getBranch<uint16_t>("RECHB::Track", "NDF_nomm"); 	 //number of degrees of freedom in track fitting with no MM
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== RECHB::TrackCross:Track Cross information for Particles bank ===================
    fRECHB_TrackCross_index = fHipoReader->getBranch<uint16_t>("RECHB::TrackCross", "index"); 	 //index of the cross in the specific detector bank
    fRECHB_TrackCross_pindex = fHipoReader->getBranch<uint16_t>("RECHB::TrackCross", "pindex"); 	 //row number in the track bank hit is associated with
    fRECHB_TrackCross_detector = fHipoReader->getBranch<uint8_t>("RECHB::TrackCross", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fRECHB_TrackCross_sector = fHipoReader->getBranch<uint8_t>("RECHB::TrackCross", "sector"); 	 //sector of the track
    fRECHB_TrackCross_layer = fHipoReader->getBranch<uint8_t>("RECHB::TrackCross", "layer"); 	 //layer of the track
    fRECHB_TrackCross_c_x = fHipoReader->getBranch<float>("RECHB::TrackCross", "c_x"); 	 //Cross x-position in the lab (in cm)
    fRECHB_TrackCross_c_y = fHipoReader->getBranch<float>("RECHB::TrackCross", "c_y"); 	 //Cross y-position in the lab (in cm)
    fRECHB_TrackCross_c_z = fHipoReader->getBranch<float>("RECHB::TrackCross", "c_z"); 	 //Cross z-position in the lab (in cm)
    fRECHB_TrackCross_c_ux = fHipoReader->getBranch<float>("RECHB::TrackCross", "c_ux"); 	 //Cross unit x-direction vector in the lab
    fRECHB_TrackCross_c_uy = fHipoReader->getBranch<float>("RECHB::TrackCross", "c_uy"); 	 //Cross unit y-direction vector in the lab
    fRECHB_TrackCross_c_uz = fHipoReader->getBranch<float>("RECHB::TrackCross", "c_uz"); 	 //Cross unit z-direction vector in the lab
    fRECHB_TrackCross_status = fHipoReader->getBranch<uint16_t>("RECHB::TrackCross", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Event:Event Header Bank ===================
    fREC_Event_NRUN = fHipoReader->getBranch<uint32_t>("REC::Event", "NRUN"); 	 //Run Number
    fREC_Event_NEVENT = fHipoReader->getBranch<uint32_t>("REC::Event", "NEVENT"); 	 //Event Number
    fREC_Event_EVNTime = fHipoReader->getBranch<float>("REC::Event", "EVNTime"); 	 //Event Time
    fREC_Event_TYPE = fHipoReader->getBranch<uint8_t>("REC::Event", "TYPE"); 	 //Event Type (Data or MC)
    fREC_Event_EvCAT = fHipoReader->getBranch<uint16_t>("REC::Event", "EvCAT"); 	 //Event Category, if >0:  e-, e-p, e-pi+.....
    fREC_Event_NPGP = fHipoReader->getBranch<uint16_t>("REC::Event", "NPGP"); 	 //Number of Final (Timed-based) Reconstructed Particles*100 + Number of Geometrically Reconstructed Particles
    fREC_Event_TRG = fHipoReader->getBranch<uint64_t>("REC::Event", "TRG"); 	 //Trigger Type (CLAS12_e-, FT_CLAS12_h, CLAS12_H,...) + Prescale Factor for that Trigger
    fREC_Event_BCG = fHipoReader->getBranch<float>("REC::Event", "BCG"); 	 //Faraday Cup Gated (Coulomb)
    fREC_Event_LT = fHipoReader->getBranch<double>("REC::Event", "LT"); 	 //Clock
    fREC_Event_STTime = fHipoReader->getBranch<float>("REC::Event", "STTime"); 	 //Event Start Time (ns)
    fREC_Event_RFTime = fHipoReader->getBranch<float>("REC::Event", "RFTime"); 	 //RF Time (ns)
    fREC_Event_Helic = fHipoReader->getBranch<uint8_t>("REC::Event", "Helic"); 	 //Helicity of Event
    fREC_Event_PTIME = fHipoReader->getBranch<float>("REC::Event", "PTIME"); 	 //Event Processing Time (UNIX Time = seconds)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Particle:Reconstructed Particle Information ===================
    fREC_Particle_pid = fHipoReader->getBranch<uint32_t>("REC::Particle", "pid"); 	 //particle id in LUND conventions
    fREC_Particle_px = fHipoReader->getBranch<float>("REC::Particle", "px"); 	 //x component of the momentum
    fREC_Particle_py = fHipoReader->getBranch<float>("REC::Particle", "py"); 	 //y component of the momentum
    fREC_Particle_pz = fHipoReader->getBranch<float>("REC::Particle", "pz"); 	 //z component of the momentum
    fREC_Particle_vx = fHipoReader->getBranch<float>("REC::Particle", "vx"); 	 //x component of the vertex
    fREC_Particle_vy = fHipoReader->getBranch<float>("REC::Particle", "vy"); 	 //y component of the vertex
    fREC_Particle_vz = fHipoReader->getBranch<float>("REC::Particle", "vz"); 	 //z component of the vertex
    fREC_Particle_charge = fHipoReader->getBranch<uint8_t>("REC::Particle", "charge"); 	 //particle charge
    fREC_Particle_beta = fHipoReader->getBranch<float>("REC::Particle", "beta"); 	 //particle beta measured by TOF
    fREC_Particle_chi2pid = fHipoReader->getBranch<float>("REC::Particle", "chi2pid"); 	 //Chi2 of assigned PID
    fREC_Particle_status = fHipoReader->getBranch<uint16_t>("REC::Particle", "status"); 	 //particle status (represents detector collection it passed)
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Calorimeter:Calorimeter Responses for Particles bank ===================
    fREC_Calorimeter_index = fHipoReader->getBranch<uint16_t>("REC::Calorimeter", "index"); 	 //index of the hit in the specific detector bank
    fREC_Calorimeter_pindex = fHipoReader->getBranch<uint16_t>("REC::Calorimeter", "pindex"); 	 //row number in the particle bank hit is associated with
    fREC_Calorimeter_detector = fHipoReader->getBranch<uint8_t>("REC::Calorimeter", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fREC_Calorimeter_sector = fHipoReader->getBranch<uint8_t>("REC::Calorimeter", "sector"); 	 //Sector of the Detector hit
    fREC_Calorimeter_layer = fHipoReader->getBranch<uint8_t>("REC::Calorimeter", "layer"); 	 //Layer of the Detector hit
    fREC_Calorimeter_energy = fHipoReader->getBranch<float>("REC::Calorimeter", "energy"); 	 //Energy associated with the hit
    fREC_Calorimeter_time = fHipoReader->getBranch<float>("REC::Calorimeter", "time"); 	 //Time associated with the hit
    fREC_Calorimeter_path = fHipoReader->getBranch<float>("REC::Calorimeter", "path"); 	 //Path from vertex to the hit position
    fREC_Calorimeter_chi2 = fHipoReader->getBranch<float>("REC::Calorimeter", "chi2"); 	 //Chi2 (or quality) of hit-track matching
    fREC_Calorimeter_x = fHipoReader->getBranch<float>("REC::Calorimeter", "x"); 	 //X coordinate of the hit
    fREC_Calorimeter_y = fHipoReader->getBranch<float>("REC::Calorimeter", "y"); 	 //Y coordinate of the hit
    fREC_Calorimeter_z = fHipoReader->getBranch<float>("REC::Calorimeter", "z"); 	 //Z coordinate of the hit
    fREC_Calorimeter_hx = fHipoReader->getBranch<float>("REC::Calorimeter", "hx"); 	 //X coordinate of the matched hit
    fREC_Calorimeter_hy = fHipoReader->getBranch<float>("REC::Calorimeter", "hy"); 	 //Y coordinate of the mathced hit
    fREC_Calorimeter_hz = fHipoReader->getBranch<float>("REC::Calorimeter", "hz"); 	 //Z coordinate of the matched hit
    fREC_Calorimeter_lu = fHipoReader->getBranch<float>("REC::Calorimeter", "lu"); 	 //distance on U-side
    fREC_Calorimeter_lv = fHipoReader->getBranch<float>("REC::Calorimeter", "lv"); 	 //distance on V-side
    fREC_Calorimeter_lw = fHipoReader->getBranch<float>("REC::Calorimeter", "lw"); 	 //distance on W-side
    fREC_Calorimeter_du = fHipoReader->getBranch<float>("REC::Calorimeter", "du"); 	 //shower width on U-side
    fREC_Calorimeter_dv = fHipoReader->getBranch<float>("REC::Calorimeter", "dv"); 	 //shower width on V-side
    fREC_Calorimeter_dw = fHipoReader->getBranch<float>("REC::Calorimeter", "dw"); 	 //shower width on w-side
    fREC_Calorimeter_m2u = fHipoReader->getBranch<float>("REC::Calorimeter", "m2u"); 	 //2nd moment of the shower on U-side
    fREC_Calorimeter_m2v = fHipoReader->getBranch<float>("REC::Calorimeter", "m2v"); 	 //2nd moment of the shower on V-side
    fREC_Calorimeter_m2w = fHipoReader->getBranch<float>("REC::Calorimeter", "m2w"); 	 //2nd moment of the shower on W-side
    fREC_Calorimeter_m3u = fHipoReader->getBranch<float>("REC::Calorimeter", "m3u"); 	 //3rd moment of the shower on U-side
    fREC_Calorimeter_m3v = fHipoReader->getBranch<float>("REC::Calorimeter", "m3v"); 	 //3rd moment of the shower on V-side
    fREC_Calorimeter_m3w = fHipoReader->getBranch<float>("REC::Calorimeter", "m3w"); 	 //3rd moment of the shower on W-side
    fREC_Calorimeter_status = fHipoReader->getBranch<uint16_t>("REC::Calorimeter", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Cherenkov:Cherenkov Responses for Particles bank ===================
    fREC_Cherenkov_index = fHipoReader->getBranch<uint16_t>("REC::Cherenkov", "index"); 	 //index of the hit in the specific detector bank
    fREC_Cherenkov_pindex = fHipoReader->getBranch<uint16_t>("REC::Cherenkov", "pindex"); 	 //row number in the particle bank hit is associated with
    fREC_Cherenkov_detector = fHipoReader->getBranch<uint8_t>("REC::Cherenkov", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fREC_Cherenkov_sector = fHipoReader->getBranch<uint8_t>("REC::Cherenkov", "sector"); 	 //Sector of the Detector hit
    fREC_Cherenkov_nphe = fHipoReader->getBranch<float>("REC::Cherenkov", "nphe"); 	 //Number of photoelectrons from Cherenkov radiation
    fREC_Cherenkov_time = fHipoReader->getBranch<float>("REC::Cherenkov", "time"); 	 //Time associated with the hit
    fREC_Cherenkov_path = fHipoReader->getBranch<float>("REC::Cherenkov", "path"); 	 //Path from vertex to the hit position
    fREC_Cherenkov_chi2 = fHipoReader->getBranch<float>("REC::Cherenkov", "chi2"); 	 //Chi2 (or quality) of hit-track matching
    fREC_Cherenkov_x = fHipoReader->getBranch<float>("REC::Cherenkov", "x"); 	 //X coordinate of the hit
    fREC_Cherenkov_y = fHipoReader->getBranch<float>("REC::Cherenkov", "y"); 	 //Y coordinate of the hit
    fREC_Cherenkov_z = fHipoReader->getBranch<float>("REC::Cherenkov", "z"); 	 //Z coordinate of the hit
    fREC_Cherenkov_theta = fHipoReader->getBranch<float>("REC::Cherenkov", "theta"); 	 //Theta of the matched hit
    fREC_Cherenkov_phi = fHipoReader->getBranch<float>("REC::Cherenkov", "phi"); 	 //Phi of the mathced hit
    fREC_Cherenkov_dtheta = fHipoReader->getBranch<float>("REC::Cherenkov", "dtheta"); 	 //Expected Theta Resolution
    fREC_Cherenkov_dphi = fHipoReader->getBranch<float>("REC::Cherenkov", "dphi"); 	 //Expected Phi Resolution
    fREC_Cherenkov_status = fHipoReader->getBranch<uint16_t>("REC::Cherenkov", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::ForwardTagger:Forward Tagger information for Particles bank ===================
    fREC_ForwardTagger_index = fHipoReader->getBranch<uint16_t>("REC::ForwardTagger", "index"); 	 //index of the cluster in the specific detector bank
    fREC_ForwardTagger_pindex = fHipoReader->getBranch<uint16_t>("REC::ForwardTagger", "pindex"); 	 //row number in the particle bank hit is associated with
    fREC_ForwardTagger_detector = fHipoReader->getBranch<uint8_t>("REC::ForwardTagger", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fREC_ForwardTagger_energy = fHipoReader->getBranch<float>("REC::ForwardTagger", "energy"); 	 //Energy associated with the cluster
    fREC_ForwardTagger_time = fHipoReader->getBranch<float>("REC::ForwardTagger", "time"); 	 //Time associated with the cluster
    fREC_ForwardTagger_path = fHipoReader->getBranch<float>("REC::ForwardTagger", "path"); 	 //Path from vertex to the cluster position
    fREC_ForwardTagger_chi2 = fHipoReader->getBranch<float>("REC::ForwardTagger", "chi2"); 	 //Chi2 (or quality) of cluster-particle matching
    fREC_ForwardTagger_x = fHipoReader->getBranch<float>("REC::ForwardTagger", "x"); 	 //X coordinate of the cluster
    fREC_ForwardTagger_y = fHipoReader->getBranch<float>("REC::ForwardTagger", "y"); 	 //Y coordinate of the cluster
    fREC_ForwardTagger_z = fHipoReader->getBranch<float>("REC::ForwardTagger", "z"); 	 //Z coordinate of the cluster
    fREC_ForwardTagger_dx = fHipoReader->getBranch<float>("REC::ForwardTagger", "dx"); 	 //Cluster width in x
    fREC_ForwardTagger_dy = fHipoReader->getBranch<float>("REC::ForwardTagger", "dy"); 	 //Cluster width in y
    fREC_ForwardTagger_radius = fHipoReader->getBranch<float>("REC::ForwardTagger", "radius"); 	 //Cluster radius
    fREC_ForwardTagger_size = fHipoReader->getBranch<uint16_t>("REC::ForwardTagger", "size"); 	 //size of the cluster
    fREC_ForwardTagger_status = fHipoReader->getBranch<uint16_t>("REC::ForwardTagger", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Scintillator:Scintillator Responses for Particles bank ===================
    fREC_Scintillator_index = fHipoReader->getBranch<uint16_t>("REC::Scintillator", "index"); 	 //index of the hit in the specific detector bank
    fREC_Scintillator_pindex = fHipoReader->getBranch<uint16_t>("REC::Scintillator", "pindex"); 	 //row number in the particle bank hit is associated with
    fREC_Scintillator_detector = fHipoReader->getBranch<uint8_t>("REC::Scintillator", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fREC_Scintillator_sector = fHipoReader->getBranch<uint8_t>("REC::Scintillator", "sector"); 	 //Sector of the Detector hit
    fREC_Scintillator_layer = fHipoReader->getBranch<uint8_t>("REC::Scintillator", "layer"); 	 //Layer of the Detector hit
    fREC_Scintillator_component = fHipoReader->getBranch<uint16_t>("REC::Scintillator", "component"); 	 //Component of the Detector hit
    fREC_Scintillator_energy = fHipoReader->getBranch<float>("REC::Scintillator", "energy"); 	 //Energy associated with the hit
    fREC_Scintillator_time = fHipoReader->getBranch<float>("REC::Scintillator", "time"); 	 //Time associated with the hit
    fREC_Scintillator_path = fHipoReader->getBranch<float>("REC::Scintillator", "path"); 	 //Path from vertex to the hit position
    fREC_Scintillator_chi2 = fHipoReader->getBranch<float>("REC::Scintillator", "chi2"); 	 //Chi2 (or quality) of hit-track matching
    fREC_Scintillator_x = fHipoReader->getBranch<float>("REC::Scintillator", "x"); 	 //X coordinate of the hit
    fREC_Scintillator_y = fHipoReader->getBranch<float>("REC::Scintillator", "y"); 	 //Y coordinate of the hit
    fREC_Scintillator_z = fHipoReader->getBranch<float>("REC::Scintillator", "z"); 	 //Z coordinate of the hit
    fREC_Scintillator_hx = fHipoReader->getBranch<float>("REC::Scintillator", "hx"); 	 //X coordinate of the matched hit
    fREC_Scintillator_hy = fHipoReader->getBranch<float>("REC::Scintillator", "hy"); 	 //Y coordinate of the mathced hit
    fREC_Scintillator_hz = fHipoReader->getBranch<float>("REC::Scintillator", "hz"); 	 //Z coordinate of the matched hit
    fREC_Scintillator_status = fHipoReader->getBranch<uint16_t>("REC::Scintillator", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Track:Track information for Particles bank ===================
    fREC_Track_index = fHipoReader->getBranch<uint16_t>("REC::Track", "index"); 	 //index of the track in the specific detector bank
    fREC_Track_pindex = fHipoReader->getBranch<uint16_t>("REC::Track", "pindex"); 	 //row number in the particle bank hit is associated with
    fREC_Track_detector = fHipoReader->getBranch<uint8_t>("REC::Track", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fREC_Track_status = fHipoReader->getBranch<uint16_t>("REC::Track", "status"); 	 //status of the track
    fREC_Track_q = fHipoReader->getBranch<uint8_t>("REC::Track", "q"); 	 //charge of the track
    fREC_Track_chi2 = fHipoReader->getBranch<float>("REC::Track", "chi2"); 	 //Chi2 (or quality) track fitting
    fREC_Track_NDF = fHipoReader->getBranch<uint16_t>("REC::Track", "NDF"); 	 //number of degrees of freedom in track fitting
    fREC_Track_px_nomm = fHipoReader->getBranch<float>("REC::Track", "px_nomm"); 	 //x component of the momentum with no MM
    fREC_Track_py_nomm = fHipoReader->getBranch<float>("REC::Track", "py_nomm"); 	 //y component of the momentum with no MM
    fREC_Track_pz_nomm = fHipoReader->getBranch<float>("REC::Track", "pz_nomm"); 	 //z component of the momentum with no MM
    fREC_Track_vx_nomm = fHipoReader->getBranch<float>("REC::Track", "vx_nomm"); 	 //x component of the vertex
    fREC_Track_vy_nomm = fHipoReader->getBranch<float>("REC::Track", "vy_nomm"); 	 //y component of the vertex
    fREC_Track_vz_nomm = fHipoReader->getBranch<float>("REC::Track", "vz_nomm"); 	 //z component of the vertex
    fREC_Track_chi2_nomm = fHipoReader->getBranch<float>("REC::Track", "chi2_nomm"); 	 //Chi2 (or quality) track fitting
    fREC_Track_NDF_nomm = fHipoReader->getBranch<uint16_t>("REC::Track", "NDF_nomm"); 	 //number of degrees of freedom in track fitting with no MM
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::TrackCross:Track Cross information for Particles bank ===================
    fREC_TrackCross_index = fHipoReader->getBranch<uint16_t>("REC::TrackCross", "index"); 	 //index of the cross in the specific detector bank
    fREC_TrackCross_pindex = fHipoReader->getBranch<uint16_t>("REC::TrackCross", "pindex"); 	 //row number in the track bank hit is associated with
    fREC_TrackCross_detector = fHipoReader->getBranch<uint8_t>("REC::TrackCross", "detector"); 	 //Detector ID, defined in COATJAVA DetectorType
    fREC_TrackCross_sector = fHipoReader->getBranch<uint8_t>("REC::TrackCross", "sector"); 	 //sector of the track
    fREC_TrackCross_layer = fHipoReader->getBranch<uint8_t>("REC::TrackCross", "layer"); 	 //layer of the track
    fREC_TrackCross_c_x = fHipoReader->getBranch<float>("REC::TrackCross", "c_x"); 	 //Cross x-position in the lab (in cm)
    fREC_TrackCross_c_y = fHipoReader->getBranch<float>("REC::TrackCross", "c_y"); 	 //Cross y-position in the lab (in cm)
    fREC_TrackCross_c_z = fHipoReader->getBranch<float>("REC::TrackCross", "c_z"); 	 //Cross z-position in the lab (in cm)
    fREC_TrackCross_c_ux = fHipoReader->getBranch<float>("REC::TrackCross", "c_ux"); 	 //Cross unit x-direction vector in the lab
    fREC_TrackCross_c_uy = fHipoReader->getBranch<float>("REC::TrackCross", "c_uy"); 	 //Cross unit y-direction vector in the lab
    fREC_TrackCross_c_uz = fHipoReader->getBranch<float>("REC::TrackCross", "c_uz"); 	 //Cross unit z-direction vector in the lab
    fREC_TrackCross_status = fHipoReader->getBranch<uint16_t>("REC::TrackCross", "status"); 	 //hit status
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::CovMat:reconstructed track covariance matrix ===================
    fREC_CovMat_index = fHipoReader->getBranch<uint16_t>("REC::CovMat", "index"); 	 //index of the track in Tracks bank
    fREC_CovMat_pindex = fHipoReader->getBranch<uint16_t>("REC::CovMat", "pindex"); 	 //row number in the particle bank hit is associated with
    fREC_CovMat_C11 = fHipoReader->getBranch<float>("REC::CovMat", "C11"); 	 //C11 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C12 = fHipoReader->getBranch<float>("REC::CovMat", "C12"); 	 //C12 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C13 = fHipoReader->getBranch<float>("REC::CovMat", "C13"); 	 //C13 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C14 = fHipoReader->getBranch<float>("REC::CovMat", "C14"); 	 //C14 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C15 = fHipoReader->getBranch<float>("REC::CovMat", "C15"); 	 //C15 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C22 = fHipoReader->getBranch<float>("REC::CovMat", "C22"); 	 //C22 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C23 = fHipoReader->getBranch<float>("REC::CovMat", "C23"); 	 //C23 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C24 = fHipoReader->getBranch<float>("REC::CovMat", "C24"); 	 //C24 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C25 = fHipoReader->getBranch<float>("REC::CovMat", "C25"); 	 //C25 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C33 = fHipoReader->getBranch<float>("REC::CovMat", "C33"); 	 //C33 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C34 = fHipoReader->getBranch<float>("REC::CovMat", "C34"); 	 //C34 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C35 = fHipoReader->getBranch<float>("REC::CovMat", "C35"); 	 //C35 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C44 = fHipoReader->getBranch<float>("REC::CovMat", "C44"); 	 //C44 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C45 = fHipoReader->getBranch<float>("REC::CovMat", "C45"); 	 //C45 covariance matrix element at last superlayer used in the fit
    fREC_CovMat_C55 = fHipoReader->getBranch<float>("REC::CovMat", "C55"); 	 //C55 covariance matrix element at last superlayer used in the fit
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::VertDoca:Track Cross information for Particles bank ===================
    fREC_VertDoca_index1 = fHipoReader->getBranch<uint16_t>("REC::VertDoca", "index1"); 	 //index of the first track in Tracks bank
    fREC_VertDoca_index2 = fHipoReader->getBranch<uint16_t>("REC::VertDoca", "index2"); 	 //index of the second track in Tracks bank
    fREC_VertDoca_x = fHipoReader->getBranch<float>("REC::VertDoca", "x"); 	 //x-position of the common vertex
    fREC_VertDoca_y = fHipoReader->getBranch<float>("REC::VertDoca", "y"); 	 //y-position of the common vertex
    fREC_VertDoca_z = fHipoReader->getBranch<float>("REC::VertDoca", "z"); 	 //z-position of the common vertex
    fREC_VertDoca_x1 = fHipoReader->getBranch<float>("REC::VertDoca", "x1"); 	 //x-position of the first track at the DOCA point
    fREC_VertDoca_y1 = fHipoReader->getBranch<float>("REC::VertDoca", "y1"); 	 //y-position of the first track at the DOCA point
    fREC_VertDoca_z1 = fHipoReader->getBranch<float>("REC::VertDoca", "z1"); 	 //z-position of the first track at the DOCA point
    fREC_VertDoca_cx1 = fHipoReader->getBranch<float>("REC::VertDoca", "cx1"); 	 //x-direction vector of the first track at the DOCA point
    fREC_VertDoca_cy1 = fHipoReader->getBranch<float>("REC::VertDoca", "cy1"); 	 //y-direction vector of the first track at the DOCA point
    fREC_VertDoca_cz1 = fHipoReader->getBranch<float>("REC::VertDoca", "cz1"); 	 //z-direction vector of the first track at the DOCA point
    fREC_VertDoca_x2 = fHipoReader->getBranch<float>("REC::VertDoca", "x2"); 	 //x-position of the second track at the DOCA point
    fREC_VertDoca_y2 = fHipoReader->getBranch<float>("REC::VertDoca", "y2"); 	 //y-position of the second track at the DOCA point
    fREC_VertDoca_z2 = fHipoReader->getBranch<float>("REC::VertDoca", "z2"); 	 //z-position of the second track at the DOCA point
    fREC_VertDoca_cx2 = fHipoReader->getBranch<float>("REC::VertDoca", "cx2"); 	 //x-direction vector of the second track at the DOCA point
    fREC_VertDoca_cy2 = fHipoReader->getBranch<float>("REC::VertDoca", "cy2"); 	 //y-direction vector of the second track at the DOCA point
    fREC_VertDoca_cz2 = fHipoReader->getBranch<float>("REC::VertDoca", "cz2"); 	 //z-direction vector of the second track at the DOCA point
    fREC_VertDoca_r = fHipoReader->getBranch<float>("REC::VertDoca", "r"); 	 //The distance between two tracks
    //--------------------------------------------------------------------------------------------------------------------------------------

    //================== REC::Traj:Trajectory information for Particles bank ===================
    fREC_Traj_pindex = fHipoReader->getBranch<uint16_t>("REC::Traj", "pindex"); 	 //index of the particle in REC::Particle
    fREC_Traj_index = fHipoReader->getBranch<uint16_t>("REC::Traj", "index"); 	 //index of the track in its tracking bank
    fREC_Traj_detId = fHipoReader->getBranch<uint16_t>("REC::Traj", "detId"); 	 //representative index of the detector
    fREC_Traj_q = fHipoReader->getBranch<uint8_t>("REC::Traj", "q"); 	 //charge of the track
    fREC_Traj_x = fHipoReader->getBranch<float>("REC::Traj", "x"); 	 //x-position of the track at the detector surface
    fREC_Traj_y = fHipoReader->getBranch<float>("REC::Traj", "y"); 	 //y-position of the track at the detector surface
    fREC_Traj_z = fHipoReader->getBranch<float>("REC::Traj", "z"); 	 //z-position of the track at the detector surface
    fREC_Traj_cx = fHipoReader->getBranch<float>("REC::Traj", "cx"); 	 //direction cosline of the track at the detector surface
    fREC_Traj_cy = fHipoReader->getBranch<float>("REC::Traj", "cy"); 	 //direction cosline of the track at the detector surface
    fREC_Traj_cz = fHipoReader->getBranch<float>("REC::Traj", "cz"); 	 //direction cosline of the track at the detector surface
    fREC_Traj_pathlength = fHipoReader->getBranch<float>("REC::Traj", "pathlength"); 	 //pathlength of the track to the detector surface from the DOCA point
    //--------------------------------------------------------------------------------------------------------------------------------------       
}
