// Filename: TClas12DetectorDC.h
// Description: CLAS12 DC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORDC_H
#define TCLAS12DETECTORDC_H

#include "TClas12Detector.h"

class TClas12DetectorDC: public TClas12Detector
{
public:
	TClas12DetectorDC(TClas12Run *run);
	~TClas12DetectorDC();
	void Init();
	void SetBranches();

protected:
//================== HitBasedTrkg::HBHits:reconstructed hits using DC wire positions ===================
	hipo::node<uint16_t> *fHitBasedTrkg_HBHits_id;   //id of the hit
	hipo::node<uint16_t> *fHitBasedTrkg_HBHits_status;   //id of the hit
	hipo::node<uint8_t> *fHitBasedTrkg_HBHits_sector;   //DC sector
	hipo::node<uint8_t> *fHitBasedTrkg_HBHits_superlayer;   //DC superlayer (1...6)
	hipo::node<uint8_t> *fHitBasedTrkg_HBHits_layer;   //DC layer in superlayer (1...6)
	hipo::node<uint16_t> *fHitBasedTrkg_HBHits_wire;   //wire id of DC
	hipo::node<uint32_t> *fHitBasedTrkg_HBHits_TDC;   //raw time of the hit
	hipo::node<float> *fHitBasedTrkg_HBHits_trkDoca;   //track doca of the hit (in cm)
	hipo::node<float> *fHitBasedTrkg_HBHits_docaError;   //error on track doca of the hit
	hipo::node<uint8_t> *fHitBasedTrkg_HBHits_LR;   //Left/Right ambiguity of the hit
	hipo::node<float> *fHitBasedTrkg_HBHits_LocX;   //x in planar local coordinate system
	hipo::node<float> *fHitBasedTrkg_HBHits_LocY;   //y in planar local coordinate system
	hipo::node<float> *fHitBasedTrkg_HBHits_X;   //wire x-coordinate  in tilted-sector
	hipo::node<float> *fHitBasedTrkg_HBHits_Z;   //wire z-coordinate  in tilted-sector
	hipo::node<float> *fHitBasedTrkg_HBHits_B;   //B-field intensity at hit position in tilted-sector system
	hipo::node<float> *fHitBasedTrkg_HBHits_TProp;   //t propagation along the wire
	hipo::node<float> *fHitBasedTrkg_HBHits_TFlight;   //time of flight correction
	hipo::node<uint16_t> *fHitBasedTrkg_HBHits_clusterID;   //ID of associated cluster
	hipo::node<uint8_t> *fHitBasedTrkg_HBHits_trkID;   //ID of associated track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBClusters:reconstructed clusters using DC wire positions ===================
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_id;   //id of the cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_status;   //status of the cluster
	hipo::node<uint8_t> *fHitBasedTrkg_HBClusters_sector;   //sector of the cluster
	hipo::node<uint8_t> *fHitBasedTrkg_HBClusters_superlayer;   //superlayer of the cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit1_ID;   //id of hit1 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit2_ID;   //id of hit2 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit3_ID;   //id of hit3 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit4_ID;   //id of hit4 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit5_ID;   //id of hit5 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit6_ID;   //id of hit6 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit7_ID;   //id of hit7 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit8_ID;   //id of hit8 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit9_ID;   //id of hit9 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit10_ID;   //id of hit10 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit11_ID;   //id of hit11 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBClusters_Hit12_ID;   //id of hit12 in cluster
	hipo::node<float> *fHitBasedTrkg_HBClusters_avgWire;   //average wire number
	hipo::node<float> *fHitBasedTrkg_HBClusters_fitChisqProb;   //fit chisq prob.
	hipo::node<float> *fHitBasedTrkg_HBClusters_fitSlope;   //line fit slope
	hipo::node<float> *fHitBasedTrkg_HBClusters_fitSlopeErr;   //error on slope
	hipo::node<float> *fHitBasedTrkg_HBClusters_fitInterc;   //line fit intercept
	hipo::node<float> *fHitBasedTrkg_HBClusters_fitIntercErr;   //error on the intercept
	hipo::node<uint8_t> *fHitBasedTrkg_HBClusters_size;   //cluster size
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBSegments:reconstructed segments using DC wire positions ===================
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_id;   //id of the segment
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_status;   //status of the segment
	hipo::node<uint8_t> *fHitBasedTrkg_HBSegments_sector;   //sector of the segment
	hipo::node<uint8_t> *fHitBasedTrkg_HBSegments_superlayer;   //superlayer of superlayer
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Cluster_ID;   //associated cluster id
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit1_ID;   //id of hit1 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit2_ID;   //id of hit2 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit3_ID;   //id of hit3 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit4_ID;   //id of hit4 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit5_ID;   //id of hit5 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit6_ID;   //id of hit6 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit7_ID;   //id of hit7 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit8_ID;   //id of hit8 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit9_ID;   //id of hit9 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit10_ID;   //id of hit10 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit11_ID;   //id of hit11 in cluster
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegments_Hit12_ID;   //id of hit12 in cluster
	hipo::node<float> *fHitBasedTrkg_HBSegments_avgWire;   //average wire number
	hipo::node<float> *fHitBasedTrkg_HBSegments_fitChisqProb;   //fit chisq prob.
	hipo::node<float> *fHitBasedTrkg_HBSegments_fitSlope;   //line fit slope
	hipo::node<float> *fHitBasedTrkg_HBSegments_fitSlopeErr;   //error on slope
	hipo::node<float> *fHitBasedTrkg_HBSegments_fitInterc;   //line fit intercept
	hipo::node<float> *fHitBasedTrkg_HBSegments_fitIntercErr;   //error on the intercept
	hipo::node<float> *fHitBasedTrkg_HBSegments_SegEndPoint1X;   //Segment 1st endpoint x coordinate in the sector coordinate system (for ced display)
	hipo::node<float> *fHitBasedTrkg_HBSegments_SegEndPoint1Z;   //Segment 1st endpoint z coordinate in the sector coordinate system (for ced display)
	hipo::node<float> *fHitBasedTrkg_HBSegments_SegEndPoint2X;   //Segment 2nd endpoint x coordinate in the sector coordinate system (for ced display)
	hipo::node<float> *fHitBasedTrkg_HBSegments_SegEndPoint2Z;   //Segment 2nd endpoint z coordinate in the sector coordinate system (for ced display)
	hipo::node<uint8_t> *fHitBasedTrkg_HBSegments_size;   //size of segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBSegmentTrajectory:reconstructed segment trajectory from hit-based tracking ===================
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegmentTrajectory_segmentID;   //id of the segment
	hipo::node<uint8_t> *fHitBasedTrkg_HBSegmentTrajectory_sector;   //sector of DC
	hipo::node<uint8_t> *fHitBasedTrkg_HBSegmentTrajectory_superlayer;   //superlayer
	hipo::node<uint8_t> *fHitBasedTrkg_HBSegmentTrajectory_layer;   //layer
	hipo::node<uint16_t> *fHitBasedTrkg_HBSegmentTrajectory_matchedHitID;   //matched hit id
	hipo::node<float> *fHitBasedTrkg_HBSegmentTrajectory_trkDoca;   //calculated track doca
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBCrosses:reconstructed segments using DC wire positions ===================
	hipo::node<uint16_t> *fHitBasedTrkg_HBCrosses_id;   //id of the cross
	hipo::node<uint16_t> *fHitBasedTrkg_HBCrosses_status;   //status of the cross
	hipo::node<uint8_t> *fHitBasedTrkg_HBCrosses_sector;   //sector of the cross
	hipo::node<uint8_t> *fHitBasedTrkg_HBCrosses_region;   //region of the cross
	hipo::node<float> *fHitBasedTrkg_HBCrosses_x;   //DC track cross x-coordinate (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_y;   //DC track cross y-coordinate (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_z;   //DC track cross z-coordinate (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_err_x;   //DC track cross x-coordinate uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_err_y;   //DC track cross y-coordinate uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_err_z;   //DC track cross z-coordinate uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_ux;   //DC track cross x-direction (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_uy;   //DC track cross y-direction (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_uz;   //DC track cross z-direction (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_err_ux;   //DC track cross x-direction uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_err_uy;   //DC track cross y-direction uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fHitBasedTrkg_HBCrosses_err_uz;   //DC track cross z-direction uncertainty (in the DC tilted sector coordinate system)
	hipo::node<uint16_t> *fHitBasedTrkg_HBCrosses_Segment1_ID;   //id of first superlater used in segment
	hipo::node<uint16_t> *fHitBasedTrkg_HBCrosses_Segment2_ID;   //id of second superlater used in segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBTracks:reconstructed tracks using DC wire positions ===================
	hipo::node<uint16_t> *fHitBasedTrkg_HBTracks_id;   //id of the track
	hipo::node<uint16_t> *fHitBasedTrkg_HBTracks_status;   //status of the track
	hipo::node<uint8_t> *fHitBasedTrkg_HBTracks_sector;   //sector of the track
	hipo::node<float> *fHitBasedTrkg_HBTracks_c1_x;   //Upstream Region 1 cross x-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_c1_y;   //Upstream Region 1 cross y-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_c1_z;   //Upstream Region 1 cross z-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_c1_ux;   //Upstream Region 1 cross unit x-direction vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_c1_uy;   //Upstream Region 1 cross unit y-direction vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_c1_uz;   //Upstream Region 1 cross unit z-direction vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_c3_x;   //Downstream Region 3 cross x-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_c3_y;   //Downstream Region 3 cross y-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_c3_z;   //Downstream Region 3 cross z-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_c3_ux;   //Downstream Region 3 cross unit x-direction vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_c3_uy;   //Downstream Region 3 cross unit y-direction vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_c3_uz;   //Downstream Region 3 cross unit z-direction vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_t1_x;   //Upstream Region 1 track x-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_t1_y;   //Upstream Region 1 track y-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_t1_z;   //Upstream Region 1 track z-position in the lab (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_t1_px;   //Upstream Region 1 track unit x-momentum vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_t1_py;   //Upstream Region 1 track unit y-momentum vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_t1_pz;   //Upstream Region 1 track unit z-momentum vector in the lab
	hipo::node<float> *fHitBasedTrkg_HBTracks_Vtx0_x;   //Vertex x-position of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_Vtx0_y;   //Vertex y-position of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_Vtx0_z;   //Vertex z-position of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_p0_x;   //3-momentum x-coordinate of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_p0_y;   //3-momentum y-coordinate of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fHitBasedTrkg_HBTracks_p0_z;   //3-momentum z-coordinate of the swam track to the DOCA to the beamline (in cm)
	hipo::node<uint16_t> *fHitBasedTrkg_HBTracks_Cross1_ID;   //id of first cross on track
	hipo::node<uint16_t> *fHitBasedTrkg_HBTracks_Cross2_ID;   //id of second cross on track
	hipo::node<uint16_t> *fHitBasedTrkg_HBTracks_Cross3_ID;   //id of third cross on track
	hipo::node<uint8_t> *fHitBasedTrkg_HBTracks_q;   //charge of the track
	hipo::node<float> *fHitBasedTrkg_HBTracks_pathlength;   //pathlength of the track
	hipo::node<float> *fHitBasedTrkg_HBTracks_chi2;   //fit chi2 of the track
	hipo::node<uint16_t> *fHitBasedTrkg_HBTracks_ndf;   //fit ndf of the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBHits:reconstructed hits using DC timing information ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_TBHits_id;   //id of the hit
	hipo::node<uint16_t> *fTimeBasedTrkg_TBHits_status;   //id of the hit
	hipo::node<uint8_t> *fTimeBasedTrkg_TBHits_sector;   //DC sector
	hipo::node<uint8_t> *fTimeBasedTrkg_TBHits_superlayer;   //DC superlayer (1...6)
	hipo::node<uint8_t> *fTimeBasedTrkg_TBHits_layer;   //DC layer in superlayer (1...6)
	hipo::node<uint16_t> *fTimeBasedTrkg_TBHits_wire;   //wire id of DC
	hipo::node<uint32_t> *fTimeBasedTrkg_TBHits_TDC;   //raw time of the hit
	hipo::node<float> *fTimeBasedTrkg_TBHits_doca;   //doca of the hit calculated from TDC (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBHits_docaError;   //uncertainty on doca of the hit calculated from TDC (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBHits_trkDoca;   //track doca of the hit (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBHits_timeResidual;   //time residual of the hit (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBHits_fitResidual;   //fit residual of the hit (in cm, from KF)
	hipo::node<uint8_t> *fTimeBasedTrkg_TBHits_LR;   //Left/Right ambiguity of the hit
	hipo::node<float> *fTimeBasedTrkg_TBHits_X;   //wire x-coordinate  in tilted-sector
	hipo::node<float> *fTimeBasedTrkg_TBHits_Z;   //wire z-coordinate  in tilted-sector
	hipo::node<float> *fTimeBasedTrkg_TBHits_B;   //B-field intensity at hit position in tilted-sector system
	hipo::node<float> *fTimeBasedTrkg_TBHits_TProp;   //t propagation along the wire (ns)
	hipo::node<float> *fTimeBasedTrkg_TBHits_TFlight;   //time of flight correction (ns)
	hipo::node<float> *fTimeBasedTrkg_TBHits_T0;   //T0 (ns)
	hipo::node<float> *fTimeBasedTrkg_TBHits_TStart;   //event start time used (ns)
	hipo::node<uint16_t> *fTimeBasedTrkg_TBHits_clusterID;   //ID of associated cluster
	hipo::node<uint8_t> *fTimeBasedTrkg_TBHits_trkID;   //ID of associated track
	hipo::node<float> *fTimeBasedTrkg_TBHits_time;   //time used in tracking (ns)
	hipo::node<float> *fTimeBasedTrkg_TBHits_beta;   //beta used in tracking
	hipo::node<float> *fTimeBasedTrkg_TBHits_tBeta;   //beta-dependent time correction used in tracking
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBClusters:reconstructed clusters using DC timing information ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_id;   //id of the cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_status;   //status of the cluster
	hipo::node<uint8_t> *fTimeBasedTrkg_TBClusters_sector;   //sector of the cluster
	hipo::node<uint8_t> *fTimeBasedTrkg_TBClusters_superlayer;   //superlayer of the cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit1_ID;   //id of hit1 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit2_ID;   //id of hit2 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit3_ID;   //id of hit3 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit4_ID;   //id of hit4 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit5_ID;   //id of hit5 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit6_ID;   //id of hit6 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit7_ID;   //id of hit7 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit8_ID;   //id of hit8 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit9_ID;   //id of hit9 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit10_ID;   //id of hit10 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit11_ID;   //id of hit11 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBClusters_Hit12_ID;   //id of hit12 in cluster
	hipo::node<float> *fTimeBasedTrkg_TBClusters_avgWire;   //average wire number
	hipo::node<float> *fTimeBasedTrkg_TBClusters_fitChisqProb;   //fit chisq prob.
	hipo::node<float> *fTimeBasedTrkg_TBClusters_fitSlope;   //line fit slope
	hipo::node<float> *fTimeBasedTrkg_TBClusters_fitSlopeErr;   //error on slope
	hipo::node<float> *fTimeBasedTrkg_TBClusters_fitInterc;   //line fit intercept
	hipo::node<float> *fTimeBasedTrkg_TBClusters_fitIntercErr;   //error on the intercept
	hipo::node<uint8_t> *fTimeBasedTrkg_TBClusters_size;   //cluster size
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBSegments:reconstructed segments using DC timing information ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_id;   //id of the segment
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_status;   //status of the segment
	hipo::node<uint8_t> *fTimeBasedTrkg_TBSegments_sector;   //sector of the segment
	hipo::node<uint8_t> *fTimeBasedTrkg_TBSegments_superlayer;   //superlayer of superlayer
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Cluster_ID;   //associated cluster id
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit1_ID;   //id of hit1 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit2_ID;   //id of hit2 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit3_ID;   //id of hit3 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit4_ID;   //id of hit4 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit5_ID;   //id of hit5 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit6_ID;   //id of hit6 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit7_ID;   //id of hit7 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit8_ID;   //id of hit8 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit9_ID;   //id of hit9 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit10_ID;   //id of hit10 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit11_ID;   //id of hit11 in cluster
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegments_Hit12_ID;   //id of hit12 in cluster
	hipo::node<float> *fTimeBasedTrkg_TBSegments_avgWire;   //average wire number
	hipo::node<float> *fTimeBasedTrkg_TBSegments_fitChisqProb;   //fit chisq prob.
	hipo::node<float> *fTimeBasedTrkg_TBSegments_fitSlope;   //line fit slope
	hipo::node<float> *fTimeBasedTrkg_TBSegments_fitSlopeErr;   //error on slope
	hipo::node<float> *fTimeBasedTrkg_TBSegments_fitInterc;   //line fit intercept
	hipo::node<float> *fTimeBasedTrkg_TBSegments_fitIntercErr;   //error on the intercept
	hipo::node<float> *fTimeBasedTrkg_TBSegments_SegEndPoint1X;   //Segment 1st endpoint x coordinate in the sector coordinate system (for ced display)
	hipo::node<float> *fTimeBasedTrkg_TBSegments_SegEndPoint1Z;   //Segment 1st endpoint z coordinate in the sector coordinate system (for ced display)
	hipo::node<float> *fTimeBasedTrkg_TBSegments_SegEndPoint2X;   //Segment 2nd endpoint x coordinate in the sector coordinate system (for ced display)
	hipo::node<float> *fTimeBasedTrkg_TBSegments_SegEndPoint2Z;   //Segment 2nd endpoint z coordinate in the sector coordinate system (for ced display)
	hipo::node<float> *fTimeBasedTrkg_TBSegments_resiSum;   //sum of hit residuals
	hipo::node<float> *fTimeBasedTrkg_TBSegments_timeSum;   //sum of the hit times
	hipo::node<uint8_t> *fTimeBasedTrkg_TBSegments_size;   //size of segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBSegmentTrajectory:reconstructed segment trajectory from hit-based tracking ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegmentTrajectory_segmentID;   //id of the segment
	hipo::node<uint8_t> *fTimeBasedTrkg_TBSegmentTrajectory_sector;   //sector of DC
	hipo::node<uint8_t> *fTimeBasedTrkg_TBSegmentTrajectory_superlayer;   //superlayer
	hipo::node<uint8_t> *fTimeBasedTrkg_TBSegmentTrajectory_layer;   //layer
	hipo::node<uint16_t> *fTimeBasedTrkg_TBSegmentTrajectory_matchedHitID;   //matched hit id
	hipo::node<float> *fTimeBasedTrkg_TBSegmentTrajectory_trkDoca;   //calculated track doca
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBCrosses:reconstructed segments using DC timing information ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_TBCrosses_id;   //id of the cross
	hipo::node<uint16_t> *fTimeBasedTrkg_TBCrosses_status;   //status of the cross
	hipo::node<uint8_t> *fTimeBasedTrkg_TBCrosses_sector;   //sector of the cross
	hipo::node<uint8_t> *fTimeBasedTrkg_TBCrosses_region;   //region of the cross
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_x;   //DC track cross x-coordinate (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_y;   //DC track cross y-coordinate (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_z;   //DC track cross z-coordinate (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_err_x;   //DC track cross x-coordinate uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_err_y;   //DC track cross y-coordinate uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_err_z;   //DC track cross z-coordinate uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_ux;   //DC track cross x-direction (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_uy;   //DC track cross y-direction (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_uz;   //DC track cross z-direction (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_err_ux;   //DC track cross x-direction uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_err_uy;   //DC track cross y-direction uncertainty (in the DC tilted sector coordinate system)
	hipo::node<float> *fTimeBasedTrkg_TBCrosses_err_uz;   //DC track cross z-direction uncertainty (in the DC tilted sector coordinate system)
	hipo::node<uint16_t> *fTimeBasedTrkg_TBCrosses_Segment1_ID;   //id of first superlayer used in segment
	hipo::node<uint16_t> *fTimeBasedTrkg_TBCrosses_Segment2_ID;   //id of second superlayer used in segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBTracks:reconstructed tracks using DC timing information ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_TBTracks_id;   //id of the track
	hipo::node<uint16_t> *fTimeBasedTrkg_TBTracks_status;   //status of the track
	hipo::node<uint8_t> *fTimeBasedTrkg_TBTracks_sector;   //sector of the track
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c1_x;   //Upstream Region 1 cross x-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c1_y;   //Upstream Region 1 cross y-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c1_z;   //Upstream Region 1 cross z-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c1_ux;   //Upstream Region 1 cross unit x-direction vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c1_uy;   //Upstream Region 1 cross unit y-direction vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c1_uz;   //Upstream Region 1 cross unit z-direction vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c3_x;   //Downstream Region 3 cross x-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c3_y;   //Downstream Region 3 cross y-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c3_z;   //Downstream Region 3 cross z-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c3_ux;   //Downstream Region 3 cross unit x-direction vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c3_uy;   //Downstream Region 3 cross unit y-direction vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_c3_uz;   //Downstream Region 3 cross unit z-direction vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_t1_x;   //Upstream Region 1 track x-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_t1_y;   //Upstream Region 1 track y-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_t1_z;   //Upstream Region 1 track z-position in the lab (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_t1_px;   //Upstream Region 1 track unit x-momentum vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_t1_py;   //Upstream Region 1 track unit y-momentum vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_t1_pz;   //Upstream Region 1 track unit z-momentum vector in the lab
	hipo::node<float> *fTimeBasedTrkg_TBTracks_Vtx0_x;   //Vertex x-position of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_Vtx0_y;   //Vertex y-position of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_Vtx0_z;   //Vertex z-position of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_p0_x;   //3-momentum x-coordinate of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_p0_y;   //3-momentum y-coordinate of the swam track to the DOCA to the beamline (in cm)
	hipo::node<float> *fTimeBasedTrkg_TBTracks_p0_z;   //3-momentum z-coordinate of the swam track to the DOCA to the beamline (in cm)
	hipo::node<uint16_t> *fTimeBasedTrkg_TBTracks_Cross1_ID;   //id of first cross on track
	hipo::node<uint16_t> *fTimeBasedTrkg_TBTracks_Cross2_ID;   //id of second cross on track
	hipo::node<uint16_t> *fTimeBasedTrkg_TBTracks_Cross3_ID;   //id of third cross on track
	hipo::node<uint8_t> *fTimeBasedTrkg_TBTracks_q;   //charge of the track
	hipo::node<float> *fTimeBasedTrkg_TBTracks_pathlength;   //pathlength of the track
	hipo::node<float> *fTimeBasedTrkg_TBTracks_chi2;   //fit chi2 of the track
	hipo::node<uint16_t> *fTimeBasedTrkg_TBTracks_ndf;   //fit ndf of the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBCovMat:reconstructed track covariance matrix ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_TBCovMat_id;   //id of the track
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C11;   //C11 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C12;   //C12 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C13;   //C13 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C14;   //C14 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C15;   //C15 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C21;   //C21 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C22;   //C22 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C23;   //C23 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C24;   //C24 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C25;   //C25 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C31;   //C31 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C32;   //C32 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C33;   //C33 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C34;   //C34 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C35;   //C35 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C41;   //C41 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C42;   //C42 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C43;   //C43 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C44;   //C44 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C45;   //C45 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C51;   //C51 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C52;   //C52 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C53;   //C53 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C54;   //C54 covariance matrix element at last superlayer used in the fit
	hipo::node<float> *fTimeBasedTrkg_TBCovMat_C55;   //C55 covariance matrix element at last superlayer used in the fit
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::Trajectory:Trajectory bank ===================
	hipo::node<uint16_t> *fTimeBasedTrkg_Trajectory_tid;   //id of the track
	hipo::node<uint16_t> *fTimeBasedTrkg_Trajectory_did;   //id of the detector
	hipo::node<float> *fTimeBasedTrkg_Trajectory_x;   //track x position at detector surface (cm)
	hipo::node<float> *fTimeBasedTrkg_Trajectory_y;   //track y position at detector surface (cm)
	hipo::node<float> *fTimeBasedTrkg_Trajectory_z;   //track z position at detector surface (cm)
	hipo::node<float> *fTimeBasedTrkg_Trajectory_tx;   //track unit direction vector x component at detector surface
	hipo::node<float> *fTimeBasedTrkg_Trajectory_ty;   //track unit direction vector y component at detector surface
	hipo::node<float> *fTimeBasedTrkg_Trajectory_tz;   //track unit direction vector z component at detector surface
	hipo::node<float> *fTimeBasedTrkg_Trajectory_B;   //B-field magnitude at detector surface (cm)
	hipo::node<float> *fTimeBasedTrkg_Trajectory_L;   //pathlength of the track from the DOCA to the Beamline to the detector surface (cm)
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== HitBasedTrkg::HBHits:reconstructed hits using DC wire positions ===================
	unsigned int GetHitBasedTrkg_HBHits_id(int i){  return (unsigned int)fHitBasedTrkg_HBHits_id->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_status(int i){  return (unsigned int)fHitBasedTrkg_HBHits_status->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_sector(int i){  return (unsigned int)fHitBasedTrkg_HBHits_sector->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_superlayer(int i){  return (unsigned int)fHitBasedTrkg_HBHits_superlayer->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_layer(int i){  return (unsigned int)fHitBasedTrkg_HBHits_layer->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_wire(int i){  return (unsigned int)fHitBasedTrkg_HBHits_wire->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_TDC(int i){  return (unsigned int)fHitBasedTrkg_HBHits_TDC->getValue(i);}
	float GetHitBasedTrkg_HBHits_trkDoca(int i){  return (float)fHitBasedTrkg_HBHits_trkDoca->getValue(i);}
	float GetHitBasedTrkg_HBHits_docaError(int i){  return (float)fHitBasedTrkg_HBHits_docaError->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_LR(int i){  return (unsigned int)fHitBasedTrkg_HBHits_LR->getValue(i);}
	float GetHitBasedTrkg_HBHits_LocX(int i){  return (float)fHitBasedTrkg_HBHits_LocX->getValue(i);}
	float GetHitBasedTrkg_HBHits_LocY(int i){  return (float)fHitBasedTrkg_HBHits_LocY->getValue(i);}
	float GetHitBasedTrkg_HBHits_X(int i){  return (float)fHitBasedTrkg_HBHits_X->getValue(i);}
	float GetHitBasedTrkg_HBHits_Z(int i){  return (float)fHitBasedTrkg_HBHits_Z->getValue(i);}
	float GetHitBasedTrkg_HBHits_B(int i){  return (float)fHitBasedTrkg_HBHits_B->getValue(i);}
	float GetHitBasedTrkg_HBHits_TProp(int i){  return (float)fHitBasedTrkg_HBHits_TProp->getValue(i);}
	float GetHitBasedTrkg_HBHits_TFlight(int i){  return (float)fHitBasedTrkg_HBHits_TFlight->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_clusterID(int i){  return (unsigned int)fHitBasedTrkg_HBHits_clusterID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBHits_trkID(int i){  return (unsigned int)fHitBasedTrkg_HBHits_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBClusters:reconstructed clusters using DC wire positions ===================
	unsigned int GetHitBasedTrkg_HBClusters_id(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_id->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_status(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_status->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_sector(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_sector->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_superlayer(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_superlayer->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit1_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit1_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit2_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit2_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit3_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit3_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit4_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit4_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit5_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit5_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit6_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit6_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit7_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit7_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit8_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit8_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit9_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit9_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit10_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit10_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit11_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit11_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_Hit12_ID(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_Hit12_ID->getValue(i);}
	float GetHitBasedTrkg_HBClusters_avgWire(int i){  return (float)fHitBasedTrkg_HBClusters_avgWire->getValue(i);}
	float GetHitBasedTrkg_HBClusters_fitChisqProb(int i){  return (float)fHitBasedTrkg_HBClusters_fitChisqProb->getValue(i);}
	float GetHitBasedTrkg_HBClusters_fitSlope(int i){  return (float)fHitBasedTrkg_HBClusters_fitSlope->getValue(i);}
	float GetHitBasedTrkg_HBClusters_fitSlopeErr(int i){  return (float)fHitBasedTrkg_HBClusters_fitSlopeErr->getValue(i);}
	float GetHitBasedTrkg_HBClusters_fitInterc(int i){  return (float)fHitBasedTrkg_HBClusters_fitInterc->getValue(i);}
	float GetHitBasedTrkg_HBClusters_fitIntercErr(int i){  return (float)fHitBasedTrkg_HBClusters_fitIntercErr->getValue(i);}
	unsigned int GetHitBasedTrkg_HBClusters_size(int i){  return (unsigned int)fHitBasedTrkg_HBClusters_size->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBSegments:reconstructed segments using DC wire positions ===================
	unsigned int GetHitBasedTrkg_HBSegments_id(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_id->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_status(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_status->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_sector(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_sector->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_superlayer(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_superlayer->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Cluster_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Cluster_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit1_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit1_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit2_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit2_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit3_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit3_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit4_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit4_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit5_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit5_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit6_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit6_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit7_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit7_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit8_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit8_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit9_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit9_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit10_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit10_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit11_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit11_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_Hit12_ID(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_Hit12_ID->getValue(i);}
	float GetHitBasedTrkg_HBSegments_avgWire(int i){  return (float)fHitBasedTrkg_HBSegments_avgWire->getValue(i);}
	float GetHitBasedTrkg_HBSegments_fitChisqProb(int i){  return (float)fHitBasedTrkg_HBSegments_fitChisqProb->getValue(i);}
	float GetHitBasedTrkg_HBSegments_fitSlope(int i){  return (float)fHitBasedTrkg_HBSegments_fitSlope->getValue(i);}
	float GetHitBasedTrkg_HBSegments_fitSlopeErr(int i){  return (float)fHitBasedTrkg_HBSegments_fitSlopeErr->getValue(i);}
	float GetHitBasedTrkg_HBSegments_fitInterc(int i){  return (float)fHitBasedTrkg_HBSegments_fitInterc->getValue(i);}
	float GetHitBasedTrkg_HBSegments_fitIntercErr(int i){  return (float)fHitBasedTrkg_HBSegments_fitIntercErr->getValue(i);}
	float GetHitBasedTrkg_HBSegments_SegEndPoint1X(int i){  return (float)fHitBasedTrkg_HBSegments_SegEndPoint1X->getValue(i);}
	float GetHitBasedTrkg_HBSegments_SegEndPoint1Z(int i){  return (float)fHitBasedTrkg_HBSegments_SegEndPoint1Z->getValue(i);}
	float GetHitBasedTrkg_HBSegments_SegEndPoint2X(int i){  return (float)fHitBasedTrkg_HBSegments_SegEndPoint2X->getValue(i);}
	float GetHitBasedTrkg_HBSegments_SegEndPoint2Z(int i){  return (float)fHitBasedTrkg_HBSegments_SegEndPoint2Z->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegments_size(int i){  return (unsigned int)fHitBasedTrkg_HBSegments_size->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBSegmentTrajectory:reconstructed segment trajectory from hit-based tracking ===================
	unsigned int GetHitBasedTrkg_HBSegmentTrajectory_segmentID(int i){  return (unsigned int)fHitBasedTrkg_HBSegmentTrajectory_segmentID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegmentTrajectory_sector(int i){  return (unsigned int)fHitBasedTrkg_HBSegmentTrajectory_sector->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegmentTrajectory_superlayer(int i){  return (unsigned int)fHitBasedTrkg_HBSegmentTrajectory_superlayer->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegmentTrajectory_layer(int i){  return (unsigned int)fHitBasedTrkg_HBSegmentTrajectory_layer->getValue(i);}
	unsigned int GetHitBasedTrkg_HBSegmentTrajectory_matchedHitID(int i){  return (unsigned int)fHitBasedTrkg_HBSegmentTrajectory_matchedHitID->getValue(i);}
	float GetHitBasedTrkg_HBSegmentTrajectory_trkDoca(int i){  return (float)fHitBasedTrkg_HBSegmentTrajectory_trkDoca->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBCrosses:reconstructed segments using DC wire positions ===================
	unsigned int GetHitBasedTrkg_HBCrosses_id(int i){  return (unsigned int)fHitBasedTrkg_HBCrosses_id->getValue(i);}
	unsigned int GetHitBasedTrkg_HBCrosses_status(int i){  return (unsigned int)fHitBasedTrkg_HBCrosses_status->getValue(i);}
	unsigned int GetHitBasedTrkg_HBCrosses_sector(int i){  return (unsigned int)fHitBasedTrkg_HBCrosses_sector->getValue(i);}
	unsigned int GetHitBasedTrkg_HBCrosses_region(int i){  return (unsigned int)fHitBasedTrkg_HBCrosses_region->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_x(int i){  return (float)fHitBasedTrkg_HBCrosses_x->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_y(int i){  return (float)fHitBasedTrkg_HBCrosses_y->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_z(int i){  return (float)fHitBasedTrkg_HBCrosses_z->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_err_x(int i){  return (float)fHitBasedTrkg_HBCrosses_err_x->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_err_y(int i){  return (float)fHitBasedTrkg_HBCrosses_err_y->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_err_z(int i){  return (float)fHitBasedTrkg_HBCrosses_err_z->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_ux(int i){  return (float)fHitBasedTrkg_HBCrosses_ux->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_uy(int i){  return (float)fHitBasedTrkg_HBCrosses_uy->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_uz(int i){  return (float)fHitBasedTrkg_HBCrosses_uz->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_err_ux(int i){  return (float)fHitBasedTrkg_HBCrosses_err_ux->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_err_uy(int i){  return (float)fHitBasedTrkg_HBCrosses_err_uy->getValue(i);}
	float GetHitBasedTrkg_HBCrosses_err_uz(int i){  return (float)fHitBasedTrkg_HBCrosses_err_uz->getValue(i);}
	unsigned int GetHitBasedTrkg_HBCrosses_Segment1_ID(int i){  return (unsigned int)fHitBasedTrkg_HBCrosses_Segment1_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBCrosses_Segment2_ID(int i){  return (unsigned int)fHitBasedTrkg_HBCrosses_Segment2_ID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBTracks:reconstructed tracks using DC wire positions ===================
	unsigned int GetHitBasedTrkg_HBTracks_id(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_id->getValue(i);}
	unsigned int GetHitBasedTrkg_HBTracks_status(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_status->getValue(i);}
	unsigned int GetHitBasedTrkg_HBTracks_sector(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_sector->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c1_x(int i){  return (float)fHitBasedTrkg_HBTracks_c1_x->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c1_y(int i){  return (float)fHitBasedTrkg_HBTracks_c1_y->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c1_z(int i){  return (float)fHitBasedTrkg_HBTracks_c1_z->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c1_ux(int i){  return (float)fHitBasedTrkg_HBTracks_c1_ux->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c1_uy(int i){  return (float)fHitBasedTrkg_HBTracks_c1_uy->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c1_uz(int i){  return (float)fHitBasedTrkg_HBTracks_c1_uz->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c3_x(int i){  return (float)fHitBasedTrkg_HBTracks_c3_x->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c3_y(int i){  return (float)fHitBasedTrkg_HBTracks_c3_y->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c3_z(int i){  return (float)fHitBasedTrkg_HBTracks_c3_z->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c3_ux(int i){  return (float)fHitBasedTrkg_HBTracks_c3_ux->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c3_uy(int i){  return (float)fHitBasedTrkg_HBTracks_c3_uy->getValue(i);}
	float GetHitBasedTrkg_HBTracks_c3_uz(int i){  return (float)fHitBasedTrkg_HBTracks_c3_uz->getValue(i);}
	float GetHitBasedTrkg_HBTracks_t1_x(int i){  return (float)fHitBasedTrkg_HBTracks_t1_x->getValue(i);}
	float GetHitBasedTrkg_HBTracks_t1_y(int i){  return (float)fHitBasedTrkg_HBTracks_t1_y->getValue(i);}
	float GetHitBasedTrkg_HBTracks_t1_z(int i){  return (float)fHitBasedTrkg_HBTracks_t1_z->getValue(i);}
	float GetHitBasedTrkg_HBTracks_t1_px(int i){  return (float)fHitBasedTrkg_HBTracks_t1_px->getValue(i);}
	float GetHitBasedTrkg_HBTracks_t1_py(int i){  return (float)fHitBasedTrkg_HBTracks_t1_py->getValue(i);}
	float GetHitBasedTrkg_HBTracks_t1_pz(int i){  return (float)fHitBasedTrkg_HBTracks_t1_pz->getValue(i);}
	float GetHitBasedTrkg_HBTracks_Vtx0_x(int i){  return (float)fHitBasedTrkg_HBTracks_Vtx0_x->getValue(i);}
	float GetHitBasedTrkg_HBTracks_Vtx0_y(int i){  return (float)fHitBasedTrkg_HBTracks_Vtx0_y->getValue(i);}
	float GetHitBasedTrkg_HBTracks_Vtx0_z(int i){  return (float)fHitBasedTrkg_HBTracks_Vtx0_z->getValue(i);}
	float GetHitBasedTrkg_HBTracks_p0_x(int i){  return (float)fHitBasedTrkg_HBTracks_p0_x->getValue(i);}
	float GetHitBasedTrkg_HBTracks_p0_y(int i){  return (float)fHitBasedTrkg_HBTracks_p0_y->getValue(i);}
	float GetHitBasedTrkg_HBTracks_p0_z(int i){  return (float)fHitBasedTrkg_HBTracks_p0_z->getValue(i);}
	unsigned int GetHitBasedTrkg_HBTracks_Cross1_ID(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_Cross1_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBTracks_Cross2_ID(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_Cross2_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBTracks_Cross3_ID(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_Cross3_ID->getValue(i);}
	unsigned int GetHitBasedTrkg_HBTracks_q(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_q->getValue(i);}
	float GetHitBasedTrkg_HBTracks_pathlength(int i){  return (float)fHitBasedTrkg_HBTracks_pathlength->getValue(i);}
	float GetHitBasedTrkg_HBTracks_chi2(int i){  return (float)fHitBasedTrkg_HBTracks_chi2->getValue(i);}
	unsigned int GetHitBasedTrkg_HBTracks_ndf(int i){  return (unsigned int)fHitBasedTrkg_HBTracks_ndf->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBHits:reconstructed hits using DC timing information ===================
	unsigned int GetTimeBasedTrkg_TBHits_id(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_id->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_status(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_status->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_sector(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_sector->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_superlayer(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_superlayer->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_layer(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_layer->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_wire(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_wire->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_TDC(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_TDC->getValue(i);}
	float GetTimeBasedTrkg_TBHits_doca(int i){  return (float)fTimeBasedTrkg_TBHits_doca->getValue(i);}
	float GetTimeBasedTrkg_TBHits_docaError(int i){  return (float)fTimeBasedTrkg_TBHits_docaError->getValue(i);}
	float GetTimeBasedTrkg_TBHits_trkDoca(int i){  return (float)fTimeBasedTrkg_TBHits_trkDoca->getValue(i);}
	float GetTimeBasedTrkg_TBHits_timeResidual(int i){  return (float)fTimeBasedTrkg_TBHits_timeResidual->getValue(i);}
	float GetTimeBasedTrkg_TBHits_fitResidual(int i){  return (float)fTimeBasedTrkg_TBHits_fitResidual->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_LR(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_LR->getValue(i);}
	float GetTimeBasedTrkg_TBHits_X(int i){  return (float)fTimeBasedTrkg_TBHits_X->getValue(i);}
	float GetTimeBasedTrkg_TBHits_Z(int i){  return (float)fTimeBasedTrkg_TBHits_Z->getValue(i);}
	float GetTimeBasedTrkg_TBHits_B(int i){  return (float)fTimeBasedTrkg_TBHits_B->getValue(i);}
	float GetTimeBasedTrkg_TBHits_TProp(int i){  return (float)fTimeBasedTrkg_TBHits_TProp->getValue(i);}
	float GetTimeBasedTrkg_TBHits_TFlight(int i){  return (float)fTimeBasedTrkg_TBHits_TFlight->getValue(i);}
	float GetTimeBasedTrkg_TBHits_T0(int i){  return (float)fTimeBasedTrkg_TBHits_T0->getValue(i);}
	float GetTimeBasedTrkg_TBHits_TStart(int i){  return (float)fTimeBasedTrkg_TBHits_TStart->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_clusterID(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_clusterID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBHits_trkID(int i){  return (unsigned int)fTimeBasedTrkg_TBHits_trkID->getValue(i);}
	float GetTimeBasedTrkg_TBHits_time(int i){  return (float)fTimeBasedTrkg_TBHits_time->getValue(i);}
	float GetTimeBasedTrkg_TBHits_beta(int i){  return (float)fTimeBasedTrkg_TBHits_beta->getValue(i);}
	float GetTimeBasedTrkg_TBHits_tBeta(int i){  return (float)fTimeBasedTrkg_TBHits_tBeta->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBClusters:reconstructed clusters using DC timing information ===================
	unsigned int GetTimeBasedTrkg_TBClusters_id(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_id->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_status(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_status->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_sector(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_sector->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_superlayer(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_superlayer->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit1_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit1_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit2_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit2_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit3_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit3_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit4_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit4_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit5_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit5_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit6_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit6_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit7_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit7_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit8_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit8_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit9_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit9_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit10_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit10_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit11_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit11_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_Hit12_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_Hit12_ID->getValue(i);}
	float GetTimeBasedTrkg_TBClusters_avgWire(int i){  return (float)fTimeBasedTrkg_TBClusters_avgWire->getValue(i);}
	float GetTimeBasedTrkg_TBClusters_fitChisqProb(int i){  return (float)fTimeBasedTrkg_TBClusters_fitChisqProb->getValue(i);}
	float GetTimeBasedTrkg_TBClusters_fitSlope(int i){  return (float)fTimeBasedTrkg_TBClusters_fitSlope->getValue(i);}
	float GetTimeBasedTrkg_TBClusters_fitSlopeErr(int i){  return (float)fTimeBasedTrkg_TBClusters_fitSlopeErr->getValue(i);}
	float GetTimeBasedTrkg_TBClusters_fitInterc(int i){  return (float)fTimeBasedTrkg_TBClusters_fitInterc->getValue(i);}
	float GetTimeBasedTrkg_TBClusters_fitIntercErr(int i){  return (float)fTimeBasedTrkg_TBClusters_fitIntercErr->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBClusters_size(int i){  return (unsigned int)fTimeBasedTrkg_TBClusters_size->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBSegments:reconstructed segments using DC timing information ===================
	unsigned int GetTimeBasedTrkg_TBSegments_id(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_id->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_status(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_status->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_sector(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_sector->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_superlayer(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_superlayer->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Cluster_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Cluster_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit1_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit1_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit2_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit2_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit3_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit3_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit4_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit4_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit5_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit5_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit6_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit6_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit7_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit7_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit8_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit8_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit9_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit9_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit10_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit10_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit11_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit11_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_Hit12_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_Hit12_ID->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_avgWire(int i){  return (float)fTimeBasedTrkg_TBSegments_avgWire->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_fitChisqProb(int i){  return (float)fTimeBasedTrkg_TBSegments_fitChisqProb->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_fitSlope(int i){  return (float)fTimeBasedTrkg_TBSegments_fitSlope->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_fitSlopeErr(int i){  return (float)fTimeBasedTrkg_TBSegments_fitSlopeErr->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_fitInterc(int i){  return (float)fTimeBasedTrkg_TBSegments_fitInterc->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_fitIntercErr(int i){  return (float)fTimeBasedTrkg_TBSegments_fitIntercErr->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_SegEndPoint1X(int i){  return (float)fTimeBasedTrkg_TBSegments_SegEndPoint1X->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_SegEndPoint1Z(int i){  return (float)fTimeBasedTrkg_TBSegments_SegEndPoint1Z->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_SegEndPoint2X(int i){  return (float)fTimeBasedTrkg_TBSegments_SegEndPoint2X->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_SegEndPoint2Z(int i){  return (float)fTimeBasedTrkg_TBSegments_SegEndPoint2Z->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_resiSum(int i){  return (float)fTimeBasedTrkg_TBSegments_resiSum->getValue(i);}
	float GetTimeBasedTrkg_TBSegments_timeSum(int i){  return (float)fTimeBasedTrkg_TBSegments_timeSum->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegments_size(int i){  return (unsigned int)fTimeBasedTrkg_TBSegments_size->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBSegmentTrajectory:reconstructed segment trajectory from hit-based tracking ===================
	unsigned int GetTimeBasedTrkg_TBSegmentTrajectory_segmentID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegmentTrajectory_segmentID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegmentTrajectory_sector(int i){  return (unsigned int)fTimeBasedTrkg_TBSegmentTrajectory_sector->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegmentTrajectory_superlayer(int i){  return (unsigned int)fTimeBasedTrkg_TBSegmentTrajectory_superlayer->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegmentTrajectory_layer(int i){  return (unsigned int)fTimeBasedTrkg_TBSegmentTrajectory_layer->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBSegmentTrajectory_matchedHitID(int i){  return (unsigned int)fTimeBasedTrkg_TBSegmentTrajectory_matchedHitID->getValue(i);}
	float GetTimeBasedTrkg_TBSegmentTrajectory_trkDoca(int i){  return (float)fTimeBasedTrkg_TBSegmentTrajectory_trkDoca->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBCrosses:reconstructed segments using DC timing information ===================
	unsigned int GetTimeBasedTrkg_TBCrosses_id(int i){  return (unsigned int)fTimeBasedTrkg_TBCrosses_id->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBCrosses_status(int i){  return (unsigned int)fTimeBasedTrkg_TBCrosses_status->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBCrosses_sector(int i){  return (unsigned int)fTimeBasedTrkg_TBCrosses_sector->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBCrosses_region(int i){  return (unsigned int)fTimeBasedTrkg_TBCrosses_region->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_x(int i){  return (float)fTimeBasedTrkg_TBCrosses_x->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_y(int i){  return (float)fTimeBasedTrkg_TBCrosses_y->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_z(int i){  return (float)fTimeBasedTrkg_TBCrosses_z->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_err_x(int i){  return (float)fTimeBasedTrkg_TBCrosses_err_x->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_err_y(int i){  return (float)fTimeBasedTrkg_TBCrosses_err_y->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_err_z(int i){  return (float)fTimeBasedTrkg_TBCrosses_err_z->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_ux(int i){  return (float)fTimeBasedTrkg_TBCrosses_ux->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_uy(int i){  return (float)fTimeBasedTrkg_TBCrosses_uy->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_uz(int i){  return (float)fTimeBasedTrkg_TBCrosses_uz->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_err_ux(int i){  return (float)fTimeBasedTrkg_TBCrosses_err_ux->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_err_uy(int i){  return (float)fTimeBasedTrkg_TBCrosses_err_uy->getValue(i);}
	float GetTimeBasedTrkg_TBCrosses_err_uz(int i){  return (float)fTimeBasedTrkg_TBCrosses_err_uz->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBCrosses_Segment1_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBCrosses_Segment1_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBCrosses_Segment2_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBCrosses_Segment2_ID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBTracks:reconstructed tracks using DC timing information ===================
	unsigned int GetTimeBasedTrkg_TBTracks_id(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_id->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBTracks_status(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_status->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBTracks_sector(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_sector->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c1_x(int i){  return (float)fTimeBasedTrkg_TBTracks_c1_x->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c1_y(int i){  return (float)fTimeBasedTrkg_TBTracks_c1_y->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c1_z(int i){  return (float)fTimeBasedTrkg_TBTracks_c1_z->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c1_ux(int i){  return (float)fTimeBasedTrkg_TBTracks_c1_ux->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c1_uy(int i){  return (float)fTimeBasedTrkg_TBTracks_c1_uy->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c1_uz(int i){  return (float)fTimeBasedTrkg_TBTracks_c1_uz->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c3_x(int i){  return (float)fTimeBasedTrkg_TBTracks_c3_x->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c3_y(int i){  return (float)fTimeBasedTrkg_TBTracks_c3_y->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c3_z(int i){  return (float)fTimeBasedTrkg_TBTracks_c3_z->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c3_ux(int i){  return (float)fTimeBasedTrkg_TBTracks_c3_ux->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c3_uy(int i){  return (float)fTimeBasedTrkg_TBTracks_c3_uy->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_c3_uz(int i){  return (float)fTimeBasedTrkg_TBTracks_c3_uz->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_t1_x(int i){  return (float)fTimeBasedTrkg_TBTracks_t1_x->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_t1_y(int i){  return (float)fTimeBasedTrkg_TBTracks_t1_y->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_t1_z(int i){  return (float)fTimeBasedTrkg_TBTracks_t1_z->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_t1_px(int i){  return (float)fTimeBasedTrkg_TBTracks_t1_px->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_t1_py(int i){  return (float)fTimeBasedTrkg_TBTracks_t1_py->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_t1_pz(int i){  return (float)fTimeBasedTrkg_TBTracks_t1_pz->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_Vtx0_x(int i){  return (float)fTimeBasedTrkg_TBTracks_Vtx0_x->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_Vtx0_y(int i){  return (float)fTimeBasedTrkg_TBTracks_Vtx0_y->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_Vtx0_z(int i){  return (float)fTimeBasedTrkg_TBTracks_Vtx0_z->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_p0_x(int i){  return (float)fTimeBasedTrkg_TBTracks_p0_x->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_p0_y(int i){  return (float)fTimeBasedTrkg_TBTracks_p0_y->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_p0_z(int i){  return (float)fTimeBasedTrkg_TBTracks_p0_z->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBTracks_Cross1_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_Cross1_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBTracks_Cross2_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_Cross2_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBTracks_Cross3_ID(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_Cross3_ID->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBTracks_q(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_q->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_pathlength(int i){  return (float)fTimeBasedTrkg_TBTracks_pathlength->getValue(i);}
	float GetTimeBasedTrkg_TBTracks_chi2(int i){  return (float)fTimeBasedTrkg_TBTracks_chi2->getValue(i);}
	unsigned int GetTimeBasedTrkg_TBTracks_ndf(int i){  return (unsigned int)fTimeBasedTrkg_TBTracks_ndf->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBCovMat:reconstructed track covariance matrix ===================
	unsigned int GetTimeBasedTrkg_TBCovMat_id(int i){  return (unsigned int)fTimeBasedTrkg_TBCovMat_id->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C11(int i){  return (float)fTimeBasedTrkg_TBCovMat_C11->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C12(int i){  return (float)fTimeBasedTrkg_TBCovMat_C12->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C13(int i){  return (float)fTimeBasedTrkg_TBCovMat_C13->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C14(int i){  return (float)fTimeBasedTrkg_TBCovMat_C14->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C15(int i){  return (float)fTimeBasedTrkg_TBCovMat_C15->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C21(int i){  return (float)fTimeBasedTrkg_TBCovMat_C21->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C22(int i){  return (float)fTimeBasedTrkg_TBCovMat_C22->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C23(int i){  return (float)fTimeBasedTrkg_TBCovMat_C23->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C24(int i){  return (float)fTimeBasedTrkg_TBCovMat_C24->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C25(int i){  return (float)fTimeBasedTrkg_TBCovMat_C25->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C31(int i){  return (float)fTimeBasedTrkg_TBCovMat_C31->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C32(int i){  return (float)fTimeBasedTrkg_TBCovMat_C32->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C33(int i){  return (float)fTimeBasedTrkg_TBCovMat_C33->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C34(int i){  return (float)fTimeBasedTrkg_TBCovMat_C34->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C35(int i){  return (float)fTimeBasedTrkg_TBCovMat_C35->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C41(int i){  return (float)fTimeBasedTrkg_TBCovMat_C41->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C42(int i){  return (float)fTimeBasedTrkg_TBCovMat_C42->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C43(int i){  return (float)fTimeBasedTrkg_TBCovMat_C43->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C44(int i){  return (float)fTimeBasedTrkg_TBCovMat_C44->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C45(int i){  return (float)fTimeBasedTrkg_TBCovMat_C45->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C51(int i){  return (float)fTimeBasedTrkg_TBCovMat_C51->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C52(int i){  return (float)fTimeBasedTrkg_TBCovMat_C52->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C53(int i){  return (float)fTimeBasedTrkg_TBCovMat_C53->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C54(int i){  return (float)fTimeBasedTrkg_TBCovMat_C54->getValue(i);}
	float GetTimeBasedTrkg_TBCovMat_C55(int i){  return (float)fTimeBasedTrkg_TBCovMat_C55->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::Trajectory:Trajectory bank ===================
	unsigned int GetTimeBasedTrkg_Trajectory_tid(int i){  return (unsigned int)fTimeBasedTrkg_Trajectory_tid->getValue(i);}
	unsigned int GetTimeBasedTrkg_Trajectory_did(int i){  return (unsigned int)fTimeBasedTrkg_Trajectory_did->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_x(int i){  return (float)fTimeBasedTrkg_Trajectory_x->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_y(int i){  return (float)fTimeBasedTrkg_Trajectory_y->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_z(int i){  return (float)fTimeBasedTrkg_Trajectory_z->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_tx(int i){  return (float)fTimeBasedTrkg_Trajectory_tx->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_ty(int i){  return (float)fTimeBasedTrkg_Trajectory_ty->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_tz(int i){  return (float)fTimeBasedTrkg_Trajectory_tz->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_B(int i){  return (float)fTimeBasedTrkg_Trajectory_B->getValue(i);}
	float GetTimeBasedTrkg_Trajectory_L(int i){  return (float)fTimeBasedTrkg_Trajectory_L->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorDC,0)
};
#endif




