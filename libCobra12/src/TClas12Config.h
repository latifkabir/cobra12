// Filename: TClas12Config.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Fri Jul 27 00:19:40 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12CONFIG_H
#define TCLAS12CONFIG_H

#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cassert>
#include "TObject.h"

using namespace std;

class TClas12Config : public TObject
{
    //-----Buffer for each configuration field to be read goes here------
    string fConfigFile;
    string fDataPath;
    string fResultsPath;
    string fCobra12Home;
    string fFilePattern;
    string fVaultPath;
    
    void CheckValidity();    
public:
    TClas12Config(string file );
    TClas12Config();
    ~TClas12Config();
    void LoadConfig();
    void Print();

    //----- Getter for each configuration field -------------
    const string & GetCobra12Home();
    const string & GetConfigPath();
    const string & GetDataPath();
    const string & GetResultsPath();
    const string & GetVaultPath();
    const string & GetFilePattern();
    
    ClassDef(TClas12Config,0)
};

#endif
