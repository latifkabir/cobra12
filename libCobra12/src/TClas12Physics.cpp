// Filename: TClas12Reaction.cpp
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat May 12 18:05:00 2018 (-0400)
// URL: jlab.org/~latif

/*


Instantiate the object with beam energy.

Add primary outgoing particle e-  4-momentum (Lorentz Vector).

Add final state outgoing particle-1 4-momentum (Lorentz Vector).
Add final state outgoing particle-2 4-momentum (Lorentz Vector).
Add final state outgoing particle-2 4-momentum (Lorentz Vector).
.
.
.
Add final state outgoing particle-N 4-momentum (Lorentz Vector).



Getter functions for:
Calculate Q^2
Calculate W
Calculate y
Calculate z
Calculate Invariant mass of any two pair of particles.
Calculate missing mass 

See my notes on basic quantities for additional calculable quantities.

 */
