// Filename: TClas12DetectorDC.cc
// Description: CLAS12 DC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorDC.h"


ClassImp(TClas12DetectorDC)
//-------------------------------------------------------------------------------------
TClas12DetectorDC::TClas12DetectorDC(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorDC::~TClas12DetectorDC()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorDC::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorDC::SetBranches()
{
//================== HitBasedTrkg::HBHits:reconstructed hits using DC wire positions ===================
	fHitBasedTrkg_HBHits_id = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBHits", "id"); 	 //id of the hit
	fHitBasedTrkg_HBHits_status = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBHits", "status"); 	 //id of the hit
	fHitBasedTrkg_HBHits_sector = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBHits", "sector"); 	 //DC sector
	fHitBasedTrkg_HBHits_superlayer = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBHits", "superlayer"); 	 //DC superlayer (1...6)
	fHitBasedTrkg_HBHits_layer = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBHits", "layer"); 	 //DC layer in superlayer (1...6)
	fHitBasedTrkg_HBHits_wire = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBHits", "wire"); 	 //wire id of DC
	fHitBasedTrkg_HBHits_TDC = fHipoReader->getBranch<uint32_t>("HitBasedTrkg::HBHits", "TDC"); 	 //raw time of the hit
	fHitBasedTrkg_HBHits_trkDoca = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "trkDoca"); 	 //track doca of the hit (in cm)
	fHitBasedTrkg_HBHits_docaError = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "docaError"); 	 //error on track doca of the hit
	fHitBasedTrkg_HBHits_LR = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBHits", "LR"); 	 //Left/Right ambiguity of the hit
	fHitBasedTrkg_HBHits_LocX = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "LocX"); 	 //x in planar local coordinate system
	fHitBasedTrkg_HBHits_LocY = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "LocY"); 	 //y in planar local coordinate system
	fHitBasedTrkg_HBHits_X = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "X"); 	 //wire x-coordinate  in tilted-sector
	fHitBasedTrkg_HBHits_Z = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "Z"); 	 //wire z-coordinate  in tilted-sector
	fHitBasedTrkg_HBHits_B = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "B"); 	 //B-field intensity at hit position in tilted-sector system
	fHitBasedTrkg_HBHits_TProp = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "TProp"); 	 //t propagation along the wire
	fHitBasedTrkg_HBHits_TFlight = fHipoReader->getBranch<float>("HitBasedTrkg::HBHits", "TFlight"); 	 //time of flight correction
	fHitBasedTrkg_HBHits_clusterID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBHits", "clusterID"); 	 //ID of associated cluster
	fHitBasedTrkg_HBHits_trkID = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBHits", "trkID"); 	 //ID of associated track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBClusters:reconstructed clusters using DC wire positions ===================
	fHitBasedTrkg_HBClusters_id = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "id"); 	 //id of the cluster
	fHitBasedTrkg_HBClusters_status = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "status"); 	 //status of the cluster
	fHitBasedTrkg_HBClusters_sector = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBClusters", "sector"); 	 //sector of the cluster
	fHitBasedTrkg_HBClusters_superlayer = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBClusters", "superlayer"); 	 //superlayer of the cluster
	fHitBasedTrkg_HBClusters_Hit1_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit1_ID"); 	 //id of hit1 in cluster
	fHitBasedTrkg_HBClusters_Hit2_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit2_ID"); 	 //id of hit2 in cluster
	fHitBasedTrkg_HBClusters_Hit3_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit3_ID"); 	 //id of hit3 in cluster
	fHitBasedTrkg_HBClusters_Hit4_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit4_ID"); 	 //id of hit4 in cluster
	fHitBasedTrkg_HBClusters_Hit5_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit5_ID"); 	 //id of hit5 in cluster
	fHitBasedTrkg_HBClusters_Hit6_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit6_ID"); 	 //id of hit6 in cluster
	fHitBasedTrkg_HBClusters_Hit7_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit7_ID"); 	 //id of hit7 in cluster
	fHitBasedTrkg_HBClusters_Hit8_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit8_ID"); 	 //id of hit8 in cluster
	fHitBasedTrkg_HBClusters_Hit9_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit9_ID"); 	 //id of hit9 in cluster
	fHitBasedTrkg_HBClusters_Hit10_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit10_ID"); 	 //id of hit10 in cluster
	fHitBasedTrkg_HBClusters_Hit11_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit11_ID"); 	 //id of hit11 in cluster
	fHitBasedTrkg_HBClusters_Hit12_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBClusters", "Hit12_ID"); 	 //id of hit12 in cluster
	fHitBasedTrkg_HBClusters_avgWire = fHipoReader->getBranch<float>("HitBasedTrkg::HBClusters", "avgWire"); 	 //average wire number
	fHitBasedTrkg_HBClusters_fitChisqProb = fHipoReader->getBranch<float>("HitBasedTrkg::HBClusters", "fitChisqProb"); 	 //fit chisq prob.
	fHitBasedTrkg_HBClusters_fitSlope = fHipoReader->getBranch<float>("HitBasedTrkg::HBClusters", "fitSlope"); 	 //line fit slope
	fHitBasedTrkg_HBClusters_fitSlopeErr = fHipoReader->getBranch<float>("HitBasedTrkg::HBClusters", "fitSlopeErr"); 	 //error on slope
	fHitBasedTrkg_HBClusters_fitInterc = fHipoReader->getBranch<float>("HitBasedTrkg::HBClusters", "fitInterc"); 	 //line fit intercept
	fHitBasedTrkg_HBClusters_fitIntercErr = fHipoReader->getBranch<float>("HitBasedTrkg::HBClusters", "fitIntercErr"); 	 //error on the intercept
	fHitBasedTrkg_HBClusters_size = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBClusters", "size"); 	 //cluster size
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBSegments:reconstructed segments using DC wire positions ===================
	fHitBasedTrkg_HBSegments_id = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "id"); 	 //id of the segment
	fHitBasedTrkg_HBSegments_status = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "status"); 	 //status of the segment
	fHitBasedTrkg_HBSegments_sector = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBSegments", "sector"); 	 //sector of the segment
	fHitBasedTrkg_HBSegments_superlayer = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBSegments", "superlayer"); 	 //superlayer of superlayer
	fHitBasedTrkg_HBSegments_Cluster_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Cluster_ID"); 	 //associated cluster id
	fHitBasedTrkg_HBSegments_Hit1_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit1_ID"); 	 //id of hit1 in cluster
	fHitBasedTrkg_HBSegments_Hit2_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit2_ID"); 	 //id of hit2 in cluster
	fHitBasedTrkg_HBSegments_Hit3_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit3_ID"); 	 //id of hit3 in cluster
	fHitBasedTrkg_HBSegments_Hit4_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit4_ID"); 	 //id of hit4 in cluster
	fHitBasedTrkg_HBSegments_Hit5_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit5_ID"); 	 //id of hit5 in cluster
	fHitBasedTrkg_HBSegments_Hit6_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit6_ID"); 	 //id of hit6 in cluster
	fHitBasedTrkg_HBSegments_Hit7_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit7_ID"); 	 //id of hit7 in cluster
	fHitBasedTrkg_HBSegments_Hit8_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit8_ID"); 	 //id of hit8 in cluster
	fHitBasedTrkg_HBSegments_Hit9_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit9_ID"); 	 //id of hit9 in cluster
	fHitBasedTrkg_HBSegments_Hit10_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit10_ID"); 	 //id of hit10 in cluster
	fHitBasedTrkg_HBSegments_Hit11_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit11_ID"); 	 //id of hit11 in cluster
	fHitBasedTrkg_HBSegments_Hit12_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegments", "Hit12_ID"); 	 //id of hit12 in cluster
	fHitBasedTrkg_HBSegments_avgWire = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "avgWire"); 	 //average wire number
	fHitBasedTrkg_HBSegments_fitChisqProb = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "fitChisqProb"); 	 //fit chisq prob.
	fHitBasedTrkg_HBSegments_fitSlope = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "fitSlope"); 	 //line fit slope
	fHitBasedTrkg_HBSegments_fitSlopeErr = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "fitSlopeErr"); 	 //error on slope
	fHitBasedTrkg_HBSegments_fitInterc = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "fitInterc"); 	 //line fit intercept
	fHitBasedTrkg_HBSegments_fitIntercErr = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "fitIntercErr"); 	 //error on the intercept
	fHitBasedTrkg_HBSegments_SegEndPoint1X = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "SegEndPoint1X"); 	 //Segment 1st endpoint x coordinate in the sector coordinate system (for ced display)
	fHitBasedTrkg_HBSegments_SegEndPoint1Z = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "SegEndPoint1Z"); 	 //Segment 1st endpoint z coordinate in the sector coordinate system (for ced display)
	fHitBasedTrkg_HBSegments_SegEndPoint2X = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "SegEndPoint2X"); 	 //Segment 2nd endpoint x coordinate in the sector coordinate system (for ced display)
	fHitBasedTrkg_HBSegments_SegEndPoint2Z = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegments", "SegEndPoint2Z"); 	 //Segment 2nd endpoint z coordinate in the sector coordinate system (for ced display)
	fHitBasedTrkg_HBSegments_size = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBSegments", "size"); 	 //size of segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBSegmentTrajectory:reconstructed segment trajectory from hit-based tracking ===================
	fHitBasedTrkg_HBSegmentTrajectory_segmentID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegmentTrajectory", "segmentID"); 	 //id of the segment
	fHitBasedTrkg_HBSegmentTrajectory_sector = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBSegmentTrajectory", "sector"); 	 //sector of DC
	fHitBasedTrkg_HBSegmentTrajectory_superlayer = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBSegmentTrajectory", "superlayer"); 	 //superlayer
	fHitBasedTrkg_HBSegmentTrajectory_layer = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBSegmentTrajectory", "layer"); 	 //layer
	fHitBasedTrkg_HBSegmentTrajectory_matchedHitID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBSegmentTrajectory", "matchedHitID"); 	 //matched hit id
	fHitBasedTrkg_HBSegmentTrajectory_trkDoca = fHipoReader->getBranch<float>("HitBasedTrkg::HBSegmentTrajectory", "trkDoca"); 	 //calculated track doca
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBCrosses:reconstructed segments using DC wire positions ===================
	fHitBasedTrkg_HBCrosses_id = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBCrosses", "id"); 	 //id of the cross
	fHitBasedTrkg_HBCrosses_status = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBCrosses", "status"); 	 //status of the cross
	fHitBasedTrkg_HBCrosses_sector = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBCrosses", "sector"); 	 //sector of the cross
	fHitBasedTrkg_HBCrosses_region = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBCrosses", "region"); 	 //region of the cross
	fHitBasedTrkg_HBCrosses_x = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "x"); 	 //DC track cross x-coordinate (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_y = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "y"); 	 //DC track cross y-coordinate (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_z = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "z"); 	 //DC track cross z-coordinate (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_err_x = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "err_x"); 	 //DC track cross x-coordinate uncertainty (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_err_y = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "err_y"); 	 //DC track cross y-coordinate uncertainty (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_err_z = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "err_z"); 	 //DC track cross z-coordinate uncertainty (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_ux = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "ux"); 	 //DC track cross x-direction (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_uy = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "uy"); 	 //DC track cross y-direction (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_uz = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "uz"); 	 //DC track cross z-direction (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_err_ux = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "err_ux"); 	 //DC track cross x-direction uncertainty (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_err_uy = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "err_uy"); 	 //DC track cross y-direction uncertainty (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_err_uz = fHipoReader->getBranch<float>("HitBasedTrkg::HBCrosses", "err_uz"); 	 //DC track cross z-direction uncertainty (in the DC tilted sector coordinate system)
	fHitBasedTrkg_HBCrosses_Segment1_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBCrosses", "Segment1_ID"); 	 //id of first superlater used in segment
	fHitBasedTrkg_HBCrosses_Segment2_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBCrosses", "Segment2_ID"); 	 //id of second superlater used in segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== HitBasedTrkg::HBTracks:reconstructed tracks using DC wire positions ===================
	fHitBasedTrkg_HBTracks_id = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBTracks", "id"); 	 //id of the track
	fHitBasedTrkg_HBTracks_status = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBTracks", "status"); 	 //status of the track
	fHitBasedTrkg_HBTracks_sector = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBTracks", "sector"); 	 //sector of the track
	fHitBasedTrkg_HBTracks_c1_x = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c1_x"); 	 //Upstream Region 1 cross x-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_c1_y = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c1_y"); 	 //Upstream Region 1 cross y-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_c1_z = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c1_z"); 	 //Upstream Region 1 cross z-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_c1_ux = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c1_ux"); 	 //Upstream Region 1 cross unit x-direction vector in the lab
	fHitBasedTrkg_HBTracks_c1_uy = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c1_uy"); 	 //Upstream Region 1 cross unit y-direction vector in the lab
	fHitBasedTrkg_HBTracks_c1_uz = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c1_uz"); 	 //Upstream Region 1 cross unit z-direction vector in the lab
	fHitBasedTrkg_HBTracks_c3_x = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c3_x"); 	 //Downstream Region 3 cross x-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_c3_y = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c3_y"); 	 //Downstream Region 3 cross y-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_c3_z = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c3_z"); 	 //Downstream Region 3 cross z-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_c3_ux = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c3_ux"); 	 //Downstream Region 3 cross unit x-direction vector in the lab
	fHitBasedTrkg_HBTracks_c3_uy = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c3_uy"); 	 //Downstream Region 3 cross unit y-direction vector in the lab
	fHitBasedTrkg_HBTracks_c3_uz = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "c3_uz"); 	 //Downstream Region 3 cross unit z-direction vector in the lab
	fHitBasedTrkg_HBTracks_t1_x = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "t1_x"); 	 //Upstream Region 1 track x-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_t1_y = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "t1_y"); 	 //Upstream Region 1 track y-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_t1_z = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "t1_z"); 	 //Upstream Region 1 track z-position in the lab (in cm)
	fHitBasedTrkg_HBTracks_t1_px = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "t1_px"); 	 //Upstream Region 1 track unit x-momentum vector in the lab
	fHitBasedTrkg_HBTracks_t1_py = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "t1_py"); 	 //Upstream Region 1 track unit y-momentum vector in the lab
	fHitBasedTrkg_HBTracks_t1_pz = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "t1_pz"); 	 //Upstream Region 1 track unit z-momentum vector in the lab
	fHitBasedTrkg_HBTracks_Vtx0_x = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "Vtx0_x"); 	 //Vertex x-position of the swam track to the DOCA to the beamline (in cm)
	fHitBasedTrkg_HBTracks_Vtx0_y = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "Vtx0_y"); 	 //Vertex y-position of the swam track to the DOCA to the beamline (in cm)
	fHitBasedTrkg_HBTracks_Vtx0_z = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "Vtx0_z"); 	 //Vertex z-position of the swam track to the DOCA to the beamline (in cm)
	fHitBasedTrkg_HBTracks_p0_x = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "p0_x"); 	 //3-momentum x-coordinate of the swam track to the DOCA to the beamline (in cm)
	fHitBasedTrkg_HBTracks_p0_y = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "p0_y"); 	 //3-momentum y-coordinate of the swam track to the DOCA to the beamline (in cm)
	fHitBasedTrkg_HBTracks_p0_z = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "p0_z"); 	 //3-momentum z-coordinate of the swam track to the DOCA to the beamline (in cm)
	fHitBasedTrkg_HBTracks_Cross1_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBTracks", "Cross1_ID"); 	 //id of first cross on track
	fHitBasedTrkg_HBTracks_Cross2_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBTracks", "Cross2_ID"); 	 //id of second cross on track
	fHitBasedTrkg_HBTracks_Cross3_ID = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBTracks", "Cross3_ID"); 	 //id of third cross on track
	fHitBasedTrkg_HBTracks_q = fHipoReader->getBranch<uint8_t>("HitBasedTrkg::HBTracks", "q"); 	 //charge of the track
	fHitBasedTrkg_HBTracks_pathlength = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "pathlength"); 	 //pathlength of the track
	fHitBasedTrkg_HBTracks_chi2 = fHipoReader->getBranch<float>("HitBasedTrkg::HBTracks", "chi2"); 	 //fit chi2 of the track
	fHitBasedTrkg_HBTracks_ndf = fHipoReader->getBranch<uint16_t>("HitBasedTrkg::HBTracks", "ndf"); 	 //fit ndf of the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBHits:reconstructed hits using DC timing information ===================
	fTimeBasedTrkg_TBHits_id = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBHits", "id"); 	 //id of the hit
	fTimeBasedTrkg_TBHits_status = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBHits", "status"); 	 //id of the hit
	fTimeBasedTrkg_TBHits_sector = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBHits", "sector"); 	 //DC sector
	fTimeBasedTrkg_TBHits_superlayer = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBHits", "superlayer"); 	 //DC superlayer (1...6)
	fTimeBasedTrkg_TBHits_layer = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBHits", "layer"); 	 //DC layer in superlayer (1...6)
	fTimeBasedTrkg_TBHits_wire = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBHits", "wire"); 	 //wire id of DC
	fTimeBasedTrkg_TBHits_TDC = fHipoReader->getBranch<uint32_t>("TimeBasedTrkg::TBHits", "TDC"); 	 //raw time of the hit
	fTimeBasedTrkg_TBHits_doca = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "doca"); 	 //doca of the hit calculated from TDC (in cm)
	fTimeBasedTrkg_TBHits_docaError = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "docaError"); 	 //uncertainty on doca of the hit calculated from TDC (in cm)
	fTimeBasedTrkg_TBHits_trkDoca = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "trkDoca"); 	 //track doca of the hit (in cm)
	fTimeBasedTrkg_TBHits_timeResidual = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "timeResidual"); 	 //time residual of the hit (in cm)
	fTimeBasedTrkg_TBHits_fitResidual = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "fitResidual"); 	 //fit residual of the hit (in cm, from KF)
	fTimeBasedTrkg_TBHits_LR = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBHits", "LR"); 	 //Left/Right ambiguity of the hit
	fTimeBasedTrkg_TBHits_X = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "X"); 	 //wire x-coordinate  in tilted-sector
	fTimeBasedTrkg_TBHits_Z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "Z"); 	 //wire z-coordinate  in tilted-sector
	fTimeBasedTrkg_TBHits_B = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "B"); 	 //B-field intensity at hit position in tilted-sector system
	fTimeBasedTrkg_TBHits_TProp = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "TProp"); 	 //t propagation along the wire (ns)
	fTimeBasedTrkg_TBHits_TFlight = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "TFlight"); 	 //time of flight correction (ns)
	fTimeBasedTrkg_TBHits_T0 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "T0"); 	 //T0 (ns)
	fTimeBasedTrkg_TBHits_TStart = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "TStart"); 	 //event start time used (ns)
	fTimeBasedTrkg_TBHits_clusterID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBHits", "clusterID"); 	 //ID of associated cluster
	fTimeBasedTrkg_TBHits_trkID = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBHits", "trkID"); 	 //ID of associated track
	fTimeBasedTrkg_TBHits_time = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "time"); 	 //time used in tracking (ns)
	fTimeBasedTrkg_TBHits_beta = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "beta"); 	 //beta used in tracking
	fTimeBasedTrkg_TBHits_tBeta = fHipoReader->getBranch<float>("TimeBasedTrkg::TBHits", "tBeta"); 	 //beta-dependent time correction used in tracking
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBClusters:reconstructed clusters using DC timing information ===================
	fTimeBasedTrkg_TBClusters_id = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "id"); 	 //id of the cluster
	fTimeBasedTrkg_TBClusters_status = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "status"); 	 //status of the cluster
	fTimeBasedTrkg_TBClusters_sector = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBClusters", "sector"); 	 //sector of the cluster
	fTimeBasedTrkg_TBClusters_superlayer = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBClusters", "superlayer"); 	 //superlayer of the cluster
	fTimeBasedTrkg_TBClusters_Hit1_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit1_ID"); 	 //id of hit1 in cluster
	fTimeBasedTrkg_TBClusters_Hit2_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit2_ID"); 	 //id of hit2 in cluster
	fTimeBasedTrkg_TBClusters_Hit3_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit3_ID"); 	 //id of hit3 in cluster
	fTimeBasedTrkg_TBClusters_Hit4_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit4_ID"); 	 //id of hit4 in cluster
	fTimeBasedTrkg_TBClusters_Hit5_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit5_ID"); 	 //id of hit5 in cluster
	fTimeBasedTrkg_TBClusters_Hit6_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit6_ID"); 	 //id of hit6 in cluster
	fTimeBasedTrkg_TBClusters_Hit7_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit7_ID"); 	 //id of hit7 in cluster
	fTimeBasedTrkg_TBClusters_Hit8_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit8_ID"); 	 //id of hit8 in cluster
	fTimeBasedTrkg_TBClusters_Hit9_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit9_ID"); 	 //id of hit9 in cluster
	fTimeBasedTrkg_TBClusters_Hit10_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit10_ID"); 	 //id of hit10 in cluster
	fTimeBasedTrkg_TBClusters_Hit11_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit11_ID"); 	 //id of hit11 in cluster
	fTimeBasedTrkg_TBClusters_Hit12_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBClusters", "Hit12_ID"); 	 //id of hit12 in cluster
	fTimeBasedTrkg_TBClusters_avgWire = fHipoReader->getBranch<float>("TimeBasedTrkg::TBClusters", "avgWire"); 	 //average wire number
	fTimeBasedTrkg_TBClusters_fitChisqProb = fHipoReader->getBranch<float>("TimeBasedTrkg::TBClusters", "fitChisqProb"); 	 //fit chisq prob.
	fTimeBasedTrkg_TBClusters_fitSlope = fHipoReader->getBranch<float>("TimeBasedTrkg::TBClusters", "fitSlope"); 	 //line fit slope
	fTimeBasedTrkg_TBClusters_fitSlopeErr = fHipoReader->getBranch<float>("TimeBasedTrkg::TBClusters", "fitSlopeErr"); 	 //error on slope
	fTimeBasedTrkg_TBClusters_fitInterc = fHipoReader->getBranch<float>("TimeBasedTrkg::TBClusters", "fitInterc"); 	 //line fit intercept
	fTimeBasedTrkg_TBClusters_fitIntercErr = fHipoReader->getBranch<float>("TimeBasedTrkg::TBClusters", "fitIntercErr"); 	 //error on the intercept
	fTimeBasedTrkg_TBClusters_size = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBClusters", "size"); 	 //cluster size
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBSegments:reconstructed segments using DC timing information ===================
	fTimeBasedTrkg_TBSegments_id = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "id"); 	 //id of the segment
	fTimeBasedTrkg_TBSegments_status = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "status"); 	 //status of the segment
	fTimeBasedTrkg_TBSegments_sector = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBSegments", "sector"); 	 //sector of the segment
	fTimeBasedTrkg_TBSegments_superlayer = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBSegments", "superlayer"); 	 //superlayer of superlayer
	fTimeBasedTrkg_TBSegments_Cluster_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Cluster_ID"); 	 //associated cluster id
	fTimeBasedTrkg_TBSegments_Hit1_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit1_ID"); 	 //id of hit1 in cluster
	fTimeBasedTrkg_TBSegments_Hit2_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit2_ID"); 	 //id of hit2 in cluster
	fTimeBasedTrkg_TBSegments_Hit3_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit3_ID"); 	 //id of hit3 in cluster
	fTimeBasedTrkg_TBSegments_Hit4_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit4_ID"); 	 //id of hit4 in cluster
	fTimeBasedTrkg_TBSegments_Hit5_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit5_ID"); 	 //id of hit5 in cluster
	fTimeBasedTrkg_TBSegments_Hit6_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit6_ID"); 	 //id of hit6 in cluster
	fTimeBasedTrkg_TBSegments_Hit7_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit7_ID"); 	 //id of hit7 in cluster
	fTimeBasedTrkg_TBSegments_Hit8_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit8_ID"); 	 //id of hit8 in cluster
	fTimeBasedTrkg_TBSegments_Hit9_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit9_ID"); 	 //id of hit9 in cluster
	fTimeBasedTrkg_TBSegments_Hit10_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit10_ID"); 	 //id of hit10 in cluster
	fTimeBasedTrkg_TBSegments_Hit11_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit11_ID"); 	 //id of hit11 in cluster
	fTimeBasedTrkg_TBSegments_Hit12_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegments", "Hit12_ID"); 	 //id of hit12 in cluster
	fTimeBasedTrkg_TBSegments_avgWire = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "avgWire"); 	 //average wire number
	fTimeBasedTrkg_TBSegments_fitChisqProb = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "fitChisqProb"); 	 //fit chisq prob.
	fTimeBasedTrkg_TBSegments_fitSlope = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "fitSlope"); 	 //line fit slope
	fTimeBasedTrkg_TBSegments_fitSlopeErr = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "fitSlopeErr"); 	 //error on slope
	fTimeBasedTrkg_TBSegments_fitInterc = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "fitInterc"); 	 //line fit intercept
	fTimeBasedTrkg_TBSegments_fitIntercErr = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "fitIntercErr"); 	 //error on the intercept
	fTimeBasedTrkg_TBSegments_SegEndPoint1X = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "SegEndPoint1X"); 	 //Segment 1st endpoint x coordinate in the sector coordinate system (for ced display)
	fTimeBasedTrkg_TBSegments_SegEndPoint1Z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "SegEndPoint1Z"); 	 //Segment 1st endpoint z coordinate in the sector coordinate system (for ced display)
	fTimeBasedTrkg_TBSegments_SegEndPoint2X = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "SegEndPoint2X"); 	 //Segment 2nd endpoint x coordinate in the sector coordinate system (for ced display)
	fTimeBasedTrkg_TBSegments_SegEndPoint2Z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "SegEndPoint2Z"); 	 //Segment 2nd endpoint z coordinate in the sector coordinate system (for ced display)
	fTimeBasedTrkg_TBSegments_resiSum = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "resiSum"); 	 //sum of hit residuals
	fTimeBasedTrkg_TBSegments_timeSum = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegments", "timeSum"); 	 //sum of the hit times
	fTimeBasedTrkg_TBSegments_size = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBSegments", "size"); 	 //size of segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBSegmentTrajectory:reconstructed segment trajectory from hit-based tracking ===================
	fTimeBasedTrkg_TBSegmentTrajectory_segmentID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegmentTrajectory", "segmentID"); 	 //id of the segment
	fTimeBasedTrkg_TBSegmentTrajectory_sector = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBSegmentTrajectory", "sector"); 	 //sector of DC
	fTimeBasedTrkg_TBSegmentTrajectory_superlayer = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBSegmentTrajectory", "superlayer"); 	 //superlayer
	fTimeBasedTrkg_TBSegmentTrajectory_layer = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBSegmentTrajectory", "layer"); 	 //layer
	fTimeBasedTrkg_TBSegmentTrajectory_matchedHitID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBSegmentTrajectory", "matchedHitID"); 	 //matched hit id
	fTimeBasedTrkg_TBSegmentTrajectory_trkDoca = fHipoReader->getBranch<float>("TimeBasedTrkg::TBSegmentTrajectory", "trkDoca"); 	 //calculated track doca
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBCrosses:reconstructed segments using DC timing information ===================
	fTimeBasedTrkg_TBCrosses_id = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBCrosses", "id"); 	 //id of the cross
	fTimeBasedTrkg_TBCrosses_status = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBCrosses", "status"); 	 //status of the cross
	fTimeBasedTrkg_TBCrosses_sector = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBCrosses", "sector"); 	 //sector of the cross
	fTimeBasedTrkg_TBCrosses_region = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBCrosses", "region"); 	 //region of the cross
	fTimeBasedTrkg_TBCrosses_x = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "x"); 	 //DC track cross x-coordinate (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_y = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "y"); 	 //DC track cross y-coordinate (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "z"); 	 //DC track cross z-coordinate (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_err_x = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "err_x"); 	 //DC track cross x-coordinate uncertainty (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_err_y = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "err_y"); 	 //DC track cross y-coordinate uncertainty (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_err_z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "err_z"); 	 //DC track cross z-coordinate uncertainty (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_ux = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "ux"); 	 //DC track cross x-direction (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_uy = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "uy"); 	 //DC track cross y-direction (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_uz = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "uz"); 	 //DC track cross z-direction (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_err_ux = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "err_ux"); 	 //DC track cross x-direction uncertainty (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_err_uy = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "err_uy"); 	 //DC track cross y-direction uncertainty (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_err_uz = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCrosses", "err_uz"); 	 //DC track cross z-direction uncertainty (in the DC tilted sector coordinate system)
	fTimeBasedTrkg_TBCrosses_Segment1_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBCrosses", "Segment1_ID"); 	 //id of first superlayer used in segment
	fTimeBasedTrkg_TBCrosses_Segment2_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBCrosses", "Segment2_ID"); 	 //id of second superlayer used in segment
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBTracks:reconstructed tracks using DC timing information ===================
	fTimeBasedTrkg_TBTracks_id = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBTracks", "id"); 	 //id of the track
	fTimeBasedTrkg_TBTracks_status = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBTracks", "status"); 	 //status of the track
	fTimeBasedTrkg_TBTracks_sector = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBTracks", "sector"); 	 //sector of the track
	fTimeBasedTrkg_TBTracks_c1_x = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c1_x"); 	 //Upstream Region 1 cross x-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_c1_y = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c1_y"); 	 //Upstream Region 1 cross y-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_c1_z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c1_z"); 	 //Upstream Region 1 cross z-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_c1_ux = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c1_ux"); 	 //Upstream Region 1 cross unit x-direction vector in the lab
	fTimeBasedTrkg_TBTracks_c1_uy = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c1_uy"); 	 //Upstream Region 1 cross unit y-direction vector in the lab
	fTimeBasedTrkg_TBTracks_c1_uz = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c1_uz"); 	 //Upstream Region 1 cross unit z-direction vector in the lab
	fTimeBasedTrkg_TBTracks_c3_x = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c3_x"); 	 //Downstream Region 3 cross x-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_c3_y = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c3_y"); 	 //Downstream Region 3 cross y-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_c3_z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c3_z"); 	 //Downstream Region 3 cross z-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_c3_ux = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c3_ux"); 	 //Downstream Region 3 cross unit x-direction vector in the lab
	fTimeBasedTrkg_TBTracks_c3_uy = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c3_uy"); 	 //Downstream Region 3 cross unit y-direction vector in the lab
	fTimeBasedTrkg_TBTracks_c3_uz = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "c3_uz"); 	 //Downstream Region 3 cross unit z-direction vector in the lab
	fTimeBasedTrkg_TBTracks_t1_x = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "t1_x"); 	 //Upstream Region 1 track x-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_t1_y = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "t1_y"); 	 //Upstream Region 1 track y-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_t1_z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "t1_z"); 	 //Upstream Region 1 track z-position in the lab (in cm)
	fTimeBasedTrkg_TBTracks_t1_px = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "t1_px"); 	 //Upstream Region 1 track unit x-momentum vector in the lab
	fTimeBasedTrkg_TBTracks_t1_py = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "t1_py"); 	 //Upstream Region 1 track unit y-momentum vector in the lab
	fTimeBasedTrkg_TBTracks_t1_pz = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "t1_pz"); 	 //Upstream Region 1 track unit z-momentum vector in the lab
	fTimeBasedTrkg_TBTracks_Vtx0_x = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "Vtx0_x"); 	 //Vertex x-position of the swam track to the DOCA to the beamline (in cm)
	fTimeBasedTrkg_TBTracks_Vtx0_y = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "Vtx0_y"); 	 //Vertex y-position of the swam track to the DOCA to the beamline (in cm)
	fTimeBasedTrkg_TBTracks_Vtx0_z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "Vtx0_z"); 	 //Vertex z-position of the swam track to the DOCA to the beamline (in cm)
	fTimeBasedTrkg_TBTracks_p0_x = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "p0_x"); 	 //3-momentum x-coordinate of the swam track to the DOCA to the beamline (in cm)
	fTimeBasedTrkg_TBTracks_p0_y = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "p0_y"); 	 //3-momentum y-coordinate of the swam track to the DOCA to the beamline (in cm)
	fTimeBasedTrkg_TBTracks_p0_z = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "p0_z"); 	 //3-momentum z-coordinate of the swam track to the DOCA to the beamline (in cm)
	fTimeBasedTrkg_TBTracks_Cross1_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBTracks", "Cross1_ID"); 	 //id of first cross on track
	fTimeBasedTrkg_TBTracks_Cross2_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBTracks", "Cross2_ID"); 	 //id of second cross on track
	fTimeBasedTrkg_TBTracks_Cross3_ID = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBTracks", "Cross3_ID"); 	 //id of third cross on track
	fTimeBasedTrkg_TBTracks_q = fHipoReader->getBranch<uint8_t>("TimeBasedTrkg::TBTracks", "q"); 	 //charge of the track
	fTimeBasedTrkg_TBTracks_pathlength = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "pathlength"); 	 //pathlength of the track
	fTimeBasedTrkg_TBTracks_chi2 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBTracks", "chi2"); 	 //fit chi2 of the track
	fTimeBasedTrkg_TBTracks_ndf = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBTracks", "ndf"); 	 //fit ndf of the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::TBCovMat:reconstructed track covariance matrix ===================
	fTimeBasedTrkg_TBCovMat_id = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::TBCovMat", "id"); 	 //id of the track
	fTimeBasedTrkg_TBCovMat_C11 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C11"); 	 //C11 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C12 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C12"); 	 //C12 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C13 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C13"); 	 //C13 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C14 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C14"); 	 //C14 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C15 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C15"); 	 //C15 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C21 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C21"); 	 //C21 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C22 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C22"); 	 //C22 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C23 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C23"); 	 //C23 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C24 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C24"); 	 //C24 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C25 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C25"); 	 //C25 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C31 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C31"); 	 //C31 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C32 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C32"); 	 //C32 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C33 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C33"); 	 //C33 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C34 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C34"); 	 //C34 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C35 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C35"); 	 //C35 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C41 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C41"); 	 //C41 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C42 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C42"); 	 //C42 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C43 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C43"); 	 //C43 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C44 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C44"); 	 //C44 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C45 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C45"); 	 //C45 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C51 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C51"); 	 //C51 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C52 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C52"); 	 //C52 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C53 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C53"); 	 //C53 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C54 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C54"); 	 //C54 covariance matrix element at last superlayer used in the fit
	fTimeBasedTrkg_TBCovMat_C55 = fHipoReader->getBranch<float>("TimeBasedTrkg::TBCovMat", "C55"); 	 //C55 covariance matrix element at last superlayer used in the fit
//--------------------------------------------------------------------------------------------------------------------------------------

//================== TimeBasedTrkg::Trajectory:Trajectory bank ===================
	fTimeBasedTrkg_Trajectory_tid = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::Trajectory", "tid"); 	 //id of the track
	fTimeBasedTrkg_Trajectory_did = fHipoReader->getBranch<uint16_t>("TimeBasedTrkg::Trajectory", "did"); 	 //id of the detector
	fTimeBasedTrkg_Trajectory_x = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "x"); 	 //track x position at detector surface (cm)
	fTimeBasedTrkg_Trajectory_y = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "y"); 	 //track y position at detector surface (cm)
	fTimeBasedTrkg_Trajectory_z = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "z"); 	 //track z position at detector surface (cm)
	fTimeBasedTrkg_Trajectory_tx = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "tx"); 	 //track unit direction vector x component at detector surface
	fTimeBasedTrkg_Trajectory_ty = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "ty"); 	 //track unit direction vector y component at detector surface
	fTimeBasedTrkg_Trajectory_tz = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "tz"); 	 //track unit direction vector z component at detector surface
	fTimeBasedTrkg_Trajectory_B = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "B"); 	 //B-field magnitude at detector surface (cm)
	fTimeBasedTrkg_Trajectory_L = fHipoReader->getBranch<float>("TimeBasedTrkg::Trajectory", "L"); 	 //pathlength of the track from the DOCA to the Beamline to the detector surface (cm)
//--------------------------------------------------------------------------------------------------------------------------------------

}
