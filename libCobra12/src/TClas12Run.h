// Filename: TClas12Run.h
// Description: CLAS12 Run class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12RUN_H
#define TCLAS12RUN_H

#include "TClas12Event.h"
#include "TClas12Detector.h"
#include "TMath.h"

class TClas12DC;
class TClas12ECAL;
using namespace std;

class TClas12Run: public TClas12Event
{
    Int_t fPartSize; // Length of particle bank for current event                
    Int_t fCherSize; // Length of CC bank for current event
    Int_t fCaloSize; // Length of Calorimeter bank for current event
    Int_t fScinSize; // Length of Scintillator bank for current event
    Int_t fForwSize; // Length of Forward tagger bank for current event
    Int_t fTracSize; // Length of Track bank for current event
    Int_t fTrajSize; // Length of Traj bank for current event
    
    TClas12DC *fDC;
    TClas12ECAL *fECAL;
    Bool_t fIsDCAdded;
    Bool_t fIsECALAdded;
    void GenerateMap();
    void UpdateDetectors();

public:    

    //------- Mapping field ------------
    unordered_multimap <Int_t, Int_t> *fPartMap;    //PID to particle bank index multi-map
    unordered_multimap <Int_t, Int_t> *fCharMap;    //Particle charge to particle bank index multi-map

    unordered_map <Int_t, Int_t> *fCherMap;         // Generated Hash key to CC bank index map 
    unordered_map <Int_t, Int_t> *fCaloMap;         // Generated Hash key to Calorimeter bank index map
    unordered_map <Int_t, Int_t> *fScinMap;         // Generated Hash key to Scin bank index map 
    unordered_map <Int_t, Int_t> *fForwMap;         // Generated Hash key to Forward tagger index map
    unordered_map <Int_t, Int_t> *fTracMap;         // Generated Hash key to Track bank index map
    unordered_map <Int_t, Int_t> *fTrajMap;         // Generated Hash key to Traj bank index map
    
    TClas12Run(Int_t runNumber, Int_t fileNo);
    TClas12Run(TString fileName);
    ~TClas12Run();
    void Init();
    Bool_t HasEvent();
    
    Int_t GetDetectorID(TString detectorName);
    TClas12Detector* AddDetector(TString detectorName);
    
    //----------------------------- Check if detector has response/hit -----------------
    //Particle Bank
    Bool_t HasPartBank() {return (fPartSize > 0);}
    
    //CC
    Bool_t HasCherBank() {return (fCherSize > 0);}
    Bool_t HasHTCCHit(Int_t index){ return (fCherMap->find(GetHTCCHashKey(index)) != fCherMap->end()); }
    Bool_t HasLTCCHit(Int_t index){ return (fCherMap->find(GetLTCCHashKey(index)) != fCherMap->end()); }
    //Calorimeter Bank
    Bool_t HasCaloBank() {return (fCaloSize >0);}
    Bool_t HasECInnerHit(Int_t index){ return (fCaloMap->find(GetECInnerHashKey(index)) != fCaloMap->end()); }
    Bool_t HasECOuterHit(Int_t index){ return (fCaloMap->find(GetECOuterHashKey(index)) != fCaloMap->end()); }
    Bool_t HasPCalHit(Int_t index){ return (fCaloMap->find(GetPCalHashKey(index)) != fCaloMap->end()); }
    //Scintillator Bank
    Bool_t HasScinBank() {return (fScinSize > 0);}

    //Track Bank
    Bool_t HasTracBank() {return (fTracSize > 0);}

    //Traj Bank
    Bool_t HasTrajBank() {return (fTrajSize > 0);}

    //Forward Tagger Bank
    Bool_t HasForwBank() {return (fForwSize > 0);}
    
    //------------ Getter Functions ----------
    //Event Length
    Int_t GetSizePart() {return fPartSize;}
    Int_t GetSizeCher() {return fCherSize;}
    Int_t GetSizeCalo() {return fCaloSize;}
    Int_t GetSizeScin() {return fScinSize;}
    Int_t GetSizeForw() {return fForwSize;}
    Int_t GetSizeTrac() {return fTracSize;}
    Int_t GetSizeTraj() {return fTrajSize;}
    //--Particle Bank--
    Float_t GetVz(Int_t index) {return (Float_t)fREC_Particle_vz->getValue(index);}
    Float_t GetPx(Int_t index) {return (Float_t)fREC_Particle_px->getValue(index);}
    Float_t GetPy(Int_t index) {return (Float_t)fREC_Particle_py->getValue(index);}
    Float_t GetPz(Int_t index) {return (Float_t)fREC_Particle_pz->getValue(index);}
    Double_t GetP(Int_t index) {return sqrt(GetPx(index)*GetPx(index) + GetPy(index)*GetPy(index) + GetPz(index)*GetPz(index));}
    Double_t GetThetaRad(Int_t index) {return acos(GetPz(index) / GetP(index));}
    Double_t GetPhiRad(Int_t index) {return atan2(GetPy(index), GetPx(index));}    
    Double_t GetThetaDeg(Int_t index) {return GetThetaRad(index)*TMath::RadToDeg();}
    Double_t GetPhiDeg(Int_t index) {return GetPhiRad(index)*TMath::RadToDeg();}
    //--Hash Key--
    Int_t GetECOuterHashKey(Int_t index) {return (100*index + 10*GetDetectorID("ECAL") + GetDetectorID("ECOUTER"));}
    Int_t GetECInnerHashKey(Int_t index) {return (100*index + 10*GetDetectorID("ECAL") + GetDetectorID("ECINNER"));}
    Int_t GetPCalHashKey(Int_t index) {return (100*index + 10*GetDetectorID("ECAL") + GetDetectorID("PCAL"));}
    Int_t GetLTCCHashKey(Int_t index) {return (100*index + 10*GetDetectorID("LTCC"));}
    Int_t GetHTCCHashKey(Int_t index) {return (100*index + 10*GetDetectorID("HTCC"));}

    //--CC Bank--
    Float_t GetLTCCnphe(Int_t index){return fREC_Cherenkov_nphe->getValue(fCherMap->at(GetLTCCHashKey(index)));}
    Float_t GetHTCCnphe(Int_t index){return fREC_Cherenkov_nphe->getValue(fCherMap->at(GetHTCCHashKey(index)));}

    //--Calorimeter Bank--
    Float_t GetECInnerEnergy(Int_t index){return fREC_Calorimeter_energy->getValue(fCaloMap->at(GetECInnerHashKey(index)));}
    Float_t GetECOuterEnergy(Int_t index){return fREC_Calorimeter_energy->getValue(fCaloMap->at(GetECOuterHashKey(index)));}
    Float_t GetPCalEnergy(Int_t index){return fREC_Calorimeter_energy->getValue(fCaloMap->at(GetPCalHashKey(index)));}
    
    Int_t GetCountWithPID(Int_t pid){return (fPartMap->count(pid));}
    Int_t GetCountWithCharge(Int_t charge){return (fCharMap->count(charge));}
    pair<unordered_multimap<Int_t, Int_t>::iterator, unordered_multimap<Int_t, Int_t>::iterator> GetItrPairForCharge(Int_t charge){return fCharMap->equal_range(charge);}
    pair<unordered_multimap<Int_t, Int_t>::iterator, unordered_multimap<Int_t, Int_t>::iterator> GetItrPairForPID(Int_t pid){return fPartMap->equal_range(pid);}
    Int_t GetEventNo() {return fEventNo;}    
    //--------------------------------
    ClassDef(TClas12Run,0)    
};
#endif    
