// Filename: TClas12DetectorECAL.h
// Description: CLAS12 ECAL base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORECAL_H
#define TCLAS12DETECTORECAL_H

#include "TClas12Detector.h"

class TClas12DetectorECAL: public TClas12Detector
{
public:
	TClas12DetectorECAL(TClas12Run *run);
	~TClas12DetectorECAL();
	void Init();
	void SetBranches();

protected:
//================== ECAL::hits:reconstructed hits from ECAL ===================
	hipo::node<uint16_t> *fECAL_hits_id;   //id of the hit
	hipo::node<uint16_t> *fECAL_hits_status;   //status of the hit
	hipo::node<uint8_t> *fECAL_hits_sector;   //sector of ECAL
	hipo::node<uint8_t> *fECAL_hits_layer;   //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	hipo::node<uint8_t> *fECAL_hits_strip;   //Strip number
	hipo::node<uint8_t> *fECAL_hits_peakid;   //Peak id
	hipo::node<float> *fECAL_hits_energy;   //Energy of the hit
	hipo::node<float> *fECAL_hits_time;   //Time of the hit
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::peaks:reconstructed peaks from ECAL ===================
	hipo::node<uint16_t> *fECAL_peaks_id;   //id of the hit
	hipo::node<uint16_t> *fECAL_peaks_status;   //status of the hit
	hipo::node<uint8_t> *fECAL_peaks_sector;   //sector of ECAL
	hipo::node<uint8_t> *fECAL_peaks_layer;   //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	hipo::node<float> *fECAL_peaks_energy;   //Energy of the hit
	hipo::node<float> *fECAL_peaks_time;   //Time of the hit
	hipo::node<float> *fECAL_peaks_xo;   //strip origin X coordinate
	hipo::node<float> *fECAL_peaks_yo;   //strip origin Y coordinate
	hipo::node<float> *fECAL_peaks_zo;   //strip origin Z coordinate
	hipo::node<float> *fECAL_peaks_xe;   //strip end    X coordinate
	hipo::node<float> *fECAL_peaks_ye;   //strip end    Y coordinate
	hipo::node<float> *fECAL_peaks_ze;   //strip end    Z coordinate
	hipo::node<float> *fECAL_peaks_width;   //width of the peak
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::clusters:reconstructed clusters in ECAL ===================
	hipo::node<uint16_t> *fECAL_clusters_id;   //id of the hit
	hipo::node<uint16_t> *fECAL_clusters_status;   //status of the hit
	hipo::node<uint8_t> *fECAL_clusters_sector;   //sector of ECAL
	hipo::node<uint8_t> *fECAL_clusters_layer;   //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	hipo::node<float> *fECAL_clusters_x;   //X coordinate of the hit
	hipo::node<float> *fECAL_clusters_y;   //Y coordinate of the hit
	hipo::node<float> *fECAL_clusters_z;   //Z coordinate of the hit
	hipo::node<float> *fECAL_clusters_energy;   //Energy of the hit
	hipo::node<float> *fECAL_clusters_time;   //Energy of the hit
	hipo::node<float> *fECAL_clusters_widthU;   //width of U peak
	hipo::node<float> *fECAL_clusters_widthV;   //width of V peak
	hipo::node<float> *fECAL_clusters_widthW;   //width of W peak
	hipo::node<uint8_t> *fECAL_clusters_idU;   //id of U peak
	hipo::node<uint8_t> *fECAL_clusters_idV;   //id of V peak
	hipo::node<uint8_t> *fECAL_clusters_idW;   //id of W peak
	hipo::node<uint32_t> *fECAL_clusters_coordU;   //U coordinate 
	hipo::node<uint32_t> *fECAL_clusters_coordV;   //V coordinate
	hipo::node<uint32_t> *fECAL_clusters_coordW;   //W coordinate
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::calib:Raw and recon peak energy from ECAL ===================
	hipo::node<uint8_t> *fECAL_calib_sector;   //sector of ECAL
	hipo::node<uint8_t> *fECAL_calib_layer;   //Layer of ECAL (1-3:PCAL, 4-6:ECIN, 7-9:ECOUT
	hipo::node<float> *fECAL_calib_energy;   //Energy of the hit
	hipo::node<float> *fECAL_calib_rawEU;   //raw U peak energy
	hipo::node<float> *fECAL_calib_rawEV;   //raw V peak energy
	hipo::node<float> *fECAL_calib_rawEW;   //raw W peak energy
	hipo::node<float> *fECAL_calib_recEU;   //recon U peak energy
	hipo::node<float> *fECAL_calib_recEV;   //recon V peak energy
	hipo::node<float> *fECAL_calib_recEW;   //recon W peak energy
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::moments:ECCAL bank for clusters containing distances an moments ===================
	hipo::node<float> *fECAL_moments_distU;   //distance fomr u edge
	hipo::node<float> *fECAL_moments_distV;   //distance from v edge
	hipo::node<float> *fECAL_moments_distW;   //distance from w edge
	hipo::node<float> *fECAL_moments_m1u;   //second moment
	hipo::node<float> *fECAL_moments_m1v;   //second moment
	hipo::node<float> *fECAL_moments_m1w;   //second moment
	hipo::node<float> *fECAL_moments_m2u;   //second moment
	hipo::node<float> *fECAL_moments_m2v;   //second moment
	hipo::node<float> *fECAL_moments_m2w;   //second moment
	hipo::node<float> *fECAL_moments_m3u;   //third moment
	hipo::node<float> *fECAL_moments_m3v;   //third moment
	hipo::node<float> *fECAL_moments_m3w;   //third moment
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== ECAL::hits:reconstructed hits from ECAL ===================
	unsigned int GetECAL_hits_id(int i){  return (unsigned int)fECAL_hits_id->getValue(i);}
	unsigned int GetECAL_hits_status(int i){  return (unsigned int)fECAL_hits_status->getValue(i);}
	unsigned int GetECAL_hits_sector(int i){  return (unsigned int)fECAL_hits_sector->getValue(i);}
	unsigned int GetECAL_hits_layer(int i){  return (unsigned int)fECAL_hits_layer->getValue(i);}
	unsigned int GetECAL_hits_strip(int i){  return (unsigned int)fECAL_hits_strip->getValue(i);}
	unsigned int GetECAL_hits_peakid(int i){  return (unsigned int)fECAL_hits_peakid->getValue(i);}
	float GetECAL_hits_energy(int i){  return (float)fECAL_hits_energy->getValue(i);}
	float GetECAL_hits_time(int i){  return (float)fECAL_hits_time->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::peaks:reconstructed peaks from ECAL ===================
	unsigned int GetECAL_peaks_id(int i){  return (unsigned int)fECAL_peaks_id->getValue(i);}
	unsigned int GetECAL_peaks_status(int i){  return (unsigned int)fECAL_peaks_status->getValue(i);}
	unsigned int GetECAL_peaks_sector(int i){  return (unsigned int)fECAL_peaks_sector->getValue(i);}
	unsigned int GetECAL_peaks_layer(int i){  return (unsigned int)fECAL_peaks_layer->getValue(i);}
	float GetECAL_peaks_energy(int i){  return (float)fECAL_peaks_energy->getValue(i);}
	float GetECAL_peaks_time(int i){  return (float)fECAL_peaks_time->getValue(i);}
	float GetECAL_peaks_xo(int i){  return (float)fECAL_peaks_xo->getValue(i);}
	float GetECAL_peaks_yo(int i){  return (float)fECAL_peaks_yo->getValue(i);}
	float GetECAL_peaks_zo(int i){  return (float)fECAL_peaks_zo->getValue(i);}
	float GetECAL_peaks_xe(int i){  return (float)fECAL_peaks_xe->getValue(i);}
	float GetECAL_peaks_ye(int i){  return (float)fECAL_peaks_ye->getValue(i);}
	float GetECAL_peaks_ze(int i){  return (float)fECAL_peaks_ze->getValue(i);}
	float GetECAL_peaks_width(int i){  return (float)fECAL_peaks_width->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::clusters:reconstructed clusters in ECAL ===================
	unsigned int GetECAL_clusters_id(int i){  return (unsigned int)fECAL_clusters_id->getValue(i);}
	unsigned int GetECAL_clusters_status(int i){  return (unsigned int)fECAL_clusters_status->getValue(i);}
	unsigned int GetECAL_clusters_sector(int i){  return (unsigned int)fECAL_clusters_sector->getValue(i);}
	unsigned int GetECAL_clusters_layer(int i){  return (unsigned int)fECAL_clusters_layer->getValue(i);}
	float GetECAL_clusters_x(int i){  return (float)fECAL_clusters_x->getValue(i);}
	float GetECAL_clusters_y(int i){  return (float)fECAL_clusters_y->getValue(i);}
	float GetECAL_clusters_z(int i){  return (float)fECAL_clusters_z->getValue(i);}
	float GetECAL_clusters_energy(int i){  return (float)fECAL_clusters_energy->getValue(i);}
	float GetECAL_clusters_time(int i){  return (float)fECAL_clusters_time->getValue(i);}
	float GetECAL_clusters_widthU(int i){  return (float)fECAL_clusters_widthU->getValue(i);}
	float GetECAL_clusters_widthV(int i){  return (float)fECAL_clusters_widthV->getValue(i);}
	float GetECAL_clusters_widthW(int i){  return (float)fECAL_clusters_widthW->getValue(i);}
	unsigned int GetECAL_clusters_idU(int i){  return (unsigned int)fECAL_clusters_idU->getValue(i);}
	unsigned int GetECAL_clusters_idV(int i){  return (unsigned int)fECAL_clusters_idV->getValue(i);}
	unsigned int GetECAL_clusters_idW(int i){  return (unsigned int)fECAL_clusters_idW->getValue(i);}
	unsigned int GetECAL_clusters_coordU(int i){  return (unsigned int)fECAL_clusters_coordU->getValue(i);}
	unsigned int GetECAL_clusters_coordV(int i){  return (unsigned int)fECAL_clusters_coordV->getValue(i);}
	unsigned int GetECAL_clusters_coordW(int i){  return (unsigned int)fECAL_clusters_coordW->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::calib:Raw and recon peak energy from ECAL ===================
	unsigned int GetECAL_calib_sector(int i){  return (unsigned int)fECAL_calib_sector->getValue(i);}
	unsigned int GetECAL_calib_layer(int i){  return (unsigned int)fECAL_calib_layer->getValue(i);}
	float GetECAL_calib_energy(int i){  return (float)fECAL_calib_energy->getValue(i);}
	float GetECAL_calib_rawEU(int i){  return (float)fECAL_calib_rawEU->getValue(i);}
	float GetECAL_calib_rawEV(int i){  return (float)fECAL_calib_rawEV->getValue(i);}
	float GetECAL_calib_rawEW(int i){  return (float)fECAL_calib_rawEW->getValue(i);}
	float GetECAL_calib_recEU(int i){  return (float)fECAL_calib_recEU->getValue(i);}
	float GetECAL_calib_recEV(int i){  return (float)fECAL_calib_recEV->getValue(i);}
	float GetECAL_calib_recEW(int i){  return (float)fECAL_calib_recEW->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== ECAL::moments:ECCAL bank for clusters containing distances an moments ===================
	float GetECAL_moments_distU(int i){  return (float)fECAL_moments_distU->getValue(i);}
	float GetECAL_moments_distV(int i){  return (float)fECAL_moments_distV->getValue(i);}
	float GetECAL_moments_distW(int i){  return (float)fECAL_moments_distW->getValue(i);}
	float GetECAL_moments_m1u(int i){  return (float)fECAL_moments_m1u->getValue(i);}
	float GetECAL_moments_m1v(int i){  return (float)fECAL_moments_m1v->getValue(i);}
	float GetECAL_moments_m1w(int i){  return (float)fECAL_moments_m1w->getValue(i);}
	float GetECAL_moments_m2u(int i){  return (float)fECAL_moments_m2u->getValue(i);}
	float GetECAL_moments_m2v(int i){  return (float)fECAL_moments_m2v->getValue(i);}
	float GetECAL_moments_m2w(int i){  return (float)fECAL_moments_m2w->getValue(i);}
	float GetECAL_moments_m3u(int i){  return (float)fECAL_moments_m3u->getValue(i);}
	float GetECAL_moments_m3v(int i){  return (float)fECAL_moments_m3v->getValue(i);}
	float GetECAL_moments_m3w(int i){  return (float)fECAL_moments_m3w->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorECAL,0)
};
#endif




