// Filename: help.cpp
// Description: 
// Author: Latiful Kabir < siplukabir@gmail.com >
// Created: Mon Mar 28 15:12:34 2016 (-0400)
// URL: latifkabir.github.io

#include<iostream>
#include<sstream>
#include<fstream>
#include "TString.h"
#include "TClas12.h"

using namespace std;

void help()
{
    string str;
    TString lineStr;
    str = TClas12::Config->GetCobra12Home() + (string) "/libCobra12/src/";
	
    str = str + "LinkDef.h";
    //cout << str <<endl;

    ifstream myFile(str.c_str());
    if(!myFile)
    {
	cout << "Source file NOT found" <<endl;
	return;
    }
    cout << "\t\t=========================================================================" <<endl;

    cout << "\t\t|\t\t\t Classes/Functions inside libCobra12        \t|" <<endl;
    cout << "\t\t=========================================================================" <<endl;
    while(getline(myFile,str))
    {
	//cout << str <<endl;
	lineStr = str.c_str();
	// if(str[0]=='/' && str[1]=='/')
	//     continue;
	if(str == "#endif")
	    continue;
	if(str == "#ifdef __CINT__")
	    continue;

	cout << lineStr.ReplaceAll(";", "").ReplaceAll("#pragma link C++ ", "").ReplaceAll("//", " : ") <<endl;
    }

    myFile.close();
    cout<< "\n-------------------------------------------------------------------------------------------" <<endl;  
    cout << "| Type 'help(\"function or class name\")' for details of any specific function or class     |"<<endl;
    cout<<  "-------------------------------------------------------------------------------------------" <<endl;  
    
    str = TClas12::Config->GetCobra12Home() + (string) "/analysis/src/";
    str = str + "LinkDef.h";
    //cout << str <<endl;

    ifstream myFile2(str.c_str());
    if(!myFile2)
    {
	cout << "Source file NOT found" <<endl;
	return;
    }
    cout << "\n\n\t\t=================================================================================" <<endl;

    cout << "\t\t|\t\t\t   Functions inside Ananlysis   \t\t\t|" <<endl;
    cout << "\t\t=================================================================================" <<endl;
  
    while(getline(myFile2,str))
    {
	lineStr = str.c_str();
	if(str[0] == '/' && str[1] == '/')
	    continue;
	if(str == "#endif")
	    continue;
	if(str == "#ifdef __CINT__")
	    continue;
	//cout << str <<endl;
	cout << lineStr.ReplaceAll(";", "").ReplaceAll("#pragma link C++ ", "").ReplaceAll("//", " : ") <<endl;
    }
    myFile2.close();
    cout<< "\n-------------------------------------------------------------------------------------------" <<endl;  
    cout << "| Type 'help(\"function or class name\")' for details of any specific function or class     |"<<endl;
    cout<<  "-------------------------------------------------------------------------------------------" <<endl;  
    
}


void help(string file)
{
    string str = TClas12::Config->GetCobra12Home() + (string) "/libCobra12/src/";
    str = str + file + ".h";
    ifstream myFile(str.c_str());

    if(!myFile)
    {
	str = TClas12::Config->GetCobra12Home() + (string) "/analysis/src/";
	str = str + file + ".h";
        myFile.open(str.c_str());	
    }

    if(!myFile)
    {
	cout << "Source file NOT found" <<endl;
	return;
    }
    while(getline(myFile,str))       
	cout << str <<endl;

    myFile.close();
}

// int main(int argc, char *argv[])
// {
//     help();
//     help("DecayAnalysis");
//     return 0;
// }
