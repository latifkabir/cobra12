// Filename: TClas12ECAL.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12ECAL.h"
#include "TClas12.h"

ClassImp(TClas12ECAL)
//-------------------------------------------------------------------------------------
TClas12ECAL::TClas12ECAL(TClas12Run *run):TClas12DetectorECAL(run)
{
    Init();
}
//-------------------------------------------------------------------------------------
TClas12ECAL::~TClas12ECAL()
{
    delete fHitsMap;
    delete fPeaksMap;
    delete fClustersMap;
    // delete fCalibMap;
    // delete fMomentsMap;
}
//-------------------------------------------------------------------------------------
void TClas12ECAL::Init()
{
    fHitsSize = 0;
    fPeaksSize = 0;
    fClustersSize = 0;
    fCalibSize = 0;
    fMomentsSize = 0;

    fHitsMap = new unordered_map <Int_t, Int_t>();
    fPeaksMap = new unordered_map <Int_t, Int_t>();
    fClustersMap = new unordered_map <Int_t, Int_t>(); 
    // fCalibMap = new unordered_map <Int_t, Int_t>(); 
    // fMomentsMap = new unordered_map <Int_t, Int_t>();
}
//-------------------------------------------------------------------------------------
void TClas12ECAL::Update()
{
    fHitsSize = fECAL_hits_id->getLength();
    fPeaksSize = fECAL_peaks_id->getLength();
    fClustersSize = fECAL_clusters_id->getLength();
    fCalibSize = fECAL_calib_sector->getLength();
    fMomentsSize = fECAL_moments_distU->getLength();

    if(TClas12::fTurnOnECMap)
	GenerateMap();	
}


//-------------------------------------------------------------------------------------
void TClas12ECAL::GenerateMap()
{    
    fHitsMap->clear();
    fPeaksMap->clear();
    fClustersMap->clear();
    // fCalibMap->clear();
    // fMomentsMap->clear();
    
    for(Int_t i = 0; i < fHitsSize; ++i)   
	fHitsMap->insert({GetECAL_hits_id(i), i});
    for(Int_t i = 0; i < fPeaksSize; ++i)   
    	fPeaksMap->insert({GetECAL_peaks_id(i), i});
    for(Int_t i = 0; i < fClustersSize; ++i)   
    	fClustersMap->insert({GetECAL_clusters_id(i), i});
}

//---------------- Get row/index number associated with desired id ---------    
Int_t TClas12ECAL::GetIndexHits(Int_t id)
{
    auto itr = fHitsMap->find(id);
    if(itr != fHitsMap->end())
	return itr->second;
    else
	return -1;
}
Int_t TClas12ECAL::GetIndexPeaks(Int_t id)
{
    auto itr = fPeaksMap->find(id);
    if(itr != fPeaksMap->end())
     	return itr->second;
    else
	return -1;
}
Int_t TClas12ECAL::GetIndexClusters(Int_t id)
{
    auto itr = fClustersMap->find(id);
    if(itr != fClustersMap->end())
	return itr->second;
    else
	return -1;
}
