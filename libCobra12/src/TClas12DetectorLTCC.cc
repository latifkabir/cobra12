// Filename: TClas12DetectorLTCC.cc
// Description: CLAS12 LTCC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorLTCC.h"


ClassImp(TClas12DetectorLTCC)
//-------------------------------------------------------------------------------------
TClas12DetectorLTCC::TClas12DetectorLTCC(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorLTCC::~TClas12DetectorLTCC()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorLTCC::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorLTCC::SetBranches()
{
//================== LTCC::clusters:reconstructed clusters from the LTCC ===================
	fLTCC_clusters_id = fHipoReader->getBranch<uint16_t>("LTCC::clusters", "id"); 	 //id of the cluster
	fLTCC_clusters_status = fHipoReader->getBranch<uint8_t>("LTCC::clusters", "status"); 	 //good (1) or bad (0)
	fLTCC_clusters_sector = fHipoReader->getBranch<uint8_t>("LTCC::clusters", "sector"); 	 //sector of LTCC
	fLTCC_clusters_segment = fHipoReader->getBranch<uint16_t>("LTCC::clusters", "segment"); 	 //weighted central cluster segment
	fLTCC_clusters_x = fHipoReader->getBranch<float>("LTCC::clusters", "x"); 	 //crude cluster center (cm)
	fLTCC_clusters_y = fHipoReader->getBranch<float>("LTCC::clusters", "y"); 	 //crude cluster center (cm)
	fLTCC_clusters_z = fHipoReader->getBranch<float>("LTCC::clusters", "z"); 	 //crude cluster center (cm)
	fLTCC_clusters_nphe = fHipoReader->getBranch<float>("LTCC::clusters", "nphe"); 	 //number of photo-electrons
	fLTCC_clusters_time = fHipoReader->getBranch<float>("LTCC::clusters", "time"); 	 //cluster time
	fLTCC_clusters_nHits = fHipoReader->getBranch<uint16_t>("LTCC::clusters", "nHits"); 	 //number of hits in this cluster
	fLTCC_clusters_minTheta = fHipoReader->getBranch<float>("LTCC::clusters", "minTheta"); 	 //minimum theta angle of the cluster (deg)
	fLTCC_clusters_maxTheta = fHipoReader->getBranch<float>("LTCC::clusters", "maxTheta"); 	 //maximum theta angle of the cluster (deg)
	fLTCC_clusters_minPhi = fHipoReader->getBranch<float>("LTCC::clusters", "minPhi"); 	 //minimum phi angle of the cluster (deg)
	fLTCC_clusters_maxPhi = fHipoReader->getBranch<float>("LTCC::clusters", "maxPhi"); 	 //maximum phi angle of the cluster (deg)
//--------------------------------------------------------------------------------------------------------------------------------------

}
