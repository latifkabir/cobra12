// Filename: TClas12ECAL.h
// Description: CLAS12 ECAL class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12ECAL_H
#define TCLAS12ECAL_H

#include <unordered_map>
#include "TClas12DetectorECAL.h"

using namespace std;

class TClas12ECAL: public TClas12DetectorECAL
{
    Int_t fHitsSize;
    Int_t fPeaksSize;
    Int_t fClustersSize;
    Int_t fCalibSize;
    Int_t fMomentsSize;

    //------- Mapping field ------------
    unordered_map <Int_t, Int_t> *fHitsMap;         // Id to row number map
    unordered_map <Int_t, Int_t> *fPeaksMap;        // Id to row number map
    unordered_map <Int_t, Int_t> *fClustersMap;     // Id to row number map 
    // unordered_map <Int_t, Int_t> *fCalibMap;        // ? to row number map 
    // unordered_map <Int_t, Int_t> *fMomentsMap;      // ? to row number map
      
public:
    TClas12ECAL(TClas12Run *run);
    ~TClas12ECAL();
    void Init();
    void Update();
    void GenerateMap();
    
    //--------------- Getter Functions -----------------------------
    Bool_t HasHits(){ return (fHitsSize > 0);}
    Bool_t HasPeaks(){ return (fPeaksSize > 0);}
    Bool_t HasClusters(){ return (fClustersSize > 0);}
    Bool_t HasCalib(){ return (fCalibSize > 0);}
    Bool_t HasMoments(){ return (fMomentsSize > 0);}
    
    //--------------- Getter Functions -----------------------------
    Int_t GetSizeHits(){ return fHitsSize;}
    Int_t GetSizePeaks(){ return fPeaksSize;}
    Int_t GetSizeClusters(){ return fClustersSize;}
    Int_t GetSizeCalib(){ return fCalibSize;}
    Int_t GetSizeMoments(){ return fMomentsSize;}

    //---------------- Get row/index number associated with desired id ---------    
    Int_t GetIndexHits(Int_t id);
    Int_t GetIndexPeaks(Int_t id);
    Int_t GetIndexClusters(Int_t id);
    // Int_t GetIndexCalib(Int_t id);
    // Int_t GetIndexMoments(Int_t id);
        
    ClassDef(TClas12ECAL,0)    
};
#endif    
