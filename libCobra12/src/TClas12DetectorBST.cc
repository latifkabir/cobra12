// Filename: TClas12DetectorBST.cc
// Description: CLAS12 BST base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorBST.h"


ClassImp(TClas12DetectorBST)
//-------------------------------------------------------------------------------------
TClas12DetectorBST::TClas12DetectorBST(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorBST::~TClas12DetectorBST()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorBST::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorBST::SetBranches()
{
//================== BSTRec::Hits:reconstructed BST hits on track ===================
	fBSTRec_Hits_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Hits", "ID"); 	 //hit ID
	fBSTRec_Hits_sector = fHipoReader->getBranch<uint8_t>("BSTRec::Hits", "sector"); 	 //hit sector
	fBSTRec_Hits_layer = fHipoReader->getBranch<uint8_t>("BSTRec::Hits", "layer"); 	 //hit layer
	fBSTRec_Hits_strip = fHipoReader->getBranch<uint32_t>("BSTRec::Hits", "strip"); 	 //hit strip
	fBSTRec_Hits_fitResidual = fHipoReader->getBranch<float>("BSTRec::Hits", "fitResidual"); 	 //fitted hit residual
	fBSTRec_Hits_trkingStat = fHipoReader->getBranch<uint32_t>("BSTRec::Hits", "trkingStat"); 	 //tracking status
	fBSTRec_Hits_clusterID = fHipoReader->getBranch<uint16_t>("BSTRec::Hits", "clusterID"); 	 //associated cluster ID
	fBSTRec_Hits_trkID = fHipoReader->getBranch<uint16_t>("BSTRec::Hits", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::Clusters:reconstructed BST clusters ===================
	fBSTRec_Clusters_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "ID"); 	 //ID
	fBSTRec_Clusters_sector = fHipoReader->getBranch<uint8_t>("BSTRec::Clusters", "sector"); 	 //sector
	fBSTRec_Clusters_layer = fHipoReader->getBranch<uint8_t>("BSTRec::Clusters", "layer"); 	 //layer
	fBSTRec_Clusters_size = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "size"); 	 //cluster size
	fBSTRec_Clusters_ETot = fHipoReader->getBranch<float>("BSTRec::Clusters", "ETot"); 	 //cluster total energy
	fBSTRec_Clusters_seedE = fHipoReader->getBranch<float>("BSTRec::Clusters", "seedE"); 	 //energy of the seed 
	fBSTRec_Clusters_seedStrip = fHipoReader->getBranch<uint32_t>("BSTRec::Clusters", "seedStrip"); 	 //seed strip
	fBSTRec_Clusters_centroid = fHipoReader->getBranch<float>("BSTRec::Clusters", "centroid"); 	 //centroid strip number
	fBSTRec_Clusters_centroidResidual = fHipoReader->getBranch<float>("BSTRec::Clusters", "centroidResidual"); 	 //centroid residual
	fBSTRec_Clusters_seedResidual = fHipoReader->getBranch<float>("BSTRec::Clusters", "seedResidual"); 	 //seed residual
	fBSTRec_Clusters_Hit1_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "Hit1_ID"); 	 //Index of hit 1 in cluster
	fBSTRec_Clusters_Hit2_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "Hit2_ID"); 	 //Index of hit 2 in cluster
	fBSTRec_Clusters_Hit3_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "Hit3_ID"); 	 //Index of hit 3 in cluster
	fBSTRec_Clusters_Hit4_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "Hit4_ID"); 	 //Index of hit 4 in cluster
	fBSTRec_Clusters_Hit5_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "Hit5_ID"); 	 //Index of hit 5 in cluster
	fBSTRec_Clusters_trkID = fHipoReader->getBranch<uint16_t>("BSTRec::Clusters", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::Crosses:reconstructed BST crosses ===================
	fBSTRec_Crosses_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Crosses", "ID"); 	 //ID
	fBSTRec_Crosses_sector = fHipoReader->getBranch<uint8_t>("BSTRec::Crosses", "sector"); 	 //sector
	fBSTRec_Crosses_region = fHipoReader->getBranch<uint8_t>("BSTRec::Crosses", "region"); 	 //region
	fBSTRec_Crosses_x = fHipoReader->getBranch<float>("BSTRec::Crosses", "x"); 	 //BMT cross x-coordinate
	fBSTRec_Crosses_y = fHipoReader->getBranch<float>("BSTRec::Crosses", "y"); 	 //BMT cross y-coordinate
	fBSTRec_Crosses_z = fHipoReader->getBranch<float>("BSTRec::Crosses", "z"); 	 //BMT cross z-coordinate
	fBSTRec_Crosses_err_x = fHipoReader->getBranch<float>("BSTRec::Crosses", "err_x"); 	 //BMT cross x-coordinate error
	fBSTRec_Crosses_err_y = fHipoReader->getBranch<float>("BSTRec::Crosses", "err_y"); 	 //BMT cross y-coordinate error
	fBSTRec_Crosses_err_z = fHipoReader->getBranch<float>("BSTRec::Crosses", "err_z"); 	 //BMT cross z-coordinate error
	fBSTRec_Crosses_ux = fHipoReader->getBranch<float>("BSTRec::Crosses", "ux"); 	 //BMT cross x-direction (track unit tangent vector at the cross)
	fBSTRec_Crosses_uy = fHipoReader->getBranch<float>("BSTRec::Crosses", "uy"); 	 //BMT cross y-direction (track unit tangent vector at the cross)
	fBSTRec_Crosses_uz = fHipoReader->getBranch<float>("BSTRec::Crosses", "uz"); 	 //BMT cross z-direction (track unit tangent vector at the cross)
	fBSTRec_Crosses_Cluster1_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Crosses", "Cluster1_ID"); 	 //ID of the  cluster in the Cross
	fBSTRec_Crosses_Cluster2_ID = fHipoReader->getBranch<uint16_t>("BSTRec::Crosses", "Cluster2_ID"); 	 //ID of the top layer  cluster in the Cross
	fBSTRec_Crosses_trkID = fHipoReader->getBranch<uint16_t>("BSTRec::Crosses", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::LayerEffs:layer efficiencies ===================
	fBSTRec_LayerEffs_sector = fHipoReader->getBranch<uint8_t>("BSTRec::LayerEffs", "sector"); 	 //sector
	fBSTRec_LayerEffs_layer = fHipoReader->getBranch<uint8_t>("BSTRec::LayerEffs", "layer"); 	 //layer
	fBSTRec_LayerEffs_residual = fHipoReader->getBranch<float>("BSTRec::LayerEffs", "residual"); 	 //excluded layer residual of the matched cluster centroid
	fBSTRec_LayerEffs_status = fHipoReader->getBranch<uint8_t>("BSTRec::LayerEffs", "status"); 	 //status (-1: not used, i.e. no track; 0: inefficient; 1: efficient
//--------------------------------------------------------------------------------------------------------------------------------------

}
