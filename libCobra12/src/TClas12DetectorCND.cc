// Filename: TClas12DetectorCND.cc
// Description: CLAS12 CND base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorCND.h"


ClassImp(TClas12DetectorCND)
//-------------------------------------------------------------------------------------
TClas12DetectorCND::TClas12DetectorCND(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorCND::~TClas12DetectorCND()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorCND::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorCND::SetBranches()
{
//================== CND::hits:reconstructed hit info from CND ===================
	fCND_hits_id = fHipoReader->getBranch<uint16_t>("CND::hits", "id"); 	 //id of the hit
	fCND_hits_status = fHipoReader->getBranch<uint16_t>("CND::hits", "status"); 	 //status of the hit
	fCND_hits_trkID = fHipoReader->getBranch<uint16_t>("CND::hits", "trkID"); 	 //match CVT track index
	fCND_hits_sector = fHipoReader->getBranch<uint8_t>("CND::hits", "sector"); 	 //sector of CND
	fCND_hits_layer = fHipoReader->getBranch<uint8_t>("CND::hits", "layer"); 	 //panel id of CND
	fCND_hits_component = fHipoReader->getBranch<uint16_t>("CND::hits", "component"); 	 //paddle id of CND
	fCND_hits_energy = fHipoReader->getBranch<float>("CND::hits", "energy"); 	 //E dep (MeV) of hit
	fCND_hits_time = fHipoReader->getBranch<float>("CND::hits", "time"); 	 //Hit time (ns)
	fCND_hits_energy_unc = fHipoReader->getBranch<float>("CND::hits", "energy_unc"); 	 //E dep unc (MeV) of hit
	fCND_hits_time_unc = fHipoReader->getBranch<float>("CND::hits", "time_unc"); 	 //Hit time unc (ns)
	fCND_hits_x = fHipoReader->getBranch<float>("CND::hits", "x"); 	 //Global X coor (cm) of hit
	fCND_hits_y = fHipoReader->getBranch<float>("CND::hits", "y"); 	 //Global Y coor (cm) of hit
	fCND_hits_z = fHipoReader->getBranch<float>("CND::hits", "z"); 	 //Global Z coor (cm) of hit
	fCND_hits_x_unc = fHipoReader->getBranch<float>("CND::hits", "x_unc"); 	 //Global X coor unc (cm) of hit
	fCND_hits_y_unc = fHipoReader->getBranch<float>("CND::hits", "y_unc"); 	 //Global Y coor unc (cm) of hit
	fCND_hits_z_unc = fHipoReader->getBranch<float>("CND::hits", "z_unc"); 	 //Global Z coor unc (cm) of hit
	fCND_hits_tx = fHipoReader->getBranch<float>("CND::hits", "tx"); 	 //Global X coor (cm) of hit from CVT info -trkID index
	fCND_hits_ty = fHipoReader->getBranch<float>("CND::hits", "ty"); 	 //Global Y coor (cm) of hit from CVT info - trkID index
	fCND_hits_tz = fHipoReader->getBranch<float>("CND::hits", "tz"); 	 //Global Z coor (cm) of hit from CVT info - trkID index
	fCND_hits_tlength = fHipoReader->getBranch<float>("CND::hits", "tlength"); 	 //pathlength of the track from the entrance point to the exit point through the hit bar
	fCND_hits_pathlength = fHipoReader->getBranch<float>("CND::hits", "pathlength"); 	 //pathlength of the track from the vertex (doca point to the beamline to the midpoint between the entrance and exit of the hit bar
	fCND_hits_indexLadc = fHipoReader->getBranch<uint16_t>("CND::hits", "indexLadc"); 	 //index of the left adc
	fCND_hits_indexRadc = fHipoReader->getBranch<uint16_t>("CND::hits", "indexRadc"); 	 //index of the right adc
	fCND_hits_indexLtdc = fHipoReader->getBranch<uint16_t>("CND::hits", "indexLtdc"); 	 //index of the left tdc
	fCND_hits_indexRtdc = fHipoReader->getBranch<uint16_t>("CND::hits", "indexRtdc"); 	 //index of the right tdc
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CND::clusters:reconstructed cluster info from CND and CTOF ===================
	fCND_clusters_id = fHipoReader->getBranch<uint16_t>("CND::clusters", "id"); 	 //id of the cluster
	fCND_clusters_sector = fHipoReader->getBranch<uint8_t>("CND::clusters", "sector"); 	 //sector of the cluster's seed hit
	fCND_clusters_layer = fHipoReader->getBranch<uint8_t>("CND::clusters", "layer"); 	 //layer of the cluster's seed hit
	fCND_clusters_component = fHipoReader->getBranch<uint16_t>("CND::clusters", "component"); 	 //component of the cluster's seed hit
	fCND_clusters_nhits = fHipoReader->getBranch<uint16_t>("CND::clusters", "nhits"); 	 //number of hits of the cluster
	fCND_clusters_energy = fHipoReader->getBranch<float>("CND::clusters", "energy"); 	 //Sum of E dep (MeV) of the hits
	fCND_clusters_x = fHipoReader->getBranch<float>("CND::clusters", "x"); 	 //center of X coor (cm) of the hits
	fCND_clusters_y = fHipoReader->getBranch<float>("CND::clusters", "y"); 	 //center of Y coor (cm) of the hits
	fCND_clusters_z = fHipoReader->getBranch<float>("CND::clusters", "z"); 	 //center of Z coor (cm) of the hits
	fCND_clusters_time = fHipoReader->getBranch<float>("CND::clusters", "time"); 	 //center of time (ns) of the hits
	fCND_clusters_status = fHipoReader->getBranch<uint16_t>("CND::clusters", "status"); 	 //cluster status
//--------------------------------------------------------------------------------------------------------------------------------------

}
