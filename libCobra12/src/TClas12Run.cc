// Filename: TClas12Run.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12Run.h"
#include "TApplication.h"
#include "TClas12DC.h"
#include "TClas12ECAL.h"
#include "TClas12.h"

ClassImp(TClas12Run)
//---------------------------------------------------------------------
TClas12Run::TClas12Run(Int_t runNumber, Int_t fileNo):TClas12Event(runNumber, fileNo)
{
    Init();
}
//---------------------------------------------------------------------
TClas12Run::TClas12Run(TString fileName):TClas12Event(fileName)
{
    Init();
}
//---------------------------------------------------------------------
TClas12Run::~TClas12Run()
{
    delete fPartMap;
    delete fCharMap;
    delete fCherMap;
    delete fCaloMap;
    delete fScinMap;
    delete fForwMap;
    delete fTracMap;
    delete fTrajMap;
}
//---------------------------------------------------------------------
void TClas12Run::Init()
{
    fPartMap = new unordered_multimap <Int_t, Int_t>();
    fCharMap = new unordered_multimap <Int_t, Int_t>();
    
    fCherMap = new unordered_map <Int_t, Int_t>();    
    fCaloMap = new unordered_map <Int_t, Int_t>();    
    fScinMap = new unordered_map <Int_t, Int_t>();    
    fForwMap = new unordered_map <Int_t, Int_t>();    
    fTracMap = new unordered_map <Int_t, Int_t>();    
    fTrajMap = new unordered_map <Int_t, Int_t>();    
    fEventNo = -1;

    fIsDCAdded = kFALSE;   
    fIsECALAdded = kFALSE;   
}
//---------------------------------------------------------------------------
Bool_t TClas12Run::HasEvent()
{
    Bool_t hasEvent = fHipoReader->next();

    if(!hasEvent)
	return hasEvent;

    fPartSize = fREC_Particle_pid->length();
    fCherSize = fREC_Cherenkov_pindex->length();
    fCaloSize = fREC_Calorimeter_pindex->length();
    fScinSize = fREC_Scintillator_pindex->length();
    fTracSize = fREC_Track_pindex->length();
    fTrajSize = fREC_Traj_pindex->length();
    fForwSize = fREC_ForwardTagger_pindex->length();

    if(TClas12::fTurnOnEventMap)
	GenerateMap();
    UpdateDetectors();
    ++fEventNo;

    return hasEvent;
}
//---------------------------------------------------------------------
void TClas12Run::GenerateMap()
{
    fPartMap->clear();
    fCharMap->clear();
    fCherMap->clear();
    fCaloMap->clear();
    fScinMap->clear();
    fForwMap->clear();
    fTracMap->clear();
    fTrajMap->clear();

    Int_t pid = -1;
    Int_t charge = -10;
    Int_t hashKey = -1;
    Int_t pindex = -1;
    Int_t detector_type = -1;
    Int_t layer = -1;

    //--------- Particle -----------
    for(Int_t i = 0; i < fPartSize; ++i)
    {
	pid = fREC_Particle_pid->getValue(i);
	fPartMap->insert({pid,i});
    }
    //--------- Charge -----------
    for(Int_t i = 0; i < fPartSize; ++i)
    {
	charge = fREC_Particle_charge->getValue(i);
	fCharMap->insert({charge,i});
    }    
    //------- ECal -----------
    for(Int_t i = 0; i < fCaloSize; ++i)
    {
	pindex = fREC_Calorimeter_pindex->getValue(i);
	detector_type = fREC_Calorimeter_detector->getValue(i);
	layer = fREC_Calorimeter_layer->getValue(i);
	hashKey = 100*pindex + 10*detector_type + layer;
	fCaloMap->insert({hashKey,i});
    }
    //---------- Scintillator/ TOF -----------
    for(Int_t i = 0; i < fScinSize; ++i)
    {
	pindex = fREC_Scintillator_pindex->getValue(i);
	detector_type = fREC_Scintillator_detector->getValue(i);
	layer = fREC_Scintillator_layer->getValue(i);
	hashKey = 100*pindex + 10*detector_type + layer;
	fScinMap->insert({hashKey,i});
    }
    //------- CC -------------
    for(Int_t i = 0; i < fCherSize; ++i)
    {
	pindex = fREC_Cherenkov_pindex->getValue(i);
	detector_type = fREC_Cherenkov_detector->getValue(i);
	hashKey = 100*pindex + 10*detector_type;
	fCherMap->insert({hashKey,i});
    }
    //---------- Forwardtagger ---------
    for(Int_t i = 0; i < fForwSize; ++i)
    {
	pindex = fREC_ForwardTagger_pindex->getValue(i);
	detector_type = fREC_ForwardTagger_detector->getValue(i);
	hashKey = 100*pindex + 10*detector_type;
	fForwMap->insert({hashKey,i});
    }
    //--------- Track (DC) ----------
    for(Int_t i = 0; i < fTracSize; ++i)
    {
	pindex = fREC_Track_pindex->getValue(i);
	detector_type = fREC_Track_detector->getValue(i);
	hashKey = 100*pindex + 10*detector_type;
	fTracMap->insert({hashKey,i});
    }
    //------------ Trajectory ---------
    for(Int_t i = 0; i < fTrajSize; ++i)
    {
	pindex = fREC_Traj_pindex->getValue(i);
	detector_type = fREC_Traj_detId->getValue(i);
	hashKey = 100*pindex + 10*detector_type;
	fTrajMap->insert({hashKey,i});
    }    
}
//---------------------------------------------------------------------------
Int_t TClas12Run::GetDetectorID(TString detectorName)
{
    // ----------- Following assignments are based on CoatJava convention. see detector class coatjava source coad for details. -------------------
       	
    if(detectorName == "BMT")
	return 1;    
    else if(detectorName == "BST")
	return 2;
    else if(detectorName ==  "CND")
	return 3;
    else if(detectorName ==  "CTOF")
	return 4;
    else if(detectorName == "CVT")
	return 5;
    else if(detectorName ==  "DC")
	return 6;
    else if(detectorName ==  "ECAL")
	return 7;
    else if(detectorName ==  "FMT")
	return 8;
    else if(detectorName ==  "FT")
	return 9;
    else if(detectorName ==  "FTCAL")
	return 10;
    else if(detectorName ==  "FTHODO")
	return 11;
    else if(detectorName ==  "FTOF")
	return 12;
    else if(detectorName ==  "FTTRK")
	return 13;
    else if(detectorName ==  "HTCC")
	return 15;
    else if(detectorName ==  "LTCC")
	return 16;
    else if(detectorName ==  "RF")
	return 17;
    else if(detectorName ==  "RICH")
	return 18;
    else if(detectorName ==  "RTPC")
	return 19;
    else if(detectorName ==  "HEL")
	return 20;
    else if(detectorName ==  "ECIN")               // Obsolere ??
	return 110;
    else if(detectorName ==  "ECOUT")              // Obsolete or duplication ?? 
	return 111;
    else if(detectorName ==  "ECTOT")
	return 112;
    else if(detectorName ==  "LAC")
	return 113;
    else if(detectorName ==  "SC")
	return 114;
    else if(detectorName ==  "CC")
	return 115;
    else if(detectorName ==  "SVT")
	return 220;

	// FIXME add layer conventions here, e.g.:
	// (or maybe they are defined somewhere else?)
    else if(detectorName ==  "FTOF1A")
	return 1;
    else if(detectorName ==  "FTOF1B")
	return 2;
    else if(detectorName ==  "FTOF2")
	return 3;
    else if(detectorName ==  "PCAL")
	return 1;
    else if(detectorName ==  "ECINNER")
	return 4;
    else if(detectorName ==  "ECOUTER")
	return 7;
    else
    {
	cout << "Invalid Detector Name" <<endl;
	gApplication->Terminate();
	return -1;
    }
}
//--------------------------------------------------------------------------
TClas12Detector* TClas12Run::AddDetector(TString detectorName)
{
    if(detectorName == "DC")
    {
	fDC = new TClas12DC(this);
	fIsDCAdded = kTRUE;
	cout << "Added detector DC" <<endl;
	return fDC;
    }
    else if(detectorName == "ECAL")
    {
	fECAL = new TClas12ECAL(this);
	fIsECALAdded = kTRUE;
	cout << "Added detector ECAL" <<endl;
	return fECAL;
    }
    else
    {
	cout << "Invalid detector name: "<< detectorName <<endl;
	TClas12::Exit();	
    }
}
//---------------------------------------------------------------------------
void TClas12Run::UpdateDetectors()
{
    if(fIsDCAdded)
	fDC->Update();
    if(fIsECALAdded)
	fECAL->Update();
}

