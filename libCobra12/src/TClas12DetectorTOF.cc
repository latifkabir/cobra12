// Filename: TClas12DetectorTOF.cc
// Description: CLAS12 TOF base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorTOF.h"


ClassImp(TClas12DetectorTOF)
//-------------------------------------------------------------------------------------
TClas12DetectorTOF::TClas12DetectorTOF(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorTOF::~TClas12DetectorTOF()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorTOF::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorTOF::SetBranches()
{
//================== FTOF::rawhits:reconstructed hit info from L/R FTOF PMTs ===================
	fFTOF_rawhits_id = fHipoReader->getBranch<uint16_t>("FTOF::rawhits", "id"); 	 //id of the hit
	fFTOF_rawhits_status = fHipoReader->getBranch<uint16_t>("FTOF::rawhits", "status"); 	 //status of the hit
	fFTOF_rawhits_sector = fHipoReader->getBranch<uint8_t>("FTOF::rawhits", "sector"); 	 //sector of FTOF
	fFTOF_rawhits_layer = fHipoReader->getBranch<uint8_t>("FTOF::rawhits", "layer"); 	 //panel id of FTOF (1-1A, 2-1B, 3-2
	fFTOF_rawhits_component = fHipoReader->getBranch<uint16_t>("FTOF::rawhits", "component"); 	 //paddle id of FTOF
	fFTOF_rawhits_energy_left = fHipoReader->getBranch<float>("FTOF::rawhits", "energy_left"); 	 //E dep (MeV) from L PMT
	fFTOF_rawhits_energy_right = fHipoReader->getBranch<float>("FTOF::rawhits", "energy_right"); 	 //E dep (MeV) from R PMT
	fFTOF_rawhits_time_left = fHipoReader->getBranch<float>("FTOF::rawhits", "time_left"); 	 //Hit time (ns) from L PMT
	fFTOF_rawhits_time_right = fHipoReader->getBranch<float>("FTOF::rawhits", "time_right"); 	 //Hit time (ns) from R PMT
	fFTOF_rawhits_energy_left_unc = fHipoReader->getBranch<float>("FTOF::rawhits", "energy_left_unc"); 	 //E dep unc (MeV) from L PMT
	fFTOF_rawhits_energy_right_unc = fHipoReader->getBranch<float>("FTOF::rawhits", "energy_right_unc"); 	 //E dep unc (MeV) from R PMT
	fFTOF_rawhits_time_left_unc = fHipoReader->getBranch<float>("FTOF::rawhits", "time_left_unc"); 	 //Hit time unc (ns) from L PMT
	fFTOF_rawhits_time_right_unc = fHipoReader->getBranch<float>("FTOF::rawhits", "time_right_unc"); 	 //Hit time unc (ns) from R PMT
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::hits:reconstructed hit info from FTOF ===================
	fFTOF_hits_id = fHipoReader->getBranch<uint16_t>("FTOF::hits", "id"); 	 //id of the hit
	fFTOF_hits_status = fHipoReader->getBranch<uint16_t>("FTOF::hits", "status"); 	 //status of the hit
	fFTOF_hits_trackid = fHipoReader->getBranch<uint16_t>("FTOF::hits", "trackid"); 	 //matched DC track id
	fFTOF_hits_sector = fHipoReader->getBranch<uint8_t>("FTOF::hits", "sector"); 	 //sector of FTOF
	fFTOF_hits_layer = fHipoReader->getBranch<uint8_t>("FTOF::hits", "layer"); 	 //panel id of FTOF (1-1A, 2-1B, 3-2
	fFTOF_hits_component = fHipoReader->getBranch<uint16_t>("FTOF::hits", "component"); 	 //paddle id of FTOF
	fFTOF_hits_energy = fHipoReader->getBranch<float>("FTOF::hits", "energy"); 	 //E dep (MeV) of hit
	fFTOF_hits_time = fHipoReader->getBranch<float>("FTOF::hits", "time"); 	 //Hit time (ns)
	fFTOF_hits_energy_unc = fHipoReader->getBranch<float>("FTOF::hits", "energy_unc"); 	 //E dep unc (MeV) of hit
	fFTOF_hits_time_unc = fHipoReader->getBranch<float>("FTOF::hits", "time_unc"); 	 //Hit time unc (ns)
	fFTOF_hits_x = fHipoReader->getBranch<float>("FTOF::hits", "x"); 	 //Global X coor (cm) of hit
	fFTOF_hits_y = fHipoReader->getBranch<float>("FTOF::hits", "y"); 	 //Global Y coor (cm) of hit
	fFTOF_hits_z = fHipoReader->getBranch<float>("FTOF::hits", "z"); 	 //Global Z coor (cm) of hit
	fFTOF_hits_x_unc = fHipoReader->getBranch<float>("FTOF::hits", "x_unc"); 	 //Global X coor unc (cm) of hit
	fFTOF_hits_y_unc = fHipoReader->getBranch<float>("FTOF::hits", "y_unc"); 	 //Global Y coor unc (cm) of hit
	fFTOF_hits_z_unc = fHipoReader->getBranch<float>("FTOF::hits", "z_unc"); 	 //Global Z coor unc (cm) of hit
	fFTOF_hits_tx = fHipoReader->getBranch<float>("FTOF::hits", "tx"); 	 //Global X coor (cm) of hit from DC info - trackid index
	fFTOF_hits_ty = fHipoReader->getBranch<float>("FTOF::hits", "ty"); 	 //Global Y coor (cm) of hit from DC info - trackid index
	fFTOF_hits_tz = fHipoReader->getBranch<float>("FTOF::hits", "tz"); 	 //Global Z coor (cm) of hit from DC info - trackid index
	fFTOF_hits_adc_idx1 = fHipoReader->getBranch<uint16_t>("FTOF::hits", "adc_idx1"); 	 //index of hit with ADCL in adc bank, starts at zero
	fFTOF_hits_adc_idx2 = fHipoReader->getBranch<uint16_t>("FTOF::hits", "adc_idx2"); 	 //index of hit with ADCR in adc bank, starts at zero
	fFTOF_hits_tdc_idx1 = fHipoReader->getBranch<uint16_t>("FTOF::hits", "tdc_idx1"); 	 //index of hit with TDCL in tdc bank, starts at zero
	fFTOF_hits_tdc_idx2 = fHipoReader->getBranch<uint16_t>("FTOF::hits", "tdc_idx2"); 	 //index of hit with TDCR in tdc bank, starts at zero
	fFTOF_hits_pathLength = fHipoReader->getBranch<float>("FTOF::hits", "pathLength"); 	 //pathlength of the track from the vertex (doca point to the beamline to the midpoint between the entrance and exit of the hit bar
	fFTOF_hits_pathLengthThruBar = fHipoReader->getBranch<float>("FTOF::hits", "pathLengthThruBar"); 	 //pathlength of the track from the entrance point to the exit point through the hit bar 
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::clusters:reconstructed clusters from FTOF ===================
	fFTOF_clusters_id = fHipoReader->getBranch<uint16_t>("FTOF::clusters", "id"); 	 //id of the hit
	fFTOF_clusters_status = fHipoReader->getBranch<uint16_t>("FTOF::clusters", "status"); 	 //status of the cluster
	fFTOF_clusters_trackid = fHipoReader->getBranch<uint16_t>("FTOF::clusters", "trackid"); 	 //matched DC track id
	fFTOF_clusters_sector = fHipoReader->getBranch<uint8_t>("FTOF::clusters", "sector"); 	 //sector of FTOF
	fFTOF_clusters_layer = fHipoReader->getBranch<uint8_t>("FTOF::clusters", "layer"); 	 //panel id of FTOF (1-1A, 2-1B, 3-2
	fFTOF_clusters_component = fHipoReader->getBranch<uint16_t>("FTOF::clusters", "component"); 	 //paddle id of FTOF hit with lowest paddle id in cluster 
	fFTOF_clusters_energy = fHipoReader->getBranch<float>("FTOF::clusters", "energy"); 	 //E dep (MeV) of the cluster
	fFTOF_clusters_time = fHipoReader->getBranch<float>("FTOF::clusters", "time"); 	 //Cluster hit time (ns)
	fFTOF_clusters_energy_unc = fHipoReader->getBranch<float>("FTOF::clusters", "energy_unc"); 	 //E dep unc (MeV) of the cluster
	fFTOF_clusters_time_unc = fHipoReader->getBranch<float>("FTOF::clusters", "time_unc"); 	 //Cluster hit time unc (ns)
	fFTOF_clusters_x = fHipoReader->getBranch<float>("FTOF::clusters", "x"); 	 //Global X coor (cm) of cluster hit
	fFTOF_clusters_y = fHipoReader->getBranch<float>("FTOF::clusters", "y"); 	 //Global Y coor (cm) of cluster hit
	fFTOF_clusters_z = fHipoReader->getBranch<float>("FTOF::clusters", "z"); 	 //Global Z coor (cm) of cluster hit
	fFTOF_clusters_x_unc = fHipoReader->getBranch<float>("FTOF::clusters", "x_unc"); 	 //Global X coor unc (cm)  of cluster hit
	fFTOF_clusters_y_unc = fHipoReader->getBranch<float>("FTOF::clusters", "y_unc"); 	 //Global Y coor unc (cm) of cluster hit
	fFTOF_clusters_z_unc = fHipoReader->getBranch<float>("FTOF::clusters", "z_unc"); 	 //Global Z coor unc (cm) of cluster hit
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::matchedclusters:matched clusters from FTOF ===================
	fFTOF_matchedclusters_sector = fHipoReader->getBranch<uint8_t>("FTOF::matchedclusters", "sector"); 	 //sector of FTOF
	fFTOF_matchedclusters_paddle_id1A = fHipoReader->getBranch<uint16_t>("FTOF::matchedclusters", "paddle_id1A"); 	 //paddle id of hit with lowest paddle id in cluster for panel 1A
	fFTOF_matchedclusters_paddle_id1B = fHipoReader->getBranch<uint16_t>("FTOF::matchedclusters", "paddle_id1B"); 	 //paddle id of hit with lowest paddle id in cluster for panel 1B
	fFTOF_matchedclusters_clus_1Aid = fHipoReader->getBranch<uint16_t>("FTOF::matchedclusters", "clus_1Aid"); 	 //id of cluster in 1A
	fFTOF_matchedclusters_clus_1Bid = fHipoReader->getBranch<uint16_t>("FTOF::matchedclusters", "clus_1Bid"); 	 //id of cluster in 1B
	fFTOF_matchedclusters_clusSize_1A = fHipoReader->getBranch<uint16_t>("FTOF::matchedclusters", "clusSize_1A"); 	 //size of cluster in 1A
	fFTOF_matchedclusters_clusSize_1B = fHipoReader->getBranch<uint16_t>("FTOF::matchedclusters", "clusSize_1B"); 	 //size of cluster in 1B
	fFTOF_matchedclusters_tminAlgo_1B_tCorr = fHipoReader->getBranch<float>("FTOF::matchedclusters", "tminAlgo_1B_tCorr"); 	 //corrected hit time using tmin algorithm to compute the path length between 1A and 1B
	fFTOF_matchedclusters_midbarAlgo_1B_tCorr = fHipoReader->getBranch<float>("FTOF::matchedclusters", "midbarAlgo_1B_tCorr"); 	 //corrected hit time using middle of bar algorithm to compute the path length between 1A and 1B
	fFTOF_matchedclusters_EmaxAlgo_1B_tCorr = fHipoReader->getBranch<float>("FTOF::matchedclusters", "EmaxAlgo_1B_tCorr"); 	 //corrected hit time using Emax algorithm to compute the path length between 1A and 1B
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CTOF::rawhits:reconstructed hit info from U/D CTOF PMTs ===================
	fCTOF_rawhits_id = fHipoReader->getBranch<uint16_t>("CTOF::rawhits", "id"); 	 //id of the hit
	fCTOF_rawhits_status = fHipoReader->getBranch<uint16_t>("CTOF::rawhits", "status"); 	 //id of the hit
	fCTOF_rawhits_component = fHipoReader->getBranch<uint16_t>("CTOF::rawhits", "component"); 	 //paddle id of CTOF
	fCTOF_rawhits_energy_up = fHipoReader->getBranch<float>("CTOF::rawhits", "energy_up"); 	 //E dep (MeV) from U PMT
	fCTOF_rawhits_energy_down = fHipoReader->getBranch<float>("CTOF::rawhits", "energy_down"); 	 //E dep (MeV) from D PMT
	fCTOF_rawhits_time_up = fHipoReader->getBranch<float>("CTOF::rawhits", "time_up"); 	 //Hit time (ns) from U PMT
	fCTOF_rawhits_time_down = fHipoReader->getBranch<float>("CTOF::rawhits", "time_down"); 	 //Hit time (ns) from D PMT
	fCTOF_rawhits_energy_up_unc = fHipoReader->getBranch<float>("CTOF::rawhits", "energy_up_unc"); 	 //E dep unc (MeV) from U PMT
	fCTOF_rawhits_energy_down_unc = fHipoReader->getBranch<float>("CTOF::rawhits", "energy_down_unc"); 	 //E dep unc (MeV) from D PMT
	fCTOF_rawhits_time_up_unc = fHipoReader->getBranch<float>("CTOF::rawhits", "time_up_unc"); 	 //Hit time unc (ns) from U PMT
	fCTOF_rawhits_time_down_unc = fHipoReader->getBranch<float>("CTOF::rawhits", "time_down_unc"); 	 //Hit time unc (ns) from D PMT
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CTOF::hits:reconstructed hit info from CTOF ===================
	fCTOF_hits_id = fHipoReader->getBranch<uint16_t>("CTOF::hits", "id"); 	 //id of the hit
	fCTOF_hits_status = fHipoReader->getBranch<uint16_t>("CTOF::hits", "status"); 	 //status of the hit
	fCTOF_hits_trkID = fHipoReader->getBranch<uint16_t>("CTOF::hits", "trkID"); 	 //match CVT track id
	fCTOF_hits_sector = fHipoReader->getBranch<uint8_t>("CTOF::hits", "sector"); 	 //sector of CTOF
	fCTOF_hits_layer = fHipoReader->getBranch<uint8_t>("CTOF::hits", "layer"); 	 //panel id of CTOF
	fCTOF_hits_component = fHipoReader->getBranch<uint16_t>("CTOF::hits", "component"); 	 //paddle id of CTOF
	fCTOF_hits_energy = fHipoReader->getBranch<float>("CTOF::hits", "energy"); 	 //E dep (MeV) of hit
	fCTOF_hits_time = fHipoReader->getBranch<float>("CTOF::hits", "time"); 	 //Hit time (ns)
	fCTOF_hits_energy_unc = fHipoReader->getBranch<float>("CTOF::hits", "energy_unc"); 	 //E dep unc (MeV) of hit
	fCTOF_hits_time_unc = fHipoReader->getBranch<float>("CTOF::hits", "time_unc"); 	 //Hit time unc (ns)
	fCTOF_hits_x = fHipoReader->getBranch<float>("CTOF::hits", "x"); 	 //Global X coor (cm) of hit
	fCTOF_hits_y = fHipoReader->getBranch<float>("CTOF::hits", "y"); 	 //Global Y coor (cm) of hit
	fCTOF_hits_z = fHipoReader->getBranch<float>("CTOF::hits", "z"); 	 //Global Z coor (cm) of hit
	fCTOF_hits_x_unc = fHipoReader->getBranch<float>("CTOF::hits", "x_unc"); 	 //Global X coor unc (cm) of hit
	fCTOF_hits_y_unc = fHipoReader->getBranch<float>("CTOF::hits", "y_unc"); 	 //Global Y coor unc (cm) of hit
	fCTOF_hits_z_unc = fHipoReader->getBranch<float>("CTOF::hits", "z_unc"); 	 //Global Z coor unc (cm) of hit
	fCTOF_hits_tx = fHipoReader->getBranch<float>("CTOF::hits", "tx"); 	 //Global X coor (cm) of hit from CVT info -trkID index
	fCTOF_hits_ty = fHipoReader->getBranch<float>("CTOF::hits", "ty"); 	 //Global Y coor (cm) of hit from CVT info - trkID index
	fCTOF_hits_tz = fHipoReader->getBranch<float>("CTOF::hits", "tz"); 	 //Global Z coor (cm) of hit from CVT info - trkID index
	fCTOF_hits_adc_idx1 = fHipoReader->getBranch<uint16_t>("CTOF::hits", "adc_idx1"); 	 //index of hit with ADCU in adc bank, starts at zero
	fCTOF_hits_adc_idx2 = fHipoReader->getBranch<uint16_t>("CTOF::hits", "adc_idx2"); 	 //index of hit with ADCD in adc bank, starts at zero
	fCTOF_hits_tdc_idx1 = fHipoReader->getBranch<uint16_t>("CTOF::hits", "tdc_idx1"); 	 //index of hit with TDCU in tdc bank, starts at zero
	fCTOF_hits_tdc_idx2 = fHipoReader->getBranch<uint16_t>("CTOF::hits", "tdc_idx2"); 	 //index of hit with TDCD in tdc bank, starts at zero
	fCTOF_hits_pathLength = fHipoReader->getBranch<float>("CTOF::hits", "pathLength"); 	 //pathlength of the track from the vertex (doca point to the beamline to the midpoint between the entrance and exit of the hit bar
	fCTOF_hits_pathLengthThruBar = fHipoReader->getBranch<float>("CTOF::hits", "pathLengthThruBar"); 	 //pathlength of the track from the entrance point to the exit point through the hit bar
//--------------------------------------------------------------------------------------------------------------------------------------

}
