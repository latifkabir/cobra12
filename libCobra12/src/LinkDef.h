#ifdef __CINT__

#pragma link C++ namespace hipo;                     // Namespace hipo
#pragma link C++ function help;                      // Print Info
#pragma link C++ class TClas12;                      // Collection of Static Functions
#pragma link C++ class TClas12Config;                // Configuration manager class
#pragma link C++ function jsonEx;                    // Print bank definition
#pragma link C++ function PrintRunList;              // Print bank definition

//:============ IO Module ============: :
#pragma link C++ class TClas12Event;                 // Abstract Base class for TClas12Run
#pragma link C++ class TClas12Run;                   // The main Class for the CLAS12 to read run data
#pragma link C++ class TClas12Detector;              // Abstract Class for CLAS12 Detectors 
#pragma link C++ class TClas12DetectorDC;            // CLAS12 Drift Chamber Base Class 
#pragma link C++ class TClas12DC;                    // CLAS12 Drift Chamber Class 
#pragma link C++ class TClas12DetectorTOF;           // CLAS12 Detector TOF Base Class 
//#pragma link C++ class TClas12TOF;                   // CLAS12 TOF Class 
#pragma link C++ class TClas12DetectorECAL;          // CLAS12 Detector ECAL Base Class 
#pragma link C++ class TClas12ECAL;                  // CLAS12 ECAL Class 
#pragma link C++ class TClas12DetectorHTCC;          // CLAS12 Detector HTCC Base Class 
//#pragma link C++ class TClas12HTCC;                  // CLAS12 HTCC Class 
#pragma link C++ class TClas12DetectorLTCC;          // CLAS12 Detector LTCC Base Class 
//#pragma link C++ class TClas12LTCC;                  // CLAS12 LTCC Class 
#pragma link C++ class TClas12DetectorCVT;           // CLAS12 Detector CVT Base Class 
//#pragma link C++ class TClas12CVT;                   // CLAS12 CVT Class 
#pragma link C++ class TClas12DetectorCND;           // CLAS12 Detector CND Base Class 
//#pragma link C++ class TClas12CND;                   // CLAS12 CND Class 
#pragma link C++ class TClas12DetectorFT;            // CLAS12 Detector FT Base Class 
//#pragma link C++ class TClas12FT;                    // CLAS12 FT Class 
#pragma link C++ class TClas12DetectorFMT;           // CLAS12 Detector FMT Base Class 
//#pragma link C++ class TClas12FMT;                   // CLAS12 FMT Class 
#pragma link C++ class TClas12DetectorRICH;          // CLAS12 Detector RICH Base Class 
//#pragma link C++ class TClas12RICH;                  // CLAS12 RICH Class 
#pragma link C++ class TClas12DetectorBMT;           // CLAS12 Detector BMT Base Class 
//#pragma link C++ class TClas12BMT;                   // CLAS12 BMT Class 
#pragma link C++ class TClas12DetectorBST;           // CLAS12 Detector BST Base Class 
//#pragma link C++ class TClas12BST;                   // CLAS12 BST Class 
#pragma link C++ class TClas12DetectorMC;            // CLAS12 MC Base Class 
//#pragma link C++ class TClas12MC;                    // CLAS12 MC Class 
#pragma link C++ class TClas12DetectorHEADER;        // CLAS12 HEADER Base Class 
//#pragma link C++ class TClas12HEADER;                // CLAS12 HEADER Class 

//:============ PID Module ============: :
//#pragma link C++ class TClas12ExclusivityPID;        // CLAS12 PID Implementation Using Exclusivity Cut
//#pragma link C++ class TClas12KinFittingPID;         // CLAS12 PID Implementation Using Kinematic Fitting
//#pragma link C++ class TClas12MLPID;                 // CLAS12 PID Implementation Using Machine Learning

//:============ Utility Module ============: :
//#pragma link C++ class TClas12FileMerger;            // DST Root file merger
//#pragma link C++ class TClas12CCDB;                  // CLAS12 CCDB Interface
//#pragma link C++ class TClas12RCDB;                  // CLAS12 RCDB Interface


#endif
