// Filename: TClas12DetectorMC.cc
// Description: CLAS12 MC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorMC.h"


ClassImp(TClas12DetectorMC)
//-------------------------------------------------------------------------------------
TClas12DetectorMC::TClas12DetectorMC(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorMC::~TClas12DetectorMC()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorMC::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorMC::SetBranches()
{
//================== MC::Header:Head bank for the generated event ===================
	fMC_Header_run = fHipoReader->getBranch<uint32_t>("MC::Header", "run"); 	 //Run number
	fMC_Header_event = fHipoReader->getBranch<uint32_t>("MC::Header", "event"); 	 //Event number
	fMC_Header_type = fHipoReader->getBranch<uint8_t>("MC::Header", "type"); 	 //Event type
	fMC_Header_helicity = fHipoReader->getBranch<float>("MC::Header", "helicity"); 	 //Beam helicity
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Event:Lund header bank for the generated event ===================
	fMC_Event_npart = fHipoReader->getBranch<uint16_t>("MC::Event", "npart"); 	 //number of particles in the event
	fMC_Event_atarget = fHipoReader->getBranch<uint16_t>("MC::Event", "atarget"); 	 //Mass number of the target
	fMC_Event_ztarget = fHipoReader->getBranch<uint16_t>("MC::Event", "ztarget"); 	 //Atomic number oif the target
	fMC_Event_ptarget = fHipoReader->getBranch<float>("MC::Event", "ptarget"); 	 //Target polarization
	fMC_Event_pbeam = fHipoReader->getBranch<float>("MC::Event", "pbeam"); 	 //Beam polarization
	fMC_Event_btype = fHipoReader->getBranch<uint16_t>("MC::Event", "btype"); 	 //Beam type, electron=11, photon=22
	fMC_Event_ebeam = fHipoReader->getBranch<float>("MC::Event", "ebeam"); 	 //Beam energy (GeV)
	fMC_Event_targetid = fHipoReader->getBranch<uint16_t>("MC::Event", "targetid"); 	 //Interacted nucleaon ID (proton=2212, neutron=2112
	fMC_Event_processid = fHipoReader->getBranch<uint16_t>("MC::Event", "processid"); 	 //Process ID
	fMC_Event_weight = fHipoReader->getBranch<float>("MC::Event", "weight"); 	 //Event weight
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Particle:Generated Particle information ===================
	fMC_Particle_pid = fHipoReader->getBranch<uint32_t>("MC::Particle", "pid"); 	 //particle id
	fMC_Particle_px = fHipoReader->getBranch<float>("MC::Particle", "px"); 	 //x component of the momentum
	fMC_Particle_py = fHipoReader->getBranch<float>("MC::Particle", "py"); 	 //y component of the momentum
	fMC_Particle_pz = fHipoReader->getBranch<float>("MC::Particle", "pz"); 	 //z component of the momentum
	fMC_Particle_vx = fHipoReader->getBranch<float>("MC::Particle", "vx"); 	 //x component of the vertex
	fMC_Particle_vy = fHipoReader->getBranch<float>("MC::Particle", "vy"); 	 //y component of the vertex
	fMC_Particle_vz = fHipoReader->getBranch<float>("MC::Particle", "vz"); 	 //z component of the vertex
	fMC_Particle_vt = fHipoReader->getBranch<float>("MC::Particle", "vt"); 	 //vertex time
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Lund:Generated Particle information from Lund ===================
	fMC_Lund_index = fHipoReader->getBranch<uint8_t>("MC::Lund", "index"); 	 //index of the first daughter
	fMC_Lund_type = fHipoReader->getBranch<uint8_t>("MC::Lund", "type"); 	 //particle type (1 is active)
	fMC_Lund_pid = fHipoReader->getBranch<uint32_t>("MC::Lund", "pid"); 	 //particle id
	fMC_Lund_parent = fHipoReader->getBranch<uint8_t>("MC::Lund", "parent"); 	 //index of the parent
	fMC_Lund_daughter = fHipoReader->getBranch<uint8_t>("MC::Lund", "daughter"); 	 //index of the first daughter
	fMC_Lund_px = fHipoReader->getBranch<float>("MC::Lund", "px"); 	 //x component of the momentum
	fMC_Lund_py = fHipoReader->getBranch<float>("MC::Lund", "py"); 	 //y component of the momentum
	fMC_Lund_pz = fHipoReader->getBranch<float>("MC::Lund", "pz"); 	 //z component of the momentum
	fMC_Lund_E = fHipoReader->getBranch<float>("MC::Lund", "E"); 	 //Energy of the particle
	fMC_Lund_mass = fHipoReader->getBranch<float>("MC::Lund", "mass"); 	 //mass of the particle
	fMC_Lund_vx = fHipoReader->getBranch<float>("MC::Lund", "vx"); 	 //x component of the vertex
	fMC_Lund_vy = fHipoReader->getBranch<float>("MC::Lund", "vy"); 	 //y component of the vertex
	fMC_Lund_vz = fHipoReader->getBranch<float>("MC::Lund", "vz"); 	 //z component of the vertex
	fMC_Lund_ltime = fHipoReader->getBranch<float>("MC::Lund", "ltime"); 	 //particle lifetime
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::True:True detector information from GEANT4 ===================
	fMC_True_detector = fHipoReader->getBranch<uint8_t>("MC::True", "detector"); 	 //detector ID
	fMC_True_pid = fHipoReader->getBranch<uint32_t>("MC::True", "pid"); 	 //ID of the first particle entering the sensitive volume
	fMC_True_mpid = fHipoReader->getBranch<uint32_t>("MC::True", "mpid"); 	 //ID of the mother of the first particle entering the sensitive volume
	fMC_True_tid = fHipoReader->getBranch<uint32_t>("MC::True", "tid"); 	 //Track ID of the first particle entering the sensitive volume
	fMC_True_mtid = fHipoReader->getBranch<uint32_t>("MC::True", "mtid"); 	 //Track ID of the mother of the first particle entering the sensitive volume
	fMC_True_otid = fHipoReader->getBranch<uint32_t>("MC::True", "otid"); 	 //Track ID of the original track that generated the first particle entering the sensitive volume
	fMC_True_trackE = fHipoReader->getBranch<float>("MC::True", "trackE"); 	 //Energy of the track
	fMC_True_totEdep = fHipoReader->getBranch<float>("MC::True", "totEdep"); 	 //Total Energy Deposited
	fMC_True_avgX = fHipoReader->getBranch<float>("MC::True", "avgX"); 	 //Average X position in global reference system
	fMC_True_avgY = fHipoReader->getBranch<float>("MC::True", "avgY"); 	 //Average Y position in global reference system
	fMC_True_avgZ = fHipoReader->getBranch<float>("MC::True", "avgZ"); 	 //Average Z position in global reference system
	fMC_True_avgLx = fHipoReader->getBranch<float>("MC::True", "avgLx"); 	 //Average X position in local reference system
	fMC_True_avgLy = fHipoReader->getBranch<float>("MC::True", "avgLy"); 	 //Average Y position in local reference system
	fMC_True_avgLz = fHipoReader->getBranch<float>("MC::True", "avgLz"); 	 //Average Z position in local reference system
	fMC_True_px = fHipoReader->getBranch<float>("MC::True", "px"); 	 //x component of momentum of the particle entering the sensitive volume
	fMC_True_py = fHipoReader->getBranch<float>("MC::True", "py"); 	 //y component of momentum of the particle entering the sensitive volume
	fMC_True_pz = fHipoReader->getBranch<float>("MC::True", "pz"); 	 //z component of momentum of the particle entering the sensitive volume
	fMC_True_vx = fHipoReader->getBranch<float>("MC::True", "vx"); 	 //x component of primary vertex of the particle entering the sensitive volume
	fMC_True_vy = fHipoReader->getBranch<float>("MC::True", "vy"); 	 //y component of primary vertex of the particle entering the sensitive volume
	fMC_True_vz = fHipoReader->getBranch<float>("MC::True", "vz"); 	 //z component of primary vertex of the particle entering the sensitive volume
	fMC_True_mvx = fHipoReader->getBranch<float>("MC::True", "mvx"); 	 //x component of primary vertex of the mother of the particle entering the sensitive volume
	fMC_True_mvy = fHipoReader->getBranch<float>("MC::True", "mvy"); 	 //y component of primary vertex of the mother of the particle entering the sensitive volume
	fMC_True_mvz = fHipoReader->getBranch<float>("MC::True", "mvz"); 	 //z component of primary vertex of the mother of the particle entering the sensitive volume
	fMC_True_avgT = fHipoReader->getBranch<float>("MC::True", "avgT"); 	 //Average time
	fMC_True_nsteps = fHipoReader->getBranch<uint32_t>("MC::True", "nsteps"); 	 //Number of geant4 steps
	fMC_True_procID = fHipoReader->getBranch<uint32_t>("MC::True", "procID"); 	 //Process that created the FP. see gemc.jlab.org
	fMC_True_hitn = fHipoReader->getBranch<uint32_t>("MC::True", "hitn"); 	 //Hit number
//--------------------------------------------------------------------------------------------------------------------------------------

}
