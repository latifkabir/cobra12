// Filename: TClas12DetectorRICH.cc
// Description: CLAS12 RICH base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorRICH.h"


ClassImp(TClas12DetectorRICH)
//-------------------------------------------------------------------------------------
TClas12DetectorRICH::TClas12DetectorRICH(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorRICH::~TClas12DetectorRICH()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorRICH::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorRICH::SetBranches()
{
//================== RICH::hits:Reconstructed Hits in RICH ===================
	fRICH_hits_id = fHipoReader->getBranch<uint16_t>("RICH::hits", "id"); 	 //Hit id
	fRICH_hits_sector = fHipoReader->getBranch<uint16_t>("RICH::hits", "sector"); 	 //Hit sector
	fRICH_hits_tile = fHipoReader->getBranch<uint16_t>("RICH::hits", "tile"); 	 //Hit tile
	fRICH_hits_pmt = fHipoReader->getBranch<uint16_t>("RICH::hits", "pmt"); 	 //Hit pmt
	fRICH_hits_anode = fHipoReader->getBranch<uint16_t>("RICH::hits", "anode"); 	 //Hit anode
	fRICH_hits_idx = fHipoReader->getBranch<uint16_t>("RICH::hits", "idx"); 	 //Hit idx
	fRICH_hits_idy = fHipoReader->getBranch<uint16_t>("RICH::hits", "idy"); 	 //Hit idy
	fRICH_hits_glx = fHipoReader->getBranch<uint16_t>("RICH::hits", "glx"); 	 //Hit glx
	fRICH_hits_gly = fHipoReader->getBranch<uint16_t>("RICH::hits", "gly"); 	 //Hit gly
	fRICH_hits_time = fHipoReader->getBranch<uint16_t>("RICH::hits", "time"); 	 //Hit time
	fRICH_hits_cluster = fHipoReader->getBranch<uint16_t>("RICH::hits", "cluster"); 	 //Hit cluster
	fRICH_hits_xtalk = fHipoReader->getBranch<uint16_t>("RICH::hits", "xtalk"); 	 //Hit xtalk
	fRICH_hits_duration = fHipoReader->getBranch<uint16_t>("RICH::hits", "duration"); 	 //Hit duration
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RICH::clusters:Reconstructed Clusters in RICH ===================
	fRICH_clusters_id = fHipoReader->getBranch<uint16_t>("RICH::clusters", "id"); 	 //Clu id
	fRICH_clusters_size = fHipoReader->getBranch<uint16_t>("RICH::clusters", "size"); 	 //Clu size
	fRICH_clusters_sector = fHipoReader->getBranch<uint16_t>("RICH::clusters", "sector"); 	 //Clu sector
	fRICH_clusters_tile = fHipoReader->getBranch<uint16_t>("RICH::clusters", "tile"); 	 //Clu tile
	fRICH_clusters_pmt = fHipoReader->getBranch<uint16_t>("RICH::clusters", "pmt"); 	 //Clu pmt
	fRICH_clusters_charge = fHipoReader->getBranch<float>("RICH::clusters", "charge"); 	 //Clu charge
	fRICH_clusters_time = fHipoReader->getBranch<float>("RICH::clusters", "time"); 	 //Clu time
	fRICH_clusters_x = fHipoReader->getBranch<float>("RICH::clusters", "x"); 	 //Clu x
	fRICH_clusters_y = fHipoReader->getBranch<float>("RICH::clusters", "y"); 	 //Clu y
	fRICH_clusters_z = fHipoReader->getBranch<float>("RICH::clusters", "z"); 	 //Clu z
	fRICH_clusters_wtime = fHipoReader->getBranch<float>("RICH::clusters", "wtime"); 	 //Clu wtime
	fRICH_clusters_wx = fHipoReader->getBranch<float>("RICH::clusters", "wx"); 	 //Clu wx
	fRICH_clusters_wy = fHipoReader->getBranch<float>("RICH::clusters", "wy"); 	 //Clu wy
	fRICH_clusters_wz = fHipoReader->getBranch<float>("RICH::clusters", "wz"); 	 //Clu wz
//--------------------------------------------------------------------------------------------------------------------------------------

}
