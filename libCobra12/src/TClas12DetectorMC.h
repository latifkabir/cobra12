// Filename: TClas12DetectorMC.h
// Description: CLAS12 MC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORMC_H
#define TCLAS12DETECTORMC_H

#include "TClas12Detector.h"

class TClas12DetectorMC: public TClas12Detector
{
public:
	TClas12DetectorMC(TClas12Run *run);
	~TClas12DetectorMC();
	void Init();
	void SetBranches();

protected:
//================== MC::Header:Head bank for the generated event ===================
	hipo::node<uint32_t> *fMC_Header_run;   //Run number
	hipo::node<uint32_t> *fMC_Header_event;   //Event number
	hipo::node<uint8_t> *fMC_Header_type;   //Event type
	hipo::node<float> *fMC_Header_helicity;   //Beam helicity
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Event:Lund header bank for the generated event ===================
	hipo::node<uint16_t> *fMC_Event_npart;   //number of particles in the event
	hipo::node<uint16_t> *fMC_Event_atarget;   //Mass number of the target
	hipo::node<uint16_t> *fMC_Event_ztarget;   //Atomic number oif the target
	hipo::node<float> *fMC_Event_ptarget;   //Target polarization
	hipo::node<float> *fMC_Event_pbeam;   //Beam polarization
	hipo::node<uint16_t> *fMC_Event_btype;   //Beam type, electron=11, photon=22
	hipo::node<float> *fMC_Event_ebeam;   //Beam energy (GeV)
	hipo::node<uint16_t> *fMC_Event_targetid;   //Interacted nucleaon ID (proton=2212, neutron=2112
	hipo::node<uint16_t> *fMC_Event_processid;   //Process ID
	hipo::node<float> *fMC_Event_weight;   //Event weight
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Particle:Generated Particle information ===================
	hipo::node<uint32_t> *fMC_Particle_pid;   //particle id
	hipo::node<float> *fMC_Particle_px;   //x component of the momentum
	hipo::node<float> *fMC_Particle_py;   //y component of the momentum
	hipo::node<float> *fMC_Particle_pz;   //z component of the momentum
	hipo::node<float> *fMC_Particle_vx;   //x component of the vertex
	hipo::node<float> *fMC_Particle_vy;   //y component of the vertex
	hipo::node<float> *fMC_Particle_vz;   //z component of the vertex
	hipo::node<float> *fMC_Particle_vt;   //vertex time
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Lund:Generated Particle information from Lund ===================
	hipo::node<uint8_t> *fMC_Lund_index;   //index of the first daughter
	hipo::node<uint8_t> *fMC_Lund_type;   //particle type (1 is active)
	hipo::node<uint32_t> *fMC_Lund_pid;   //particle id
	hipo::node<uint8_t> *fMC_Lund_parent;   //index of the parent
	hipo::node<uint8_t> *fMC_Lund_daughter;   //index of the first daughter
	hipo::node<float> *fMC_Lund_px;   //x component of the momentum
	hipo::node<float> *fMC_Lund_py;   //y component of the momentum
	hipo::node<float> *fMC_Lund_pz;   //z component of the momentum
	hipo::node<float> *fMC_Lund_E;   //Energy of the particle
	hipo::node<float> *fMC_Lund_mass;   //mass of the particle
	hipo::node<float> *fMC_Lund_vx;   //x component of the vertex
	hipo::node<float> *fMC_Lund_vy;   //y component of the vertex
	hipo::node<float> *fMC_Lund_vz;   //z component of the vertex
	hipo::node<float> *fMC_Lund_ltime;   //particle lifetime
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::True:True detector information from GEANT4 ===================
	hipo::node<uint8_t> *fMC_True_detector;   //detector ID
	hipo::node<uint32_t> *fMC_True_pid;   //ID of the first particle entering the sensitive volume
	hipo::node<uint32_t> *fMC_True_mpid;   //ID of the mother of the first particle entering the sensitive volume
	hipo::node<uint32_t> *fMC_True_tid;   //Track ID of the first particle entering the sensitive volume
	hipo::node<uint32_t> *fMC_True_mtid;   //Track ID of the mother of the first particle entering the sensitive volume
	hipo::node<uint32_t> *fMC_True_otid;   //Track ID of the original track that generated the first particle entering the sensitive volume
	hipo::node<float> *fMC_True_trackE;   //Energy of the track
	hipo::node<float> *fMC_True_totEdep;   //Total Energy Deposited
	hipo::node<float> *fMC_True_avgX;   //Average X position in global reference system
	hipo::node<float> *fMC_True_avgY;   //Average Y position in global reference system
	hipo::node<float> *fMC_True_avgZ;   //Average Z position in global reference system
	hipo::node<float> *fMC_True_avgLx;   //Average X position in local reference system
	hipo::node<float> *fMC_True_avgLy;   //Average Y position in local reference system
	hipo::node<float> *fMC_True_avgLz;   //Average Z position in local reference system
	hipo::node<float> *fMC_True_px;   //x component of momentum of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_py;   //y component of momentum of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_pz;   //z component of momentum of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_vx;   //x component of primary vertex of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_vy;   //y component of primary vertex of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_vz;   //z component of primary vertex of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_mvx;   //x component of primary vertex of the mother of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_mvy;   //y component of primary vertex of the mother of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_mvz;   //z component of primary vertex of the mother of the particle entering the sensitive volume
	hipo::node<float> *fMC_True_avgT;   //Average time
	hipo::node<uint32_t> *fMC_True_nsteps;   //Number of geant4 steps
	hipo::node<uint32_t> *fMC_True_procID;   //Process that created the FP. see gemc.jlab.org
	hipo::node<uint32_t> *fMC_True_hitn;   //Hit number
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== MC::Header:Head bank for the generated event ===================
	unsigned int GetMC_Header_run(int i){  return (unsigned int)fMC_Header_run->getValue(i);}
	unsigned int GetMC_Header_event(int i){  return (unsigned int)fMC_Header_event->getValue(i);}
	unsigned int GetMC_Header_type(int i){  return (unsigned int)fMC_Header_type->getValue(i);}
	float GetMC_Header_helicity(int i){  return (float)fMC_Header_helicity->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Event:Lund header bank for the generated event ===================
	unsigned int GetMC_Event_npart(int i){  return (unsigned int)fMC_Event_npart->getValue(i);}
	unsigned int GetMC_Event_atarget(int i){  return (unsigned int)fMC_Event_atarget->getValue(i);}
	unsigned int GetMC_Event_ztarget(int i){  return (unsigned int)fMC_Event_ztarget->getValue(i);}
	float GetMC_Event_ptarget(int i){  return (float)fMC_Event_ptarget->getValue(i);}
	float GetMC_Event_pbeam(int i){  return (float)fMC_Event_pbeam->getValue(i);}
	unsigned int GetMC_Event_btype(int i){  return (unsigned int)fMC_Event_btype->getValue(i);}
	float GetMC_Event_ebeam(int i){  return (float)fMC_Event_ebeam->getValue(i);}
	unsigned int GetMC_Event_targetid(int i){  return (unsigned int)fMC_Event_targetid->getValue(i);}
	unsigned int GetMC_Event_processid(int i){  return (unsigned int)fMC_Event_processid->getValue(i);}
	float GetMC_Event_weight(int i){  return (float)fMC_Event_weight->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Particle:Generated Particle information ===================
	unsigned int GetMC_Particle_pid(int i){  return (unsigned int)fMC_Particle_pid->getValue(i);}
	float GetMC_Particle_px(int i){  return (float)fMC_Particle_px->getValue(i);}
	float GetMC_Particle_py(int i){  return (float)fMC_Particle_py->getValue(i);}
	float GetMC_Particle_pz(int i){  return (float)fMC_Particle_pz->getValue(i);}
	float GetMC_Particle_vx(int i){  return (float)fMC_Particle_vx->getValue(i);}
	float GetMC_Particle_vy(int i){  return (float)fMC_Particle_vy->getValue(i);}
	float GetMC_Particle_vz(int i){  return (float)fMC_Particle_vz->getValue(i);}
	float GetMC_Particle_vt(int i){  return (float)fMC_Particle_vt->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::Lund:Generated Particle information from Lund ===================
	unsigned int GetMC_Lund_index(int i){  return (unsigned int)fMC_Lund_index->getValue(i);}
	unsigned int GetMC_Lund_type(int i){  return (unsigned int)fMC_Lund_type->getValue(i);}
	unsigned int GetMC_Lund_pid(int i){  return (unsigned int)fMC_Lund_pid->getValue(i);}
	unsigned int GetMC_Lund_parent(int i){  return (unsigned int)fMC_Lund_parent->getValue(i);}
	unsigned int GetMC_Lund_daughter(int i){  return (unsigned int)fMC_Lund_daughter->getValue(i);}
	float GetMC_Lund_px(int i){  return (float)fMC_Lund_px->getValue(i);}
	float GetMC_Lund_py(int i){  return (float)fMC_Lund_py->getValue(i);}
	float GetMC_Lund_pz(int i){  return (float)fMC_Lund_pz->getValue(i);}
	float GetMC_Lund_E(int i){  return (float)fMC_Lund_E->getValue(i);}
	float GetMC_Lund_mass(int i){  return (float)fMC_Lund_mass->getValue(i);}
	float GetMC_Lund_vx(int i){  return (float)fMC_Lund_vx->getValue(i);}
	float GetMC_Lund_vy(int i){  return (float)fMC_Lund_vy->getValue(i);}
	float GetMC_Lund_vz(int i){  return (float)fMC_Lund_vz->getValue(i);}
	float GetMC_Lund_ltime(int i){  return (float)fMC_Lund_ltime->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== MC::True:True detector information from GEANT4 ===================
	unsigned int GetMC_True_detector(int i){  return (unsigned int)fMC_True_detector->getValue(i);}
	unsigned int GetMC_True_pid(int i){  return (unsigned int)fMC_True_pid->getValue(i);}
	unsigned int GetMC_True_mpid(int i){  return (unsigned int)fMC_True_mpid->getValue(i);}
	unsigned int GetMC_True_tid(int i){  return (unsigned int)fMC_True_tid->getValue(i);}
	unsigned int GetMC_True_mtid(int i){  return (unsigned int)fMC_True_mtid->getValue(i);}
	unsigned int GetMC_True_otid(int i){  return (unsigned int)fMC_True_otid->getValue(i);}
	float GetMC_True_trackE(int i){  return (float)fMC_True_trackE->getValue(i);}
	float GetMC_True_totEdep(int i){  return (float)fMC_True_totEdep->getValue(i);}
	float GetMC_True_avgX(int i){  return (float)fMC_True_avgX->getValue(i);}
	float GetMC_True_avgY(int i){  return (float)fMC_True_avgY->getValue(i);}
	float GetMC_True_avgZ(int i){  return (float)fMC_True_avgZ->getValue(i);}
	float GetMC_True_avgLx(int i){  return (float)fMC_True_avgLx->getValue(i);}
	float GetMC_True_avgLy(int i){  return (float)fMC_True_avgLy->getValue(i);}
	float GetMC_True_avgLz(int i){  return (float)fMC_True_avgLz->getValue(i);}
	float GetMC_True_px(int i){  return (float)fMC_True_px->getValue(i);}
	float GetMC_True_py(int i){  return (float)fMC_True_py->getValue(i);}
	float GetMC_True_pz(int i){  return (float)fMC_True_pz->getValue(i);}
	float GetMC_True_vx(int i){  return (float)fMC_True_vx->getValue(i);}
	float GetMC_True_vy(int i){  return (float)fMC_True_vy->getValue(i);}
	float GetMC_True_vz(int i){  return (float)fMC_True_vz->getValue(i);}
	float GetMC_True_mvx(int i){  return (float)fMC_True_mvx->getValue(i);}
	float GetMC_True_mvy(int i){  return (float)fMC_True_mvy->getValue(i);}
	float GetMC_True_mvz(int i){  return (float)fMC_True_mvz->getValue(i);}
	float GetMC_True_avgT(int i){  return (float)fMC_True_avgT->getValue(i);}
	unsigned int GetMC_True_nsteps(int i){  return (unsigned int)fMC_True_nsteps->getValue(i);}
	unsigned int GetMC_True_procID(int i){  return (unsigned int)fMC_True_procID->getValue(i);}
	unsigned int GetMC_True_hitn(int i){  return (unsigned int)fMC_True_hitn->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorMC,0)
};
#endif




