// Filename: TClas12Particle.cpp
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat May 12 18:00:06 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12PID_H
#define TCLAS12PID_H

#include "TClas12Run.h"

class TClas12Particle: public TObject
{

    TClas12Run *fClasRun;

public:
    TClas12Particle();
    
    ClassDef(TClas12Particle,0)    
};


#endif    
