// Filename: TClas12Config.cpp
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Fri Jul 27 00:20:20 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12Config.h"
#include "TClas12.h"
#include "TApplication.h"
using namespace std;

ClassImp(TClas12Config)

TClas12Config::TClas12Config()
{
    if(getenv("COBRA12") == nullptr)
    {
    	cout << "\nThe environment COBRA12 must be set" <<endl;
    	gApplication->Terminate();
    }
    else if(((string)getenv("COBRA12")).empty())
	fCobra12Home = "./";	    
    else
    	fCobra12Home = getenv("COBRA12");

    if(TClas12::IsValid(fCobra12Home + (string)"/config/config.cfg"))
	fConfigFile = fCobra12Home + (string)"/config/config.cfg";
    else
	fConfigFile = "./config/config.cfg";
    fDataPath = "./";
    fResultsPath = "./";
    fVaultPath = "./";
    LoadConfig();
    CheckValidity();
}

TClas12Config::TClas12Config(string file)
{
    if(getenv("COBRA12") == nullptr)
    {
    	cout << "\nThe environment COBRA12 must be set" <<endl;
    	gApplication->Terminate();
    }
    else if(((string)getenv("COBRA12")).empty())
	fCobra12Home = "./";	    
    else
    	fCobra12Home = getenv("COBRA12");	

    fConfigFile = file;
    LoadConfig();
    CheckValidity();
}

TClas12Config::~TClas12Config()
{
    
}

void TClas12Config::LoadConfig()
{
    ifstream configFile;
    configFile.open( fConfigFile, ifstream::in);
    if(!configFile)
        cout<<"TClas12Config: Config file NOT found."<<endl;
    
    string line;
    while( getline(configFile, line) )
    {
	line.erase( remove_if(line.begin(), line.end(), ::isspace), line.end() ); // strip spaces
	if(line.find("#") == 0) continue; //skip comments

	char* tokens = strtok( (char*)line.data(), " :,");
	while( tokens != NULL)
	{
	    string s = tokens;
	    if(s == "DATA_PATH")
	    {
		tokens = strtok(NULL, " :,");
		fDataPath = tokens;
	    }
	    if(s == "FILE_NAME_PATTERN")
	    {
		tokens = strtok(NULL, " :,");
		fFilePattern = tokens;
	    }
	    else if(s == "RESULTS_PATH")
	    {
		tokens = strtok(NULL, " :,");
		fResultsPath = tokens;
	    }
	    else if(s == "VALUT_PATH")
	    {
		tokens = strtok(NULL, " :,");
		fVaultPath = tokens;
	    }
	    else
	    {
		tokens = strtok(NULL, " :,");
	    }
	}
    }
    configFile.close();
}

const string& TClas12Config::GetDataPath()
{
    return fDataPath;
}

const string& TClas12Config::GetResultsPath()
{
    return fResultsPath;
}

const string& TClas12Config::GetCobra12Home()
{
    return fCobra12Home;
}

const string& TClas12Config::GetConfigPath()
{
    return fConfigFile;
}

const string& TClas12Config::GetVaultPath()
{
    return fVaultPath;
}

const string& TClas12Config::GetFilePattern()
{
    return fFilePattern;
}

void TClas12Config::Print()
{
    cout << "============== Clas12 Configuration ===================" <<endl;
    cout << "Cobra12 Home: "<< GetCobra12Home() <<endl;
    cout << "Config file: "<< GetConfigPath() <<endl;
    cout << "Data file path: "<< GetDataPath() <<endl;
    cout << "Data file pattern: "<< GetFilePattern() <<endl;
    cout << "Results path: "<< GetResultsPath() <<endl;
    cout << "=======================================================" <<endl;    
}

void TClas12Config::CheckValidity()
{
    if(!TClas12::IsValid(GetCobra12Home()) || !TClas12::IsValid(GetConfigPath()) || !TClas12::IsValid(GetDataPath()) || !TClas12::IsValid(GetResultsPath()))
	cout << "\nPlease fix the invalid path\n" <<endl;
}
