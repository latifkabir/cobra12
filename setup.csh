#!/bin/csh

# Filename: setup.csh
# Description: Set environment for COBRA12
# Author: Latif Kabir < latif@jlab.org >
# Created: Sun Jul 29 15:11:07 2018 (-0400)
# URL: jlab.org/~latif

set called=($_)
if ("$called" != "") then
  set scriptdir=$called[2]
  set COBRA12=`dirname $scriptdir`
  set COBRA12=`c\d $COBRA12 && pwd` 
else
  set scriptdir=$1
  set COBRA12=$scriptdir
endif
setenv COBRA12 ${COBRA12}

## Global
if ( ! ($?JLAB_SOFTWARE) ) then
    if ( -f /site/12gev_phys/softenv.csh ) then
	source /site/12gev_phys/softenv.csh 2.0 >& /dev/null
    endif
endif

## COBRA12
setenv LD_LIBRARY_PATH "${LD_LIBRARY_PATH}:${COBRA12}/libCobra12/lib:${COBRA12}/analysis/lib:${COBRA12}/external/hipo-io/lib:${COBRA12}/external/lz4/lib"
setenv PATH "${PATH}:${COBRA12}/bin"



