#!/bin/bash

# Filename: setup.sh
# Description: Set environment for COBRA12
# Author: Latif Kabir < latif@jlab.org >
# Created: Sun Jul 29 15:11:34 2018 (-0400)
# URL: jlab.org/~latif

COBRA12="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## Global
if ! [ -n "$JLAB_SOFTWARE" ]   
then
    if [ -f /site/12gev_phys/softenv.sh ]
    then 
	source /site/12gev_phys/softenv.sh 2.0 >& /dev/null
    fi
fi

#COBRA12
export COBRA12
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$COBRA12/libCobra12/lib:$COBRA12/analysis/lib:$COBRA12/external/hipo-io/lib:$COBRA12/external/lz4/lib
export PATH=$PATH:$COBRA12/bin



