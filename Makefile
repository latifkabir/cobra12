# Filename: Makefile
# Description: Makefile for building COBRA12 analysis framework
# Author: Latif Kabir < latif@jlab.org >
# Created: Wed Nov 15 01:00:17 2017 (-0500)
# URL: jlab.org/~latif

ifndef COBRA12
  $(error $$COBRA12 environment variable not defined. source setup.[c]sh first)
endif

# directories
LZ4_DIR = external/lz4
HIPO_DIR = external/hipo-io
LIB_DIR = libCobra12
ANA_DIR = analysis

# makefile name
MAKE_FILE = Makefile

####### Build rules
first: all

.PHONY: lz4 hipo lib ana

all: lz4 hipo lib ana 
	@echo "done!"
lz4:
	$(MAKE) -C $(LZ4_DIR) -f $(MAKE_FILE)

hipo:
	$(MAKE) -C $(HIPO_DIR) -f $(MAKE_FILE)

lib:
	$(MAKE) -C $(LIB_DIR) -f $(MAKE_FILE)

ana: lib
	$(MAKE) -C $(ANA_DIR) -f $(MAKE_FILE)

####### Clean
clean: cleanlz4 cleanhipo cleanlib cleanana 

.PHONY: cleanlz4 cleanhipo cleanlib cleanana 

cleanlz4:
	$(MAKE) -C $(LZ4_DIR) -f $(MAKE_FILE) clean

cleanhipo:
	$(MAKE) -C $(HIPO_DIR) -f $(MAKE_FILE) clean

cleanlib:
	$(MAKE) -C $(LIB_DIR) -f $(MAKE_FILE) clean

cleanana:
	$(MAKE) -C $(ANA_DIR) -f $(MAKE_FILE) clean

