
1. I had to include std=c++11 flag in the scons file in order to be able to successfully compile it.

2. In text.cpp file in line 22. I had to type cast to bool the right part.


//--------------------------------------------------

1. The HIPO file uses LZ4 compression (See Gagik's presentation on HIPO file system). You need liblz4 installed on your system inorder to decode the hipo file using C++.

Install using
```
sudo apt-get install liblz4-dev
```
After installation, if you do ```locate liblz4.so```, you should be able to locate it.

2. The source code checks for a flag ```__LZ4__``` to  check if liblz4 is installed. Even if you install LZ4, the flag might not be set in your system. So you can comment the flags.

3. Now compile as
```
g++ -o HbAna *.cc -llz4
```
4. Run the program ```HbAna``` with the provided HIPO file.


5. In my CLAS12CPP repository I have lz4 added as submodule for github.com/lz4. It compiled successfully on ifarm.
