// Filename: Distribution1.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include "TCLAS12Run.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"

void Distribution1(Int_t runNumber, Int_t nFiles)
{
    gStyle->SetOptStat(0);

    TH2F *h2EcalPos = new TH2F("ECAL_pos","ECAL Position; X [cm]; Y[cm]", 200, -500, 500, 200, 500, 500);
    Int_t ecounter = 0;
    Int_t sec;
    Float_t x = 0;
    Float_t y = 0;
    
    for(Int_t fileNo = 0; fileNo < nFiles; ++fileNo)
    {	
	TCLAS12Run *clas = new TCLAS12Run(runNumber, fileNo); 

	if(!clas->fFileExist)
	    continue;

	clas->Register("EVENT");
    
	while(clas->fHipoReader->next())
	{
	    Int_t length = clas->fREC_Calorimeter_sector->length();
	    for(Int_t k = 0; k < length; k++)
	    {
		x = (Float_t)clas->fREC_Calorimeter_x->getValue(k);
		y = (Float_t)clas->fREC_Calorimeter_y->getValue(k);
		h2EcalPos->Fill(x,y);
		    
	    }
	    ecounter++;
	}
	delete clas;
    }
    if(ecounter == 0)
	return;
    TCanvas *c2 = new TCanvas(); 
    h2EcalPos->Draw("colz");        
}
