
#include <cstdlib>
#include <iostream>

#include "reader.h"    //<----------- For this script this is the only one you need for interpreter.


using namespace std;

void ReadHipo3()
{
    std::cout << "start file reading test" << '\n';

    char filename[128] = "/home/latif/DATA/hb/rg-a/cooked/out_clas_003050.hipo.0";

    hipo::reader reader;
    reader.open(filename);

    reader.showInfo();

    hipo::node<uint8_t>   *node_sector    = reader.getBranch<uint8_t>("HitBasedTrkg::HBHits","sector");
    hipo::node<uint8_t>   *node_layer     = reader.getBranch<uint8_t>("HitBasedTrkg::HBHits","layer");
    hipo::node<uint8_t>   *node_superlayer     = reader.getBranch<uint8_t>("HitBasedTrkg::HBHits","superlayer");
    hipo::node<uint32_t>  *node_tdc       = reader.getBranch<uint32_t>("HitBasedTrkg::HBHits","TDC");

    int nrecords = reader.getRecordCount();

    printf("-----> file contains %d records\n",nrecords);
    printf("\n\n");
    printf("-----> start reading records.\n");

    int ecounter = 0;
    TH1F *hist = new TH1F("hist","TDC", 200, 0,1000);
    while(reader.next()==true)
    {
      int length = node_sector->getLength();
      printf("event # %d , DC data size = %d\n",ecounter,length);
      for(int k = 0; k < length; k++)
      {	  
	  if((int)node_superlayer->getValue(k) == 5)
	      hist->Fill((int)node_tdc->getValue(k));
	}
         ecounter++;
    }
    printf("-----> done reading records.\n");


    hist->Draw();
    return 0;
}
