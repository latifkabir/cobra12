// Filename: TClas12DetectorHTCC.h
// Description: CLAS12 HTCC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORHTCC_H
#define TCLAS12DETECTORHTCC_H

#include "TClas12Detector.h"

class TClas12DetectorHTCC: public TClas12Detector
{
public:
	TClas12DetectorHTCC(TClas12Run *run);
	~TClas12DetectorHTCC();
	void Init();
	void SetBranches();

protected:
//================== HTCC::rec:reconstructed clusters in HTCC ===================
	hipo::node<uint16_t> *fHTCC_rec_id;   //id of the hit
	hipo::node<uint16_t> *fHTCC_rec_nhits;   //number of hits in cluster
	hipo::node<float> *fHTCC_rec_nphe;   //number of photoelectrons in cluster
	hipo::node<uint16_t> *fHTCC_rec_ntheta;   //ntheta
	hipo::node<uint16_t> *fHTCC_rec_nphi;   //nphi
	hipo::node<uint16_t> *fHTCC_rec_mintheta;   //Minimum theta angle
	hipo::node<uint16_t> *fHTCC_rec_maxtheta;   //Maximum theta angle
	hipo::node<uint16_t> *fHTCC_rec_minphi;   //Minimum phi angle
	hipo::node<uint16_t> *fHTCC_rec_maxphi;   //Maximum phi angle
	hipo::node<float> *fHTCC_rec_time;   //Time at the vertex
	hipo::node<float> *fHTCC_rec_theta;   //Theta of the cluster
	hipo::node<float> *fHTCC_rec_dtheta;   //Error of the theta
	hipo::node<float> *fHTCC_rec_phi;   //Phi of the cluster
	hipo::node<float> *fHTCC_rec_dphi;   //Error of the phi
	hipo::node<float> *fHTCC_rec_x;   //X coordinate of the hit
	hipo::node<float> *fHTCC_rec_y;   //Y coordinate of the hit
	hipo::node<float> *fHTCC_rec_z;   //Z coordinate of the hit
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== HTCC::rec:reconstructed clusters in HTCC ===================
	unsigned int GetHTCC_rec_id(int i){  return (unsigned int)fHTCC_rec_id->getValue(i);}
	unsigned int GetHTCC_rec_nhits(int i){  return (unsigned int)fHTCC_rec_nhits->getValue(i);}
	float GetHTCC_rec_nphe(int i){  return (float)fHTCC_rec_nphe->getValue(i);}
	unsigned int GetHTCC_rec_ntheta(int i){  return (unsigned int)fHTCC_rec_ntheta->getValue(i);}
	unsigned int GetHTCC_rec_nphi(int i){  return (unsigned int)fHTCC_rec_nphi->getValue(i);}
	unsigned int GetHTCC_rec_mintheta(int i){  return (unsigned int)fHTCC_rec_mintheta->getValue(i);}
	unsigned int GetHTCC_rec_maxtheta(int i){  return (unsigned int)fHTCC_rec_maxtheta->getValue(i);}
	unsigned int GetHTCC_rec_minphi(int i){  return (unsigned int)fHTCC_rec_minphi->getValue(i);}
	unsigned int GetHTCC_rec_maxphi(int i){  return (unsigned int)fHTCC_rec_maxphi->getValue(i);}
	float GetHTCC_rec_time(int i){  return (float)fHTCC_rec_time->getValue(i);}
	float GetHTCC_rec_theta(int i){  return (float)fHTCC_rec_theta->getValue(i);}
	float GetHTCC_rec_dtheta(int i){  return (float)fHTCC_rec_dtheta->getValue(i);}
	float GetHTCC_rec_phi(int i){  return (float)fHTCC_rec_phi->getValue(i);}
	float GetHTCC_rec_dphi(int i){  return (float)fHTCC_rec_dphi->getValue(i);}
	float GetHTCC_rec_x(int i){  return (float)fHTCC_rec_x->getValue(i);}
	float GetHTCC_rec_y(int i){  return (float)fHTCC_rec_y->getValue(i);}
	float GetHTCC_rec_z(int i){  return (float)fHTCC_rec_z->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorHTCC,0)
};
#endif




