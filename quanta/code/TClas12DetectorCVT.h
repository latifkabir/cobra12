// Filename: TClas12DetectorCVT.h
// Description: CLAS12 CVT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORCVT_H
#define TCLAS12DETECTORCVT_H

#include "TClas12Detector.h"

class TClas12DetectorCVT: public TClas12Detector
{
public:
	TClas12DetectorCVT(TClas12Run *run);
	~TClas12DetectorCVT();
	void Init();
	void SetBranches();

protected:
//================== CVTRec::Tracks:reconstructed SVT tracks ===================
	hipo::node<uint16_t> *fCVTRec_Tracks_ID;   //ID
	hipo::node<uint8_t> *fCVTRec_Tracks_fittingMethod;   //fitting method (1 for global fit, 2 for Kalman Filter)
	hipo::node<float> *fCVTRec_Tracks_c_x;   //x-coordinate of a helical trk point extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	hipo::node<float> *fCVTRec_Tracks_c_y;   //y-coordinate of a helical trk point extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	hipo::node<float> *fCVTRec_Tracks_c_z;   //z-coordinate of a helical trk point extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)b
	hipo::node<float> *fCVTRec_Tracks_c_ux;   //x-coordinate of a helical trk direction extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	hipo::node<float> *fCVTRec_Tracks_c_uy;   //y-coordinate of a helical trk direction extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	hipo::node<float> *fCVTRec_Tracks_c_uz;   //z-coordinate of a helical trk direction extrapolated to a cylinder at 25 cm radius from the lab origin (cm unit)
	hipo::node<float> *fCVTRec_Tracks_pathlength;   //total pathlength from the origin to the reference point (in cm)
	hipo::node<uint8_t> *fCVTRec_Tracks_q;   //charge
	hipo::node<float> *fCVTRec_Tracks_p;   //total momentum
	hipo::node<float> *fCVTRec_Tracks_pt;   //transverse momentum
	hipo::node<float> *fCVTRec_Tracks_phi0;   //helical track fit parameter: phi at DOCA
	hipo::node<float> *fCVTRec_Tracks_tandip;   //helical track fit parameter: dip angle
	hipo::node<float> *fCVTRec_Tracks_z0;   //helical track fit parameter: value of z at the DOCA
	hipo::node<float> *fCVTRec_Tracks_d0;   //helical track fit parameter: Distance of Closest Approach
	hipo::node<float> *fCVTRec_Tracks_cov_d02;   //helical track fit covariance matrix element : delta_d0^2
	hipo::node<float> *fCVTRec_Tracks_cov_d0phi0;   //helical track fit covariance matrix element : delta_d0.delta_phi0
	hipo::node<float> *fCVTRec_Tracks_cov_d0rho;   //helical track fit covariance matrix element : delta_d0.delta_rho
	hipo::node<float> *fCVTRec_Tracks_cov_phi02;   //helical track fit covariance matrix element : delta_phi0^2
	hipo::node<float> *fCVTRec_Tracks_cov_phi0rho;   //helical track fit covariance matrix element : delta_phi0.delta_rho
	hipo::node<float> *fCVTRec_Tracks_cov_rho2;   //helical track fit covariance matrix element : delta_rho.delta_rho
	hipo::node<float> *fCVTRec_Tracks_cov_z02;   //helical track fit covariance matrix element : delta_z0^2
	hipo::node<float> *fCVTRec_Tracks_cov_tandip2;   //helical track fit covariance matrix element : delta_tandip^2
	hipo::node<float> *fCVTRec_Tracks_circlefit_chi2_per_ndf;   //Circle Fit chi^2/ndf 
	hipo::node<float> *fCVTRec_Tracks_linefit_chi2_per_ndf;   // Line Fit chi^2/ndf 
	hipo::node<float> *fCVTRec_Tracks_chi2;   //Fit chi^2 
	hipo::node<uint16_t> *fCVTRec_Tracks_ndf;   //Fit ndf 
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross1_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross2_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross3_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross4_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross5_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross6_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross7_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross8_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Tracks_Cross9_ID;   //ID of cross in the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CVTRec::Cosmics:reconstructed SVT straight tracks ===================
	hipo::node<uint16_t> *fCVTRec_Cosmics_ID;   //ID
	hipo::node<float> *fCVTRec_Cosmics_trkline_yx_slope;   //trkline_yx_slope
	hipo::node<float> *fCVTRec_Cosmics_trkline_yx_interc;   //trkline_yx_interc
	hipo::node<float> *fCVTRec_Cosmics_trkline_yz_slope;   //trkline_yz_slope
	hipo::node<float> *fCVTRec_Cosmics_trkline_yz_interc;   //trkline_yz_interc
	hipo::node<float> *fCVTRec_Cosmics_theta;   //theta (deg)
	hipo::node<float> *fCVTRec_Cosmics_phi;   //phi (deg)
	hipo::node<float> *fCVTRec_Cosmics_chi2;   //Fit chi^2 
	hipo::node<uint16_t> *fCVTRec_Cosmics_ndf;   // number of degrees of freedom used in the fit
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross1_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross2_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross3_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross4_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross5_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross6_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross7_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross8_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross9_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross10_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross11_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross12_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross13_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross14_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross15_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross16_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross17_ID;   //ID of cross in the track
	hipo::node<uint16_t> *fCVTRec_Cosmics_Cross18_ID;   //ID of cross in the track
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CVTRec::Trajectory:reconstructed SVT straight tracks trajectory ===================
	hipo::node<uint16_t> *fCVTRec_Trajectory_ID;   //ID
	hipo::node<uint8_t> *fCVTRec_Trajectory_LayerTrackIntersPlane;   //Layer of intersection of Track 
	hipo::node<uint8_t> *fCVTRec_Trajectory_SectorTrackIntersPlane;   //Sector of intersection of Track 
	hipo::node<float> *fCVTRec_Trajectory_XtrackIntersPlane;   //x of intersection of Track 
	hipo::node<float> *fCVTRec_Trajectory_YtrackIntersPlane;   //y of intersection of Track 
	hipo::node<float> *fCVTRec_Trajectory_ZtrackIntersPlane;   //z of intersection of Track 
	hipo::node<float> *fCVTRec_Trajectory_PhiTrackIntersPlane;   //Local phi of intersection of Track
	hipo::node<float> *fCVTRec_Trajectory_ThetaTrackIntersPlane;   //Local theta of intersection of Track
	hipo::node<float> *fCVTRec_Trajectory_trkToMPlnAngl;   //Local angle of intersection of Track with the module surface
	hipo::node<float> *fCVTRec_Trajectory_CalcCentroidStrip;   //Estimated Centroid
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== CVTRec::Tracks:reconstructed SVT tracks ===================
	unsigned int GetCVTRec_Tracks_ID(int i){  return (unsigned int)fCVTRec_Tracks_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_fittingMethod(int i){  return (unsigned int)fCVTRec_Tracks_fittingMethod->getValue(i);}
	float GetCVTRec_Tracks_c_x(int i){  return (float)fCVTRec_Tracks_c_x->getValue(i);}
	float GetCVTRec_Tracks_c_y(int i){  return (float)fCVTRec_Tracks_c_y->getValue(i);}
	float GetCVTRec_Tracks_c_z(int i){  return (float)fCVTRec_Tracks_c_z->getValue(i);}
	float GetCVTRec_Tracks_c_ux(int i){  return (float)fCVTRec_Tracks_c_ux->getValue(i);}
	float GetCVTRec_Tracks_c_uy(int i){  return (float)fCVTRec_Tracks_c_uy->getValue(i);}
	float GetCVTRec_Tracks_c_uz(int i){  return (float)fCVTRec_Tracks_c_uz->getValue(i);}
	float GetCVTRec_Tracks_pathlength(int i){  return (float)fCVTRec_Tracks_pathlength->getValue(i);}
	unsigned int GetCVTRec_Tracks_q(int i){  return (unsigned int)fCVTRec_Tracks_q->getValue(i);}
	float GetCVTRec_Tracks_p(int i){  return (float)fCVTRec_Tracks_p->getValue(i);}
	float GetCVTRec_Tracks_pt(int i){  return (float)fCVTRec_Tracks_pt->getValue(i);}
	float GetCVTRec_Tracks_phi0(int i){  return (float)fCVTRec_Tracks_phi0->getValue(i);}
	float GetCVTRec_Tracks_tandip(int i){  return (float)fCVTRec_Tracks_tandip->getValue(i);}
	float GetCVTRec_Tracks_z0(int i){  return (float)fCVTRec_Tracks_z0->getValue(i);}
	float GetCVTRec_Tracks_d0(int i){  return (float)fCVTRec_Tracks_d0->getValue(i);}
	float GetCVTRec_Tracks_cov_d02(int i){  return (float)fCVTRec_Tracks_cov_d02->getValue(i);}
	float GetCVTRec_Tracks_cov_d0phi0(int i){  return (float)fCVTRec_Tracks_cov_d0phi0->getValue(i);}
	float GetCVTRec_Tracks_cov_d0rho(int i){  return (float)fCVTRec_Tracks_cov_d0rho->getValue(i);}
	float GetCVTRec_Tracks_cov_phi02(int i){  return (float)fCVTRec_Tracks_cov_phi02->getValue(i);}
	float GetCVTRec_Tracks_cov_phi0rho(int i){  return (float)fCVTRec_Tracks_cov_phi0rho->getValue(i);}
	float GetCVTRec_Tracks_cov_rho2(int i){  return (float)fCVTRec_Tracks_cov_rho2->getValue(i);}
	float GetCVTRec_Tracks_cov_z02(int i){  return (float)fCVTRec_Tracks_cov_z02->getValue(i);}
	float GetCVTRec_Tracks_cov_tandip2(int i){  return (float)fCVTRec_Tracks_cov_tandip2->getValue(i);}
	float GetCVTRec_Tracks_circlefit_chi2_per_ndf(int i){  return (float)fCVTRec_Tracks_circlefit_chi2_per_ndf->getValue(i);}
	float GetCVTRec_Tracks_linefit_chi2_per_ndf(int i){  return (float)fCVTRec_Tracks_linefit_chi2_per_ndf->getValue(i);}
	float GetCVTRec_Tracks_chi2(int i){  return (float)fCVTRec_Tracks_chi2->getValue(i);}
	unsigned int GetCVTRec_Tracks_ndf(int i){  return (unsigned int)fCVTRec_Tracks_ndf->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross1_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross1_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross2_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross2_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross3_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross3_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross4_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross4_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross5_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross5_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross6_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross6_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross7_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross7_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross8_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross8_ID->getValue(i);}
	unsigned int GetCVTRec_Tracks_Cross9_ID(int i){  return (unsigned int)fCVTRec_Tracks_Cross9_ID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CVTRec::Cosmics:reconstructed SVT straight tracks ===================
	unsigned int GetCVTRec_Cosmics_ID(int i){  return (unsigned int)fCVTRec_Cosmics_ID->getValue(i);}
	float GetCVTRec_Cosmics_trkline_yx_slope(int i){  return (float)fCVTRec_Cosmics_trkline_yx_slope->getValue(i);}
	float GetCVTRec_Cosmics_trkline_yx_interc(int i){  return (float)fCVTRec_Cosmics_trkline_yx_interc->getValue(i);}
	float GetCVTRec_Cosmics_trkline_yz_slope(int i){  return (float)fCVTRec_Cosmics_trkline_yz_slope->getValue(i);}
	float GetCVTRec_Cosmics_trkline_yz_interc(int i){  return (float)fCVTRec_Cosmics_trkline_yz_interc->getValue(i);}
	float GetCVTRec_Cosmics_theta(int i){  return (float)fCVTRec_Cosmics_theta->getValue(i);}
	float GetCVTRec_Cosmics_phi(int i){  return (float)fCVTRec_Cosmics_phi->getValue(i);}
	float GetCVTRec_Cosmics_chi2(int i){  return (float)fCVTRec_Cosmics_chi2->getValue(i);}
	unsigned int GetCVTRec_Cosmics_ndf(int i){  return (unsigned int)fCVTRec_Cosmics_ndf->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross1_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross1_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross2_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross2_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross3_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross3_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross4_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross4_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross5_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross5_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross6_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross6_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross7_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross7_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross8_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross8_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross9_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross9_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross10_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross10_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross11_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross11_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross12_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross12_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross13_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross13_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross14_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross14_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross15_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross15_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross16_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross16_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross17_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross17_ID->getValue(i);}
	unsigned int GetCVTRec_Cosmics_Cross18_ID(int i){  return (unsigned int)fCVTRec_Cosmics_Cross18_ID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CVTRec::Trajectory:reconstructed SVT straight tracks trajectory ===================
	unsigned int GetCVTRec_Trajectory_ID(int i){  return (unsigned int)fCVTRec_Trajectory_ID->getValue(i);}
	unsigned int GetCVTRec_Trajectory_LayerTrackIntersPlane(int i){  return (unsigned int)fCVTRec_Trajectory_LayerTrackIntersPlane->getValue(i);}
	unsigned int GetCVTRec_Trajectory_SectorTrackIntersPlane(int i){  return (unsigned int)fCVTRec_Trajectory_SectorTrackIntersPlane->getValue(i);}
	float GetCVTRec_Trajectory_XtrackIntersPlane(int i){  return (float)fCVTRec_Trajectory_XtrackIntersPlane->getValue(i);}
	float GetCVTRec_Trajectory_YtrackIntersPlane(int i){  return (float)fCVTRec_Trajectory_YtrackIntersPlane->getValue(i);}
	float GetCVTRec_Trajectory_ZtrackIntersPlane(int i){  return (float)fCVTRec_Trajectory_ZtrackIntersPlane->getValue(i);}
	float GetCVTRec_Trajectory_PhiTrackIntersPlane(int i){  return (float)fCVTRec_Trajectory_PhiTrackIntersPlane->getValue(i);}
	float GetCVTRec_Trajectory_ThetaTrackIntersPlane(int i){  return (float)fCVTRec_Trajectory_ThetaTrackIntersPlane->getValue(i);}
	float GetCVTRec_Trajectory_trkToMPlnAngl(int i){  return (float)fCVTRec_Trajectory_trkToMPlnAngl->getValue(i);}
	float GetCVTRec_Trajectory_CalcCentroidStrip(int i){  return (float)fCVTRec_Trajectory_CalcCentroidStrip->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorCVT,0)
};
#endif




