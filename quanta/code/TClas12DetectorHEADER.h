// Filename: TClas12DetectorHEADER.h
// Description: CLAS12 HEADER base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORHEADER_H
#define TCLAS12DETECTORHEADER_H

#include "TClas12Detector.h"

class TClas12DetectorHEADER: public TClas12Detector
{
public:
	TClas12DetectorHEADER(TClas12Run *run);
	~TClas12DetectorHEADER();
	void Init();
	void SetBranches();

protected:
//================== RUN::config:Run Configuration ===================
	hipo::node<uint32_t> *fRUN_config_run;   //RUN number from CODA or GEMC
	hipo::node<uint32_t> *fRUN_config_event;   //Event number
	hipo::node<uint32_t> *fRUN_config_unixtime;   //Unix time
	hipo::node<uint64_t> *fRUN_config_trigger;   //trigger bits
	hipo::node<uint64_t> *fRUN_config_timestamp;   //time stamp from Trigger Interface (TI) board in 4 ns cycles)
	hipo::node<uint8_t> *fRUN_config_type;   //type of the run
	hipo::node<uint8_t> *fRUN_config_mode;   //run mode
	hipo::node<float> *fRUN_config_torus;   //torus setting relative value(-1.0 to 1.0)
	hipo::node<float> *fRUN_config_solenoid;   //solenoid field setting (-1.0 to 1.0)
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RUN::rf:RF information ===================
	hipo::node<uint16_t> *fRUN_rf_id;   //id of the RF signal
	hipo::node<float> *fRUN_rf_time;   //time of RF signal
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RUN::trigger:RUN trigger information ===================
	hipo::node<uint32_t> *fRUN_trigger_id;   //id
	hipo::node<uint32_t> *fRUN_trigger_trigger;   //trigger word
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== RUN::config:Run Configuration ===================
	unsigned int GetRUN_config_run(int i){  return (unsigned int)fRUN_config_run->getValue(i);}
	unsigned int GetRUN_config_event(int i){  return (unsigned int)fRUN_config_event->getValue(i);}
	unsigned int GetRUN_config_unixtime(int i){  return (unsigned int)fRUN_config_unixtime->getValue(i);}
	unsigned long GetRUN_config_trigger(int i){  return (unsigned long)fRUN_config_trigger->getValue(i);}
	unsigned long GetRUN_config_timestamp(int i){  return (unsigned long)fRUN_config_timestamp->getValue(i);}
	unsigned int GetRUN_config_type(int i){  return (unsigned int)fRUN_config_type->getValue(i);}
	unsigned int GetRUN_config_mode(int i){  return (unsigned int)fRUN_config_mode->getValue(i);}
	float GetRUN_config_torus(int i){  return (float)fRUN_config_torus->getValue(i);}
	float GetRUN_config_solenoid(int i){  return (float)fRUN_config_solenoid->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RUN::rf:RF information ===================
	unsigned int GetRUN_rf_id(int i){  return (unsigned int)fRUN_rf_id->getValue(i);}
	float GetRUN_rf_time(int i){  return (float)fRUN_rf_time->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RUN::trigger:RUN trigger information ===================
	unsigned int GetRUN_trigger_id(int i){  return (unsigned int)fRUN_trigger_id->getValue(i);}
	unsigned int GetRUN_trigger_trigger(int i){  return (unsigned int)fRUN_trigger_trigger->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorHEADER,0)
};
#endif




