// Filename: TClas12DetectorFT.h
// Description: CLAS12 FT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORFT_H
#define TCLAS12DETECTORFT_H

#include "TClas12Detector.h"

class TClas12DetectorFT: public TClas12Detector
{
public:
	TClas12DetectorFT(TClas12Run *run);
	~TClas12DetectorFT();
	void Init();
	void SetBranches();

protected:
//================== FTCAL::hits:Reconstructed Hits in FT calorimeter ===================
	hipo::node<uint8_t> *fFTCAL_hits_idx;   //x id of grid
	hipo::node<uint8_t> *fFTCAL_hits_idy;   //y id of grid
	hipo::node<float> *fFTCAL_hits_x;   //Hit X position (cm)
	hipo::node<float> *fFTCAL_hits_y;   //Hit Y position (cm)
	hipo::node<float> *fFTCAL_hits_z;   //Hit Z position (cm)
	hipo::node<float> *fFTCAL_hits_energy;   //Hit Energy
	hipo::node<float> *fFTCAL_hits_time;   //Hit Time
	hipo::node<uint16_t> *fFTCAL_hits_hitID;   //Hit Pointer to ADC bank
	hipo::node<uint16_t> *fFTCAL_hits_clusterID;   //Hit Pointer to Cluster Bank
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTCAL::clusters:Reconstructed Clusters in FT calorimeter ===================
	hipo::node<uint16_t> *fFTCAL_clusters_size;   //Cluster size
	hipo::node<uint16_t> *fFTCAL_clusters_id;   //Cluster ID
	hipo::node<float> *fFTCAL_clusters_x;   //Cluster centroid X moment (cm)
	hipo::node<float> *fFTCAL_clusters_y;   //Cluster centroid Y moment (cm)
	hipo::node<float> *fFTCAL_clusters_z;   //Cluster centroid Z moment (cm)
	hipo::node<float> *fFTCAL_clusters_widthX;   //Cluster width in x (cm)
	hipo::node<float> *fFTCAL_clusters_widthY;   //Cluster width in y (cm)
	hipo::node<float> *fFTCAL_clusters_radius;   //Cluster radius (cm)
	hipo::node<float> *fFTCAL_clusters_time;   //Cluster timing information
	hipo::node<float> *fFTCAL_clusters_energy;   //Cluster total energy
	hipo::node<float> *fFTCAL_clusters_maxEnergy;   //Cluster maximum energy
	hipo::node<float> *fFTCAL_clusters_recEnergy;   //Cluster reconstructed energy
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTHODO::hits:Reconstructed Hits in FT hodoscope ===================
	hipo::node<uint8_t> *fFTHODO_hits_sector;   //sector number
	hipo::node<uint8_t> *fFTHODO_hits_layer;   //layer number
	hipo::node<uint16_t> *fFTHODO_hits_component;   //component number
	hipo::node<float> *fFTHODO_hits_x;   //Hit X position (cm)
	hipo::node<float> *fFTHODO_hits_y;   //Hit Y position (cm)
	hipo::node<float> *fFTHODO_hits_z;   //Hit Z position (cm)
	hipo::node<float> *fFTHODO_hits_energy;   //Hit Energy
	hipo::node<float> *fFTHODO_hits_time;   //Hit Time
	hipo::node<uint16_t> *fFTHODO_hits_hitID;   //Hit Pointer to ADC bank
	hipo::node<uint16_t> *fFTHODO_hits_clusterID;   //Hit Pointer to Cluster Bank
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTHODO::clusters:Reconstructed clusters in FT hodoscope ===================
	hipo::node<uint16_t> *fFTHODO_clusters_size;   //Cluster size
	hipo::node<uint16_t> *fFTHODO_clusters_id;   //Cluster ID
	hipo::node<float> *fFTHODO_clusters_x;   //Cluster centroid X moment (cm)
	hipo::node<float> *fFTHODO_clusters_y;   //Cluster centroid Y moment (cm)
	hipo::node<float> *fFTHODO_clusters_z;   //Cluster centroid Z moment (cm)
	hipo::node<float> *fFTHODO_clusters_widthX;   //Cluster width in x (cm)
	hipo::node<float> *fFTHODO_clusters_widthY;   //Cluster width in y (cm)
	hipo::node<float> *fFTHODO_clusters_radius;   //Cluster radius (cm)
	hipo::node<float> *fFTHODO_clusters_time;   //Cluster timing information
	hipo::node<float> *fFTHODO_clusters_energy;   //Cluster total energy
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FT::particles:Reconstructed Particles in FT ===================
	hipo::node<uint16_t> *fFT_particles_id;   //track ID
	hipo::node<uint8_t> *fFT_particles_charge;   //track Charge
	hipo::node<float> *fFT_particles_energy;   //track Energy
	hipo::node<float> *fFT_particles_cx;   //track Cx
	hipo::node<float> *fFT_particles_cy;   //track Cy
	hipo::node<float> *fFT_particles_cz;   //track Cz
	hipo::node<float> *fFT_particles_time;   //track Time
	hipo::node<uint16_t> *fFT_particles_calID;   //pointer to cluster bank
	hipo::node<uint16_t> *fFT_particles_hodoID;   //pointer to signal bank
	hipo::node<uint16_t> *fFT_particles_trkID;   //pointer to cross banks
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== FTCAL::hits:Reconstructed Hits in FT calorimeter ===================
	unsigned int GetFTCAL_hits_idx(int i){  return (unsigned int)fFTCAL_hits_idx->getValue(i);}
	unsigned int GetFTCAL_hits_idy(int i){  return (unsigned int)fFTCAL_hits_idy->getValue(i);}
	float GetFTCAL_hits_x(int i){  return (float)fFTCAL_hits_x->getValue(i);}
	float GetFTCAL_hits_y(int i){  return (float)fFTCAL_hits_y->getValue(i);}
	float GetFTCAL_hits_z(int i){  return (float)fFTCAL_hits_z->getValue(i);}
	float GetFTCAL_hits_energy(int i){  return (float)fFTCAL_hits_energy->getValue(i);}
	float GetFTCAL_hits_time(int i){  return (float)fFTCAL_hits_time->getValue(i);}
	unsigned int GetFTCAL_hits_hitID(int i){  return (unsigned int)fFTCAL_hits_hitID->getValue(i);}
	unsigned int GetFTCAL_hits_clusterID(int i){  return (unsigned int)fFTCAL_hits_clusterID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTCAL::clusters:Reconstructed Clusters in FT calorimeter ===================
	unsigned int GetFTCAL_clusters_size(int i){  return (unsigned int)fFTCAL_clusters_size->getValue(i);}
	unsigned int GetFTCAL_clusters_id(int i){  return (unsigned int)fFTCAL_clusters_id->getValue(i);}
	float GetFTCAL_clusters_x(int i){  return (float)fFTCAL_clusters_x->getValue(i);}
	float GetFTCAL_clusters_y(int i){  return (float)fFTCAL_clusters_y->getValue(i);}
	float GetFTCAL_clusters_z(int i){  return (float)fFTCAL_clusters_z->getValue(i);}
	float GetFTCAL_clusters_widthX(int i){  return (float)fFTCAL_clusters_widthX->getValue(i);}
	float GetFTCAL_clusters_widthY(int i){  return (float)fFTCAL_clusters_widthY->getValue(i);}
	float GetFTCAL_clusters_radius(int i){  return (float)fFTCAL_clusters_radius->getValue(i);}
	float GetFTCAL_clusters_time(int i){  return (float)fFTCAL_clusters_time->getValue(i);}
	float GetFTCAL_clusters_energy(int i){  return (float)fFTCAL_clusters_energy->getValue(i);}
	float GetFTCAL_clusters_maxEnergy(int i){  return (float)fFTCAL_clusters_maxEnergy->getValue(i);}
	float GetFTCAL_clusters_recEnergy(int i){  return (float)fFTCAL_clusters_recEnergy->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTHODO::hits:Reconstructed Hits in FT hodoscope ===================
	unsigned int GetFTHODO_hits_sector(int i){  return (unsigned int)fFTHODO_hits_sector->getValue(i);}
	unsigned int GetFTHODO_hits_layer(int i){  return (unsigned int)fFTHODO_hits_layer->getValue(i);}
	unsigned int GetFTHODO_hits_component(int i){  return (unsigned int)fFTHODO_hits_component->getValue(i);}
	float GetFTHODO_hits_x(int i){  return (float)fFTHODO_hits_x->getValue(i);}
	float GetFTHODO_hits_y(int i){  return (float)fFTHODO_hits_y->getValue(i);}
	float GetFTHODO_hits_z(int i){  return (float)fFTHODO_hits_z->getValue(i);}
	float GetFTHODO_hits_energy(int i){  return (float)fFTHODO_hits_energy->getValue(i);}
	float GetFTHODO_hits_time(int i){  return (float)fFTHODO_hits_time->getValue(i);}
	unsigned int GetFTHODO_hits_hitID(int i){  return (unsigned int)fFTHODO_hits_hitID->getValue(i);}
	unsigned int GetFTHODO_hits_clusterID(int i){  return (unsigned int)fFTHODO_hits_clusterID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTHODO::clusters:Reconstructed clusters in FT hodoscope ===================
	unsigned int GetFTHODO_clusters_size(int i){  return (unsigned int)fFTHODO_clusters_size->getValue(i);}
	unsigned int GetFTHODO_clusters_id(int i){  return (unsigned int)fFTHODO_clusters_id->getValue(i);}
	float GetFTHODO_clusters_x(int i){  return (float)fFTHODO_clusters_x->getValue(i);}
	float GetFTHODO_clusters_y(int i){  return (float)fFTHODO_clusters_y->getValue(i);}
	float GetFTHODO_clusters_z(int i){  return (float)fFTHODO_clusters_z->getValue(i);}
	float GetFTHODO_clusters_widthX(int i){  return (float)fFTHODO_clusters_widthX->getValue(i);}
	float GetFTHODO_clusters_widthY(int i){  return (float)fFTHODO_clusters_widthY->getValue(i);}
	float GetFTHODO_clusters_radius(int i){  return (float)fFTHODO_clusters_radius->getValue(i);}
	float GetFTHODO_clusters_time(int i){  return (float)fFTHODO_clusters_time->getValue(i);}
	float GetFTHODO_clusters_energy(int i){  return (float)fFTHODO_clusters_energy->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FT::particles:Reconstructed Particles in FT ===================
	unsigned int GetFT_particles_id(int i){  return (unsigned int)fFT_particles_id->getValue(i);}
	unsigned int GetFT_particles_charge(int i){  return (unsigned int)fFT_particles_charge->getValue(i);}
	float GetFT_particles_energy(int i){  return (float)fFT_particles_energy->getValue(i);}
	float GetFT_particles_cx(int i){  return (float)fFT_particles_cx->getValue(i);}
	float GetFT_particles_cy(int i){  return (float)fFT_particles_cy->getValue(i);}
	float GetFT_particles_cz(int i){  return (float)fFT_particles_cz->getValue(i);}
	float GetFT_particles_time(int i){  return (float)fFT_particles_time->getValue(i);}
	unsigned int GetFT_particles_calID(int i){  return (unsigned int)fFT_particles_calID->getValue(i);}
	unsigned int GetFT_particles_hodoID(int i){  return (unsigned int)fFT_particles_hodoID->getValue(i);}
	unsigned int GetFT_particles_trkID(int i){  return (unsigned int)fFT_particles_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorFT,0)
};
#endif




