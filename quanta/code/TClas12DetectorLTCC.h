// Filename: TClas12DetectorLTCC.h
// Description: CLAS12 LTCC base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORLTCC_H
#define TCLAS12DETECTORLTCC_H

#include "TClas12Detector.h"

class TClas12DetectorLTCC: public TClas12Detector
{
public:
	TClas12DetectorLTCC(TClas12Run *run);
	~TClas12DetectorLTCC();
	void Init();
	void SetBranches();

protected:
//================== LTCC::clusters:reconstructed clusters from the LTCC ===================
	hipo::node<uint16_t> *fLTCC_clusters_id;   //id of the cluster
	hipo::node<uint8_t> *fLTCC_clusters_status;   //good (1) or bad (0)
	hipo::node<uint8_t> *fLTCC_clusters_sector;   //sector of LTCC
	hipo::node<uint16_t> *fLTCC_clusters_segment;   //weighted central cluster segment
	hipo::node<float> *fLTCC_clusters_x;   //crude cluster center (cm)
	hipo::node<float> *fLTCC_clusters_y;   //crude cluster center (cm)
	hipo::node<float> *fLTCC_clusters_z;   //crude cluster center (cm)
	hipo::node<float> *fLTCC_clusters_nphe;   //number of photo-electrons
	hipo::node<float> *fLTCC_clusters_time;   //cluster time
	hipo::node<uint16_t> *fLTCC_clusters_nHits;   //number of hits in this cluster
	hipo::node<float> *fLTCC_clusters_minTheta;   //minimum theta angle of the cluster (deg)
	hipo::node<float> *fLTCC_clusters_maxTheta;   //maximum theta angle of the cluster (deg)
	hipo::node<float> *fLTCC_clusters_minPhi;   //minimum phi angle of the cluster (deg)
	hipo::node<float> *fLTCC_clusters_maxPhi;   //maximum phi angle of the cluster (deg)
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== LTCC::clusters:reconstructed clusters from the LTCC ===================
	unsigned int GetLTCC_clusters_id(int i){  return (unsigned int)fLTCC_clusters_id->getValue(i);}
	unsigned int GetLTCC_clusters_status(int i){  return (unsigned int)fLTCC_clusters_status->getValue(i);}
	unsigned int GetLTCC_clusters_sector(int i){  return (unsigned int)fLTCC_clusters_sector->getValue(i);}
	unsigned int GetLTCC_clusters_segment(int i){  return (unsigned int)fLTCC_clusters_segment->getValue(i);}
	float GetLTCC_clusters_x(int i){  return (float)fLTCC_clusters_x->getValue(i);}
	float GetLTCC_clusters_y(int i){  return (float)fLTCC_clusters_y->getValue(i);}
	float GetLTCC_clusters_z(int i){  return (float)fLTCC_clusters_z->getValue(i);}
	float GetLTCC_clusters_nphe(int i){  return (float)fLTCC_clusters_nphe->getValue(i);}
	float GetLTCC_clusters_time(int i){  return (float)fLTCC_clusters_time->getValue(i);}
	unsigned int GetLTCC_clusters_nHits(int i){  return (unsigned int)fLTCC_clusters_nHits->getValue(i);}
	float GetLTCC_clusters_minTheta(int i){  return (float)fLTCC_clusters_minTheta->getValue(i);}
	float GetLTCC_clusters_maxTheta(int i){  return (float)fLTCC_clusters_maxTheta->getValue(i);}
	float GetLTCC_clusters_minPhi(int i){  return (float)fLTCC_clusters_minPhi->getValue(i);}
	float GetLTCC_clusters_maxPhi(int i){  return (float)fLTCC_clusters_maxPhi->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorLTCC,0)
};
#endif




