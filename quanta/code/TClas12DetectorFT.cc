// Filename: TClas12DetectorFT.cc
// Description: CLAS12 FT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorFT.h"


ClassImp(TClas12DetectorFT)
//-------------------------------------------------------------------------------------
TClas12DetectorFT::TClas12DetectorFT(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorFT::~TClas12DetectorFT()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorFT::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorFT::SetBranches()
{
//================== FTCAL::hits:Reconstructed Hits in FT calorimeter ===================
	fFTCAL_hits_idx = fHipoReader->getBranch<uint8_t>("FTCAL::hits", "idx"); 	 //x id of grid
	fFTCAL_hits_idy = fHipoReader->getBranch<uint8_t>("FTCAL::hits", "idy"); 	 //y id of grid
	fFTCAL_hits_x = fHipoReader->getBranch<float>("FTCAL::hits", "x"); 	 //Hit X position (cm)
	fFTCAL_hits_y = fHipoReader->getBranch<float>("FTCAL::hits", "y"); 	 //Hit Y position (cm)
	fFTCAL_hits_z = fHipoReader->getBranch<float>("FTCAL::hits", "z"); 	 //Hit Z position (cm)
	fFTCAL_hits_energy = fHipoReader->getBranch<float>("FTCAL::hits", "energy"); 	 //Hit Energy
	fFTCAL_hits_time = fHipoReader->getBranch<float>("FTCAL::hits", "time"); 	 //Hit Time
	fFTCAL_hits_hitID = fHipoReader->getBranch<uint16_t>("FTCAL::hits", "hitID"); 	 //Hit Pointer to ADC bank
	fFTCAL_hits_clusterID = fHipoReader->getBranch<uint16_t>("FTCAL::hits", "clusterID"); 	 //Hit Pointer to Cluster Bank
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTCAL::clusters:Reconstructed Clusters in FT calorimeter ===================
	fFTCAL_clusters_size = fHipoReader->getBranch<uint16_t>("FTCAL::clusters", "size"); 	 //Cluster size
	fFTCAL_clusters_id = fHipoReader->getBranch<uint16_t>("FTCAL::clusters", "id"); 	 //Cluster ID
	fFTCAL_clusters_x = fHipoReader->getBranch<float>("FTCAL::clusters", "x"); 	 //Cluster centroid X moment (cm)
	fFTCAL_clusters_y = fHipoReader->getBranch<float>("FTCAL::clusters", "y"); 	 //Cluster centroid Y moment (cm)
	fFTCAL_clusters_z = fHipoReader->getBranch<float>("FTCAL::clusters", "z"); 	 //Cluster centroid Z moment (cm)
	fFTCAL_clusters_widthX = fHipoReader->getBranch<float>("FTCAL::clusters", "widthX"); 	 //Cluster width in x (cm)
	fFTCAL_clusters_widthY = fHipoReader->getBranch<float>("FTCAL::clusters", "widthY"); 	 //Cluster width in y (cm)
	fFTCAL_clusters_radius = fHipoReader->getBranch<float>("FTCAL::clusters", "radius"); 	 //Cluster radius (cm)
	fFTCAL_clusters_time = fHipoReader->getBranch<float>("FTCAL::clusters", "time"); 	 //Cluster timing information
	fFTCAL_clusters_energy = fHipoReader->getBranch<float>("FTCAL::clusters", "energy"); 	 //Cluster total energy
	fFTCAL_clusters_maxEnergy = fHipoReader->getBranch<float>("FTCAL::clusters", "maxEnergy"); 	 //Cluster maximum energy
	fFTCAL_clusters_recEnergy = fHipoReader->getBranch<float>("FTCAL::clusters", "recEnergy"); 	 //Cluster reconstructed energy
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTHODO::hits:Reconstructed Hits in FT hodoscope ===================
	fFTHODO_hits_sector = fHipoReader->getBranch<uint8_t>("FTHODO::hits", "sector"); 	 //sector number
	fFTHODO_hits_layer = fHipoReader->getBranch<uint8_t>("FTHODO::hits", "layer"); 	 //layer number
	fFTHODO_hits_component = fHipoReader->getBranch<uint16_t>("FTHODO::hits", "component"); 	 //component number
	fFTHODO_hits_x = fHipoReader->getBranch<float>("FTHODO::hits", "x"); 	 //Hit X position (cm)
	fFTHODO_hits_y = fHipoReader->getBranch<float>("FTHODO::hits", "y"); 	 //Hit Y position (cm)
	fFTHODO_hits_z = fHipoReader->getBranch<float>("FTHODO::hits", "z"); 	 //Hit Z position (cm)
	fFTHODO_hits_energy = fHipoReader->getBranch<float>("FTHODO::hits", "energy"); 	 //Hit Energy
	fFTHODO_hits_time = fHipoReader->getBranch<float>("FTHODO::hits", "time"); 	 //Hit Time
	fFTHODO_hits_hitID = fHipoReader->getBranch<uint16_t>("FTHODO::hits", "hitID"); 	 //Hit Pointer to ADC bank
	fFTHODO_hits_clusterID = fHipoReader->getBranch<uint16_t>("FTHODO::hits", "clusterID"); 	 //Hit Pointer to Cluster Bank
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTHODO::clusters:Reconstructed clusters in FT hodoscope ===================
	fFTHODO_clusters_size = fHipoReader->getBranch<uint16_t>("FTHODO::clusters", "size"); 	 //Cluster size
	fFTHODO_clusters_id = fHipoReader->getBranch<uint16_t>("FTHODO::clusters", "id"); 	 //Cluster ID
	fFTHODO_clusters_x = fHipoReader->getBranch<float>("FTHODO::clusters", "x"); 	 //Cluster centroid X moment (cm)
	fFTHODO_clusters_y = fHipoReader->getBranch<float>("FTHODO::clusters", "y"); 	 //Cluster centroid Y moment (cm)
	fFTHODO_clusters_z = fHipoReader->getBranch<float>("FTHODO::clusters", "z"); 	 //Cluster centroid Z moment (cm)
	fFTHODO_clusters_widthX = fHipoReader->getBranch<float>("FTHODO::clusters", "widthX"); 	 //Cluster width in x (cm)
	fFTHODO_clusters_widthY = fHipoReader->getBranch<float>("FTHODO::clusters", "widthY"); 	 //Cluster width in y (cm)
	fFTHODO_clusters_radius = fHipoReader->getBranch<float>("FTHODO::clusters", "radius"); 	 //Cluster radius (cm)
	fFTHODO_clusters_time = fHipoReader->getBranch<float>("FTHODO::clusters", "time"); 	 //Cluster timing information
	fFTHODO_clusters_energy = fHipoReader->getBranch<float>("FTHODO::clusters", "energy"); 	 //Cluster total energy
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FT::particles:Reconstructed Particles in FT ===================
	fFT_particles_id = fHipoReader->getBranch<uint16_t>("FT::particles", "id"); 	 //track ID
	fFT_particles_charge = fHipoReader->getBranch<uint8_t>("FT::particles", "charge"); 	 //track Charge
	fFT_particles_energy = fHipoReader->getBranch<float>("FT::particles", "energy"); 	 //track Energy
	fFT_particles_cx = fHipoReader->getBranch<float>("FT::particles", "cx"); 	 //track Cx
	fFT_particles_cy = fHipoReader->getBranch<float>("FT::particles", "cy"); 	 //track Cy
	fFT_particles_cz = fHipoReader->getBranch<float>("FT::particles", "cz"); 	 //track Cz
	fFT_particles_time = fHipoReader->getBranch<float>("FT::particles", "time"); 	 //track Time
	fFT_particles_calID = fHipoReader->getBranch<uint16_t>("FT::particles", "calID"); 	 //pointer to cluster bank
	fFT_particles_hodoID = fHipoReader->getBranch<uint16_t>("FT::particles", "hodoID"); 	 //pointer to signal bank
	fFT_particles_trkID = fHipoReader->getBranch<uint16_t>("FT::particles", "trkID"); 	 //pointer to cross banks
//--------------------------------------------------------------------------------------------------------------------------------------

}
