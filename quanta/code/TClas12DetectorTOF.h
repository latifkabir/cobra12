// Filename: TClas12DetectorTOF.h
// Description: CLAS12 TOF base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORTOF_H
#define TCLAS12DETECTORTOF_H

#include "TClas12Detector.h"

class TClas12DetectorTOF: public TClas12Detector
{
public:
	TClas12DetectorTOF(TClas12Run *run);
	~TClas12DetectorTOF();
	void Init();
	void SetBranches();

protected:
//================== FTOF::rawhits:reconstructed hit info from L/R FTOF PMTs ===================
	hipo::node<uint16_t> *fFTOF_rawhits_id;   //id of the hit
	hipo::node<uint16_t> *fFTOF_rawhits_status;   //status of the hit
	hipo::node<uint8_t> *fFTOF_rawhits_sector;   //sector of FTOF
	hipo::node<uint8_t> *fFTOF_rawhits_layer;   //panel id of FTOF (1-1A, 2-1B, 3-2
	hipo::node<uint16_t> *fFTOF_rawhits_component;   //paddle id of FTOF
	hipo::node<float> *fFTOF_rawhits_energy_left;   //E dep (MeV) from L PMT
	hipo::node<float> *fFTOF_rawhits_energy_right;   //E dep (MeV) from R PMT
	hipo::node<float> *fFTOF_rawhits_time_left;   //Hit time (ns) from L PMT
	hipo::node<float> *fFTOF_rawhits_time_right;   //Hit time (ns) from R PMT
	hipo::node<float> *fFTOF_rawhits_energy_left_unc;   //E dep unc (MeV) from L PMT
	hipo::node<float> *fFTOF_rawhits_energy_right_unc;   //E dep unc (MeV) from R PMT
	hipo::node<float> *fFTOF_rawhits_time_left_unc;   //Hit time unc (ns) from L PMT
	hipo::node<float> *fFTOF_rawhits_time_right_unc;   //Hit time unc (ns) from R PMT
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::hits:reconstructed hit info from FTOF ===================
	hipo::node<uint16_t> *fFTOF_hits_id;   //id of the hit
	hipo::node<uint16_t> *fFTOF_hits_status;   //status of the hit
	hipo::node<uint16_t> *fFTOF_hits_trackid;   //matched DC track id
	hipo::node<uint8_t> *fFTOF_hits_sector;   //sector of FTOF
	hipo::node<uint8_t> *fFTOF_hits_layer;   //panel id of FTOF (1-1A, 2-1B, 3-2
	hipo::node<uint16_t> *fFTOF_hits_component;   //paddle id of FTOF
	hipo::node<float> *fFTOF_hits_energy;   //E dep (MeV) of hit
	hipo::node<float> *fFTOF_hits_time;   //Hit time (ns)
	hipo::node<float> *fFTOF_hits_energy_unc;   //E dep unc (MeV) of hit
	hipo::node<float> *fFTOF_hits_time_unc;   //Hit time unc (ns)
	hipo::node<float> *fFTOF_hits_x;   //Global X coor (cm) of hit
	hipo::node<float> *fFTOF_hits_y;   //Global Y coor (cm) of hit
	hipo::node<float> *fFTOF_hits_z;   //Global Z coor (cm) of hit
	hipo::node<float> *fFTOF_hits_x_unc;   //Global X coor unc (cm) of hit
	hipo::node<float> *fFTOF_hits_y_unc;   //Global Y coor unc (cm) of hit
	hipo::node<float> *fFTOF_hits_z_unc;   //Global Z coor unc (cm) of hit
	hipo::node<float> *fFTOF_hits_tx;   //Global X coor (cm) of hit from DC info - trackid index
	hipo::node<float> *fFTOF_hits_ty;   //Global Y coor (cm) of hit from DC info - trackid index
	hipo::node<float> *fFTOF_hits_tz;   //Global Z coor (cm) of hit from DC info - trackid index
	hipo::node<uint16_t> *fFTOF_hits_adc_idx1;   //index of hit with ADCL in adc bank, starts at zero
	hipo::node<uint16_t> *fFTOF_hits_adc_idx2;   //index of hit with ADCR in adc bank, starts at zero
	hipo::node<uint16_t> *fFTOF_hits_tdc_idx1;   //index of hit with TDCL in tdc bank, starts at zero
	hipo::node<uint16_t> *fFTOF_hits_tdc_idx2;   //index of hit with TDCR in tdc bank, starts at zero
	hipo::node<float> *fFTOF_hits_pathLength;   //pathlength of the track from the vertex (doca point to the beamline to the midpoint between the entrance and exit of the hit bar
	hipo::node<float> *fFTOF_hits_pathLengthThruBar;   //pathlength of the track from the entrance point to the exit point through the hit bar 
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::clusters:reconstructed clusters from FTOF ===================
	hipo::node<uint16_t> *fFTOF_clusters_id;   //id of the hit
	hipo::node<uint16_t> *fFTOF_clusters_status;   //status of the cluster
	hipo::node<uint16_t> *fFTOF_clusters_trackid;   //matched DC track id
	hipo::node<uint8_t> *fFTOF_clusters_sector;   //sector of FTOF
	hipo::node<uint8_t> *fFTOF_clusters_layer;   //panel id of FTOF (1-1A, 2-1B, 3-2
	hipo::node<uint16_t> *fFTOF_clusters_component;   //paddle id of FTOF hit with lowest paddle id in cluster 
	hipo::node<float> *fFTOF_clusters_energy;   //E dep (MeV) of the cluster
	hipo::node<float> *fFTOF_clusters_time;   //Cluster hit time (ns)
	hipo::node<float> *fFTOF_clusters_energy_unc;   //E dep unc (MeV) of the cluster
	hipo::node<float> *fFTOF_clusters_time_unc;   //Cluster hit time unc (ns)
	hipo::node<float> *fFTOF_clusters_x;   //Global X coor (cm) of cluster hit
	hipo::node<float> *fFTOF_clusters_y;   //Global Y coor (cm) of cluster hit
	hipo::node<float> *fFTOF_clusters_z;   //Global Z coor (cm) of cluster hit
	hipo::node<float> *fFTOF_clusters_x_unc;   //Global X coor unc (cm)  of cluster hit
	hipo::node<float> *fFTOF_clusters_y_unc;   //Global Y coor unc (cm) of cluster hit
	hipo::node<float> *fFTOF_clusters_z_unc;   //Global Z coor unc (cm) of cluster hit
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::matchedclusters:matched clusters from FTOF ===================
	hipo::node<uint8_t> *fFTOF_matchedclusters_sector;   //sector of FTOF
	hipo::node<uint16_t> *fFTOF_matchedclusters_paddle_id1A;   //paddle id of hit with lowest paddle id in cluster for panel 1A
	hipo::node<uint16_t> *fFTOF_matchedclusters_paddle_id1B;   //paddle id of hit with lowest paddle id in cluster for panel 1B
	hipo::node<uint16_t> *fFTOF_matchedclusters_clus_1Aid;   //id of cluster in 1A
	hipo::node<uint16_t> *fFTOF_matchedclusters_clus_1Bid;   //id of cluster in 1B
	hipo::node<uint16_t> *fFTOF_matchedclusters_clusSize_1A;   //size of cluster in 1A
	hipo::node<uint16_t> *fFTOF_matchedclusters_clusSize_1B;   //size of cluster in 1B
	hipo::node<float> *fFTOF_matchedclusters_tminAlgo_1B_tCorr;   //corrected hit time using tmin algorithm to compute the path length between 1A and 1B
	hipo::node<float> *fFTOF_matchedclusters_midbarAlgo_1B_tCorr;   //corrected hit time using middle of bar algorithm to compute the path length between 1A and 1B
	hipo::node<float> *fFTOF_matchedclusters_EmaxAlgo_1B_tCorr;   //corrected hit time using Emax algorithm to compute the path length between 1A and 1B
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CTOF::rawhits:reconstructed hit info from U/D CTOF PMTs ===================
	hipo::node<uint16_t> *fCTOF_rawhits_id;   //id of the hit
	hipo::node<uint16_t> *fCTOF_rawhits_status;   //id of the hit
	hipo::node<uint16_t> *fCTOF_rawhits_component;   //paddle id of CTOF
	hipo::node<float> *fCTOF_rawhits_energy_up;   //E dep (MeV) from U PMT
	hipo::node<float> *fCTOF_rawhits_energy_down;   //E dep (MeV) from D PMT
	hipo::node<float> *fCTOF_rawhits_time_up;   //Hit time (ns) from U PMT
	hipo::node<float> *fCTOF_rawhits_time_down;   //Hit time (ns) from D PMT
	hipo::node<float> *fCTOF_rawhits_energy_up_unc;   //E dep unc (MeV) from U PMT
	hipo::node<float> *fCTOF_rawhits_energy_down_unc;   //E dep unc (MeV) from D PMT
	hipo::node<float> *fCTOF_rawhits_time_up_unc;   //Hit time unc (ns) from U PMT
	hipo::node<float> *fCTOF_rawhits_time_down_unc;   //Hit time unc (ns) from D PMT
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CTOF::hits:reconstructed hit info from CTOF ===================
	hipo::node<uint16_t> *fCTOF_hits_id;   //id of the hit
	hipo::node<uint16_t> *fCTOF_hits_status;   //status of the hit
	hipo::node<uint16_t> *fCTOF_hits_trkID;   //match CVT track id
	hipo::node<uint8_t> *fCTOF_hits_sector;   //sector of CTOF
	hipo::node<uint8_t> *fCTOF_hits_layer;   //panel id of CTOF
	hipo::node<uint16_t> *fCTOF_hits_component;   //paddle id of CTOF
	hipo::node<float> *fCTOF_hits_energy;   //E dep (MeV) of hit
	hipo::node<float> *fCTOF_hits_time;   //Hit time (ns)
	hipo::node<float> *fCTOF_hits_energy_unc;   //E dep unc (MeV) of hit
	hipo::node<float> *fCTOF_hits_time_unc;   //Hit time unc (ns)
	hipo::node<float> *fCTOF_hits_x;   //Global X coor (cm) of hit
	hipo::node<float> *fCTOF_hits_y;   //Global Y coor (cm) of hit
	hipo::node<float> *fCTOF_hits_z;   //Global Z coor (cm) of hit
	hipo::node<float> *fCTOF_hits_x_unc;   //Global X coor unc (cm) of hit
	hipo::node<float> *fCTOF_hits_y_unc;   //Global Y coor unc (cm) of hit
	hipo::node<float> *fCTOF_hits_z_unc;   //Global Z coor unc (cm) of hit
	hipo::node<float> *fCTOF_hits_tx;   //Global X coor (cm) of hit from CVT info -trkID index
	hipo::node<float> *fCTOF_hits_ty;   //Global Y coor (cm) of hit from CVT info - trkID index
	hipo::node<float> *fCTOF_hits_tz;   //Global Z coor (cm) of hit from CVT info - trkID index
	hipo::node<uint16_t> *fCTOF_hits_adc_idx1;   //index of hit with ADCU in adc bank, starts at zero
	hipo::node<uint16_t> *fCTOF_hits_adc_idx2;   //index of hit with ADCD in adc bank, starts at zero
	hipo::node<uint16_t> *fCTOF_hits_tdc_idx1;   //index of hit with TDCU in tdc bank, starts at zero
	hipo::node<uint16_t> *fCTOF_hits_tdc_idx2;   //index of hit with TDCD in tdc bank, starts at zero
	hipo::node<float> *fCTOF_hits_pathLength;   //pathlength of the track from the vertex (doca point to the beamline to the midpoint between the entrance and exit of the hit bar
	hipo::node<float> *fCTOF_hits_pathLengthThruBar;   //pathlength of the track from the entrance point to the exit point through the hit bar
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== FTOF::rawhits:reconstructed hit info from L/R FTOF PMTs ===================
	unsigned int GetFTOF_rawhits_id(int i){  return (unsigned int)fFTOF_rawhits_id->getValue(i);}
	unsigned int GetFTOF_rawhits_status(int i){  return (unsigned int)fFTOF_rawhits_status->getValue(i);}
	unsigned int GetFTOF_rawhits_sector(int i){  return (unsigned int)fFTOF_rawhits_sector->getValue(i);}
	unsigned int GetFTOF_rawhits_layer(int i){  return (unsigned int)fFTOF_rawhits_layer->getValue(i);}
	unsigned int GetFTOF_rawhits_component(int i){  return (unsigned int)fFTOF_rawhits_component->getValue(i);}
	float GetFTOF_rawhits_energy_left(int i){  return (float)fFTOF_rawhits_energy_left->getValue(i);}
	float GetFTOF_rawhits_energy_right(int i){  return (float)fFTOF_rawhits_energy_right->getValue(i);}
	float GetFTOF_rawhits_time_left(int i){  return (float)fFTOF_rawhits_time_left->getValue(i);}
	float GetFTOF_rawhits_time_right(int i){  return (float)fFTOF_rawhits_time_right->getValue(i);}
	float GetFTOF_rawhits_energy_left_unc(int i){  return (float)fFTOF_rawhits_energy_left_unc->getValue(i);}
	float GetFTOF_rawhits_energy_right_unc(int i){  return (float)fFTOF_rawhits_energy_right_unc->getValue(i);}
	float GetFTOF_rawhits_time_left_unc(int i){  return (float)fFTOF_rawhits_time_left_unc->getValue(i);}
	float GetFTOF_rawhits_time_right_unc(int i){  return (float)fFTOF_rawhits_time_right_unc->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::hits:reconstructed hit info from FTOF ===================
	unsigned int GetFTOF_hits_id(int i){  return (unsigned int)fFTOF_hits_id->getValue(i);}
	unsigned int GetFTOF_hits_status(int i){  return (unsigned int)fFTOF_hits_status->getValue(i);}
	unsigned int GetFTOF_hits_trackid(int i){  return (unsigned int)fFTOF_hits_trackid->getValue(i);}
	unsigned int GetFTOF_hits_sector(int i){  return (unsigned int)fFTOF_hits_sector->getValue(i);}
	unsigned int GetFTOF_hits_layer(int i){  return (unsigned int)fFTOF_hits_layer->getValue(i);}
	unsigned int GetFTOF_hits_component(int i){  return (unsigned int)fFTOF_hits_component->getValue(i);}
	float GetFTOF_hits_energy(int i){  return (float)fFTOF_hits_energy->getValue(i);}
	float GetFTOF_hits_time(int i){  return (float)fFTOF_hits_time->getValue(i);}
	float GetFTOF_hits_energy_unc(int i){  return (float)fFTOF_hits_energy_unc->getValue(i);}
	float GetFTOF_hits_time_unc(int i){  return (float)fFTOF_hits_time_unc->getValue(i);}
	float GetFTOF_hits_x(int i){  return (float)fFTOF_hits_x->getValue(i);}
	float GetFTOF_hits_y(int i){  return (float)fFTOF_hits_y->getValue(i);}
	float GetFTOF_hits_z(int i){  return (float)fFTOF_hits_z->getValue(i);}
	float GetFTOF_hits_x_unc(int i){  return (float)fFTOF_hits_x_unc->getValue(i);}
	float GetFTOF_hits_y_unc(int i){  return (float)fFTOF_hits_y_unc->getValue(i);}
	float GetFTOF_hits_z_unc(int i){  return (float)fFTOF_hits_z_unc->getValue(i);}
	float GetFTOF_hits_tx(int i){  return (float)fFTOF_hits_tx->getValue(i);}
	float GetFTOF_hits_ty(int i){  return (float)fFTOF_hits_ty->getValue(i);}
	float GetFTOF_hits_tz(int i){  return (float)fFTOF_hits_tz->getValue(i);}
	unsigned int GetFTOF_hits_adc_idx1(int i){  return (unsigned int)fFTOF_hits_adc_idx1->getValue(i);}
	unsigned int GetFTOF_hits_adc_idx2(int i){  return (unsigned int)fFTOF_hits_adc_idx2->getValue(i);}
	unsigned int GetFTOF_hits_tdc_idx1(int i){  return (unsigned int)fFTOF_hits_tdc_idx1->getValue(i);}
	unsigned int GetFTOF_hits_tdc_idx2(int i){  return (unsigned int)fFTOF_hits_tdc_idx2->getValue(i);}
	float GetFTOF_hits_pathLength(int i){  return (float)fFTOF_hits_pathLength->getValue(i);}
	float GetFTOF_hits_pathLengthThruBar(int i){  return (float)fFTOF_hits_pathLengthThruBar->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::clusters:reconstructed clusters from FTOF ===================
	unsigned int GetFTOF_clusters_id(int i){  return (unsigned int)fFTOF_clusters_id->getValue(i);}
	unsigned int GetFTOF_clusters_status(int i){  return (unsigned int)fFTOF_clusters_status->getValue(i);}
	unsigned int GetFTOF_clusters_trackid(int i){  return (unsigned int)fFTOF_clusters_trackid->getValue(i);}
	unsigned int GetFTOF_clusters_sector(int i){  return (unsigned int)fFTOF_clusters_sector->getValue(i);}
	unsigned int GetFTOF_clusters_layer(int i){  return (unsigned int)fFTOF_clusters_layer->getValue(i);}
	unsigned int GetFTOF_clusters_component(int i){  return (unsigned int)fFTOF_clusters_component->getValue(i);}
	float GetFTOF_clusters_energy(int i){  return (float)fFTOF_clusters_energy->getValue(i);}
	float GetFTOF_clusters_time(int i){  return (float)fFTOF_clusters_time->getValue(i);}
	float GetFTOF_clusters_energy_unc(int i){  return (float)fFTOF_clusters_energy_unc->getValue(i);}
	float GetFTOF_clusters_time_unc(int i){  return (float)fFTOF_clusters_time_unc->getValue(i);}
	float GetFTOF_clusters_x(int i){  return (float)fFTOF_clusters_x->getValue(i);}
	float GetFTOF_clusters_y(int i){  return (float)fFTOF_clusters_y->getValue(i);}
	float GetFTOF_clusters_z(int i){  return (float)fFTOF_clusters_z->getValue(i);}
	float GetFTOF_clusters_x_unc(int i){  return (float)fFTOF_clusters_x_unc->getValue(i);}
	float GetFTOF_clusters_y_unc(int i){  return (float)fFTOF_clusters_y_unc->getValue(i);}
	float GetFTOF_clusters_z_unc(int i){  return (float)fFTOF_clusters_z_unc->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FTOF::matchedclusters:matched clusters from FTOF ===================
	unsigned int GetFTOF_matchedclusters_sector(int i){  return (unsigned int)fFTOF_matchedclusters_sector->getValue(i);}
	unsigned int GetFTOF_matchedclusters_paddle_id1A(int i){  return (unsigned int)fFTOF_matchedclusters_paddle_id1A->getValue(i);}
	unsigned int GetFTOF_matchedclusters_paddle_id1B(int i){  return (unsigned int)fFTOF_matchedclusters_paddle_id1B->getValue(i);}
	unsigned int GetFTOF_matchedclusters_clus_1Aid(int i){  return (unsigned int)fFTOF_matchedclusters_clus_1Aid->getValue(i);}
	unsigned int GetFTOF_matchedclusters_clus_1Bid(int i){  return (unsigned int)fFTOF_matchedclusters_clus_1Bid->getValue(i);}
	unsigned int GetFTOF_matchedclusters_clusSize_1A(int i){  return (unsigned int)fFTOF_matchedclusters_clusSize_1A->getValue(i);}
	unsigned int GetFTOF_matchedclusters_clusSize_1B(int i){  return (unsigned int)fFTOF_matchedclusters_clusSize_1B->getValue(i);}
	float GetFTOF_matchedclusters_tminAlgo_1B_tCorr(int i){  return (float)fFTOF_matchedclusters_tminAlgo_1B_tCorr->getValue(i);}
	float GetFTOF_matchedclusters_midbarAlgo_1B_tCorr(int i){  return (float)fFTOF_matchedclusters_midbarAlgo_1B_tCorr->getValue(i);}
	float GetFTOF_matchedclusters_EmaxAlgo_1B_tCorr(int i){  return (float)fFTOF_matchedclusters_EmaxAlgo_1B_tCorr->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CTOF::rawhits:reconstructed hit info from U/D CTOF PMTs ===================
	unsigned int GetCTOF_rawhits_id(int i){  return (unsigned int)fCTOF_rawhits_id->getValue(i);}
	unsigned int GetCTOF_rawhits_status(int i){  return (unsigned int)fCTOF_rawhits_status->getValue(i);}
	unsigned int GetCTOF_rawhits_component(int i){  return (unsigned int)fCTOF_rawhits_component->getValue(i);}
	float GetCTOF_rawhits_energy_up(int i){  return (float)fCTOF_rawhits_energy_up->getValue(i);}
	float GetCTOF_rawhits_energy_down(int i){  return (float)fCTOF_rawhits_energy_down->getValue(i);}
	float GetCTOF_rawhits_time_up(int i){  return (float)fCTOF_rawhits_time_up->getValue(i);}
	float GetCTOF_rawhits_time_down(int i){  return (float)fCTOF_rawhits_time_down->getValue(i);}
	float GetCTOF_rawhits_energy_up_unc(int i){  return (float)fCTOF_rawhits_energy_up_unc->getValue(i);}
	float GetCTOF_rawhits_energy_down_unc(int i){  return (float)fCTOF_rawhits_energy_down_unc->getValue(i);}
	float GetCTOF_rawhits_time_up_unc(int i){  return (float)fCTOF_rawhits_time_up_unc->getValue(i);}
	float GetCTOF_rawhits_time_down_unc(int i){  return (float)fCTOF_rawhits_time_down_unc->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== CTOF::hits:reconstructed hit info from CTOF ===================
	unsigned int GetCTOF_hits_id(int i){  return (unsigned int)fCTOF_hits_id->getValue(i);}
	unsigned int GetCTOF_hits_status(int i){  return (unsigned int)fCTOF_hits_status->getValue(i);}
	unsigned int GetCTOF_hits_trkID(int i){  return (unsigned int)fCTOF_hits_trkID->getValue(i);}
	unsigned int GetCTOF_hits_sector(int i){  return (unsigned int)fCTOF_hits_sector->getValue(i);}
	unsigned int GetCTOF_hits_layer(int i){  return (unsigned int)fCTOF_hits_layer->getValue(i);}
	unsigned int GetCTOF_hits_component(int i){  return (unsigned int)fCTOF_hits_component->getValue(i);}
	float GetCTOF_hits_energy(int i){  return (float)fCTOF_hits_energy->getValue(i);}
	float GetCTOF_hits_time(int i){  return (float)fCTOF_hits_time->getValue(i);}
	float GetCTOF_hits_energy_unc(int i){  return (float)fCTOF_hits_energy_unc->getValue(i);}
	float GetCTOF_hits_time_unc(int i){  return (float)fCTOF_hits_time_unc->getValue(i);}
	float GetCTOF_hits_x(int i){  return (float)fCTOF_hits_x->getValue(i);}
	float GetCTOF_hits_y(int i){  return (float)fCTOF_hits_y->getValue(i);}
	float GetCTOF_hits_z(int i){  return (float)fCTOF_hits_z->getValue(i);}
	float GetCTOF_hits_x_unc(int i){  return (float)fCTOF_hits_x_unc->getValue(i);}
	float GetCTOF_hits_y_unc(int i){  return (float)fCTOF_hits_y_unc->getValue(i);}
	float GetCTOF_hits_z_unc(int i){  return (float)fCTOF_hits_z_unc->getValue(i);}
	float GetCTOF_hits_tx(int i){  return (float)fCTOF_hits_tx->getValue(i);}
	float GetCTOF_hits_ty(int i){  return (float)fCTOF_hits_ty->getValue(i);}
	float GetCTOF_hits_tz(int i){  return (float)fCTOF_hits_tz->getValue(i);}
	unsigned int GetCTOF_hits_adc_idx1(int i){  return (unsigned int)fCTOF_hits_adc_idx1->getValue(i);}
	unsigned int GetCTOF_hits_adc_idx2(int i){  return (unsigned int)fCTOF_hits_adc_idx2->getValue(i);}
	unsigned int GetCTOF_hits_tdc_idx1(int i){  return (unsigned int)fCTOF_hits_tdc_idx1->getValue(i);}
	unsigned int GetCTOF_hits_tdc_idx2(int i){  return (unsigned int)fCTOF_hits_tdc_idx2->getValue(i);}
	float GetCTOF_hits_pathLength(int i){  return (float)fCTOF_hits_pathLength->getValue(i);}
	float GetCTOF_hits_pathLengthThruBar(int i){  return (float)fCTOF_hits_pathLengthThruBar->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorTOF,0)
};
#endif




