// Filename: TClas12DetectorFMT.h
// Description: CLAS12 FMT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORFMT_H
#define TCLAS12DETECTORFMT_H

#include "TClas12Detector.h"

class TClas12DetectorFMT: public TClas12Detector
{
public:
	TClas12DetectorFMT(TClas12Run *run);
	~TClas12DetectorFMT();
	void Init();
	void SetBranches();

protected:
//================== FMTRec::Hits:reconstructed FMT hits ===================
	hipo::node<uint16_t> *fFMTRec_Hits_ID;   //hit ID
	hipo::node<uint8_t> *fFMTRec_Hits_sector;   //hit sector
	hipo::node<uint8_t> *fFMTRec_Hits_layer;   //hit layer
	hipo::node<uint32_t> *fFMTRec_Hits_strip;   //hit strip
	hipo::node<float> *fFMTRec_Hits_fitResidual;   //fitted hit residual
	hipo::node<uint32_t> *fFMTRec_Hits_trkingStat;   //tracking status
	hipo::node<uint16_t> *fFMTRec_Hits_clusterID;   //associated cluster ID
	hipo::node<uint16_t> *fFMTRec_Hits_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FMTRec::Clusters:reconstructed FMT clusters ===================
	hipo::node<uint16_t> *fFMTRec_Clusters_ID;   //ID
	hipo::node<uint8_t> *fFMTRec_Clusters_sector;   //sector
	hipo::node<uint8_t> *fFMTRec_Clusters_layer;   //layer
	hipo::node<uint16_t> *fFMTRec_Clusters_size;   //cluster size
	hipo::node<float> *fFMTRec_Clusters_ETot;   //cluster total energy
	hipo::node<float> *fFMTRec_Clusters_seedE;   //energy of the seed 
	hipo::node<uint32_t> *fFMTRec_Clusters_seedStrip;   //seed strip
	hipo::node<float> *fFMTRec_Clusters_centroid;   //centroid strip number
	hipo::node<float> *fFMTRec_Clusters_centroidResidual;   //centroid residual
	hipo::node<float> *fFMTRec_Clusters_seedResidual;   //seed residual
	hipo::node<uint16_t> *fFMTRec_Clusters_Hit1_ID;   //Index of hit 1 in cluster
	hipo::node<uint16_t> *fFMTRec_Clusters_Hit2_ID;   //Index of hit 2 in cluster
	hipo::node<uint16_t> *fFMTRec_Clusters_Hit3_ID;   //Index of hit 3 in cluster
	hipo::node<uint16_t> *fFMTRec_Clusters_Hit4_ID;   //Index of hit 4 in cluster
	hipo::node<uint16_t> *fFMTRec_Clusters_Hit5_ID;   //Index of hit 5 in cluster
	hipo::node<uint16_t> *fFMTRec_Clusters_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FMTRec::Crosses:reconstructed FMT crosses ===================
	hipo::node<uint16_t> *fFMTRec_Crosses_ID;   //ID
	hipo::node<uint8_t> *fFMTRec_Crosses_sector;   //sector
	hipo::node<uint8_t> *fFMTRec_Crosses_region;   //region
	hipo::node<float> *fFMTRec_Crosses_x;   //BMT cross x-coordinate
	hipo::node<float> *fFMTRec_Crosses_y;   //BMT cross y-coordinate
	hipo::node<float> *fFMTRec_Crosses_z;   //BMT cross z-coordinate
	hipo::node<float> *fFMTRec_Crosses_err_x;   //BMT cross x-coordinate error
	hipo::node<float> *fFMTRec_Crosses_err_y;   //BMT cross y-coordinate error
	hipo::node<float> *fFMTRec_Crosses_err_z;   //BMT cross z-coordinate error
	hipo::node<float> *fFMTRec_Crosses_ux;   //BMT cross x-direction (track unit tangent vector at the cross)
	hipo::node<float> *fFMTRec_Crosses_uy;   //BMT cross y-direction (track unit tangent vector at the cross)
	hipo::node<float> *fFMTRec_Crosses_uz;   //BMT cross z-direction (track unit tangent vector at the cross)
	hipo::node<uint16_t> *fFMTRec_Crosses_Cluster1_ID;   //ID of the  cluster in the Cross
	hipo::node<uint16_t> *fFMTRec_Crosses_Cluster2_ID;   //ID of the top layer  cluster in the Cross
	hipo::node<uint16_t> *fFMTRec_Crosses_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== FMTRec::Hits:reconstructed FMT hits ===================
	unsigned int GetFMTRec_Hits_ID(int i){  return (unsigned int)fFMTRec_Hits_ID->getValue(i);}
	unsigned int GetFMTRec_Hits_sector(int i){  return (unsigned int)fFMTRec_Hits_sector->getValue(i);}
	unsigned int GetFMTRec_Hits_layer(int i){  return (unsigned int)fFMTRec_Hits_layer->getValue(i);}
	unsigned int GetFMTRec_Hits_strip(int i){  return (unsigned int)fFMTRec_Hits_strip->getValue(i);}
	float GetFMTRec_Hits_fitResidual(int i){  return (float)fFMTRec_Hits_fitResidual->getValue(i);}
	unsigned int GetFMTRec_Hits_trkingStat(int i){  return (unsigned int)fFMTRec_Hits_trkingStat->getValue(i);}
	unsigned int GetFMTRec_Hits_clusterID(int i){  return (unsigned int)fFMTRec_Hits_clusterID->getValue(i);}
	unsigned int GetFMTRec_Hits_trkID(int i){  return (unsigned int)fFMTRec_Hits_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FMTRec::Clusters:reconstructed FMT clusters ===================
	unsigned int GetFMTRec_Clusters_ID(int i){  return (unsigned int)fFMTRec_Clusters_ID->getValue(i);}
	unsigned int GetFMTRec_Clusters_sector(int i){  return (unsigned int)fFMTRec_Clusters_sector->getValue(i);}
	unsigned int GetFMTRec_Clusters_layer(int i){  return (unsigned int)fFMTRec_Clusters_layer->getValue(i);}
	unsigned int GetFMTRec_Clusters_size(int i){  return (unsigned int)fFMTRec_Clusters_size->getValue(i);}
	float GetFMTRec_Clusters_ETot(int i){  return (float)fFMTRec_Clusters_ETot->getValue(i);}
	float GetFMTRec_Clusters_seedE(int i){  return (float)fFMTRec_Clusters_seedE->getValue(i);}
	unsigned int GetFMTRec_Clusters_seedStrip(int i){  return (unsigned int)fFMTRec_Clusters_seedStrip->getValue(i);}
	float GetFMTRec_Clusters_centroid(int i){  return (float)fFMTRec_Clusters_centroid->getValue(i);}
	float GetFMTRec_Clusters_centroidResidual(int i){  return (float)fFMTRec_Clusters_centroidResidual->getValue(i);}
	float GetFMTRec_Clusters_seedResidual(int i){  return (float)fFMTRec_Clusters_seedResidual->getValue(i);}
	unsigned int GetFMTRec_Clusters_Hit1_ID(int i){  return (unsigned int)fFMTRec_Clusters_Hit1_ID->getValue(i);}
	unsigned int GetFMTRec_Clusters_Hit2_ID(int i){  return (unsigned int)fFMTRec_Clusters_Hit2_ID->getValue(i);}
	unsigned int GetFMTRec_Clusters_Hit3_ID(int i){  return (unsigned int)fFMTRec_Clusters_Hit3_ID->getValue(i);}
	unsigned int GetFMTRec_Clusters_Hit4_ID(int i){  return (unsigned int)fFMTRec_Clusters_Hit4_ID->getValue(i);}
	unsigned int GetFMTRec_Clusters_Hit5_ID(int i){  return (unsigned int)fFMTRec_Clusters_Hit5_ID->getValue(i);}
	unsigned int GetFMTRec_Clusters_trkID(int i){  return (unsigned int)fFMTRec_Clusters_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== FMTRec::Crosses:reconstructed FMT crosses ===================
	unsigned int GetFMTRec_Crosses_ID(int i){  return (unsigned int)fFMTRec_Crosses_ID->getValue(i);}
	unsigned int GetFMTRec_Crosses_sector(int i){  return (unsigned int)fFMTRec_Crosses_sector->getValue(i);}
	unsigned int GetFMTRec_Crosses_region(int i){  return (unsigned int)fFMTRec_Crosses_region->getValue(i);}
	float GetFMTRec_Crosses_x(int i){  return (float)fFMTRec_Crosses_x->getValue(i);}
	float GetFMTRec_Crosses_y(int i){  return (float)fFMTRec_Crosses_y->getValue(i);}
	float GetFMTRec_Crosses_z(int i){  return (float)fFMTRec_Crosses_z->getValue(i);}
	float GetFMTRec_Crosses_err_x(int i){  return (float)fFMTRec_Crosses_err_x->getValue(i);}
	float GetFMTRec_Crosses_err_y(int i){  return (float)fFMTRec_Crosses_err_y->getValue(i);}
	float GetFMTRec_Crosses_err_z(int i){  return (float)fFMTRec_Crosses_err_z->getValue(i);}
	float GetFMTRec_Crosses_ux(int i){  return (float)fFMTRec_Crosses_ux->getValue(i);}
	float GetFMTRec_Crosses_uy(int i){  return (float)fFMTRec_Crosses_uy->getValue(i);}
	float GetFMTRec_Crosses_uz(int i){  return (float)fFMTRec_Crosses_uz->getValue(i);}
	unsigned int GetFMTRec_Crosses_Cluster1_ID(int i){  return (unsigned int)fFMTRec_Crosses_Cluster1_ID->getValue(i);}
	unsigned int GetFMTRec_Crosses_Cluster2_ID(int i){  return (unsigned int)fFMTRec_Crosses_Cluster2_ID->getValue(i);}
	unsigned int GetFMTRec_Crosses_trkID(int i){  return (unsigned int)fFMTRec_Crosses_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorFMT,0)
};
#endif




