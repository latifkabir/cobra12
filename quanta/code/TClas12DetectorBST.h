// Filename: TClas12DetectorBST.h
// Description: CLAS12 BST base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORBST_H
#define TCLAS12DETECTORBST_H

#include "TClas12Detector.h"

class TClas12DetectorBST: public TClas12Detector
{
public:
	TClas12DetectorBST(TClas12Run *run);
	~TClas12DetectorBST();
	void Init();
	void SetBranches();

protected:
//================== BSTRec::Hits:reconstructed BST hits on track ===================
	hipo::node<uint16_t> *fBSTRec_Hits_ID;   //hit ID
	hipo::node<uint8_t> *fBSTRec_Hits_sector;   //hit sector
	hipo::node<uint8_t> *fBSTRec_Hits_layer;   //hit layer
	hipo::node<uint32_t> *fBSTRec_Hits_strip;   //hit strip
	hipo::node<float> *fBSTRec_Hits_fitResidual;   //fitted hit residual
	hipo::node<uint32_t> *fBSTRec_Hits_trkingStat;   //tracking status
	hipo::node<uint16_t> *fBSTRec_Hits_clusterID;   //associated cluster ID
	hipo::node<uint16_t> *fBSTRec_Hits_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::Clusters:reconstructed BST clusters ===================
	hipo::node<uint16_t> *fBSTRec_Clusters_ID;   //ID
	hipo::node<uint8_t> *fBSTRec_Clusters_sector;   //sector
	hipo::node<uint8_t> *fBSTRec_Clusters_layer;   //layer
	hipo::node<uint16_t> *fBSTRec_Clusters_size;   //cluster size
	hipo::node<float> *fBSTRec_Clusters_ETot;   //cluster total energy
	hipo::node<float> *fBSTRec_Clusters_seedE;   //energy of the seed 
	hipo::node<uint32_t> *fBSTRec_Clusters_seedStrip;   //seed strip
	hipo::node<float> *fBSTRec_Clusters_centroid;   //centroid strip number
	hipo::node<float> *fBSTRec_Clusters_centroidResidual;   //centroid residual
	hipo::node<float> *fBSTRec_Clusters_seedResidual;   //seed residual
	hipo::node<uint16_t> *fBSTRec_Clusters_Hit1_ID;   //Index of hit 1 in cluster
	hipo::node<uint16_t> *fBSTRec_Clusters_Hit2_ID;   //Index of hit 2 in cluster
	hipo::node<uint16_t> *fBSTRec_Clusters_Hit3_ID;   //Index of hit 3 in cluster
	hipo::node<uint16_t> *fBSTRec_Clusters_Hit4_ID;   //Index of hit 4 in cluster
	hipo::node<uint16_t> *fBSTRec_Clusters_Hit5_ID;   //Index of hit 5 in cluster
	hipo::node<uint16_t> *fBSTRec_Clusters_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::Crosses:reconstructed BST crosses ===================
	hipo::node<uint16_t> *fBSTRec_Crosses_ID;   //ID
	hipo::node<uint8_t> *fBSTRec_Crosses_sector;   //sector
	hipo::node<uint8_t> *fBSTRec_Crosses_region;   //region
	hipo::node<float> *fBSTRec_Crosses_x;   //BMT cross x-coordinate
	hipo::node<float> *fBSTRec_Crosses_y;   //BMT cross y-coordinate
	hipo::node<float> *fBSTRec_Crosses_z;   //BMT cross z-coordinate
	hipo::node<float> *fBSTRec_Crosses_err_x;   //BMT cross x-coordinate error
	hipo::node<float> *fBSTRec_Crosses_err_y;   //BMT cross y-coordinate error
	hipo::node<float> *fBSTRec_Crosses_err_z;   //BMT cross z-coordinate error
	hipo::node<float> *fBSTRec_Crosses_ux;   //BMT cross x-direction (track unit tangent vector at the cross)
	hipo::node<float> *fBSTRec_Crosses_uy;   //BMT cross y-direction (track unit tangent vector at the cross)
	hipo::node<float> *fBSTRec_Crosses_uz;   //BMT cross z-direction (track unit tangent vector at the cross)
	hipo::node<uint16_t> *fBSTRec_Crosses_Cluster1_ID;   //ID of the  cluster in the Cross
	hipo::node<uint16_t> *fBSTRec_Crosses_Cluster2_ID;   //ID of the top layer  cluster in the Cross
	hipo::node<uint16_t> *fBSTRec_Crosses_trkID;   //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::LayerEffs:layer efficiencies ===================
	hipo::node<uint8_t> *fBSTRec_LayerEffs_sector;   //sector
	hipo::node<uint8_t> *fBSTRec_LayerEffs_layer;   //layer
	hipo::node<float> *fBSTRec_LayerEffs_residual;   //excluded layer residual of the matched cluster centroid
	hipo::node<uint8_t> *fBSTRec_LayerEffs_status;   //status (-1: not used, i.e. no track; 0: inefficient; 1: efficient
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== BSTRec::Hits:reconstructed BST hits on track ===================
	unsigned int GetBSTRec_Hits_ID(int i){  return (unsigned int)fBSTRec_Hits_ID->getValue(i);}
	unsigned int GetBSTRec_Hits_sector(int i){  return (unsigned int)fBSTRec_Hits_sector->getValue(i);}
	unsigned int GetBSTRec_Hits_layer(int i){  return (unsigned int)fBSTRec_Hits_layer->getValue(i);}
	unsigned int GetBSTRec_Hits_strip(int i){  return (unsigned int)fBSTRec_Hits_strip->getValue(i);}
	float GetBSTRec_Hits_fitResidual(int i){  return (float)fBSTRec_Hits_fitResidual->getValue(i);}
	unsigned int GetBSTRec_Hits_trkingStat(int i){  return (unsigned int)fBSTRec_Hits_trkingStat->getValue(i);}
	unsigned int GetBSTRec_Hits_clusterID(int i){  return (unsigned int)fBSTRec_Hits_clusterID->getValue(i);}
	unsigned int GetBSTRec_Hits_trkID(int i){  return (unsigned int)fBSTRec_Hits_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::Clusters:reconstructed BST clusters ===================
	unsigned int GetBSTRec_Clusters_ID(int i){  return (unsigned int)fBSTRec_Clusters_ID->getValue(i);}
	unsigned int GetBSTRec_Clusters_sector(int i){  return (unsigned int)fBSTRec_Clusters_sector->getValue(i);}
	unsigned int GetBSTRec_Clusters_layer(int i){  return (unsigned int)fBSTRec_Clusters_layer->getValue(i);}
	unsigned int GetBSTRec_Clusters_size(int i){  return (unsigned int)fBSTRec_Clusters_size->getValue(i);}
	float GetBSTRec_Clusters_ETot(int i){  return (float)fBSTRec_Clusters_ETot->getValue(i);}
	float GetBSTRec_Clusters_seedE(int i){  return (float)fBSTRec_Clusters_seedE->getValue(i);}
	unsigned int GetBSTRec_Clusters_seedStrip(int i){  return (unsigned int)fBSTRec_Clusters_seedStrip->getValue(i);}
	float GetBSTRec_Clusters_centroid(int i){  return (float)fBSTRec_Clusters_centroid->getValue(i);}
	float GetBSTRec_Clusters_centroidResidual(int i){  return (float)fBSTRec_Clusters_centroidResidual->getValue(i);}
	float GetBSTRec_Clusters_seedResidual(int i){  return (float)fBSTRec_Clusters_seedResidual->getValue(i);}
	unsigned int GetBSTRec_Clusters_Hit1_ID(int i){  return (unsigned int)fBSTRec_Clusters_Hit1_ID->getValue(i);}
	unsigned int GetBSTRec_Clusters_Hit2_ID(int i){  return (unsigned int)fBSTRec_Clusters_Hit2_ID->getValue(i);}
	unsigned int GetBSTRec_Clusters_Hit3_ID(int i){  return (unsigned int)fBSTRec_Clusters_Hit3_ID->getValue(i);}
	unsigned int GetBSTRec_Clusters_Hit4_ID(int i){  return (unsigned int)fBSTRec_Clusters_Hit4_ID->getValue(i);}
	unsigned int GetBSTRec_Clusters_Hit5_ID(int i){  return (unsigned int)fBSTRec_Clusters_Hit5_ID->getValue(i);}
	unsigned int GetBSTRec_Clusters_trkID(int i){  return (unsigned int)fBSTRec_Clusters_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::Crosses:reconstructed BST crosses ===================
	unsigned int GetBSTRec_Crosses_ID(int i){  return (unsigned int)fBSTRec_Crosses_ID->getValue(i);}
	unsigned int GetBSTRec_Crosses_sector(int i){  return (unsigned int)fBSTRec_Crosses_sector->getValue(i);}
	unsigned int GetBSTRec_Crosses_region(int i){  return (unsigned int)fBSTRec_Crosses_region->getValue(i);}
	float GetBSTRec_Crosses_x(int i){  return (float)fBSTRec_Crosses_x->getValue(i);}
	float GetBSTRec_Crosses_y(int i){  return (float)fBSTRec_Crosses_y->getValue(i);}
	float GetBSTRec_Crosses_z(int i){  return (float)fBSTRec_Crosses_z->getValue(i);}
	float GetBSTRec_Crosses_err_x(int i){  return (float)fBSTRec_Crosses_err_x->getValue(i);}
	float GetBSTRec_Crosses_err_y(int i){  return (float)fBSTRec_Crosses_err_y->getValue(i);}
	float GetBSTRec_Crosses_err_z(int i){  return (float)fBSTRec_Crosses_err_z->getValue(i);}
	float GetBSTRec_Crosses_ux(int i){  return (float)fBSTRec_Crosses_ux->getValue(i);}
	float GetBSTRec_Crosses_uy(int i){  return (float)fBSTRec_Crosses_uy->getValue(i);}
	float GetBSTRec_Crosses_uz(int i){  return (float)fBSTRec_Crosses_uz->getValue(i);}
	unsigned int GetBSTRec_Crosses_Cluster1_ID(int i){  return (unsigned int)fBSTRec_Crosses_Cluster1_ID->getValue(i);}
	unsigned int GetBSTRec_Crosses_Cluster2_ID(int i){  return (unsigned int)fBSTRec_Crosses_Cluster2_ID->getValue(i);}
	unsigned int GetBSTRec_Crosses_trkID(int i){  return (unsigned int)fBSTRec_Crosses_trkID->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BSTRec::LayerEffs:layer efficiencies ===================
	unsigned int GetBSTRec_LayerEffs_sector(int i){  return (unsigned int)fBSTRec_LayerEffs_sector->getValue(i);}
	unsigned int GetBSTRec_LayerEffs_layer(int i){  return (unsigned int)fBSTRec_LayerEffs_layer->getValue(i);}
	float GetBSTRec_LayerEffs_residual(int i){  return (float)fBSTRec_LayerEffs_residual->getValue(i);}
	unsigned int GetBSTRec_LayerEffs_status(int i){  return (unsigned int)fBSTRec_LayerEffs_status->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorBST,0)
};
#endif




