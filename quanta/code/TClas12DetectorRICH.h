// Filename: TClas12DetectorRICH.h
// Description: CLAS12 RICH base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:33:05 2018 (-0400)
// URL: jlab.org/~latif

#ifndef TCLAS12DETECTORRICH_H
#define TCLAS12DETECTORRICH_H

#include "TClas12Detector.h"

class TClas12DetectorRICH: public TClas12Detector
{
public:
	TClas12DetectorRICH(TClas12Run *run);
	~TClas12DetectorRICH();
	void Init();
	void SetBranches();

protected:
//================== RICH::hits:Reconstructed Hits in RICH ===================
	hipo::node<uint16_t> *fRICH_hits_id;   //Hit id
	hipo::node<uint16_t> *fRICH_hits_sector;   //Hit sector
	hipo::node<uint16_t> *fRICH_hits_tile;   //Hit tile
	hipo::node<uint16_t> *fRICH_hits_pmt;   //Hit pmt
	hipo::node<uint16_t> *fRICH_hits_anode;   //Hit anode
	hipo::node<uint16_t> *fRICH_hits_idx;   //Hit idx
	hipo::node<uint16_t> *fRICH_hits_idy;   //Hit idy
	hipo::node<uint16_t> *fRICH_hits_glx;   //Hit glx
	hipo::node<uint16_t> *fRICH_hits_gly;   //Hit gly
	hipo::node<uint16_t> *fRICH_hits_time;   //Hit time
	hipo::node<uint16_t> *fRICH_hits_cluster;   //Hit cluster
	hipo::node<uint16_t> *fRICH_hits_xtalk;   //Hit xtalk
	hipo::node<uint16_t> *fRICH_hits_duration;   //Hit duration
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RICH::clusters:Reconstructed Clusters in RICH ===================
	hipo::node<uint16_t> *fRICH_clusters_id;   //Clu id
	hipo::node<uint16_t> *fRICH_clusters_size;   //Clu size
	hipo::node<uint16_t> *fRICH_clusters_sector;   //Clu sector
	hipo::node<uint16_t> *fRICH_clusters_tile;   //Clu tile
	hipo::node<uint16_t> *fRICH_clusters_pmt;   //Clu pmt
	hipo::node<float> *fRICH_clusters_charge;   //Clu charge
	hipo::node<float> *fRICH_clusters_time;   //Clu time
	hipo::node<float> *fRICH_clusters_x;   //Clu x
	hipo::node<float> *fRICH_clusters_y;   //Clu y
	hipo::node<float> *fRICH_clusters_z;   //Clu z
	hipo::node<float> *fRICH_clusters_wtime;   //Clu wtime
	hipo::node<float> *fRICH_clusters_wx;   //Clu wx
	hipo::node<float> *fRICH_clusters_wy;   //Clu wy
	hipo::node<float> *fRICH_clusters_wz;   //Clu wz
//--------------------------------------------------------------------------------------------------------------------------------------


public:
//================== RICH::hits:Reconstructed Hits in RICH ===================
	unsigned int GetRICH_hits_id(int i){  return (unsigned int)fRICH_hits_id->getValue(i);}
	unsigned int GetRICH_hits_sector(int i){  return (unsigned int)fRICH_hits_sector->getValue(i);}
	unsigned int GetRICH_hits_tile(int i){  return (unsigned int)fRICH_hits_tile->getValue(i);}
	unsigned int GetRICH_hits_pmt(int i){  return (unsigned int)fRICH_hits_pmt->getValue(i);}
	unsigned int GetRICH_hits_anode(int i){  return (unsigned int)fRICH_hits_anode->getValue(i);}
	unsigned int GetRICH_hits_idx(int i){  return (unsigned int)fRICH_hits_idx->getValue(i);}
	unsigned int GetRICH_hits_idy(int i){  return (unsigned int)fRICH_hits_idy->getValue(i);}
	unsigned int GetRICH_hits_glx(int i){  return (unsigned int)fRICH_hits_glx->getValue(i);}
	unsigned int GetRICH_hits_gly(int i){  return (unsigned int)fRICH_hits_gly->getValue(i);}
	unsigned int GetRICH_hits_time(int i){  return (unsigned int)fRICH_hits_time->getValue(i);}
	unsigned int GetRICH_hits_cluster(int i){  return (unsigned int)fRICH_hits_cluster->getValue(i);}
	unsigned int GetRICH_hits_xtalk(int i){  return (unsigned int)fRICH_hits_xtalk->getValue(i);}
	unsigned int GetRICH_hits_duration(int i){  return (unsigned int)fRICH_hits_duration->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

//================== RICH::clusters:Reconstructed Clusters in RICH ===================
	unsigned int GetRICH_clusters_id(int i){  return (unsigned int)fRICH_clusters_id->getValue(i);}
	unsigned int GetRICH_clusters_size(int i){  return (unsigned int)fRICH_clusters_size->getValue(i);}
	unsigned int GetRICH_clusters_sector(int i){  return (unsigned int)fRICH_clusters_sector->getValue(i);}
	unsigned int GetRICH_clusters_tile(int i){  return (unsigned int)fRICH_clusters_tile->getValue(i);}
	unsigned int GetRICH_clusters_pmt(int i){  return (unsigned int)fRICH_clusters_pmt->getValue(i);}
	float GetRICH_clusters_charge(int i){  return (float)fRICH_clusters_charge->getValue(i);}
	float GetRICH_clusters_time(int i){  return (float)fRICH_clusters_time->getValue(i);}
	float GetRICH_clusters_x(int i){  return (float)fRICH_clusters_x->getValue(i);}
	float GetRICH_clusters_y(int i){  return (float)fRICH_clusters_y->getValue(i);}
	float GetRICH_clusters_z(int i){  return (float)fRICH_clusters_z->getValue(i);}
	float GetRICH_clusters_wtime(int i){  return (float)fRICH_clusters_wtime->getValue(i);}
	float GetRICH_clusters_wx(int i){  return (float)fRICH_clusters_wx->getValue(i);}
	float GetRICH_clusters_wy(int i){  return (float)fRICH_clusters_wy->getValue(i);}
	float GetRICH_clusters_wz(int i){  return (float)fRICH_clusters_wz->getValue(i);}
//--------------------------------------------------------------------------------------------------------------------------------------

	ClassDef(TClas12DetectorRICH,0)
};
#endif




