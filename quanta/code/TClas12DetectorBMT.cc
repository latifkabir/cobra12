// Filename: TClas12DetectorBMT.cc
// Description: CLAS12 BMT base class
// Author: Latif Kabir < latif@jlab.org >
// Created: Thu May 31 13:31:42 2018 (-0400)
// URL: jlab.org/~latif


#include "TClas12DetectorBMT.h"


ClassImp(TClas12DetectorBMT)
//-------------------------------------------------------------------------------------
TClas12DetectorBMT::TClas12DetectorBMT(TClas12Run *run):TClas12Detector(run)
{
	Init();
}
//-------------------------------------------------------------------------------------
TClas12DetectorBMT::~TClas12DetectorBMT()
{


}
//-------------------------------------------------------------------------------------
void TClas12DetectorBMT::Init()
{
	SetBranches();
}
//-------------------------------------------------------------------------------------
void TClas12DetectorBMT::SetBranches()
{
//================== BMTRec::Hits:reconstructed BMT hits on track ===================
	fBMTRec_Hits_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Hits", "ID"); 	 //hit ID
	fBMTRec_Hits_sector = fHipoReader->getBranch<uint8_t>("BMTRec::Hits", "sector"); 	 //hit sector
	fBMTRec_Hits_layer = fHipoReader->getBranch<uint8_t>("BMTRec::Hits", "layer"); 	 //hit layer
	fBMTRec_Hits_strip = fHipoReader->getBranch<uint32_t>("BMTRec::Hits", "strip"); 	 //hit strip
	fBMTRec_Hits_fitResidual = fHipoReader->getBranch<float>("BMTRec::Hits", "fitResidual"); 	 //fitted hit residual
	fBMTRec_Hits_trkingStat = fHipoReader->getBranch<uint32_t>("BMTRec::Hits", "trkingStat"); 	 //tracking status
	fBMTRec_Hits_clusterID = fHipoReader->getBranch<uint16_t>("BMTRec::Hits", "clusterID"); 	 //associated cluster ID
	fBMTRec_Hits_trkID = fHipoReader->getBranch<uint16_t>("BMTRec::Hits", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::Clusters:reconstructed BMT clusters ===================
	fBMTRec_Clusters_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "ID"); 	 //ID
	fBMTRec_Clusters_sector = fHipoReader->getBranch<uint8_t>("BMTRec::Clusters", "sector"); 	 //sector
	fBMTRec_Clusters_layer = fHipoReader->getBranch<uint8_t>("BMTRec::Clusters", "layer"); 	 //layer
	fBMTRec_Clusters_size = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "size"); 	 //cluster size
	fBMTRec_Clusters_ETot = fHipoReader->getBranch<float>("BMTRec::Clusters", "ETot"); 	 //cluster total energy
	fBMTRec_Clusters_seedE = fHipoReader->getBranch<float>("BMTRec::Clusters", "seedE"); 	 //energy of the seed 
	fBMTRec_Clusters_seedStrip = fHipoReader->getBranch<uint32_t>("BMTRec::Clusters", "seedStrip"); 	 //seed strip
	fBMTRec_Clusters_centroid = fHipoReader->getBranch<float>("BMTRec::Clusters", "centroid"); 	 //centroid strip number
	fBMTRec_Clusters_centroidResidual = fHipoReader->getBranch<float>("BMTRec::Clusters", "centroidResidual"); 	 //centroid residual
	fBMTRec_Clusters_seedResidual = fHipoReader->getBranch<float>("BMTRec::Clusters", "seedResidual"); 	 //seed residual
	fBMTRec_Clusters_Hit1_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "Hit1_ID"); 	 //Index of hit 1 in cluster
	fBMTRec_Clusters_Hit2_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "Hit2_ID"); 	 //Index of hit 2 in cluster
	fBMTRec_Clusters_Hit3_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "Hit3_ID"); 	 //Index of hit 3 in cluster
	fBMTRec_Clusters_Hit4_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "Hit4_ID"); 	 //Index of hit 4 in cluster
	fBMTRec_Clusters_Hit5_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "Hit5_ID"); 	 //Index of hit 5 in cluster
	fBMTRec_Clusters_trkID = fHipoReader->getBranch<uint16_t>("BMTRec::Clusters", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::Crosses:reconstructed BMT crosses ===================
	fBMTRec_Crosses_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Crosses", "ID"); 	 //ID
	fBMTRec_Crosses_sector = fHipoReader->getBranch<uint8_t>("BMTRec::Crosses", "sector"); 	 //sector
	fBMTRec_Crosses_region = fHipoReader->getBranch<uint8_t>("BMTRec::Crosses", "region"); 	 //region
	fBMTRec_Crosses_x = fHipoReader->getBranch<float>("BMTRec::Crosses", "x"); 	 //BMT cross x-coordinate
	fBMTRec_Crosses_y = fHipoReader->getBranch<float>("BMTRec::Crosses", "y"); 	 //BMT cross y-coordinate
	fBMTRec_Crosses_z = fHipoReader->getBranch<float>("BMTRec::Crosses", "z"); 	 //BMT cross z-coordinate
	fBMTRec_Crosses_err_x = fHipoReader->getBranch<float>("BMTRec::Crosses", "err_x"); 	 //BMT cross x-coordinate error
	fBMTRec_Crosses_err_y = fHipoReader->getBranch<float>("BMTRec::Crosses", "err_y"); 	 //BMT cross y-coordinate error
	fBMTRec_Crosses_err_z = fHipoReader->getBranch<float>("BMTRec::Crosses", "err_z"); 	 //BMT cross z-coordinate error
	fBMTRec_Crosses_ux = fHipoReader->getBranch<float>("BMTRec::Crosses", "ux"); 	 //BMT cross x-direction (track unit tangent vector at the cross)
	fBMTRec_Crosses_uy = fHipoReader->getBranch<float>("BMTRec::Crosses", "uy"); 	 //BMT cross y-direction (track unit tangent vector at the cross)
	fBMTRec_Crosses_uz = fHipoReader->getBranch<float>("BMTRec::Crosses", "uz"); 	 //BMT cross z-direction (track unit tangent vector at the cross)
	fBMTRec_Crosses_Cluster1_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Crosses", "Cluster1_ID"); 	 //ID of the  cluster in the Cross
	fBMTRec_Crosses_Cluster2_ID = fHipoReader->getBranch<uint16_t>("BMTRec::Crosses", "Cluster2_ID"); 	 //ID of the top layer  cluster in the Cross
	fBMTRec_Crosses_trkID = fHipoReader->getBranch<uint16_t>("BMTRec::Crosses", "trkID"); 	 //associated track ID
//--------------------------------------------------------------------------------------------------------------------------------------

//================== BMTRec::LayerEffs:layer efficiencies ===================
	fBMTRec_LayerEffs_sector = fHipoReader->getBranch<uint8_t>("BMTRec::LayerEffs", "sector"); 	 //sector
	fBMTRec_LayerEffs_layer = fHipoReader->getBranch<uint8_t>("BMTRec::LayerEffs", "layer"); 	 //layer
	fBMTRec_LayerEffs_residual = fHipoReader->getBranch<float>("BMTRec::LayerEffs", "residual"); 	 //excluded layer residual of the matched cluster centroid
	fBMTRec_LayerEffs_status = fHipoReader->getBranch<uint8_t>("BMTRec::LayerEffs", "status"); 	 //status (-1: not used, i.e. no track; 0: inefficient; 1: efficient
//--------------------------------------------------------------------------------------------------------------------------------------

}
