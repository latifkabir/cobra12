// Filename: ECALdistributions.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12Run.h"
#include "TH1F.h"
#include "TClas12ECAL.h"

void ECALdistributions()
{
    TClas12Run *clas_run = new TClas12Run(4307, 0); 
    if(!clas_run->fFileExist)
	return;
    TClas12ECAL *ecal = (TClas12ECAL*)clas_run->AddDetector("ECAL");

    TH1F *hist = new TH1F("hist","Hit energy sec = 1, L = 1 (PCAL);energy; Counts", 200, 0, 0);
    Int_t ecounter = 0;
    Int_t sec;
    Int_t layer;
    Float_t energy;

    while(clas_run->HasEvent())
    {	
    	Int_t length = ecal->GetSizeHits();
    	for(Int_t k = 0; k < length; k++)
    	{
    	    sec = ecal->GetECAL_hits_sector(k);
    	    layer = ecal->GetECAL_hits_layer(k);
    	    energy = ecal->GetECAL_hits_energy(k);
    	    if(sec == 1 && layer == 1)
    		hist->Fill(energy);
    	}
    	ecounter++;
    }
    delete clas_run;
    
    hist->Draw();    
}
