// Filename: ExTClas12Run2.h
// Description: pi0 invariant mass calculation
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat May 12 15:53:30 2018 (-0400)
// URL: jlab.org/~latif

void ExTClas12Run2(Int_t runNumber, Int_t firstFile, Int_t lastFile);
