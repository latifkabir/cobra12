// Filename: DCDistributions.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12Run.h"
#include "TH1F.h"
#include "TClas12DC.h"
#include "TCanvas.h"

void DCDistributions3()
{
    //TClas12Run *clas_run = new TClas12Run(3050, 0); 
    TClas12Run *clas_run = new TClas12Run(4013, 0); 
    if(!clas_run->fFileExist)
	return;
    TClas12DC *dc = (TClas12DC*)clas_run->AddDetector("DC");

    TH1F *hist[6];
    for(int i = 0; i < 6; i++)
    {
	TString title = "TDC sec = 1, SL = ";
	title += (i + 1);
	title += ";TDC; Counts";
	hist[i] = new TH1F("hist", title, 200, 0,1000);
    }
    Int_t sec;
    Int_t sl;
    Int_t tdc;
    Int_t trkID; 

    while(clas_run->HasEvent())
    {	
    	Int_t length = dc->GetSizeHBHits();
    	for(Int_t k = 0; k < length; k++)
    	{
    	    sec = dc->GetHitBasedTrkg_HBHits_sector(k);
    	    sl = dc->GetHitBasedTrkg_HBHits_superlayer(k);
    	    tdc = dc->GetHitBasedTrkg_HBHits_TDC(k);
	    trkID = dc->GetHitBasedTrkg_HBHits_trkID(k);
    	    if(sec == 1 && trkID > 0)
    		hist[sl-1]->Fill(tdc);
    	}
    }
    delete clas_run;

    TCanvas *c1 = new TCanvas();
    c1->Divide(3,2);
    for(int j = 0; j < 6; ++j)
    {
	c1->cd(j+1);
	hist[j]->Draw();
    }
}
