// Filename: Rh0Analysis.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Tue May 15 01:28:46 2018 (-0400)
// URL: jlab.org/~latif

void Rho0Analysis(Int_t runNumber, Int_t firstFile, Int_t lastFile, Bool_t onFarm = kFALSE);
