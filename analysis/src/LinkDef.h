#ifdef __CINT__

//:============ Analysis Section ============: :
#pragma link C++ function DCDistributions1;          // Example code showing DC distribution 
#pragma link C++ function DCDistributions2;          // Example code showing DC distribution 
#pragma link C++ function DCDistributions3;          // Example code showing DC distribution 
#pragma link C++ function ECALdistributions;         // Example code showing ECAL distribution 
#pragma link C++ function DataQuality1;              // Make plots to check data quality (explicit way) 
#pragma link C++ function DataQuality2;              // Make plots to check data quality (compact way)
#pragma link C++ function Clas12DSTMaker;            // Make DST root file for analysis 
#pragma link C++ function KinematicDist;             // System invariant mass
#pragma link C++ function BetaVsMomentum;            // Beta vs momentumplot for positively charged hadrons
#pragma link C++ function Pi0Analysis;               // Pi0 Invariant mass Ananlysis
#pragma link C++ function Pi0Analysis2;              // Pi0 Invariant mass Ananlysis, more developed version
#pragma link C++ function Rho0Analysis;              // Rho0 Invariant mass Ananlysis
#pragma link C++ function LambdaAnalysis;            // Lambda Invariant mass Ananlysis

#pragma link C++ function ExTClas12Run0;             // Example code, distribution for e-
#pragma link C++ function ExTClas12Run1;             // Example code, distribution for e-
#pragma link C++ function ExTClas12Run2;             // Example code, pi0 Invariant mass
#pragma link C++ function ExTClas12Run3;             // Example code, beta vs momentum
#pragma link C++ function DCfitResidual;             // DC fit residual

#endif
