// Filename: DCDistributions1.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12Run.h"
#include "TH1F.h"
#include "TClas12DC.h"

void DCDistributions1()
{
    //TClas12Run *clas_run = new TClas12Run(3050, 0); 
    TClas12Run *clas_run = new TClas12Run(3971, 0); 
    if(!clas_run->fFileExist)
	return;
    TClas12DC *dc = new TClas12DC(clas_run);

    TH1F *hist = new TH1F("hist","TDC sec = 2, SL = 5;TDC; Counts", 200, 0,1000);
    Int_t ecounter = 0;
    Int_t sec;
    Int_t sl;
    Int_t tdc;

    while(clas_run->HasEvent())
    {
	dc->Update();  //<------------ You need to update maps etc explicity here
	
    	Int_t length = dc->GetSizeHBHits();
    	for(Int_t k = 0; k < length; k++)
    	{
    	    sec = dc->GetHitBasedTrkg_HBHits_sector(k);
    	    sl = dc->GetHitBasedTrkg_HBHits_superlayer(k);
    	    tdc = dc->GetHitBasedTrkg_HBHits_TDC(k);
    	    if(sec == 1 && sl == 5)
    		hist->Fill(tdc);
    	}
    	ecounter++;
    }

    delete dc;
    delete clas_run;
    
    hist->Draw();    
}
