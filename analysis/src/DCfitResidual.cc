// Filename: DCfitResidual.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include "TClas12Run.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TClas12DC.h"
#include "TClas12.h"
#include "TStopwatch.h"

void DCfitResidual()
{
    TStopwatch st;
    st.Start();
    TClas12::TurnOffMap("EVENT");
    TClas12::TurnOffMap("DC");
    
    TH1F *hist[36];
    for(Int_t i = 0; i < 36; i++)
	hist[i] = new TH1F((TString)"hist" + to_string(i+1),(TString)"fitResidual sec = 1, SL = 5, Layer "+ to_string(i+1) + (TString)"; fitResidual; Counts", 200, -5, 5);

    TGraph *gr = new TGraph();
        
    Int_t ecounter = 0;
    Int_t sec;
    Int_t sl;
    Double_t fitResidual;
    Int_t layer;

    for (Int_t fileNo = 110; fileNo < 111; ++fileNo)
    {
	TClas12Run *clas_run = new TClas12Run(2467, 110); 
	if(!clas_run->fFileExist)
	    return;
	TClas12DC *dc = (TClas12DC*)clas_run->AddDetector("DC");

	while(clas_run->HasEvent())
	{	
	    Int_t length = dc->GetSizeTBHits();
	    for(Int_t k = 0; k < length; k++)
	    {
		sec = dc->GetTimeBasedTrkg_TBHits_sector(k);
		sl = dc->GetTimeBasedTrkg_TBHits_superlayer(k);
		layer = dc->GetTimeBasedTrkg_TBHits_layer(k);
		fitResidual = dc->GetTimeBasedTrkg_TBHits_fitResidual(k);
		if(sec == 5)
		    hist[(sl-1)*6 + layer-1]->Fill(fitResidual);
	    }
	    ecounter++;
	}
	delete clas_run;
    }

    TCanvas *c1 = new TCanvas();
    c1->Divide(6,6);
    for(Int_t i = 0; i < 36; ++i)
    {
	c1->cd(i+1);
	hist[i]->Draw();
	gr->SetPoint(i, hist[i]->GetMean(), i);
    }

    TCanvas *c2 = new TCanvas();
    gr->Draw();
    st.Stop();
    st.Print();
}
