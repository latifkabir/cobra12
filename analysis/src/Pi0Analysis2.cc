// Filename: Pi0Analysis.cc
// Description: pi0 invariant mass calculation. pi0 peak is visible with run#3985, also possible 3222, 4203 etc. Not visible for runs prior to 3222. for 6.4 GeV use 3852. For 10.6 GeV, tested with run#3971 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TClas12.h"
#include "TFile.h"
#include "TLorentzVector.h"

using namespace std;

void Pi0Analysis2(Int_t runNumber, Int_t firstFile, Int_t lastFile)
{
    //gStyle->SetOptStat(0);

    TString fileName = TClas12::Config->GetResultsPath();
    fileName += "Pi0Analysis2_";
    fileName += runNumber;
    fileName += "_";
    fileName += firstFile;
    fileName += "-";
    fileName += lastFile;
    fileName += ".root";
    TFile *file = new TFile(fileName , "recreate");

    // Particle bank
    Int_t pid = -1;
    
    Float_t thetaDeg;
    Float_t phiDeg;
    Float_t px_e;
    Float_t py_e;
    Float_t pz_e;
    Float_t p_e;
    
    Int_t gindex;
    Int_t eindex;

    TLorentzVector LV[2];
    Double_t ggAngle;
    
    TH1D *pi0W = new TH1D("pi0W", "#pi^{0} Invariant mass; M_{#pi^{0}} [GeV]", 200, -0.02, 0.70);
    TH2D *ggAngleVsE = new TH2D("ggAngleVsE", "#gamma#gamma openning angle vs E_{#gamma#gamma}; E_{#gamma#gamma} [GeV]; #theta_{#gamma#gamma} [deg]", 100, 0, 6, 100, 0, 30); 
    
    for(Int_t fileNo = firstFile; fileNo <= lastFile; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;

	while(clas->HasEvent())
	{
	    if(!clas->GetSizePart() || !clas->GetSizeCher())
	    	continue;

	    // Consider only events with one e-
	    if(clas->GetCountWithPID(11) != 1)
	    	continue;

	    auto eit = clas->GetItrPairForPID(11);   // The return value is a pair representing the range of elements
	    eindex = eit.first->second;              // Index of e in particle bank (take first range and get second)
	    px_e = clas->GetPx(eindex);
	    py_e = clas->GetPy(eindex);
	    pz_e = clas->GetPz(eindex);
	    p_e = clas->GetP(eindex);
	    thetaDeg = clas->GetThetaDeg(eindex);
	    phiDeg = clas->GetPhiDeg(eindex);
	    if(thetaDeg <5 || thetaDeg >35)              // Consider only range of EC/PCAL excluding FTCal range
		continue;
	    		
	    // Require that that is a hit either from HTCC and LTCC (Need to remove Junk)  <--- Check
	    if(!clas->HasLTCCHit(eindex) || !clas->HasLTCCHit(eindex))
	    	continue;
	    
	    // We would like to identify at least two photons
	    // ------> In reality, you should consider N number of photons and consider all possible 2 photon combinations <-----
	    if(clas->GetCountWithPID(22) != 2)
		continue;
	    auto git = clas->GetItrPairForPID(22);   // The return value is a pair (of iterators) representing the range of elements

	    Int_t gNo = 0;  // g for gamma
	    for(auto ig = git.first; ig != git.second; ++ig) // There are multiple photons
	    {
		gindex = ig->second;
		if (!clas->HasPCalHit(gindex))         // Let's consider PCAL only to ensure good photon PID
		    break;
		LV[gNo].SetPxPyPzE(clas->GetPx(gindex), clas->GetPy(gindex), clas->GetPz(gindex), clas->GetP(gindex));		
		++gNo;	    
	    }

	    if(gNo != 2)
	    	continue;
	    if(LV[0].E() < 0.2 || LV[1].E() < 0.2)  // Exclude photons from Bremsstrahlung 
		continue;
	    ggAngle = LV[0].Angle(LV[1].Vect())*TMath::RadToDeg();
	    if(ggAngle < 3)                           // Lower angle value corresponds to other processes
		continue;
	    pi0W->Fill((LV[0] + LV[1]).M());
	    ggAngleVsE->Fill((LV[0] + LV[1]).E(), ggAngle);
	}
	delete clas;
    }

    TCanvas *c1 = new TCanvas();
    pi0W->Draw();
    pi0W->Write();
    
    TCanvas *c2 = new TCanvas();
    ggAngleVsE->Draw("colz");
    ggAngleVsE->Write();
}

