// Filename: Rho0Analysis.cc
// Description: rho0 invariant mass calculation. For 10.6 GeV, tested with run#3971 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TClas12.h"

using namespace std;

void LambdaAnalysis(Int_t runNumber, Int_t firstFile, Int_t lastFile, Bool_t onFarm)
{
    Double_t thetaDeg;
    Double_t thetaRad;
    Double_t phiDeg;
    Double_t px_e;
    Double_t py_e;
    Double_t pz_e;
    Double_t p_e;
    Double_t E_b = 10.594; //Beam Energy in GeV
    
    Int_t index_part;
    Int_t index_e;

    TLorentzVector LV_pr;
    TLorentzVector LV_pim;
    TLorentzVector LV_e;
    TLorentzVector LV_ep;   //Outgoing e-, e prime
    TLorentzVector LV_t;
    TLorentzVector LV_g;

    Double_t p_pr;
    Double_t p_pim;
    Double_t beta_calc;
    Double_t beta_mes;
    Double_t delta_beta;
    Double_t m_e = TClas12::GetMass("electron");
    Double_t m_pip = TClas12::GetMass("pion+");
    Double_t m_pim = TClas12::GetMass("pion-");
    Double_t m_pr = TClas12::GetMass("proton");

    Double_t W;
    Double_t Q2;
    Double_t nu;
    Double_t y;
    
    TH1D *lamdaM = new TH1D("lamdaM", "#Lambda Invariant mass; M_{#Lambda} [GeV]", 200, 1.08, 1.14);

    LV_e.SetXYZM(0, 0, E_b, TClas12::GetMass("electron"));
    LV_t.SetXYZM(0, 0, 0, TClas12::GetMass("proton"));    

    for(Int_t fileNo = firstFile; fileNo <= lastFile; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->FileExist())
	    continue;

	while(clas->HasEvent())
	{
	    if(!clas->GetSizePart() || !clas->GetSizeCher())
	    	continue;

	    // Consider only events with one e-
	    if(clas->GetCountWithPID(11) != 1)
	    	continue;

	    auto eit = clas->GetItrPairForPID(11);   // The return value is a pair representing the range of elements
	    index_e = eit.first->second;              // Index of e in particle bank (take first range and get second)
	    px_e = clas->GetPx(index_e);
	    py_e = clas->GetPy(index_e);
	    pz_e = clas->GetPz(index_e);
	    p_e = clas->GetP(index_e);
	    thetaDeg = clas->GetThetaDeg(index_e);
	    thetaRad = clas->GetThetaRad(index_e);
	    phiDeg = clas->GetPhiDeg(index_e);
	    if(thetaDeg <5 || thetaDeg >35)              // Consider only range of EC/PCAL excluding FTCal range
		continue;

	    LV_ep.SetXYZM(px_e, py_e, pz_e, m_e);
	    LV_g = LV_e - LV_ep;
	    W = (LV_g + LV_t).M();
	    nu = LV_e.E() - LV_ep.E();
	    y = nu / LV_e.E() ;
	    Q2 = 4*LV_e.E()*LV_ep.E()*pow(sin(thetaRad/2.0),2);
	    
	    //SIDIS cut
	    if(!(Q2>1) || !(W>2) || !(y<0.85))
		continue;
	    	    
	    // Require that that is a hit either from HTCC and LTCC (Need to remove Junk) <--- Check
	    if(!clas->HasLTCCHit(index_e) || !clas->HasLTCCHit(index_e))
	    	continue;
	    
	    // We would like to identify at leas one pi- and one pi+
	    // ------> In reality, you should consider N number of pr/pi- and consider all possible combinations <-----
	    if(!(clas->GetCountWithPID(2212) == 1 && clas->GetCountWithPID(-211) == 1))
		continue;
	    auto it_pr = clas->GetItrPairForPID(2212);   // The return value is a pair (of iterators) representing the range of elements
	    auto it_pim = clas->GetItrPairForPID(-211);   // The return value is a pair (of iterators) representing the range of elements

	    index_part = it_pr.first->second;	       // Proton
	    LV_pr.SetXYZM(clas->GetPx(index_part), clas->GetPy(index_part), clas->GetPz(index_part), m_pr);
	    beta_mes = clas->GetREC_Particle_beta(index_part);
	    p_pr = LV_pr.P();
	    beta_calc = p_pr/sqrt(pow(p_pr,2) + pow(m_pr,2));
	    delta_beta = beta_mes - beta_calc;
	    if(fabs(delta_beta) > 0.025)
	    	continue;
	    
	    index_part = it_pim.first->second;	       // Pion-
	    LV_pim.SetXYZM(clas->GetPx(index_part), clas->GetPy(index_part), clas->GetPz(index_part), m_pim);
	    beta_mes = clas->GetREC_Particle_beta(index_part);
	    p_pim = LV_pim.P();
	    beta_calc = p_pim/sqrt(pow(p_pim,2) + pow(m_pim,2));
	    delta_beta = beta_mes - beta_calc;
	    if(fabs(delta_beta)> 0.025)
		continue;	    
	    
	    lamdaM->Fill((LV_pr + LV_pim).M());
	}
	delete clas;
    }

    TCanvas *c1 = new TCanvas();
    lamdaM->Draw();

    TString fileName = TClas12::Config->GetVaultPath();
    if(onFarm)
    {

	fileName += "2.Lambda/";
	fileName += firstFile;
    }
    else
	fileName += TClas12::GetCounter();	    	
    fileName += ".root";
    
    TString title = "Lambda Analysis run:";
    title += runNumber;
    title += " file numbers:";
    title += firstFile;
    title += "-";
    title += lastFile;

    TFile *file = new TFile(fileName, "recreate", title);    
    lamdaM->Write();
}

