// Filename: Rho0Analysis.cc
// Description: rho0 invariant mass calculation. For 10.6 GeV, tested with run#3971 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TClas12.h"

using namespace std;

void Rho0Analysis(Int_t runNumber, Int_t firstFile, Int_t lastFile, Bool_t onFarm)
{
    Float_t thetaDeg;
    Float_t phiDeg;
    Float_t px_e;
    Float_t py_e;
    Float_t pz_e;
    Float_t p_e;
    Double_t E_b = 10.594; //Beam Energy in GeV
    
    Int_t index_pi;
    Int_t index_e;

    TLorentzVector LV_pip;
    TLorentzVector LV_pim;
    TLorentzVector LV_e;
    TLorentzVector LV_ep;   //Outgoing e-, e prime
    TLorentzVector LV_t;
    TLorentzVector LV_g;

    Double_t p_pip;
    Double_t p_pim;
    Double_t beta_calc;
    Double_t beta_mes;
    Double_t delta_beta;
    Double_t m_e = TClas12::GetMass("electron");
    Double_t m_pip = TClas12::GetMass("pion+");
    Double_t m_pim = TClas12::GetMass("pion-");

    Double_t W;
    Double_t E_h;
    Double_t nu;
    Double_t t;           // Mandelstam variable
    Double_t z;
    
    TH1D *rho0M = new TH1D("rho0M", "#rho^{0} Invariant mass; M_{#rho^{0}} [GeV]", 300, 0.0, 3.00);

    LV_e.SetXYZM(0, 0, E_b, TClas12::GetMass("electron"));
    LV_t.SetXYZM(0, 0, 0, TClas12::GetMass("proton"));       // For Incoharent process, mass of proton is taken as mass of target

    for(Int_t fileNo = firstFile; fileNo <= lastFile; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->FileExist())
	    continue;

	while(clas->HasEvent())
	{
	    if(!clas->GetSizePart() || !clas->GetSizeCher())
	    	continue;

	    // Consider only events with one e-
	    if(clas->GetCountWithPID(11) != 1)
	    	continue;

	    auto eit = clas->GetItrPairForPID(11);   // The return value is a pair representing the range of elements
	    index_e = eit.first->second;              // Index of e in particle bank (take first range and get second)
	    px_e = clas->GetPx(index_e);
	    py_e = clas->GetPy(index_e);
	    pz_e = clas->GetPz(index_e);
	    p_e = clas->GetP(index_e);
	    thetaDeg = clas->GetThetaDeg(index_e);
	    phiDeg = clas->GetPhiDeg(index_e);
	    if(thetaDeg <5 || thetaDeg >35)              // Consider only range of EC/PCAL excluding FTCal range
		continue;

	    LV_ep.SetXYZM(px_e, py_e, pz_e, m_e);
	    LV_g = LV_e - LV_ep;
	    W = (LV_g + LV_t).M();
	    E_h = (LV_g + LV_t).E();
	    nu = LV_e.E() - LV_ep.E();
	    z = E_h / nu;
	    t = (LV_e - LV_ep).M2();

	    if(!(-t > 0.1 && -t < 0.4)) // 0.1: To exclude coherent production off the nucleus and 0.4: To be in diffractive region. See rho0 CT analysis note page:33
		continue;

	    if(W <= 2)                 // To Avoid resonance region
		continue;

	    if(z <= 0.9)              // To select the elastic process
		continue;
	    
	    
	    // Require that that is a hit either from HTCC and LTCC (Need to remove Junk) <--- Check
	    if(!clas->HasLTCCHit(index_e) || !clas->HasLTCCHit(index_e))
	    	continue;
	    
	    // We would like to identify at leas one pi- and one pi+
	    // ------> In reality, you should consider N number of pi+/pi- and consider all possible 2 photon combinations <-----
	    if(!(clas->GetCountWithPID(211) == 1 && clas->GetCountWithPID(-211) == 1))
		continue;
	    auto it_pip = clas->GetItrPairForPID(211);   // The return value is a pair (of iterators) representing the range of elements
	    auto it_pim = clas->GetItrPairForPID(-211);   // The return value is a pair (of iterators) representing the range of elements

	    index_pi = it_pip.first->second;	       // Pion+
	    LV_pip.SetXYZM(clas->GetPx(index_pi), clas->GetPy(index_pi), clas->GetPz(index_pi), m_pip);
	    beta_mes = clas->GetREC_Particle_beta(index_pi);
	    p_pip = LV_pip.P();
	    beta_calc = p_pip/sqrt(pow(p_pip,2) + pow(m_pip,2));
	    delta_beta = beta_mes - beta_calc;
	    if(fabs(delta_beta)>2*0.025)
		continue;
	    
	    index_pi = it_pim.first->second;	       // Pion-
	    LV_pim.SetXYZM(clas->GetPx(index_pi), clas->GetPy(index_pi), clas->GetPz(index_pi), m_pim);
	    beta_mes = clas->GetREC_Particle_beta(index_pi);
	    p_pim = LV_pim.P();
	    beta_calc = p_pim/sqrt(pow(p_pim,2) + pow(m_pim,2));
	    delta_beta = beta_mes - beta_calc;
	    if(fabs(delta_beta)>2*0.025)
		continue;	    
	    
	    rho0M->Fill((LV_pip + LV_pim).M());
	}
	delete clas;
    }

    TCanvas *c1 = new TCanvas();
    rho0M->Draw();

    TString fileName = TClas12::Config->GetVaultPath();
    if(onFarm)
    {

	fileName += "1.Rho0/";
	fileName += firstFile;
    }
    else
	fileName += TClas12::GetCounter();	    	
    fileName += ".root";
    
    TString title = "Rho0 Analysis run:";
    title += runNumber;
    title += " file numbers:";
    title += firstFile;
    title += "-";
    title += lastFile;

    TFile *file = new TFile(fileName, "recreate", title);    
    rho0M->Write();
}

