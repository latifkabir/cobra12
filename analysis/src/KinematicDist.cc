// Filename: KinematicDist.cc
// Description: Kinematics variables distribution for CLAS12 data
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TClas12.h"
#include "TFile.h"
#include "TLorentzVector.h"


using namespace std;

void KinematicDist(Int_t runNumber, Int_t firstFile, Int_t lastFile)
{
    //gStyle->SetOptStat(0);

    TString fileName = TClas12::Config->GetResultsPath();
    fileName += "KinematicsDist_";
    fileName += runNumber;
    fileName += "_";
    fileName += firstFile;
    fileName += "-";
    fileName += lastFile;
    fileName += ".root";
    TFile *file = new TFile(fileName , "recreate");

    // Particle bank
    Int_t pid = -1;
    
    Float_t thetaDeg;
    Float_t phiDeg;
    Float_t px_e;
    Float_t py_e;
    Float_t pz_e;
    Float_t p_e;

    
    Int_t gindex;
    Int_t eindex;

    Int_t ltccKey;
    Int_t htccKey;
    Int_t ecoKey;
    Int_t eciKey;
    Int_t pcalKey;

    TLorentzVector LVe;    // Lorentz Vector for e-
    TLorentzVector LVg;    // Lorentz Vector for virtual photon
    TLorentzVector LVb;    // Lorentz Vector for beam
    TLorentzVector LVt;    // Lorentz Vector for target
    TLorentzVector LV;     // Lorentz Vector for system consisting all out going particles except outgoing e- (from beam)

    TVector3 V3e;    //  3-Vector for e-
    TVector3 V3g;    //  3-Vector for virtual photon
    TVector3 V3b;    //  3-Vector for beam
    TVector3 V3t;    //  3-Vector for target
    Double_t E_b = 6.4; // Beam energy in GeV
    
    TH1D *W = new TH1D("W", " W distribution; W [GeV]", 200, 0, 4.0);
    
    for(Int_t fileNo = firstFile; fileNo <= lastFile; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
    
	while(clas->HasEvent())
	{
	    if(!clas->GetSizePart() || !clas->GetSizeCher())
	    	continue;

	    // Consider only events with one e-
	    if(clas->fPartMap->count(11) != 1)
	    	continue;

	    auto eit = clas->fPartMap->equal_range(11);   // The return value is a pair representing the range of elements
	    eindex = eit.first->second;                         // Index of e in particle bank (take first range and get second)
	    px_e = clas->fREC_Particle_px->getValue(eindex);
	    py_e = clas->fREC_Particle_py->getValue(eindex);
	    pz_e = clas->fREC_Particle_pz->getValue(eindex);
	    p_e = sqrt(px_e*px_e + py_e*py_e + pz_e*pz_e);
	    thetaDeg = acos(pz_e/p_e)*TMath::RadToDeg();
	    phiDeg = atan2(py_e, px_e)*TMath::RadToDeg();
	    if(phiDeg <-150 || phiDeg >150)              // Consider only sector 1 , since elastic peak shifts from sec to sec
	    	continue;
	    
	    ltccKey = 100*eindex + 10*15;
	    htccKey = 100*eindex + 10*16;
		
	    // Require that that is a hit either from HTCC or LTCC (Need to remove Junk)
	    if(!(clas->fCherMap->find(ltccKey) != clas->fCherMap->end() || clas->fCherMap->find(htccKey) != clas->fCherMap->end()))
	    	continue;

	    V3e.SetXYZ(px_e, py_e, pz_e);    // Outgoing e-
	    V3b.SetXYZ(0, 0, E_b);            // E_b^2 = p^2 + E_e^2  ----> Neglecting E_e^2 = (0.00051 GeV)^2 ==> E_b = pz (since beam is along z-axis)
	    V3t.SetXYZ(0, 0, 0);             // Target is at rest

	    LVe.SetPxPyPzE(px_e, py_e, pz_e, sqrt(0.000511*0.000511 + V3e.Mag2()));
	    LVb.SetPxPyPzE(0, 0, E_b, E_b);
	    LVt.SetPxPyPzE(0, 0, 0, 0.93827);          // E_p = sqrt(0*0 + m_p^2) = m_p


	    /*
             The reaction:                                 e'   
	                                                  /
	                                                 /
                                                        / 
                                                       /
                     (beam)e ------------>------------~ 1
			                               ~
						        ~
							 ~        ________________________
						  target/proton 2 ________________________
							         _________________________    (all possible reactions)        <---------------- We want this total invariant mass. 
								 _________________________
              */
	    //From Energy-momentum conservation at 1:
	    LVg = LVb - LVe;
	    //From Energy-momentum conservation at 2:
	    LV = LVg + LVt;
	    W->Fill(LV.M());
	}
	delete clas;
    }

    TCanvas *c1 = new TCanvas();
    W->Draw();
    W->Write();   
}

