// Filename: AnalysisTree.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

void AnalysisTree(Int_t runNumber, Int_t firstFile, Int_t lastFile)
{
    TString fileName = "/home/latif/GIT/CLAS12CPP/plots/buffer/AnalysisTree";
    fileName += runNumber;
    fileName += "_";
    fileName += firstFile;
    fileName += "-";
    fileName += lastFile;
    fileName += ".root";
    TFile *file = new TFile(fileName, "recreate");

    TTree *tree = new TTree("T","T");
    
    Int_t ecounter = 0;

    Float_t x = 0;
    Float_t y = 0;

    // Particle bank
    Int_t particle_pid = -1;
    Float_t particle_vz;
    Float_t particle_p[3];

    //CC Bank
    Int_t status;
    Float_t thetaDeg;
    Float_t phiDeg;
    Int_t sec;

    tree->Branch("event", &ecounter, "event/I");
    tree->Branch("pid", &particle_pid, "pid/I");
    tree->Branch("vz", &particle_vz, "vz/F");
    tree->Branch("p", particle_p, "p[3]/F");
    tree->Branch("theta", &thetaDeg, "theta/F");
    tree->Branch("phi", &phiDeg, "theta/F");
    
    
    for(Int_t fileNo = firstFile; fileNo <= lastFile; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
	
	while(clas->fHipoReader->next())
	{
	    Int_t rec_l = clas->fREC_Particle_pid->length(); 
	    Int_t cc_l = clas->fREC_Cherenkov_pindex->length();
	    if(!rec_l || !cc_l)
	    {
		++ecounter;
		continue;
	    }

	    for(Int_t irec = 0; irec < rec_l; irec++)
	    {
		particle_pid = clas->fREC_Particle_pid->getValue(irec);	
		particle_vz = clas->fREC_Particle_vz->getValue(irec);
		particle_p[0] = clas->fREC_Particle_px->getValue(irec);
		particle_p[1] = clas->fREC_Particle_py->getValue(irec);
		particle_p[2] = clas->fREC_Particle_pz->getValue(irec);

		if(particle_pid != 11)
		    continue;

		for(Int_t icc = 0; icc < cc_l; icc++)
		{
		    Int_t cc_pindex = clas->fREC_Cherenkov_pindex->getValue(icc);
		    if(irec == cc_pindex)
		    {
			status = clas->fREC_Cherenkov_status->getValue(icc); // <--------- To be used in future when it's filled appropriately
			sec = clas->fREC_Cherenkov_sector->getValue(icc);
			thetaDeg = (Float_t)clas->fREC_Cherenkov_theta->getValue(icc)*TMath::RadToDeg();	
			phiDeg = (Float_t)clas->fREC_Cherenkov_phi->getValue(icc)*TMath::RadToDeg();
			tree->Fill();	
		    }
		}
	    }			    
	    ecounter++;
	}
	delete clas;
    }
    if(!ecounter)
	return;

    tree->Write();
    file->Close();
}
