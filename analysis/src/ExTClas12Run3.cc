// Filename: ExTClas12Run3.cc
// Description: Beta vs momentum plot
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"

using namespace std;

void ExTClas12Run3(Int_t runNumber, Int_t nFiles)
{
    gStyle->SetOptStat(0);

    // Particle bank
    Int_t pid = -1;
    Float_t vz;
    Float_t px;
    Float_t py;
    Float_t pz;
    Float_t p;
    Float_t beta;
    Float_t charge;
    Float_t thetaDeg;
    Float_t phiDeg;
    
    Int_t partIndex;
    Int_t eindex;
    Int_t ltccKey;
    Int_t htccKey;

    TH2D *betaVsP = new TH2D("betaVsP", "#beta vs p; p [GeV]; #beta", 100, 0, 7, 100, 0.5, 1.2);
    
    for(Int_t fileNo = 0; fileNo < nFiles; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
    
	while(clas->HasEvent())
	{
	    if(!clas->GetSizePart() || !clas->GetSizeCher())
	    	continue;

	    // Consider only events with one e-
	    if(clas->fPartMap->count(11) != 1)
	    	continue;

	    auto eit = clas->fPartMap->equal_range(11);   // The return value is a pair representing the range of elements
	    eindex = eit.first->second;                         // Index of e in particle bank (take first range and get second)
	    ltccKey = 100*eindex + 10*15;
	    htccKey = 100*eindex + 10*16;
		
	    // Require that there is a hit either from HTCC or LTCC (Need to remove Junk)
	    if(!(clas->fCherMap->find(ltccKey) != clas->fCherMap->end() || clas->fCherMap->find(htccKey) != clas->fCherMap->end()))
	    	continue;

	    for(auto itr = clas->fPartMap->begin(); itr != clas->fPartMap->end(); ++itr)
	    {
		partIndex = itr->second;
		pid = itr->first;
		charge = clas->fREC_Particle_charge->getValue(partIndex);
		
		if(pid  == 11 || pid == -11 || pid == 22)   // Skip e-/e+ or photon, consider hadrons only
		    continue;

		if(charge <1)   // Would like to consider positively charged hadrons 
		    continue;
		
		px = clas->fREC_Particle_px->getValue(partIndex);
		py = clas->fREC_Particle_py->getValue(partIndex);
		pz = clas->fREC_Particle_pz->getValue(partIndex);
		p = sqrt(px*px + py*py + pz*pz);
		thetaDeg = acos(pz/p)*TMath::RadToDeg();
		phiDeg = atan2(py,px)*TMath::RadToDeg();
		beta = clas->fREC_Particle_beta->getValue(partIndex);

		betaVsP->Fill(p, beta);
	    }
	}
	delete clas;
    }

    betaVsP->Draw("colz");    
}
