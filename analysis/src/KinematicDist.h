// Filename: KinematicDist.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Tue May 15 00:18:07 2018 (-0400)
// URL: jlab.org/~latif

void KinematicDist(Int_t runNumber, Int_t firstFile, Int_t lastFile);
