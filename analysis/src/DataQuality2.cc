// Filename: DataQuality2.cc
// Description: Data quality check using e-
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TClas12.h"

using namespace std;

void DataQuality2(Int_t runNumber, Int_t nFiles)
{
    gStyle->SetOptStat(0);
    TString fileName = TClas12::Config->GetResultsPath();
    fileName += "Plots_";
    fileName += runNumber;
    fileName += "_";
    
    TH1F *h1VzDist = new TH1F("h1VzDist", "Vertex z_{e} distribution; v_{z} [cm]", 200, -20, 40);
    TH1F *h1NpheLTCCDist = new TH1F("h1NpheLTCCDist", "nphe distribution (LTCC); nphe", 200, 0, 100);
    TH1F *h1NpheHTCCDist = new TH1F("h1NpheHTCCDist", "nphe distribution ((HTCC); nphe", 200, 0, 100);
    TH2F *h2VZvsTheta = new TH2F("vz_vs_theta"," v_{z} vs #theta; #theta [deg]; v_{z}", 200, 0, 40, 200, -20, 40);
    TH2F *h2VZvsPhi = new TH2F("vz_vs_phi"," v_{z} vs #phi_{e}; #phi_{e} [deg]; v_{z} [cm]", 200, -200, 200, 200, -20, 40);
    TH2F *h2ThetavsPhi = new TH2F("Theta_vs_phi"," #theta_{e} vs #phi_{e}; #phi_{e} [deg]; #theta_{e} [deg]", 200, -200, 200, 200, 0, 40);
    TH2F *h2PhivsMom = new TH2F("phi_vs_mom"," #phi_{e} vs p; p [GeV];#phi [deg]", 200, 0, 7, 200, -200, 200);

    Int_t index;
    for(Int_t fileNo = 0; fileNo < nFiles; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
    
	while(clas->HasEvent())
	{
	    if(!clas->HasPartBank() || !clas->HasCherBank())
		continue;
	    
	    // Let's look at electron only, Consider only events with one e-
	    if(clas->GetCountWithPID(11) != 1)
		continue;
	    index = clas->GetItrPairForPID(11).first->second;  // Index of e in particle bank (take first range and get second)
	           		
	    // Require that that is a hit either from HTCC or LTCC (Need to remove Junk)
	    if( !clas->HasHTCCHit(index) && !clas->HasLTCCHit(index))
		continue;

	    h1VzDist->Fill(clas->GetVz(index));
	    h2VZvsPhi->Fill(clas->GetPhiDeg(index), clas->GetVz(index));
	    h2VZvsTheta->Fill(clas->GetThetaDeg(index), clas->GetVz(index));
	    h2ThetavsPhi->Fill(clas->GetPhiDeg(index), clas->GetThetaDeg(index));
	    h2PhivsMom->Fill(clas->GetP(index), clas->GetPhiDeg(index));

	    if(clas->HasHTCCHit(index))
		h1NpheHTCCDist->Fill(clas->GetHTCCnphe(index));
	    if(clas->HasLTCCHit(index))
		h1NpheLTCCDist->Fill(clas->GetLTCCnphe(index));
	}
	delete clas;
    }
    
    TCanvas *c1 = new TCanvas(); 
    h1VzDist->Draw();
    c1->SaveAs(fileName + "DataQuality2.pdf(","pdf");
    
    TCanvas *c2 = new TCanvas(); 
    h2ThetavsPhi->Draw("colz");
    c2->SaveAs(fileName + "DataQuality2.pdf","pdf");
    
    TCanvas *c3 = new TCanvas(); 
    h2VZvsPhi->Draw("colz");        
    c3->SaveAs(fileName + "DataQuality2.pdf","pdf");
    
    TCanvas *c4 = new TCanvas(); 
    h2VZvsTheta->Draw("colz");
    c4->SaveAs(fileName + "DataQuality2.pdf","pdf");
    
    TCanvas *c5 = new TCanvas(); 
    h2PhivsMom->Draw("colz");
    c5->SaveAs(fileName + "DataQuality2.pdf","pdf");
    
    TCanvas *c6 = new TCanvas(); 
    h1NpheLTCCDist->Draw();
    c6->SaveAs(fileName + "DataQuality2.pdf","pdf");

    TCanvas *c7 = new TCanvas(); 
    h1NpheHTCCDist->Draw();
    c7->SaveAs(fileName + "DataQuality2.pdf)","pdf");
}
