// Filename: AnalysisTree.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Mon May  7 12:33:06 2018 (-0400)
// URL: jlab.org/~latif

void AnalysisTree(Int_t runNumber, Int_t firstFile, Int_t lastFile);
