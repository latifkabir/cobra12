// Filename: BetaVsMomentum.h
// Description: Beta vs momentumplot
// Author: Latif Kabir < latif@jlab.org >
// Created: Tue May 15 01:21:39 2018 (-0400)
// URL: jlab.org/~latif

void BetaVsMomentum(Int_t runNumber, Int_t nFiles);
