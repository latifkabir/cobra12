// Filename: DCDoca.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Fri Jul 20 22:47:52 2018 (-0400)
// URL: jlab.org/~latif

void DCDoca(TString docaType);  // Doca type can be either "trk" or "calc"
