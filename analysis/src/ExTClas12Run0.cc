// Filename: ExTClas12Run0.cc
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"

using namespace std;

void ExTClas12Run0(Int_t runNumber, Int_t nFiles)
{
    gStyle->SetOptStat(0);

    TH1F *h1VzDist = new TH1F("h1VzDist", "Vertex z_{e} distribution; v_{z} [cm]", 200, -20, 40);
    TH1F *h1NpheDist = new TH1F("h1NphiDist", "nphi distribution; nphi", 200, 0, 100);
    TH2F *h2VZvsTheta = new TH2F("vz_vs_theta"," v_{z} vs #theta; #theta [deg]; v_{z}", 200, 0, 40, 200, -20, 40);
    TH2F *h2VZvsPhi = new TH2F("vz_vs_phi"," v_{z} vs #phi_{e}; #phi_{e} [deg]; v_{z} [cm]", 200, -200, 200, 200, -20, 40);
    TH2F *h2ThetavsPhi = new TH2F("Theta_vs_phi"," #theta_{e} vs #phi_{e}; #phi_{e} [deg]; #theta_{e} [deg]", 200, -200, 200, 200, 0, 40);
    TH2F *h2PhivsMom = new TH2F("phi_vs_mom"," #phi_{e} vs p; p [GeV];#phi [deg]", 200, 0, 7, 200, -200, 200);
    
    Int_t ecounter = 0;

    Float_t x = 0;
    Float_t y = 0;

    // Particle bank
    Int_t pid = -1;
    Float_t vz;
    Float_t px;
    Float_t py;
    Float_t pz;
    Float_t p;
    Float_t thetaDeg;
    Float_t phiDeg;
     
    //CC Bank
    Int_t status;
    Int_t sec;
    Float_t nphe;
    
    for(Int_t fileNo = 0; fileNo < nFiles; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
    
	while(clas->fHipoReader->next())
	{
	    Int_t rec_l = clas->fREC_Particle_pid->length(); 
	    Int_t cc_l = clas->fREC_Cherenkov_pindex->length();
	    if(!rec_l || !cc_l)
	    {
		++ecounter;
		continue;
	    }

	    for(Int_t irec = 0; irec < rec_l; irec++)
	    {
		pid = clas->fREC_Particle_pid->getValue(irec);

		//Default Event builder PID cut, Look at electrons
		if(pid != 11)
		    continue;
		
		vz = clas->fREC_Particle_vz->getValue(irec);
		px = clas->fREC_Particle_px->getValue(irec);
		py = clas->fREC_Particle_py->getValue(irec);
		pz = clas->fREC_Particle_pz->getValue(irec);
		p = sqrt(px*px + py*py + pz*pz);
		thetaDeg = acos(pz/p)*TMath::RadToDeg();
		phiDeg = atan2(py,px)*TMath::RadToDeg();
		
		for(Int_t icc = 0; icc < cc_l; icc++)
		{
		    Int_t cc_pindex = clas->fREC_Cherenkov_pindex->getValue(icc);
		    if(irec == cc_pindex)
		    {
			status = clas->fREC_Cherenkov_status->getValue(icc); // <--------- To be used in future when it's filled appropriately
			sec = clas->fREC_Cherenkov_sector->getValue(icc);
			nphe = clas->fREC_Cherenkov_nphe->getValue(icc);

                        //CC cut to separate electron and pion-
			// if(nphe<1)
			//     continue;
			
			h1VzDist->Fill(vz);
			h1NpheDist->Fill(nphe);
			h2VZvsPhi->Fill(phiDeg, vz);
			h2VZvsTheta->Fill(thetaDeg, vz);
			h2ThetavsPhi->Fill(phiDeg, thetaDeg);
			h2PhivsMom->Fill(p, phiDeg);
		    }
		}
	    }			    
	    ecounter++;
	}
	delete clas;
    }
    if(!ecounter)
	return;
    TCanvas *c1 = new TCanvas(); 
    h1VzDist->Draw();
    c1->SaveAs("/home/latif/GIT/CLAS12CPP/plots/buffer/ExTClas12Run0.pdf(","pdf");
    
    TCanvas *c2 = new TCanvas(); 
    h2ThetavsPhi->Draw("colz");
    c2->SaveAs("/home/latif/GIT/CLAS12CPP/plots/buffer/ExTClas12Run0.pdf","pdf");
    
    TCanvas *c3 = new TCanvas(); 
    h2VZvsPhi->Draw("colz");        
    c3->SaveAs("/home/latif/GIT/CLAS12CPP/plots/buffer/ExTClas12Run0.pdf","pdf");
    
    TCanvas *c4 = new TCanvas(); 
    h2VZvsTheta->Draw("colz");
    c4->SaveAs("/home/latif/GIT/CLAS12CPP/plots/buffer/ExTClas12Run0.pdf","pdf");
    
    TCanvas *c5 = new TCanvas(); 
    h2PhivsMom->Draw("colz");
    c5->SaveAs("/home/latif/GIT/CLAS12CPP/plots/buffer/ExTClas12Run0.pdf","pdf");

    TCanvas *c6 = new TCanvas(); 
    h1NpheDist->Draw();

    c6->SaveAs("/home/latif/GIT/CLAS12CPP/plots/buffer/ExTClas12Run0.pdf)","pdf");
}
