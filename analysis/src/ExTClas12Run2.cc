// Filename: ExTClas12Run2.cc
// Description: pi0 invariant mass calculation. pi0 peak is visible with run#3985, also possible 3222, 4203 etc. Not visible for runs prior to 3222. for 6.4 GeV use 3852
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TClas12.h"
#include "TFile.h"
#include "TLorentzVector.h"

using namespace std;

void ExTClas12Run2(Int_t runNumber, Int_t firstFile, Int_t lastFile)
{
    //gStyle->SetOptStat(0);

    TString fileName = TClas12::Config->GetResultsPath();
    fileName += "Pi0Analysis_";
    fileName += runNumber;
    fileName += "_";
    fileName += firstFile;
    fileName += "-";
    fileName += lastFile;
    fileName += ".root";
    TFile *file = new TFile(fileName , "recreate");

    // Particle bank
    Int_t pid = -1;
    Float_t vz[2];
    Float_t px[2];
    Float_t py[2];
    Float_t pz[2];
    Float_t p[2];
    Float_t eng[2];
    
    Float_t thetaDeg;
    Float_t phiDeg;
    Float_t px_e;
    Float_t py_e;
    Float_t pz_e;
    Float_t p_e;

    
    Int_t gindex;
    Int_t eindex;

    Int_t ltccKey;
    Int_t htccKey;
    Int_t ecoKey;
    Int_t eciKey;
    Int_t pcalKey;

    Float_t p2_g1, p2_g2, Et, Pxt, Pyt, Pzt, W2, W;
    TLorentzVector LV[2];
    Double_t ggAngle;
    
    TH1D *pi0W = new TH1D("pi0W", "#pi^{0} Invariant mass; M_{#pi^{0}} [GeV]", 200, -0.02, 0.70);
    TH2D *ggAngleVsE = new TH2D("ggAngleVsE", "#gamma#gamma openning angle vs E_{#gamma#gamma}; E_{#gamma#gamma} [GeV]; #theta_{#gamma#gamma} [deg]", 100, 0, 6, 100, 0, 30); 
    
    for(Int_t fileNo = firstFile; fileNo <= lastFile; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
    
	while(clas->HasEvent())
	{
	    if(!clas->GetSizePart() || !clas->GetSizeCher())
	    	continue;

	    // Consider only events with one e-
	    if(clas->fPartMap->count(11) != 1)
	    	continue;

	    auto eit = clas->fPartMap->equal_range(11);   // The return value is a pair representing the range of elements
	    eindex = eit.first->second;                         // Index of e in particle bank (take first range and get second)
	    px_e = clas->fREC_Particle_px->getValue(eindex);
	    py_e = clas->fREC_Particle_py->getValue(eindex);
	    pz_e = clas->fREC_Particle_pz->getValue(eindex);
	    p_e = sqrt(px_e*px_e + py_e*py_e + pz_e*pz_e);
	    thetaDeg = acos(pz_e/p_e)*TMath::RadToDeg();
	    phiDeg = atan2(py_e, px_e)*TMath::RadToDeg();
	    if(thetaDeg <5 || thetaDeg >35)              // Consider only range of EC/PCAL excluding FTCal range
		continue;
	    
	    ltccKey = 100*eindex + 10*15;
	    htccKey = 100*eindex + 10*16;
		
	    // Require that that is a hit either from HTCC or LTCC (Need to remove Junk)
	    if(!(clas->fCherMap->find(ltccKey) != clas->fCherMap->end() || clas->fCherMap->find(htccKey) != clas->fCherMap->end()))
	    	continue;
	    
	    // We would like to identify at least two photons
	    if(clas->fPartMap->count(22) != 2)
		continue;
	    auto git = clas->fPartMap->equal_range(22);   // The return value is a pair representing the range of elements

	    Int_t gNo = 0;  // g for gamma
	    Int_t ecIndex_i = 0;
	    Int_t ecIndex_o = 0;
	    Int_t ecIndex_p = 0;
	    Int_t pcalIndex= 0;
	    for(auto ig = git.first; ig != git.second; ++ig) // There are multiple photons
	    {
		gindex = ig->second;
		ecoKey = 100*gindex + 10*7 + 7;
		eciKey = 100*gindex + 10*7 + 4;
		pcalKey = 100*gindex + 10*7 + 1;
		// if( !(clas->fCaloMap->find(pcalKey) != clas->fCaloMap->end() || clas->fCaloMap->find(ecoKey) != clas->fCaloMap->end() || clas->fCaloMap->find(eciKey) != clas->fCaloMap->end()) )
		//     break;

		// if(clas->fCaloMap->find(eciKey) != clas->fCaloMap->end() && clas->fCaloMap->find(ecoKey) != clas->fCaloMap->end())
		// {
		//     ecIndex_i = clas->fCaloMap->at(eciKey);
		//     ecIndex_o = clas->fCaloMap->at(ecoKey); 
		//     eng[gNo] = clas->fREC_Calorimeter_energy->getValue(ecIndex_i) +  clas->fREC_Calorimeter_energy->getValue(ecIndex_o);
		//     eng[gNo] = eng[gNo] / 0.273;
		// }
		//else
		if (clas->fCaloMap->find(pcalKey) != clas->fCaloMap->end())        // Let's consider PCAL only
		{
		    // pcalIndex = clas->fCaloMap->at(pcalKey); 
		    // eng[gNo] = clas->fREC_Calorimeter_energy->getValue(pcalIndex);
		    // eng[gNo] = eng[gNo] / 0.273;
		}
		else
		    break;
		vz[gNo] = clas->fREC_Particle_vz->getValue(gindex);
		px[gNo] = clas->fREC_Particle_px->getValue(gindex);
		py[gNo] = clas->fREC_Particle_py->getValue(gindex);
		pz[gNo] = clas->fREC_Particle_pz->getValue(gindex);
		p[gNo] = sqrt(px[gNo]*px[gNo] + py[gNo]*py[gNo] + pz[gNo]*pz[gNo]);
		LV[gNo].SetPxPyPzE(px[gNo], py[gNo], pz[gNo], p[gNo]);
		
		++gNo;	    
	    }

	    if(gNo != 2)
	    	continue;
	    //----Calculate Invariant Mass -----
	    // p2_g1 = (px[0]*px[0] + py[0]*py[0] + pz[0]*pz[0]);   
	    // p2_g2 = (px[1]*px[1] + py[1]*py[1] + pz[1]*pz[1]);   
	    // Et = sqrt(0.0*0.0 + p2_g1) + sqrt(0.0*0.0 + p2_g2);
	    // Pxt = (px[0] + px[1]);
	    // Pyt = (py[0] + py[1]);
	    // Pzt = (pz[0] + pz[1]);
	    //W2 = Et*Et - (Pxt*Pxt + Pyt*Pyt + Pzt*Pzt);
	    //W = sqrt(W2);
	    //W = sqrt(2*eng[0]*eng[1]*(1 - (px[0]*px[1] + py[0]*py[1] + pz[0]*pz[1])/(eng[0]*eng[1])));
	    //pi0W->Fill(W);
	    if(LV[0].E() < 0.2 || LV[1].E() < 0.2)  // Exclude photons from Bremsstrahlung 
		continue;
	    ggAngle = LV[0].Angle(LV[1].Vect())*TMath::RadToDeg();
	    if(ggAngle < 3)                           // Lower angle vale corresponds to other processes
		continue;
	    pi0W->Fill((LV[0] + LV[1]).M());
	    //ggAngleVsE->Fill((eng[0]+eng[1]), acos((px[0]*px[1] + py[0]*py[1] + pz[0]*pz[1])/(eng[0]*eng[1]))*TMath::RadToDeg());
	    ggAngleVsE->Fill((LV[0] + LV[1]).E(), ggAngle);
	}
	delete clas;
    }

    TCanvas *c1 = new TCanvas();
    pi0W->Draw();
    pi0W->Write();
    
    TCanvas *c2 = new TCanvas();
    ggAngleVsE->Draw("colz");
    ggAngleVsE->Write();
}

