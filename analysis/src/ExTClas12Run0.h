// Filename: ExTClas12Run0.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Tue May  1 01:22:03 2018 (-0400)
// URL: jlab.org/~latif

void ExTClas12Run0(Int_t runNumber, Int_t nFiles);
