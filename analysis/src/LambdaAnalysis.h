// Filename: LambdaAnalysis.h
// Description: 
// Author: Latif Kabir < latif@jlab.org >
// Created: Tue May 15 01:28:46 2018 (-0400)
// URL: jlab.org/~latif

void LambdaAnalysis(Int_t runNumber, Int_t firstFile, Int_t lastFile, Bool_t onFarm = kFALSE);
