// Filename: ExTClas12Run1.cc
// Description: Data quality check using e-
// Author: Latif Kabir < latif@jlab.org >
// Created: Sat Apr 28 15:38:04 2018 (-0400)
// URL: jlab.org/~latif

#include <iostream>
#include "TClas12Run.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"

using namespace std;

void ExTClas12Run1(Int_t runNumber, Int_t nFiles)
{
    gStyle->SetOptStat(0);

    TH1F *h1VzDist = new TH1F("h1VzDist", "Vertex z_{e} distribution; v_{z} [cm]", 200, -20, 40);
    TH1F *h1NpheDist = new TH1F("h1NpheDist", "nphe distribution; nphe", 200, 0, 100);
    TH2F *h2VZvsTheta = new TH2F("vz_vs_theta"," v_{z} vs #theta; #theta [deg]; v_{z}", 200, 0, 40, 200, -20, 40);
    TH2F *h2VZvsPhi = new TH2F("vz_vs_phi"," v_{z} vs #phi_{e}; #phi_{e} [deg]; v_{z} [cm]", 200, -200, 200, 200, -20, 40);
    TH2F *h2ThetavsPhi = new TH2F("Theta_vs_phi"," #theta_{e} vs #phi_{e}; #phi_{e} [deg]; #theta_{e} [deg]", 200, -200, 200, 200, 0, 40);
    TH2F *h2PhivsMom = new TH2F("phi_vs_mom"," #phi_{e} vs p; p [GeV];#phi [deg]", 200, 0, 7, 200, -200, 200);
    
    Int_t ecounter = 0;

    Float_t x = 0;
    Float_t y = 0;

    // Particle bank
    Int_t pid = -1;
    Float_t vz;
    Float_t px;
    Float_t py;
    Float_t pz;
    Float_t p;
    Float_t thetaDeg;
    Float_t phiDeg;
     
    //CC Bank
    Int_t status;
    Int_t sec;
    Float_t nphe;
    Int_t det_type;

    Int_t htccKey;
    Int_t ltccKey;
    Int_t eindex;
    
    for(Int_t fileNo = 0; fileNo < nFiles; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
    
	while(clas->HasEvent())
	{
	    ecounter++;
	    if(!clas->GetSizePart() || !clas->GetSizeCher()) // <--- For efficiency only
	    	continue;

	    // Let's look at electron only
	    // Consider only events with one e-
	    if(clas->fPartMap->count(11) != 1)
		continue;

	    auto it = clas->fPartMap->equal_range(11);         // The return value is a pair representing the range of elements
	    eindex = it.first->second;                         // Index of e in particle bank (take first range and get second)
	           
	    vz = clas->fREC_Particle_vz->getValue(eindex);
	    px = clas->fREC_Particle_px->getValue(eindex);
	    py = clas->fREC_Particle_py->getValue(eindex);
	    pz = clas->fREC_Particle_pz->getValue(eindex);
	    p = sqrt(px*px + py*py + pz*pz);
	    thetaDeg = acos(pz/p)*TMath::RadToDeg();
	    phiDeg = atan2(py,px)*TMath::RadToDeg();

	    ltccKey = 100*eindex + 10*15;
	    htccKey = 100*eindex + 10*16;
		
	    // Require that that is a hit either from HTCC or LTCC (Need to remove Junk)
	    if(!(clas->fCherMap->find(ltccKey) != clas->fCherMap->end() || clas->fCherMap->find(htccKey) != clas->fCherMap->end()))
		continue;

	    // Let's fill nphe from LTCC only
	    if(clas->fCherMap->find(htccKey) != clas->fCherMap->end())
	    {
		Int_t ihtcc = clas->fCherMap->at(htccKey);
		nphe = clas->fREC_Cherenkov_nphe->getValue(ihtcc);		   
		h1NpheDist->Fill(nphe);
	    }

	    h1VzDist->Fill(vz);
	    h2VZvsPhi->Fill(phiDeg, vz);
	    h2VZvsTheta->Fill(thetaDeg, vz);
	    h2ThetavsPhi->Fill(phiDeg, thetaDeg);
	    h2PhivsMom->Fill(p, phiDeg);		
	}
	cout<<"Event count1: "<<ecounter<<" Event count2: "<<(clas->fEventNo + 1)<<endl;
	delete clas;
    }
    if(!ecounter)
	return;
    
    TCanvas *c1 = new TCanvas(); 
    h1VzDist->Draw();
    
    TCanvas *c2 = new TCanvas(); 
    h2ThetavsPhi->Draw("colz");
    
    TCanvas *c3 = new TCanvas(); 
    h2VZvsPhi->Draw("colz");        
    
    TCanvas *c4 = new TCanvas(); 
    h2VZvsTheta->Draw("colz");
    
    TCanvas *c5 = new TCanvas(); 
    h2PhivsMom->Draw("colz");

    TCanvas *c6 = new TCanvas(); 
    h1NpheDist->Draw();
}
