COBRA12: CLAS Objective ROOT Analysis for 12 GeV
==================================================

COBRA12 is (or aimed to be) a fancy ROOT based analysis tools for CLAS12 without requiring to convert HIPO data files to root files in the first place. It is tailored to take full advantage of JLab batch farm resources. This is still under active development and many features to be added. 

Installation and setup
-----------------------

0. System requirements: `ROOT 6` and `gcc >= 4.9`.

1. To download:

    ```
    git clone --recurse-submodules https://github.com/JeffersonLab/cobra12.git
    ```

2. You need to edit the configuration file `config/config.cfg` to change various paths to match your system. The file is read on `root` start-up. You will need to re-start root every time you change the configuration.

3. Source the environment:

    ```
    source setup.[c]sh
    ```

4. To compile everything, from the top level directory of `cobra12` do:


    ```
    make
    ```
   
    Note: The `Makefiles` are written/tested on Linux OS. On Mac OS, some minor tweak might be required. This will be supported in the future.

5. Start `root` from the top level directory of `cobra12`. If initialization is successful, type `TClas12::Config->Print()`. This will print the configuration. Make sure the printed configuration is expected. Type `help()` for a list of available options. Try examples compiled from `analysis/src` directory.


Design
----------------

![](resources/images/Cobra12_Design.jpg)

- For every detector, there is a corresponding ROOT class.
- For every variable in the JSON file, there is Getter function in the corresponding detector class.
- Additional getter function to retrieve desired information.
- The main class is the `TClas12Run` that allows access to all data on an event-by-event basis. The user can just use DST bank or any detector bank.

How to add new classes or functions
----------------------------------

- New classes (building blocks) will go under `libCobra12/src` while analysis script utilizing those classes will go under `analysis/src`. 

- You need to write them programmatically, i.e. include necessary headers, follow correct syntax etc, since they will be compiled using `g++`. Give them `.cc` extension and add corresponding header with `.h` extension.

- Add an entry to the corresponding `LinkDef.h` file. Add comment to explain the purpose of the class or function. The comment is used for built in `help()` function, so try to follow same indentation/syntax.

- No change in the `Makefile` is required, it will get it automatically. Compile it from top level directory of `COBRA12` as `make lib` (if new class is added) or `make ana` (if analysis script/function is added).

- You can also write analysis script that is intended to be interpreted rather than compiled. Use a different extension other than `.cc` or put it in a different directory other than `libCobra12/src` or `analysis/src`. 

Examples
--------

**i) Detector Hit Distribution**

Consider the following example (from `analysis/src/DCdistributions.cc`) to make a simple hit distribution. 
```cpp
#include "TClas12Run.h"
#include "TH1F.h"
#include "TClas12DC.h"

void DCdistributions()
{
    TClas12Run *clas_run = new TClas12Run(4013, 0); 
    if(!clas_run->fFileExist)
	return;
    TClas12DC *dc = (TClas12DC*)clas_run->AddDetector("DC");
    //----- You can add other detectors here -----------
    
    TH1F *hist = new TH1F("hist","TDC sec = 2, SL = 5;TDC; Counts", 200, 0,1000);
    Int_t ecounter = 0;
    Int_t sec;
    Int_t sl;
    Int_t tdc;

    while(clas_run->HasEvent())
    {	
    	Int_t length = dc->GetSizeHBHits();
    	for(Int_t k = 0; k < length; k++)
    	{
    	    sec = dc->GetHitBasedTrkg_HBHits_sector(k);
    	    sl = dc->GetHitBasedTrkg_HBHits_superlayer(k);
    	    tdc = dc->GetHitBasedTrkg_HBHits_TDC(k);
    	    if(sec == 1 && sl == 5)
    		hist->Fill(tdc);
    	}
    	ecounter++;
    }
    delete clas_run;
    
    hist->Draw();    
}

```
This will give the following output:

![](resources/images/dc_tdc_dist.jpg)

**ii) Data Quality from DST bank**

Consider the example from `analysis/src/DataQuality.cc`
```cpp
    for(Int_t fileNo = 0; fileNo < nFiles; ++fileNo)
    {
	cout << "---------> Run: "<<runNumber<<"\t file no: "<<fileNo <<"<----------" <<endl;
	TClas12Run *clas = new TClas12Run(runNumber, fileNo); 
	if(!clas->fFileExist)
	    continue;
    
	while(clas->HasEvent())
	{
	    if(!clas->HasPartBank() || !clas->HasCherBank())
		continue;
	    
	    // Let's look at electron only, Consider only events with one e-
	    if(clas->GetCountWithPID(11) != 1)
		continue;
	    index = clas->GetItrPairForPID(11).first->second;  // Index of e in particle bank (take first range and get second)
	           		
	    // Require that there is a hit either from HTCC or LTCC (Need to remove Junk)
	    if( !clas->HasHTCCHit(index) && !clas->HasLTCCHit(index))
		continue;

	    h1VzDist->Fill(clas->GetVz(index));
	    h2VZvsPhi->Fill(clas->GetPhiDeg(index), clas->GetVz(index));
	    h2VZvsTheta->Fill(clas->GetThetaDeg(index), clas->GetVz(index));
	    h2ThetavsPhi->Fill(clas->GetPhiDeg(index), clas->GetThetaDeg(index));
	    h2PhivsMom->Fill(clas->GetP(index), clas->GetPhiDeg(index));

	    if(clas->HasHTCCHit(index))
		h1NpheHTCCDist->Fill(clas->GetHTCCnphe(index));
	    if(clas->HasLTCCHit(index))
		h1NpheLTCCDist->Fill(clas->GetLTCCnphe(index));
	}
	delete clas;
    }
```
This will give a bunch of plots including the following one:

![](resources/images/Data_Quality_Plot.jpg)

**iii) Physics Analysis**

Check the example `pi0` analysis in the script `analysis/src/Pi0Analysis.cc`. It will give distribution for pi0 invariant mass (the following plot) and opening angle from forward detectors. 
![](resources/images/Pi0_Example.jpg)

**iv) Inter-bank access**

The example `analysis/src/DCdoca.cc` shows how inter-bank access is achieved for a specific detector response.


Documentation
-------------

- Part of the documentation is built in. For example typing `help()` from `root` gives a list of available options. Typing `help("Class_Name")` will print the header for that class.

- Check the presentation from the clas12 software meeting [here](https://userweb.jlab.org/~latif/Hall_B/cobra12.pdf). 
